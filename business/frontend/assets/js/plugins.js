/*--------------scroll-menu-path--------------------------*/
jQuery(document).ready(function(){
	jQuery('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') || location.hostname == this.hostname) {
			var target = jQuery(this.hash);
			target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				jQuery('html, body').animate({
				scrollTop: target.offset().top - 90 }, 1000);
				return false;
			}
		}
	});
});
	jQuery(window).load(function(){
		function goToByScroll(id){
			jQuery("html, body").animate({scrollTop: jQuery("#"+id).offset().top - 90 }, 1000);
		}
		if(window.location.hash != '') {
			goToByScroll(window.location.hash.substr(1));
		}
	});
/*----------------------------------------*/

/* Seller info Slider V2 */
jQuery(document).ready(function() {
  jQuery('#gal-slide').slick({
    infinite: true,
    fade: true,
    cssEase: 'linear',
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 3000,
    //prevArrow: '<button class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
    //nextArrow: '<button class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>',
    adaptiveHeight: true,
    asNavFor: '#gal-thumb'
  });
  jQuery('#gal-thumb').slick({
    infinite: true,
    arrows: false,
	vertical: true,
    slidesToShow: 4,
    adaptiveHeight: false,
    centerMode: true,
    centerPadding: '0px',
    focusOnSelect: true,
    asNavFor: '#gal-slide',
	responsive: [
		{
		  breakpoint: 600,
		  settings: {
			vertical: false
		}
	  }
	]
  });
  
});
/* End Seller info Slider V2 */

$(document).ready(function(){
	     /* PhotoSlide */
		  $('.slider-for').slick({
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  infinite: true,
		  autoplay: true,
  		  autoplaySpeed: 3000,
		  centerMode: true,
		  arrows: false,
		  fade: true,
		  asNavFor: '.slider-nav'
		});
		$('.slider-nav').slick({
		  slidesToShow: 4,
		  slidesToScroll: 1,
		  asNavFor: '.slider-for',
		  dots: false,
		  centerMode: true,
		  focusOnSelect: true,
		  //autoplay: true,
  		  //autoplaySpeed: 2000,
		  //centerMode: true,
		  //focusOnSelect: true,
		  responsive: [
		    {
		      breakpoint: 1023,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 1
		    }
		  },
		  {
		    breakpoint: 767,
		    settings: {
		        slidesToShow: 3,
		        slidesToScroll: 1
		    }
		  }
		  ]
		});
		/* End PhotoSlide */

		/* PhotoSlide */
		  $('.slider-for-news').slick({
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  infinite: true,
		  autoplay: true,
  		  autoplaySpeed: 3000,
		  centerMode: true,
		  arrows: false,
		  fade: true,
		  asNavFor: '.slider-nav-news'
		});
		$('.slider-nav-news').slick({
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  asNavFor: '.slider-for-news',
		  dots: false,
		  centerMode: true,
		  focusOnSelect: true,
		  responsive: [
		    {
		      breakpoint: 1023,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		    }
		  },
		  {
		    breakpoint: 767,
		    settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1
		    }
		  }
		  ]
		});
		/* End PhotoSlide */

	/*--------------carousel--------------------------*/
	$('.RelatedSlideBox').slick({
		  slidesToShow: 3,
		  slidesToScroll: 1,
		  responsive: [
	    {
	      breakpoint: 770,
	      settings: {
	        arrows: true,
	        centerMode: true,
	        centerPadding: '40px',
	        slidesToShow: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        arrows: true,
	        centerMode: true,
	        centerPadding: '0px',
	        slidesToShow: 1
	      }
	    }
	  ]
	});
	/*--------------end-carousel--------------------------*/

	/*--------------Related--------------------------*/
	$('.RelatedSeminarSlideBox').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		infinite: false,
		responsive: [
	  {
		breakpoint: 1050,
		settings: {
		  arrows: true,
		  centerMode: true,
		  centerPadding: '40px',
		  slidesToShow: 3,
		  slidesToScroll: 1
		}
	  },
	  {
		breakpoint: 780,
		settings: {
		  arrows: true,
		  centerMode: true,
		  centerPadding: '40px',
		  slidesToShow: 2,
		  slidesToScroll: 1
		}
	  },
	  {
		breakpoint: 480,
		settings: {
		  arrows: true,
		  centerMode: true,
		  centerPadding: '0px',
		  slidesToShow: 1,
		  slidesToScroll: 1
		}
	  }
	]
  });
  /*--------------end-Related--------------------------*/

  /*--------------SeminarList--------------------------*/
	$('.SeminarSlideListBox').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		infinite: false,
		responsive: [
	  {
		breakpoint: 1050,
		settings: {
		  arrows: true,
		  centerMode: true,
		  centerPadding: '40px',
		  slidesToShow: 3,
		  slidesToScroll: 1
		}
	  },
	  {
		breakpoint: 780,
		settings: {
		  arrows: true,
		  centerMode: true,
		  centerPadding: '40px',
		  slidesToShow: 2,
		  slidesToScroll: 1
		}
	  },
	  {
		breakpoint: 480,
		settings: {
		  arrows: true,
		  centerMode: true,
		  centerPadding: '0px',
		  slidesToShow: 1,
		  slidesToScroll: 1
		}
	  }
	]
  });
  /*--------------end-Related--------------------------*/
  /*--------------SeminarList--------------------------*/
	$('.SeminarSlideListBoxLatest').slick({
		slidesToShow: 2,
		slidesToScroll: 1,
		infinite: false,
		responsive: [
	  {
		breakpoint: 1050,
		settings: {
		  //arrows: true,
		  //centerMode: false,
		  //centerPadding: '0px',
		  slidesToShow: 2,
		  slidesToScroll: 1
		}
	  },
	  {
		breakpoint: 780,
		settings: {
		  //arrows: true,
		  //centerMode: false,
		  //centerPadding: '0px',
		  slidesToShow: 2,
		  slidesToScroll: 1
		}
	  },
	  {
		breakpoint: 767,
		settings: {
		  //arrows: true,
		  //centerMode: false,
		  //centerPadding: '0px',
		  slidesToShow: 1,
		  slidesToScroll: 1
		}
	  }
	]
  });
  /*--------------end-Related--------------------------*/

  /*--------------TopPageSeminarSlideBox--------------------------*/
	$('.TopPageSeminarSlideBox').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		infinite: false,
		responsive: [
	  {
		breakpoint: 1023,
		settings: {
		  arrows: true,
		  centerMode: true,
		  centerPadding: '40px',
		  slidesToShow: 3,
		  slidesToScroll: 1
		}
	  },
	  {
		breakpoint: 769,
		settings: {
		  arrows: true,
		  centerMode: true,
		  centerPadding: '40px',
		  slidesToShow: 2,
		  slidesToScroll: 1
		}
	  },
	  {
		breakpoint: 480,
		settings: {
		  arrows: true,
		  centerMode: true,
		  centerPadding: '0px',
		  slidesToShow: 1,
		  slidesToScroll: 1
		}
	  }
	]
  });
  /*--------------end-TopPageSeminarSlideBox--------------------------*/

  /*--------------TopPageSeminarSlideBoxForBuyer--------------------------*/
	$('.TopPageSeminarSlideBoxForBuyer').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		infinite: false,
		responsive: [
	  {
		breakpoint: 1023,
		settings: {
		  arrows: true,
		  centerMode: false,
		  centerPadding: '0px',
		  slidesToShow: 3,
		  slidesToScroll: 1
		}
	  },
	  {
		breakpoint: 769,
		settings: {
		  arrows: true,
		  centerMode: false,
		  centerPadding: '0px',
		  slidesToShow: 2,
		  slidesToScroll: 1
		}
	  },
	  {
		breakpoint: 480,
		settings: {
		  arrows: true,
		  centerMode: false,
		  centerPadding: '0px',
		  slidesToShow: 1,
		  slidesToScroll: 1
		}
	  }
	]
  });
  /*--------------end-TopPageSeminarSlideBoxForBuyer--------------------------*/

	/*--------------BannerToppage--------------------------*/
	$('.FeatureSlideBox').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		dots: true,
		infinite: true,
		autoplay: true,
		autoplaySpeed: 3000,
		centerMode: true,
		fade: true
  });
  /*--------------End BannerToppage--------------------------*/

  /*--------------BannerToppage--------------------------*/
	$('.EventSlideBox').slick({
		slidesToShow: 1,
		infinite: false,
		  slidesToScroll: 1,
		  responsive: [
	    {
	      breakpoint: 770,
	      settings: {
	        arrows: true,
	        centerMode: false,
	        centerPadding: '0px',
	        slidesToShow: 1
	      }
	    },{
			breakpoint: 480,
			settings: {
			  arrows: true,
			  centerMode: true,
			  centerPadding: '10px',
			  slidesToShow: 1
			}
		  }
	  ]
  });
  /*--------------End BannerToppage--------------------------*/

  /*--------------PrivateSlideBox--------------------------*/
	$('.PrivateSlideBox').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		infinite: false,
		  responsive: [
	    {
	      breakpoint: 770,
	      settings: {
	        arrows: false,
	        centerMode: true,
	        centerPadding: '0px',
	        slidesToShow: 1
	      }
	    },{
			breakpoint: 480,
			settings: {
			  arrows: false,
			  centerMode: true,
			  centerPadding: '0px',
			  slidesToShow: 1
			}
		  }
	  ]
  });
  /*--------------End PrivateSlideBox--------------------------*/
		
    });

    // ContactUsPopupDisplay
	var modalContact = document.getElementById("ContactUsPopupDisplay");
	var btnContact = document.getElementById("ContactUsPopup");
	var span = document.getElementsByClassName("close")[0];
	/*btnContact.onclick = function() {
	  modalContact.style.display = "block";
	}
	span.onclick = function() {
	  modalContact.style.display = "none";
	}

	// ContactUsBranchPopupDisplay
	var modalContactBranch = document.getElementById("ContactUsBranchPopupDisplay");
	var btnContactBranch = document.getElementById("ContactUsBranchPopup");
	var spanBranch = document.getElementsByClassName("closeBranch")[0];
	btnContactBranch.onclick = function() {
	  modalContactBranch.style.display = "block";
	}
	spanBranch.onclick = function() {
	  modalContactBranch.style.display = "none";
	}
	*/

