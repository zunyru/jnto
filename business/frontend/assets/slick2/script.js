jQuery(document).ready(function() {
  jQuery('#gal-slide').slick({
    infinite: true,
    fade: true,
    cssEase: 'linear',
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<button class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
    nextArrow: '<button class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>',
    adaptiveHeight: true,
    asNavFor: '#gal-thumb'
  });
  jQuery('#gal-thumb').slick({
    infinite: true,
    arrows: false,
    slidesToShow: 4,
    adaptiveHeight: false,
    centerMode: true,
    centerPadding: '60px',
    focusOnSelect: true,
    asNavFor: '#gal-slide'
  });
  
});