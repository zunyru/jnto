<!-- Footer -->
<footer>
	<!-- Footer -->
	<div class="Footer">
		<!-- BackToTop -->
		<div class="BackToTop"><a href="#BackToTop"><img src="assets/images/icon/icon-back-to-top.svg"></a></div>
		<!-- End BackToTop -->
		
		<!-- Phase2 FooterBox -->
		<div class="FooterBox clr">
			<h2><a href="index.php" target="_blank">Resources for Travel Agents</a></h2>
			<div class="FooterBoxLeft">
				<p><a href="page-search.php" target="_blank">ค้นหาพาร์ทเนอร์ฝ่ายญี่ปุ่น<span>Discover new partners!</span></a></p>
				<p><a href="page-news-partner.php" target="_blank">ข่าวสารจากองค์กรท่องเที่ยวต่างๆ<span>JNTO Partners’ News</span></a></p>
			</div>
			<div class="FooterBoxRight">
				<p><a href="page-topic.php" target="_blank">ข่าวสารจากพาร์ทเนอร์ฝ่ายญี่ปุ่น<span>Event & Latest information</span></a></p>
				<p><a href="page-news-jnto.php" target="_blank">ข่าวสารจาก JNTO<span>JNTO News</span></a></p>
			</div>
			<div class="clr"></div>
			<div class="FooterBoxRight clr">
				<p class="SeminarLinkTxtLeft"><a href="index.php#seminarupdate" target="_blank">ดูสัมมนาออนไลน์ย้อนหลัง Online Seminar</a></p>
				<div class="MBFont"><label class="YearsBox">2020</label>| <a href="page-seminar.php" target="_blank">ครั้งที่ 1</a> <a href="page-seminar.php" target="_blank">ครั้งที่ 2</a> <a href="page-seminar.php" target="_blank">ครั้งที่ 3</a> <a href="page-seminar.php" target="_blank">ครั้งที่ 4</a> <a href="page-seminar.php" target="_blank">ครั้งที่ 5</a> <a href="page-seminar.php" target="_blank">ครั้งที่ 6</a></div>
				<div class="MBFont"><label class="YearsBox">2021</label>| <a href="page-seminar.php" target="_blank">ครั้งที่ 1</a> <a href="page-seminar.php" target="_blank">ครั้งที่ 2</a> <a href="page-seminar.php" target="_blank">ครั้งที่ 3</a> <a href="page-seminar.php" target="_blank">ครั้งที่ 4</a> <a href="page-seminar.php" target="_blank">ครั้งที่ 5</a></div>
			</div>
		</div>
		<!-- Phase2 End FooterBox -->

		<!-- FooterLogoTail -->
		<!--<div class="FooterLogoTail"><img src="assets/images/logo-jnto.svg"></div>-->
		<!-- End FooterLogoTail -->

		<!-- ULTail -->
		<div class="ULTail clr">
			<h2><a href="index.php#usefullinks" target="_blank">เว็บไซต์ที่เป็นประโยชน์ Useful Links <span></span></a></h2>
			<div class="ULTailList">
				<h3>ข้อมูลท่องเที่ยว</h3>
				<ul>
					<li><a href="https://www.jnto.or.th/contactus/download/" target="_blank">ดาวน์โหลดเอกสารเที่ยวญี่ปุ่น (ภาษาไทย)</a></li>
					<li><a href="https://www.jnto.go.jp/brochures/eng/index.php" target="_blank">ดาวน์โหลดเอกสารเที่ยวญี่ปุ่น (ภาษาอังกฤษ)</a></li>
					<li><a href="https://www.japan.travel/adventure/en/" target="_blank">เที่ยวญี่ปุ่นแนวแอดเวนเจอร์</a></li>
					<li><a href="https://www.japan.travel/snow/en/" target="_blank">ฐานข้อมูลเกี่ยวกับกิจกรรมในฤดูหนาวที่ญี่ปุ่น</a></li>
					<li><a href="https://www.japan.travel/tokyo-and-beyond-2020/en/" target="_blank">โตเกียว โอลิมปิก และพาราลิมปิก 2020</a></li>
					<li><a href="https://www.japan.travel/diving/en/" target="_blank">ดำน้ำที่ญี่ปุ่นเที่ยวญี่ปุ่นแบบหรูหรา</a></li>
				</ul>
			</div>
			<div class="ULTailList">
				<h3>ข้อมูลอินเซ็นทีฟ และ MICE</h3>
				<ul>
					<li><a href="https://www.japanmeetings.org/" target="_blank">อินเซ็นทีฟ MICE และการจัดอีเว้นท์ที่ญี่ปุ่น</a></li>
					<li><a href="https://www.jetro.go.jp/en/ind_tourism/" target="_blank">การท่องเที่ยวเชิงอุตสาหกรรมที่ญี่ปุ่น</a></li>
					<li><a href="https://education.jnto.go.jp/en/" target="_blank">การท่องเที่ยวเชิงการศึกษาที่ญี่ปุ่น</a></li>
				</ul>
			</div>
			<div class="ULTailList">
				<h3>ข้อมูลพื้นฐาน</h3>
				<ul>
					<li><a href="https://business.jnto.go.jp/" target="_blank">คลังภาพวีดีโอเที่ยวญี่ปุ่น</a></li>
					<li><a href="https://statistics.jnto.go.jp/en/" target="_blank">สถิติการท่องเที่ยวญี่ปุ่น</a></li>
					<li><a href="https://www.th.emb-japan.go.jp/itpr_th/visaindex.html" target="_blank">ข่าวสารเกี่ยวกับวีซ่า</a></li>
				</ul>
			</div>
		</div>
		<!-- End ULTail -->

		<p class="Copyright">Copyright © Japan National Tourism Organization. All Rights Reserved.</p>

	</div>
	<!-- End Footer -->

	<!-- TailLogoandLink -->
		<div class="TailLogoandLink clr">
			<!-- TailLogoandLinkBox -->
			<div class="TailLogoandLinkBox">
				<div class="LogoTail"><a href="https://www.jnto.or.th/" target="_blank"><img src="assets/images/logo-tail.png"><span>องค์การส่งเสริมการท่องเที่ยวแห่งประเทศญี่ปุ่น</span></a></div>
				<div class="JNTOEmail">info_bangkok@jnto.go.jp</div>
				<div class="JNTOWebsite"><a href="https://www.jnto.or.th" target="_blank">www.jnto.or.th</a></div>
				<div class="JNTOSocial"><!--<span>Follow us! </span>-->
				<a href="https://www.facebook.com/visitjapanth/" target="_blank"><img src="assets/images/icon/icon-facebook.svg"></a>
				<a href="https://www.instagram.com/visitjapanth/" target="_blank"><img src="assets/images/icon/icon-instagram.svg"></a>
				<a href="https://bddy.me/3oZuO3Y" target="_blank"><img src="assets/images/icon/icon-line.svg"></a>
				</div>
			</div>
			<!-- End TailLogoandLinkBox -->
		</div>
		<!-- End TailLogoandLink -->
</footer>
<!-- End Footer -->

<!-- CookiePopup -->
<!--<div class="CookiePopup">
	<div class="CookiePopupBox">
		<div class="PopupCondition">
			<h3>เงื่อนไขในการใช้เว็บไซต์</h3>
			<div class="MobileScroll">
				<p class="LineOne">
					เว็บไซต์นี้จัดทำขึ้นเพื่อผู้ประกอบการท่องเที่ยวญี่ปุ่นในประเทศไทยโดยเฉพาะ เพื่อรักษาผลประโยชน์ของท่าน <span class="Boldtxt">โปรดเก็บรักษาชื่อผู้ใช้งาน (User ID) และรหัสผ่าน (Password) ของเว็บไซต์เพื่อใช้ภายในองค์กรของท่านเท่านั้น ไม่นำไปเผยแพร่แก่หน่วยงานอื่นและผู้บริโภค</span>
				</p>
				<p class="LineTwo">
					<span class="Boldtxt">ผู้จัดทำเว็บไซต์ไม่สามารถรับประกันเกี่ยวกับเนื้อหาภายในหน้า ”ค้นหาพาร์ทเนอร์ฝ่ายญี่ปุ่น”</span> ว่าจะไม่มีข้อบกพร่องหรือการละเมิดสิทธิ เพื่อหลีกเลี่ยงความเสี่ยงต่อการละเมิดลิขสิทธิ์ที่อาจเกิดกับท่านและองค์กร <span class="Boldtxt">ห้ามท่านนำเนื้อหาไปดัดแปลง ส่งต่อ โดยไม่ได้รับอนุญาตจากหน่วยงานที่ให้ข้อมูล แม้ในวัตถุประสงค์ในการจัดทำสินค้าท่องเที่ยวก็ตาม</span><br>
					<span class="Smalltxt">*ยกเว้นกรณีที่มีการเขียนระบุคำอนุญาตอย่างชัดเจนด้วยตัวอักษร</span><br>
					<span class="Smalltxt">*เนื้อหา รวมถึง รูปภาพ วีดีโอ ตัวอักษร กราฟฟิค ดนตรี เสียง</span>
				</p>
				<p class="LineThree">
					เว็บไซต์นี้ใช้คุกกี้ในการให้บริการเพื่อให้ผู้ใช้ได้รับประสบการณ์การใช้งานเว็บไซต์ที่ดี หากคุณดำเนินการต่อ ทางเราจะถือว่าคุณยอมรับนโยบายคุกกี้ของเรา (เรียนรู้เพิ่มเติม <a href="page-privacy-policy.php" target="_blank">ที่นี่</a>)
				</p>
			</div>
			<p class="BTNAccept"><a href="javascript:void(0);" onclick="AcceptCookie();" id="AcceptCookie"><img src="assets/images/icon/icon-accept.svg"> ยอมรับ</a></p>
		</div>
	</div>
</div>-->
<!-- End CookiePopup -->

<script type="text/javascript" src="assets/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="assets/js/plugins.js"></script>
<script type="text/javascript" src="assets/slick/slick.min.js"></script>
<script type="text/javascript">
$(function(){
	/* Start */
	$('.SectionShow').hide();
	$('.SectionClick').on('click',function(){   
		$('.SectionShow').not($($(this).attr('href'))).hide();
		$($(this).attr('href')).fadeToggle(200);
	});
	/* End  */

	// Start
	$('.Tab1').on('click',function(){   
		$('.JNTO_News').show();
		$('.JNTO_Partner').hide();
		$('.Tab1').addClass("Active");
		$('.Tab2').removeClass("Active");
	});
	$('.Tab2').on('click',function(){   
		$('.JNTO_News').hide();
		$('.JNTO_Partner').show();
		$('.Tab1').removeClass("Active");
		$('.Tab2').addClass("Active");
	});
	// End 

	/* AcceptCookie */
	$('#AcceptCookie').on('click',function(){
		$('.CookiePopup').hide();
	});

});

</script>
</body>
</html>