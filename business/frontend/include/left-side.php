<!-- End Mobile Menu -->
<div class="GroupBox clr">
    <p class="GroupHeader">ウェブ表示情報の編集<span class="FontEN">Edit your web page</span></p>
    <ul>
        <li><a href="page-mypage.php" class="Active">セラー基本情報<span class="FontEN">Company information</span></a></li>
        <li><a href="page-company-toppic.php">イベント・お知らせ<span class="FontEN">Event & Latest information</span></a></li>
        <li><a href="page-privacy-setting.php">日本側セラーへの公開設定<span class="FontEN">Privacy Setting</span></a></li>
    </ul>
</div>
<div class="GroupBox clr">
    <p class="GroupHeader">ログイン情報の変更<span class="FontEN">Change login details</span></p>
    <ul>
        <li><a href="page-change-email.php">ログインメール変更<span class="FontEN">Change Login E-mail</span></a></li>
        <li><a href="page-change-password.php">パスワード変更<span class="FontEN">Change Password </span></a></li>
    </ul>
</div>
<div class="GroupBox clr">
    <p class="GroupHeader">その他｜Others</p>
    <ul>
        <li><a href="page-login-history.php">ログイン履歴<span class="FontEN">Login History</span></a></li>
        <li><a href="page-contact-staff.php">事務局へのお問い合わせ<span class="FontEN">Contact Staff</span></a></li>
        <li><a href="page-privacy-policy-disclaimer.php">個人情報保護方針<span class="FontEN">Privacy Policy</span></a></li>
    </ul>
</div>