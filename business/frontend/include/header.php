<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Resources for Travel Agents</title>
<meta name="viewport" content="width=device-width,initial-scale=1">
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<link rel="stylesheet" type="text/css" href="assets/css/style-responsive.css">
<link rel="stylesheet" type="text/css" href="assets/css/style-menu.css">
<link rel="stylesheet" type="text/css" href="assets/css/style-popup.css">
<link rel="stylesheet" type="text/css" href="assets/css/style-mypage.css">
<!-- SlickSlider -->
<link rel="stylesheet" type="text/css" href="assets/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="assets/slick/slick-theme.css"/>
<!-- End SlickSlider -->
<link rel="icon" href="favicon-32x32.png" type="image" sizes="32x32">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Montserrat:wght@300;400;500;600;700&family=Sarabun:wght@100;500&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=M+PLUS+1p:wght@100;300;400;500;700;800;900&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<div id="BackToTop"></div>
<!-- Mobile Menu -->
<nav class="mobile-menu">
	<!-- HeaderLogo -->
	<div class="HeaderLogo"><a href="https://www.jnto.or.th/" target="_blank"><img src="assets/images/logo.svg"></a> <a href="index.php" target="_blank"><span>Resources for Travel Agents</span></a></div>
	<!-- End HeaderLogo -->

	<!-- Phase2 MemberHeader -->
	<div class="MemberHeader">
		<p class="TopEmail"><a href="page-company-information.php">sarawoot.s@enfete.asia</a></p>
		<p><a href="index.php" class="BTNTopPage" target="_blank">TOP</a></p>
		<!--<p><a href="index.php" class="BTNTopPage" target="_blank">My page</a></p>-->
		<p><a href="#" class="BTNLogout">Logout</a></p>
	</div>
	<!-- End Phase2 MemberHeader -->

    <input type="checkbox" id="checkbox" class="mobile-menu__checkbox">
    <label for="checkbox" class="mobile-menu__btn"><div class="mobile-menu__icon"></div></label>
    <!-- menu__container -->
    <div class="mobile-menu__container">
	    <!-- Header -->
		<header>
			<div class="Header">
				<!-- FooterBox -->
				<div class="HeaderBox clr">
					<h2><a href="index.php" target="_blank">Resources for Travel Agents</a></h2>
					<div class="FooterBoxLeft">
						<p><a href="page-search.php" target="_blank">ค้นหาพาร์ทเนอร์ฝ่ายญี่ปุ่น<span>Discover new partners!</span></a></p>
						<p><a href="page-news-partner.php" target="_blank">ข่าวสารจากองค์กรท่องเที่ยวต่างๆ<span>JNTO Partners’ News</span></a></p>
					</div>
					
					<!-- Phase2 -->
					<div class="FooterBoxRight">
					<p><a href="page-topic.php" target="_blank">ข่าวสารจากพาร์ทเนอร์ฝ่ายญี่ปุ่น<span>Event & Latest information</span></a></p>
						<p><a href="page-news-jnto.php" target="_blank">ข่าวสารจาก JNTO<span>JNTO News</span></a></p>
					</div>
					<div class="clr"></div>
					<div class="FooterBoxRight HeaderMarginSeminare clr">
						<p class="SeminarLinkTxtLeft"><a href="index.php#seminarupdate" target="_blank">ดูสัมมนาออนไลน์ย้อนหลัง Online Seminar</a></p>
						<div class="MBFont"><label class="YearsBox">2020</label>| <a href="page-seminar.php" target="_blank">ครั้งที่ 1</a> <a href="page-seminar.php" target="_blank">ครั้งที่ 2</a> <a href="page-seminar.php" target="_blank">ครั้งที่ 3</a> <a href="page-seminar.php" target="_blank">ครั้งที่ 4</a> <a href="page-seminar.php" target="_blank">ครั้งที่ 5</a> <a href="page-seminar.php" target="_blank">ครั้งที่ 6</a></div>
						<div class="MBFont"><label class="YearsBox">2021</label>| <a href="page-seminar.php" target="_blank">ครั้งที่ 1</a> <a href="page-seminar.php" target="_blank">ครั้งที่ 2</a> <a href="page-seminar.php" target="_blank">ครั้งที่ 3</a> <a href="page-seminar.php" target="_blank">ครั้งที่ 4</a> <a href="page-seminar.php" target="_blank">ครั้งที่ 5</a></div>
					</div>
					<!-- End Phase2 -->

					<!-- ULTail -->
					<div class="ULTail clr" style="clear: both;">
						<h2><a href="index.php#usefullinks" target="_blank">เว็บไซต์ที่เป็นประโยชน์ Useful Links <span></span></a></h2>
						<div class="ULTailList">
							<h3>ข้อมูลท่องเที่ยว</h3>
							<ul>
								<li><a href="https://www.jnto.or.th/contactus/download/" target="_blank">ดาวน์โหลดเอกสารเที่ยวญี่ปุ่น (ภาษาไทย)</a></li>
								<li><a href="https://www.jnto.go.jp/brochures/eng/index.php" target="_blank">ดาวน์โหลดเอกสารเที่ยวญี่ปุ่น (ภาษาอังกฤษ)</a></li>
								<li><a href="https://www.japan.travel/adventure/en/" target="_blank">เที่ยวญี่ปุ่นแนวแอดเวนเจอร์</a></li>
								<li><a href="https://www.japan.travel/snow/en/" target="_blank">ฐานข้อมูลเกี่ยวกับกิจกรรมในฤดูหนาวที่ญี่ปุ่น</a></li>
								<li><a href="https://www.japan.travel/tokyo-and-beyond-2020/en/" target="_blank">โตเกียว โอลิมปิก และพาราลิมปิก 2020</a></li>
								<li><a href="https://www.japan.travel/diving/en/" target="_blank">ดำน้ำที่ญี่ปุ่นเที่ยวญี่ปุ่นแบบหรูหรา</a></li>
							</ul>
						</div>
						<div class="ULTailList">
							<h3>ข้อมูลอินเซ็นทีฟ และ MICE</h3>
							<ul>
								<li><a href="https://www.japanmeetings.org/" target="_blank">อินเซ็นทีฟ MICE และการจัดอีเว้นท์ที่ญี่ปุ่น</a></li>
								<li><a href="https://www.jetro.go.jp/en/ind_tourism/" target="_blank">การท่องเที่ยวเชิงอุตสาหกรรมที่ญี่ปุ่น</a></li>
								<li><a href="https://education.jnto.go.jp/en/" target="_blank">การท่องเที่ยวเชิงการศึกษาที่ญี่ปุ่น</a></li>
							</ul>
						</div>
						<div class="ULTailList">
							<h3>ข้อมูลพื้นฐาน</h3>
							<ul>
								<li><a href="https://business.jnto.go.jp/" target="_blank">คลังภาพวีดีโอเที่ยวญี่ปุ่น</a></li>
								<li><a href="https://statistics.jnto.go.jp/en/" target="_blank">สถิติการท่องเที่ยวญี่ปุ่น</a></li>
								<li><a href="https://www.th.emb-japan.go.jp/itpr_th/visaindex.html" target="_blank">ข่าวสารเกี่ยวกับวีซ่า</a></li>
							</ul>
						</div>
					</div>
					<!-- End ULTail -->
				</div>
				<!-- End FooterBox -->
			</div>
		</header>
		<!-- End Header -->
    </div>
    <!-- End menu__container -->
</nav>
<!-- End Mobile Menu -->