<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTopicTemplateEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topics', function (Blueprint $table) {
            $table->string('template', 50)->nullable();
            $table->string('photo1')->nullable();
            $table->string('photo2')->nullable();
            $table->string('photo3')->nullable();
            $table->string('photo4')->nullable();
            $table->string('pdf1')->nullable();
            $table->integer('pdf_size1')->nullable();
            $table->string('pdf2')->nullable();
            $table->integer('pdf_size2')->nullable();
            $table->string('pdf3')->nullable();
            $table->integer('pdf_size3')->nullable();
            $table->string('file_description1')->nullable();
            $table->string('file_description2')->nullable();
            $table->string('file_description3')->nullable();
            $table->string('link')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topics', function (Blueprint $table) {
            $table->dropColumn([
                'template',
                'pdf1',
                'pdf2',
                'pdf3',
                'pdf_size1',
                'pdf_size2',
                'pdf_size3',
                'photo1',
                'photo2',
                'photo3',
                'photo4',
                'file_description1',
                'file_description2',
                'file_description3',
                'link'
            ]);
        });
    }
}
