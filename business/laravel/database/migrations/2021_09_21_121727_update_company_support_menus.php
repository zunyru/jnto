<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCompanySupportMenus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_support_menus', function (Blueprint $table) {
            $table->integer('support_menu6')->nullable();
            $table->integer('support_menu7')->nullable();
            $table->integer('support_menu8')->nullable();
            $table->string('manu_name8')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_support_menus', function (Blueprint $table) {
            $table->dropColumn(['support_menu6', 'support_menu7', 'support_menu8', 'manu_name8']);
        });
    }
}
