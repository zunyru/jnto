<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCompaniesUserPasswordColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->string('user_login')->nullable();
            $table->string('password')->nullable();
            $table->dateTime('last_login')->nullable();
            $table->string('remember_token')->nullable();
            $table->dateTime('email_password_send_at')->nullable();
            $table->dateTime('email_signup_successful')->nullable();
            $table->boolean('active_status')->default(0);
            $table->string('privacy')->default('public');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn([
                'user_login',
                'password',
                'last_login',
                'remember_token',
                'email_password_send_at',
                'email_signup_successful',
                'active_status',
                'privacy'
            ]);
        });
    }
}
