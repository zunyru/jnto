<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTopicAdminEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topics', function (Blueprint $table) {
            $table->integer('company_id')->nullable()->change();
            $table->dateTime('event_date_start')->nullable();
            $table->dateTime('event_date_end')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topics', function (Blueprint $table) {
            $table->integer('company_id')->nullable(false)->change();
            $table->dropColumn([
                'event_date_end',
                'event_date_start'
            ]);
        });
    }
}
