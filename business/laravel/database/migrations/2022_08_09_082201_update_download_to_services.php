<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateDownloadToServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->tinyInteger('download_main_photo')->nullable();
            $table->tinyInteger('download_other_photo')->nullable();
            $table->tinyInteger('download_other_photo2')->nullable();
            $table->tinyInteger('download_other_photo3')->nullable();
            $table->longText('download_condition')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->dropColumn([
                'download_main_photo',
                'download_other_photo',
                'download_other_photo2',
                'download_other_photo3',
                'download_condition'
            ]);
        });
    }
}
