$(document).ready(function () {
    $.validator.addMethod('filesize' , function (value , element , param) {
        var param = param * 1000000;
        return this.optional(element) || (element.files[0].size <= param)
    } , function (param , element) {
        return "File size must be less than " + param + "MB"
    });

    $("#seminarForm").validate({
        rules:
            {
                title: {
                    required: true ,
                    maxlength: 255
                } ,
                thumbnail: {
                    extension: "png|jpg|jpeg" ,
                    filesize: 2
                } ,
                pdf1: {
                    extension: "pdf" ,
                    filesize: 5
                } ,
                pdf2: {
                    extension: "pdf" ,
                    filesize: 5
                } ,
                pdf3: {
                    extension: "pdf" ,
                    filesize: 5
                } ,
                pdf_title1: {
                    maxlength: 100
                } ,
                pdf_title2: {
                    maxlength: 100
                } ,
                pdf_title3: {
                    maxlength: 100
                } ,
                year: {
                    required: true ,
                }
            } ,
        messages: {
            thumbnail: {
                extension: "File type must be PNG or JPG." ,
            } ,
            pdf1: {
                extension: "File type must be PDF." ,
            } ,
            pdf2: {
                extension: "File type must be PDF." ,
            } ,
            pdf3: {
                extension: "File type must be PDF." ,
            } ,
        }
    });
});