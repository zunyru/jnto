$("#ContacForm").validate({
    rules: {
        company_name: {
            required: true,
            maxlength: 255
        },
        offcial_website_url: {
            required: true,
            url: true
        },
        name: {
            required: true,
            maxlength: 255
        },
        position: {
            required: true,
            maxlength: 255
        },
        email: {
            required: true,
            email: true
        },
        message: {
            required: true
        }
    },
    messages: {},
    submitHandler: function (form) {
        $('#ContactUsPopupDisplay').hide();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: form.action,
            type: form.method,
            data: $(form).serialize(),
            success: function (response) {
                if (response == 1) {
                    $('#ContactUsPopupDisplay').show();
                    $('form#ContacForm').hide();
                    $('.THanksClass').show();
                    toastr.info("Send email success");
                } else {
                    toastr.error("Something went wrong");
                }
            }
        });
    }
});

$("#ContacFormThai").validate({
    rules: {
        company_name: {
            required: true,
            maxlength: 255
        },
        offcial_website_url: {
            required: true,
            url: true
        },
        name: {
            required: true,
            maxlength: 255
        },
        position: {
            required: true,
            maxlength: 255
        },
        email: {
            required: true,
            email: true
        },
        message: {
            required: true
        }
    },
    messages: {},
    submitHandler: function (form) {
        $('#ContactUsBranchPopupDisplay').hide();
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: form.action,
            type: form.method,
            data: $(form).serialize(),
            success: function (response) {
                if (response == 1) {
                    $('#ContactUsBranchPopupDisplay').show();
                    $('form#ContacFormThai').hide();
                    $('.THanksClassBranch').show();
                    toastr.info("Send email success");
                } else {
                    toastr.error("Something went wrong");
                }
            }
        });
    }
});