$(document).ready(function () {
    $.validator.addMethod('filesize', function (value, element, param) {
        var param = param * 1000000;
        return this.optional(element) || (element.files[0].size <= param)
    }, function (param, element) {
        return "File size must be less than " + param + "MB"
    });

    $("#newsForm").validate({
        rules:
            {
                title: {
                    required: true,
                    maxlength: 255
                },
                link: {
                    required: true,
                    url: true
                },
                photo1: {
                    extension: "png|jpg|jpeg",
                    filesize: 1
                },
                photo2: {
                    extension: "png|jpg|jpeg",
                    filesize: 1
                },
                photo3: {
                    extension: "png|jpg|jpeg",
                    filesize: 1
                },
                photo4: {
                    extension: "png|jpg|jpeg",
                    filesize: 1
                },
                pdf1: {
                    extension: "pdf|doc|docx|xls|xlsx",
                    filesize: 3
                },
                pdf2: {
                    extension: "pdf|doc|docx|xls|xlsx",
                    filesize: 3
                },
                pdf3: {
                    extension: "pdf|doc|docx|xls|xlsx",
                    filesize: 3
                },
                file_explaination_title1: {
                    maxlength: 100
                },
                file_explaination_title2: {
                    maxlength: 100
                },
                file_explaination_title3: {
                    maxlength: 100
                },
                file_description1: {
                    maxlength: 100
                },
                file_description2: {
                    maxlength: 100
                },
                file_description3: {
                    maxlength: 100
                },
                event_date_start:{
                    required: true,
                },
                event_date_end:{
                    required: true,
                }
            },
        messages: {
            photo1: {
                extension: "File type must be PNG or JPG.",
            },
            photo2: {
                extension: "File type must be PNG or JPG.",
            },
            photo3: {
                extension: "File type must be PNG or JPG.",
            },
            photo4: {
                extension: "File type must be PNG or JPG.",
            },
            pdf1: {
                extension: "File type must be pdf, excel , word.",
            },
            pdf2: {
                extension: "File type must be pdf, excel , word.",
            },
            pdf3: {
                extension: "File type must be pdf, excel , word.",
            },
            pdf_only: {
                extension: "File type must be pdf, excel , word.",
            },
        }
    });
});