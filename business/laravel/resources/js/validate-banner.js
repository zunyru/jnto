$(document).ready(function () {
    $.validator.addMethod('filesize' , function (value , element , param) {
        var param = param * 1000000;
        return this.optional(element) || (element.files[0].size <= param)
    } , function (param , element) {
        return "File size must be less than " + param + "MB"
    });

    $("#seminarForm").validate({
        rules:
            {
                target: {
                    required: true ,
                } ,
                url: {
                    url: true ,
                } ,
                thumbnail: {
                    extension: "png|jpg|jpeg" ,
                    filesize: 1
                }
            } ,
        messages: {}
    });
});