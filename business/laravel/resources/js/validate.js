$(document).ready(function () {

    $.validator.addMethod('filesize' , function (value , element , param) {
        var param = param * 1000000;
        return this.optional(element) || (element.files[0].size <= param)
    } , function (param , element) {
        return "File size must be less than " + param + "MB"
    });

    $.validator.addMethod("phone" , function (value , element) {
        var regex = /^[0-9- +]+$/;
        return this.optional(element) || regex.test(value);
    } , 'Please enter phone.');

    $.validator.addMethod("eng" , function (value , element) {
        var regex = /^[a-zA-Z0-9!\/\-@#$%^&*()=?><.,:'"+ _\n\r]+$/;
        return this.optional(element) || regex.test(value);
    } , 'Please enter English.');

    $("#myform").validate({
        rules:
            {
                user_login: {
                    required: true ,
                    email: true,
                } ,
                logo: {
                    required: true ,
                    extension: "png|jpg|jpeg" ,
                    filesize: 2
                } ,
                is_partner: "required" ,
                name_en: {
                    required: true ,
                    eng: true ,
                    maxlength: 255
                } ,
                name_ja: {
                    required: true ,
                    maxlength: 255
                } ,
                "business_type_id[]": "required" ,
                address: {
                    required: true ,
                    eng: true
                } ,
                google_map_url: {
                    url: true ,
                } ,
                facebook_url: {
                    url: true ,
                } ,
                instagram_url: {
                    url: true ,
                } ,
                youtube_channel_url: {
                    url: true ,
                } ,
                twitter: {
                    url: true ,
                } ,
                line: {
                    url: true ,
                } ,
                official_website_url: {
                    url: true ,
                    required: true ,
                } ,
                contact_person_name: {
                    eng: true
                } ,
                department: {
                    required: true ,
                    eng: true
                } ,
                email: {
                    required: true ,
                    email: true
                } ,
                /*tel: {
                    required: true ,
                } ,*/
                "language_id[]": "required" ,
                is_branch_thai: "required" ,
                branch_company_name_en: {
                    required: function (element) {
                        return $("input[name$='is_branch_thai']").val() == 1;
                    } ,
                    eng: true
                } ,
                branch_address: {
                    required: function (element) {
                        return $("input[name$='is_branch_thai']").val() == 1;
                    } ,
                    eng: true
                } ,
                branch_google_map_url: {
                    url: true ,
                } ,
                branch_thai_website_url: {
                    required: function (element) {
                        return $("input[name$='is_branch_thai']").val() == 1;
                    } ,
                    url: true ,
                } ,
                branch_department: {
                    required: function (element) {
                        return $("input[name$='is_branch_thai']").val() == 1;
                    } ,
                    eng: true
                } ,
                branch_contact_person_name: {
                    eng: true
                } ,
                branch_email: {
                    required: function (element) {
                        return $("input[name$='is_branch_thai']").val() == 1;
                    } ,
                    email: true
                } ,
               /* branch_tel: {
                    required: function (element) {
                        return $("input[name$='is_branch_thai']").val() == 1;
                    } ,
                } ,*/
                "language_for_branch_id[]": {
                    required: function (element) {
                        return $("input[name$='is_branch_thai']").val() == 1;
                    } ,
                } ,

                title: {
                    required: true ,
                    maxlength: 100
                } ,
                detail: {
                    required: true ,
                    maxlength: 4000
                } ,
                main_photo: {
                    required: true ,
                    extension: "png|jpg|jpeg" ,
                    filesize: 2 ,
                } ,
                other_photo: {
                    extension: "png|jpg|jpeg" ,
                    filesize: 2
                } ,
                other_photo2: {
                    extension: "png|jpg|jpeg" ,
                    filesize: 2
                } ,
                other_photo3: {
                    extension: "png|jpg|jpeg" ,
                    filesize: 2
                } ,
                about: {
                    maxlength: 100
                } ,
                about2: {
                    maxlength: 100
                } ,
                about3: {
                    maxlength: 100
                } ,
                about_url: {
                    maxlength: 100
                } ,
                about_url2: {
                    maxlength: 100
                } ,
                about_url3: {
                    maxlength: 100
                } ,
                support_about: {
                    maxlength: 100
                } ,
                company_profile_pdf: {
                    extension: "pdf" ,
                    filesize: 5
                } ,
                company_profile_pdf2: {
                    extension: "pdf" ,
                    filesize: 5
                } ,
                company_profile_pdf3: {
                    extension: "pdf" ,
                    filesize: 5
                } ,
                /*pr_youtube: {
                    url: true
                },*/
                photo_gallery_url: {
                    url: true
                } ,
                photo_gallery_url2: {
                    url: true
                } ,
                photo_gallery_url3: {
                    url: true
                } ,
                is_accept: "required" ,

                "region[]": "required" ,


                "manu_name1": {
                    maxlength: 100
                } ,
                "manu_name2": {
                    maxlength: 100
                } ,
                "manu_name3": {
                    maxlength: 100
                } ,
                "manu_name4": {
                    maxlength: 100
                } ,
                "manu_name5": {
                    maxlength: 100
                } ,
                "manu_name6": {
                    maxlength: 100
                } ,
                "manu_name7": {
                    maxlength: 100
                } ,
                "manu_name8": {
                    maxlength: 100
                } ,

                support_menu_about_pdf: {
                    extension: "pdf" ,
                    filesize: 5
                } ,

                url_banner: {
                    url: true
                } ,

                policy: {
                    required: true
                },
                category: {
                    required:  function (element) {
                        return $("input[name='event_status']").is(':checked');
                    }
                } ,

            } ,
        messages: {
            main_photo: {
                extension: "File type must be PNG or JPG." ,
            } ,
            other_photo: {
                extension: "File type must be PNG or JPG." ,
            } ,
            other_photo2: {
                extension: "File type must be PNG or JPG." ,
            } ,
            other_photo3: {
                extension: "File type must be PNG or JPG." ,
            } ,
            support_menu_about_pdf: {
                extension: "File type must be PDF." ,
            } ,
            company_profile_pdf: {
                extension: "File type must be PDF." ,
            } ,
            company_profile_pdf2: {
                extension: "File type must be PDF." ,
            } ,
            company_profile_pdf3: {
                extension: "File type must be PDF." ,
            } ,
            logo: {
                extension: "File type must be PNG or JPG." ,
            }
        }
    });

    /*$("input[name$='logo']").change(function (file) {
        var _URL = window.URL || window.webkitURL;
        var file, img, minWidth, minHeight, maxWidth, maxHeight;
        minWidth = 300;
        if ((file = this.files[0])) {
            img = new Image();
            img.onload = function () {
                var validImage = true;
                var imgWidth = this.width;
                var imgHeight = this.height;
                if (imgWidth < minWidth) {
                    validImage = false;
                    alert('Width less than ' + minWidth + 'px');
                    $(":submit").attr("disabled", true);
                    $(":submit").css('cursor', 'no-drop');
                    $("input[name$='logo']").css('color', 'red');
                } else {
                    $(":submit").attr("disabled", false);
                    $(":submit").css('cursor', 'pointer');
                    $("input[name$='logo']").css('color', '#000');
                }
            };
            img.src = _URL.createObjectURL(file);
        }
    });*/

    /*$("input[name$='main_photo']").change(function (file) {
        var _URL = window.URL || window.webkitURL;
        var file, img, minWidth, minHeight, maxWidth, maxHeight;
        minWidth = 1200;
        minHeight = 400;
        if ((file = this.files[0])) {
            img = new Image();
            img.onload = function () {
                var validImage = true;
                var imgWidth = this.width;
                var imgHeight = this.height;
                if ((imgWidth < minWidth) || (imgHeight < minHeight)) {
                    validImage = false;
                    alert('Width less than ' + minWidth + 'px or height less than ' + minHeight + 'px');
                    $(":submit").attr("disabled", true);
                    $(":submit").css('cursor', 'no-drop');
                    $("input[name$='main_photo']").css('color', 'red');
                } else {
                    $(":submit").attr("disabled", false);
                    $(":submit").css('cursor', 'pointer');
                    $("input[name$='main_photo']").css('color', '#000');
                }
            };
            img.src = _URL.createObjectURL(file);
        }
    });*/

});
