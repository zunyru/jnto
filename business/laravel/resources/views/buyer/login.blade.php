@extends('layouts.app')
@section('content')
    <div class="container mx-auto w-1/2 p-4 h-full flex flex-col justify-center">
        <div class="items-center">
            <h1 class="text-2xl text-gray-700 text-center mt-6">JNTO <br>B2B Website<br>Login</h1>
            {{--            <p class="text-smt text-center">--}}
            {{--                <a href="{{ route('home') }}" target="_blank"> Resources for Travel Agents | タイ旅行会社向け訪日情報発信サイト</a>--}}
            {{--            </p>--}}
            <form class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4"
                  method="POST"
                  id="form-email"
                  action="{{ route('login.buyer') }}">
                @csrf
                <div class="mb-4">
                    <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                        Username
                    </label>
                    <input class="form-control focus:outline-none focus:border-blue-300"
                           id="username" type="text" placeholder="Username"
                           name="username"
                           value="{{ old('username') }}"
                           autofocus>
                    @error('username')
                    <p class="text-red-500 text-xs italic">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mb-6">
                    <label class="block text-gray-700 text-sm font-bold mb-2" for="password">
                        Password
                    </label>
                    <div class="relative">
                        <input class="form-control focus:outline-none focus:border-blue-300"
                               id="password" type="password" placeholder="******************"
                               name="password"
                               autocomplete="current-password">
                        <div class="absolute top-1 right-1">
                            <a href="javascript:void(0)" id="password-open" onclick="triggerpassword()">
                                <svg
                                        class="w-6"
                                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"/>
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"/>
                                </svg>
                            </a>
                            <a href="javascript:void(0)" id="password-close" onclick="triggerpassword()">
                                <svg
                                        class="w-6"
                                        xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                          d="M13.875 18.825A10.05 10.05 0 0112 19c-4.478 0-8.268-2.943-9.543-7a9.97 9.97 0 011.563-3.029m5.858.908a3 3 0 114.243 4.243M9.878 9.878l4.242 4.242M9.88 9.88l-3.29-3.29m7.532 7.532l3.29 3.29M3 3l3.59 3.59m0 0A9.953 9.953 0 0112 5c4.478 0 8.268 2.943 9.543 7a10.025 10.025 0 01-4.132 5.411m0 0L21 21"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                    @error('password')
                    <p class="text-red-500 text-xs italic">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mb-6">
                    <label class="inline-flex items-center">
                        <input type="checkbox" class="form-checkbox" id="remember"
                               name="remember" {{ old('remember') ? 'checked' : '' }}>
                        <span class="ml-2">remember password</span>
                    </label>
                </div>
                <div class="items-center justify-center">
                    <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                            type="submit">
                        Login
                    </button>
                </div>
            </form>

        </div>
        <div class="my-4">
            <!--<h2 class="text-center text-xl text-blue-500">JNTO B2B Website</h2>-->
            <p class="text-sm text-gray-800 ">
                เว็บไซต์นี้รวบรวมเนื้อหาที่เป็นประโยชน์ในการจัดทำและจำหน่ายสินค้าท่องเที่ยวญี่ปุ่น
                เพื่อผู้ประกอบการท่องเที่ยวญี่ปุ่นในประเทศไทยโดยเฉพาะ หากท่านต้องการรหัสผ่านเพื่อเข้าชมเว็บไซต์
                โปรดติดต่อองค์การส่งเสริมการท่องเที่ยวแห่งประเทศญี่ปุ่น-สำนักงานกรุงเทพ ผ่านทางอีเมล
                {{ setting('admin.email') }}
            </p>

            {{--            <h2 class="text-center text-xl text-blue-500 mt-10"> タイ旅行会社向け訪日情報発信サイト 情報登録フォーム</h2>--}}
            {{--            <p class="text-sm text-gray-800">--}}
            {{--                「タイ旅行会社向け訪日情報発信サイト」はJNTOからの情報発信に加え、タイからの誘客に取り組む日本側セラーの情報を集めたプラットフォームです。 情報掲載ご希望の場合は、 info_bangkok@jnto.go.jp までお問い合わせください。--}}
            {{--            </p>--}}
            {{--            <p class="text-sm text-gray-800 mt-8">--}}
            {{--                ※掲載団体数に限りがございますので、ご希望に添えない場合がございます。--}}
            {{--                　予めご了承ください。--}}
            {{--            </p>--}}

        </div>
    </div>
    <p class="text-center text-gray-500 text-xs">
        Copyright © Japan National Tourism Organization. All Rights Reserved.
    </p>
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            var open = document.getElementById("password-open");
            open.style.display = "none";
        });

        function triggerpassword() {
            var password = document.getElementById("password");
            var open = document.getElementById("password-open");
            var close = document.getElementById("password-close");
            if (password.type === "password") {
                open.style.display = "block";
                close.style.display = "none";
                password.type = "text";
            } else {
                open.style.display = "none";
                close.style.display = "block";
                password.type = "password";
            }
        }

        $("#form-email").validate({
            rules:
                {
                    username: {
                        required: true ,
                    } ,
                    password: {
                        required: true
                    }
                } ,
            messages: {
                username: {
                    required: "ユーザーネームを入力してください。กรุณากรอก Username" ,
                } ,
                password: {
                    required: "パスワードを設定してください。 กรุณากรอกรหัสผ่าน"
                } ,
            }
        });
    </script>
@endpush
