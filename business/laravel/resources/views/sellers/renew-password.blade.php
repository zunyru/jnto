@extends('sellers.app-register')

@section('content')
    <div class="HeaderRegister">
        <h1>6カ月以上ログインされていない為、パスワードをリセットしてください</h1>
        <p class="FontTH">ท่านไม่ได้ทำการล็อกอินนานเกินกว่า 6 เดือน โปรดรีเซ็ตรหัสผ่าน</p>
    </div>
    <!-- End HeaderRegister -->

    <!-- RegisterBox -->
    <form action="{{ route('seller.renew-password') }}"
          id="renew-password" method="POST">
        @csrf
        <div class="RegisterBox SetPassword">
            <input type="hidden" name="user_login" value="{{ $login }}">
            <p class="EysPasswordBox">
                <input type="password" name="password"
                       data-id="password_fn1"
                       id="password" placeholder="新しいパスワード | New password">
                <span class="EysPasswordClose password_fn1"></span>
            </p>
            @error('password')
            <label id="password-error" class="error" for="password">
                入力されたパスワードは過去に使用しているため設定できません。他のパスワードをご使用ください。
                <br>
                กรุณาตั้งรหัสที่คุณยังไม่เคยใช้ในเว็บไซต์นี้
            </label>
            @enderror
            <p class="EysPasswordBox">
                <input type="password" name="password_confirmation"
                       data-id="password_fn2"
                       placeholder="再入力 | Re-enter">
                <span class="EysPasswordClose password_fn2"></span>
            </p>
            <p class="FontJP">
                半角英文字・半角数字・半角記号（!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~）の3種類組み合わせて設定してください。<br>12文字以上で設定してください。<br>過去に使用したパスワードは使用できません。
            </p>
            <p>กรุณาตั้งรหัสที่มี ตัวอักษรภาษาอังกฤษ ตัวเลข และอักขระพิเศษ (!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~)<br>รหัสความยาวอย่างน้อย
                12 ตัวอักษร<br>กรุณาตั้งรหัสที่คุณยังไม่เคยใช้ในเว็บไซต์นี้</p>
            <p class="BTNAccept"><input type="submit" value="保存 | Save"></p>
        </div>
    </form>
    <!-- End RegisterBox -->
@endsection
@push('script')
    <script>
        $(document).ready(function () {

            $(".password_fn1").click(function () {
                var input = $("input[data-id='password_fn1']");
                if ($(this).hasClass('EysPasswordClose')) {
                    input.attr('type' , 'text');
                    $(".password_fn1").removeClass('EysPasswordClose');
                    $(".password_fn1").addClass('EysPasswordOpen');
                } else {
                    input.attr('type' , 'password');
                    $(".password_fn1").removeClass('EysPasswordOpen');
                    $(".password_fn1").addClass('EysPasswordClose');
                }
            });
            $(".password_fn2").click(function () {
                var input = $("input[data-id='password_fn2']");
                if ($(this).hasClass('EysPasswordClose')) {
                    input.attr('type' , 'text');
                    $(".password_fn2").removeClass('EysPasswordClose');
                    $(".password_fn2").addClass('EysPasswordOpen');
                } else {
                    input.attr('type' , 'password');
                    $(".password_fn2").removeClass('EysPasswordOpen');
                    $(".password_fn2").addClass('EysPasswordClose');
                }
            });

            $(".ValidateDisplay").hide();

            $.validator.addMethod("strong_password" , function (value , element) {
                let password = value;
                if (!(/^(?=.*[A-Za-z])(?=.*[0-9])(?=.*[!"#$%&'()*+,\-./:;<=>?@[\]^_`{|}~])(.{12,}$)/.test(password))) {
                    return false;
                }
                return true;
            } , function (value , element) {
                let password = $(element).val();
                if (!(/^(?=.*[A-Za-z])/.test(password))) {
                    return "半角英文字を組み合わせて設定してください。 กรุณาตั้งรหัสที่มี ตัวอักษรภาษาอังกฤษ อย่างน้อย 1 ตัวอักษร";
                } else if (!(/^(?=.*[0-9])/.test(password))) {
                    return "半角数字を組み合わせて設定してください。 กรุณาตั้งรหัสที่มี ตัวเลข อย่างน้อย 1 ตัวอักษร";
                } else if (!(/^(?=.*[!"#$%&'()*+,\-./:;<=>?@[\]^_`{|}~])/.test(password))) {
                    return "半角記号を組み合わせて設定してください。กรุณาตั้งรหัสที่มี อักขระพิเศษ (!\"#$%&'()*+,-./:;<=>?@[\]^_`{|}~) อย่างน้อย 1 ตัวอักษร";
                }
                return false;
            });

            $("#renew-password").validate({
                rules:
                    {
                        password: {
                            required: true ,
                            minlength: 12 ,
                            strong_password: true ,
                        } ,
                        password_confirmation: {
                            equalTo: "#password",
                            minlength: 12 ,
                        }
                    } ,
                messages: {
                    password: {
                        required: "パスワードを設定してください。กรุณาตั้งรหัสผ่าน" ,
                        minlength: "12文字以上で設定してください。กรุณาตั้งรหัสความยาวอย่างน้อย 12 ตัวอักษร"
                    },
                    password_confirmation:{
                        equalTo : "同じ内容を入力してください。กรุณากรอกข้อมูลให้ตรงกัน",
                        minlength: "12文字以上で設定してください。กรุณาตั้งรหัสความยาวอย่างน้อย 12 ตัวอักษร"
                    }
                }
            });
        });
    </script>
@endpush