@extends('sellers.app')

@section('content')
    <!-- MyPageTemplate -->
    <div class="MyPageTemplate">
        <!-- LeftMenu -->
        <div class="LeftMenu FontJP">
            <!-- LeftBox -->
            <div class="LeftBox clr">
                <!-- Mobile Left Menu -->
                <nav class="mobile-menu-LeftSide">
                    <input type="checkbox" id="checkboxleft" class="mobile-menu-LeftSide__checkbox">
                    <label for="checkboxleft" class="mobile-menu-LeftSide__btn">
                        <div class="mobile-menu-LeftSide__icon"></div>
                    </label>
                    <!-- menu__container -->
                    <div class="mobile-menu-LeftSide__container">
                        @include('sellers.includes.left-side',[ 'menu' => 'contact' ])
                    </div>
                    <!-- End menu__container -->
                </nav>
                <!-- End Mobile Left Menu -->
            </div>
            <!-- End LeftBox -->
        </div>
        <!-- End LeftMenu -->

        <!-- RightContent -->
        <div class="RightContent">

            <!-- OtherTemplate -->
            <div class="OtherTemplate clr">
                <h2 class="Title">事務局へのお問い合わせ | Contact Staff</h2>
                <div class="ContactStaff clr">
                    <p>

                            {!! nl2br(setting('admin.contact-jp')) !!}

                    </p>
                    <p><br></p>
                    <p class="FontTH">

                            {!! nl2br(setting('admin.contact-th') ) !!}
                    </p>
                </div>
            </div>
            <!-- End OtherTemplate -->

        </div>
        <!-- End RightContent -->
    </div>
    <!-- MyPageTemplate -->
@endsection