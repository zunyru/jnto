@extends('sellers.app')

@section('content')
    <!-- MyPageTemplate -->
    <div class="MyPageTemplate">
        <!-- LeftMenu -->
        <div class="LeftMenu FontJP">
            <!-- LeftBox -->
            <div class="LeftBox clr">
                <!-- Mobile Left Menu -->
                <nav class="mobile-menu-LeftSide">
                    <input type="checkbox" id="checkboxleft" class="mobile-menu-LeftSide__checkbox">
                    <label for="checkboxleft" class="mobile-menu-LeftSide__btn">
                        <div class="mobile-menu-LeftSide__icon"></div>
                    </label>
                    <!-- menu__container -->
                    <div class="mobile-menu-LeftSide__container">
                        @include("sellers.includes.left-side" , ['menu' => 'change-password'])
                    </div>
                    <!-- End menu__container -->
                </nav>
                <!-- End Mobile Left Menu -->
            </div>
            <!-- End LeftBox -->
        </div>
        <!-- End LeftMenu -->
        <div class="RightContent">

            <!-- Topic form -->
            <div class="FormBox clr">
                <div class="OthersPage SetPassword MarGonTop70">
                    <fieldset>
                        <div class="OthersPageBox">
                            <div class="ThanksCenter">
                                <img src="{{ asset('assets/images/icon/icon-thanks.svg') }}">
                                <p class="FontJP">パスワードの変更が完了しました</p>
                                <p class="FontJP">Your password has been changed</p>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <!-- End Topic form -->

        </div>
    </div>
@endsection