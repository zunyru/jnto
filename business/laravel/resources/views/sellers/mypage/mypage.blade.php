@extends('sellers.app')

@section('content')

    <!-- MyPageTemplate -->
    <div class="MyPageTemplate">
        <!-- LeftMenu -->
        <div class="LeftMenu FontJP">
            <!-- LeftBox -->
            <div class="LeftBox clr">
                <!-- Mobile Left Menu -->
                <nav class="mobile-menu-LeftSide">
                    <input type="checkbox" id="checkboxleft" class="mobile-menu-LeftSide__checkbox">
                    <label for="checkboxleft" class="mobile-menu-LeftSide__btn">
                        <div class="mobile-menu-LeftSide__icon"></div>
                    </label>
                    <!-- menu__container -->
                    <div class="mobile-menu-LeftSide__container">
                        @include('sellers.includes.left-side',['menu' => 'mypage'])
                    </div>
                    <!-- End menu__container -->
                </nav>
                <!-- End Mobile Left Menu -->
            </div>
            <!-- End LeftBox -->
        </div>
        <!-- End LeftMenu -->

        <form action="{{ route('business.store') }}" method="POST" enctype="multipart/form-data"
              id="myform">
            <input type="hidden" name="id" value="{{ $id }}">
            <input type="hidden" name="mode" value="edit">
            <input type="hidden" name="admin_mode" value="2">
        @csrf
        <!-- ToolsPanel -->
            <div class="ToolsPanel">
                <div class="ToolsPanelBox">
                    <div class="SectionInfo">
                        <ul>
                            <li><a href="#Section1">1</a></li>
                            <li><a href="#Section2">2</a></li>
                            <li><a href="#Section3">3</a></li>
                            <li><a href="#Section4">4</a></li>
                        </ul>
                    </div>
                    <div class="ToolsAction">
                        <p class="BTNPreview">
                            <a target="_blank"
                               href="{{ route('preview',$company->ref_id) }}">
                                <img
                                        src="{{ asset('assets/images/icon/icon-preview.svg') }}"> Preview</a></p>
                        <p class="SelectStatus"><span>Status</span>
                            <select
                                    name="status"
                                    class="block appearance-none w-full border-2 bg-white border-blue-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                    id="grid-state">
                                <option {{ $company->status == 'confirm' ? 'selected' : '' }} value="confirm">
                                    Draft
                                </option>
                                <option {{ $company->status == 'published' ? 'selected' : '' }} value="published">
                                    Published
                                </option>
                            </select>
                        </p>
                        <p class="BTNSave">
                            <button type="submit"><img src="{{ asset('assets/images/icon/icon-save.svg') }}"> Save
                            </button>
                        </p>
                    </div>
                </div>
            </div>
            <!-- ToolsPanel -->

            <!-- RightContent -->
            <div class="RightContent">

                <!-- Company information form -->
                <div class="FormBox clr">
                    <div class="FormList">

                        <!-- Phase2 มีปรับตรงนี้ --><span id="Section1"></span><!-- End Phase2 มีปรับตรงนี้ -->
                    @include('sessions.edit.company-information-edit')


                    <!-- Phase2 มีปรับตรงนี้ --><span id="Section2"></span><!-- End Phase2 มีปรับตรงนี้ -->
                    @include('sessions.edit.service-information-edit')


                    <!-- Phase2 มีปรับตรงนี้ --><span id="Section3"></span><!-- End Phase2 มีปรับตรงนี้ -->
                    @include('sessions.edit.support-menu-edit')

                    <!-- Phase2 มีปรับตรงนี้ --><span id="Section4"></span><!-- End Phase2 มีปรับตรงนี้ -->
                    @include('sessions.edit.category-tag-edit')

                    <!-- Phase2 มีปรับตรงนี้ -->
                    @include('sessions.edit.policy')
                    <!-- End Phase2 มีปรับตรงนี้ -->

                    </div>
                </div>
                <!-- End Company information form -->


            </div>
            <!-- End RightContent -->
        </form>
    </div>
    <!-- MyPageTemplate -->

@endsection
@include('layouts.validate-edit')
