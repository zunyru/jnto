@extends('sellers.app')

@section('content')
    <!-- MyPageTemplate -->
    <div class="MyPageTemplate">
        <!-- LeftMenu -->
        <div class="LeftMenu FontJP">
            <!-- LeftBox -->
            <div class="LeftBox clr">
                <!-- Mobile Left Menu -->
                <nav class="mobile-menu-LeftSide">
                    <input type="checkbox" id="checkboxleft" class="mobile-menu-LeftSide__checkbox">
                    <label for="checkboxleft" class="mobile-menu-LeftSide__btn">
                        <div class="mobile-menu-LeftSide__icon"></div>
                    </label>
                    <!-- menu__container -->
                    <div class="mobile-menu-LeftSide__container">
                        @include("sellers.includes.left-side",['menu' => 'change-email'])
                    </div>
                    <!-- End menu__container -->
                </nav>
                <!-- End Mobile Left Menu -->
            </div>
            <!-- End LeftBox -->
        </div>
        <!-- End LeftMenu -->

        <!-- RightContent -->
        <div class="RightContent">

            <!-- FormBox -->
            <div class="FormBox clr">

                <!-- RegisterStep -->
                <div class="RegisterStep clr">
                    <div class="RegisterStepBC">
                        <a class="active">
					<span class="RegisterStepBC__inner">
						<span class="RegisterStepBC__title">メールアドレスの入力</span>
						<span class="RegisterStepBC__desc">Enter <br>e-mail</span>
					</span>
                        </a>
                        <a>
					<span class="RegisterStepBC__inner">
						<span class="RegisterStepBC__title">メールの確認</span>
						<span class="RegisterStepBC__desc">Check <br>e-mail</span>
					</span>
                        </a>
                        <a>
					<span class="RegisterStepBC__inner">
						<span class="RegisterStepBC__title">パスワード設定</span>
						<span class="RegisterStepBC__desc">Set <br>Password</span>
					</span>
                        </a>
                        <a>
					<span class="RegisterStepBC__inner">
						<span class="RegisterStepBC__title">完了</span>
						<span class="RegisterStepBC__desc">Finish</span>
					</span>
                        </a>
                    </div>
                </div>
                <!-- End RegisterStep -->

                <form action="{{ route('seller.change-email') }}"
                      id="form-email" method="post">
                    @csrf
                    <div class="OthersPage ChangeEmailPass RegisterBox SetPassword">
                        <fieldset>
                            <legend>ログインメールを変更する | Change E-mail</legend>
                            <div class="OthersPageBox">
                                <p class="FontJP">新しいメールアドレス | New E-mail</p>
                                <p><input type="email" name="email_change" id="email" placeholder="メールアドレス | E-mail">
                                </p>
                                @error('email_change')
                                <label id="email-error" class="error" for="email">{{ $message }}</label>
                                @enderror
                                <p><input type="email" name="re_email" placeholder="再入力 Re-enter"></p>

                                <p class="FontJP EysPasswordClose" style="margin-top:30px;">パスワード | Password</p>
                                <p class="EysPasswordBox">
                                    <input type="password"
                                           data-id="password_fn"
                                           name="password" placeholder="パスワード | Password">
                                    <span class="EysPasswordClose password_fn"></span>
                                </p>
                                @error('password')
                                <label id="password-error" class="error" for="password">{{ $message }}</label>
                                @enderror
                                <p class="FontJP SmallFont">パスワードを忘れた方は、<a href="{{ route('forgot-password') }}"
                                                                           target="_blank">こちら</a>｜<a
                                            href="{{ route('forgot-password') }}" target="_blank">Forgot password?</a>
                                </p><br>

                                <p class="CheckboxAccept"><label>
                                        <input name="policy_check_box" type="checkbox">
                                        <span>個人情報保護方針に同意する。詳細は<a
                                                    href="{{ route('seller.privacy-policy.page') }}"
                                                    target="_blank">こちら</a><br>
					ยอมรับนโยบายการจัดการข้อมูลส่วนบุคคล ดูรายละเอียด<a href="{{ route('seller.privacy-policy.page') }}"
                                                                        target="_blank">ที่นี่</a></span></label></p>
                                <label id="policy_check_box-error" class="error" for="policy_check_box"></label>
                                <p class="BTNBorderRa"><input type="submit" value="確認メールを送信 | Send Activation E-mail">
                                </p>
                            </div>
                        </fieldset>
                    </div>
                </form>
                <!-- End FormBox -->

            </div>
            <!-- End RightContent -->
        </div>
        <!-- MyPageTemplate -->
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {

            $(".password_fn").click(function () {
                var input = $("input[data-id='password_fn']");
                if ($(this).hasClass('EysPasswordClose')) {
                    input.attr('type' , 'text');
                    $(".password_fn").removeClass('EysPasswordClose');
                    $(".password_fn").addClass('EysPasswordOpen');
                } else {
                    input.attr('type' , 'password');
                    $(".password_fn").removeClass('EysPasswordOpen');
                    $(".password_fn").addClass('EysPasswordClose');
                }
            });

            $("#form-email").validate({
                rules:
                    {
                        email_change: {
                            required: true ,
                            email: true
                        } ,
                        re_email: {
                            equalTo: "#email"
                        } ,
                        password: {
                            required: true
                        } ,
                        policy_check_box: {
                            required: true
                        }
                    },
                messages:{
                    email_change: {
                        required: "情報を入力してください。กรุณากรอกข้อมูล" ,
                        email: "メールアドレスを入力してください。กรุณากรอก E-mail"
                    } ,
                    re_email: {
                        equalTo: "確認のためにもう一度入力ください。กรุณากรอก E-mail อีกครั้ง"
                    } ,
                    password: {
                        required: "パスワードを設定してください。 กรุณากรอกรหัสผ่าน"
                    } ,
                    policy_check_box:{
                        required : "情報を入力してください。กรุณากรอกข้อมูล"
                    }
                }
            });
        });
    </script>
@endpush