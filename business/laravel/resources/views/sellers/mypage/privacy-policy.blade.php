@extends('sellers.app')

@section('content')

<!-- MyPageTemplate -->
<div class="MyPageTemplate">
	<!-- LeftMenu -->
	<div class="LeftMenu FontJP">
		<!-- LeftBox -->
		<div class="LeftBox clr">
			<!-- Mobile Left Menu -->
			<nav class="mobile-menu-LeftSide">
				<input type="checkbox" id="checkboxleft" class="mobile-menu-LeftSide__checkbox">
				<label for="checkboxleft" class="mobile-menu-LeftSide__btn"><div class="mobile-menu-LeftSide__icon"></div></label>
				<!-- menu__container -->
				<div class="mobile-menu-LeftSide__container">
					@include("sellers.includes.left-side",['menu' => 'policy'])
				</div>
				<!-- End menu__container -->
			</nav>
			<!-- End Mobile Left Menu -->
		</div>
		<!-- End LeftBox -->
	</div>
	<!-- End LeftMenu -->

	<!-- RightContent -->
	<div class="RightContent">

		<!-- OtherTemplate -->
		<div class="OtherTemplate clr">	
			<h2 class="Title">個人情報保護方針 | Privacy Policy</h2>
			<div class="ContactStaff clr">
				<p>個人情報を取り扱う際には、適正な収集・利用・管理を行います。ご登録いただいた個人情報は、旅行会社の連絡先として本サイトに掲載、本事業に関する情報のご連絡、JNTOタイ市場事業に関連する情報のご案内の目的のみに利用いたします。あらかじめご登録者の同意を得ることなく、個人情報を他の目的で利用及び第三者に提供することはございません。</p><br>
				<p>なお、ご登録者様の社名･団体名、事業内容・事業地域等の企業情報、住所、電話番号、メールアドレス、担当者の氏名（任意）等の情報について、訪日旅行商品造成促進の目的の実現のため、本サイトにてタイ旅行会社及び日タイに就航する航空会社が閲覧できる状況での公開を行うことについて、ご了承ください。</p>
				<p>JNTO個人情報保護方針は<a href="{{ setting('admin.privacy-policy-url-jp') }}" target="_blank">こちら</a>をご覧ください。</p>
				<p style="margin-top:20px;"><br></p>
				<p class="TitleSize FontTH">[นโยบายการจัดการข้อมูลส่วนบุคคล]</p>
				<p class="FontTH">สำนักงานบริหารโครงการ (“เรา”) จะรวบรวม ใช้ และเก็บรักษาข้อมูลส่วนบุคคลของท่านอย่างเหมาะสม ข้อมูลส่วนบุคคลที่ท่านลงทะเบียน จะถูกเผยแพร่บนเว็บไซต์เพื่อเป็นช่องทางให้ผู้ประกอบการท่องเที่ยวติดต่อท่าน และถูกใช้เพื่อวัตถุประสงค์ในการติดต่อเพื่อให้ข้อมูลเกี่ยวกับโครงการนี้ หรือให้ข้อมูลที่เกี่ยวข้องกับกิจกรรมขององค์การส่งเสริมการท่องเที่ยวแห่งประเทศญี่ปุ่น สำนักงานกรุงเทพฯเท่านั้น ข้อมูลส่วนบุคคลจะไม่ถูกใช้เพื่อวัตถุประสงค์อื่น หรือมอบให้กับบุคคลที่สามโดยไม่ได้รับความยินยอมล่วงหน้าจากผู้ลงทะเบียน<br><br>
				ชื่อบริษัท/ชื่อองค์กร เนื้อหาบริการ พื้นที่ที่ให้บริการ ที่อยู่ หมายเลขโทรศัพท์ อีเมล และชื่อของผู้รับผิดชอบ (ไม่บังคับกรอก) ที่ท่านลงทะเบียนบนเว็บไซต์นี้จะถูกเผยแพร่แก่ผู้ประกอบการท่องเที่ยวญี่ปุ่นในประเทศไทยและสายการบินที่ให้บริการเที่ยวบินระหว่างประเทศญี่ปุ่นและประเทศไทย เพื่อการส่งเสริมการสร้างสรรค์ผลิตภัณฑ์ท่องเที่ยวประเทศญี่ปุ่นเท่านั้น<br><br>
				ดูนโยบายความเป็นส่วนตัวของ JNTO ได้ <a href="{{ setting('admin.privacy-policy-url-th') }}" target="_blank">ที่นี่</a></p>
			</div>
			<p><br></p>
		</div>
		<!-- End OtherTemplate -->

	</div>
	<!-- End RightContent -->
</div>
<!-- MyPageTemplate -->

@endsection