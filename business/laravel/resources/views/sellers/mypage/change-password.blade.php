@extends('sellers.app')

@section('content')
    <!-- MyPageTemplate -->
    <div class="MyPageTemplate">
        <!-- LeftMenu -->
        <div class="LeftMenu FontJP">
            <!-- LeftBox -->
            <div class="LeftBox clr">
                <!-- Mobile Left Menu -->
                <nav class="mobile-menu-LeftSide">
                    <input type="checkbox" id="checkboxleft" class="mobile-menu-LeftSide__checkbox">
                    <label for="checkboxleft" class="mobile-menu-LeftSide__btn">
                        <div class="mobile-menu-LeftSide__icon"></div>
                    </label>
                    <!-- menu__container -->
                    <div class="mobile-menu-LeftSide__container">
                        @include("sellers.includes.left-side" , ['menu' => 'change-password'])
                    </div>
                    <!-- End menu__container -->
                </nav>
                <!-- End Mobile Left Menu -->
            </div>
            <!-- End LeftBox -->
        </div>
        <!-- End LeftMenu -->

        <!-- RightContent -->
        <div class="RightContent">

            <form action="{{ route('seller.change-password.store') }}"
                  id="reset-password" method="POST">
            @csrf
            <!-- Topic form -->
                <div class="FormBox clr">
                    <div class="OthersPage RegisterBox SetPassword MarGonTop70">
                        <fieldset>
                            <legend>パスワードを変更する | Change Password</legend>
                            <div class="OthersPageBox">

                                <p class="EysPasswordBox">
                                    <input type="password" name="password" id="password"
                                           data-id="password_fn1"
                                           placeholder="現在のパスワード | Password">
                                    <span class="EysPasswordClose password_fn1"></span>
                                </p>
                                @error('password')
                                <p class="Red FontJP SmallFont13">入力したパスワードが間違っています。</p>
                                <p class="Red SmallFont13">รหัสไม่ถูกต้อง โปรดลองอีกครั้ง</p>
                                @enderror
                                <p class="FontJP SmallFont13">パスワードを忘れた方は、<a href="{{ route('forgot-password') }}"
                                                                             target="_blank">こちら</a>｜<a
                                            href="{{ route('forgot-password') }}" target="_blank">Forgot password?</a>
                                </p>

                                <p style="margin-top:25px;">新しいパスワード | New password</p>
                                <p class="EysPasswordBox">
                                    <input type="password" name="new_password" id="new_password"
                                           data-id="password_fn2"
                                           placeholder="新しいパスワード | New password">
                                    <span class="EysPasswordClose password_fn2"></span>
                                </p>
                                <p class="EysPasswordBox">
                                    <input type="password" name="new_password_confirmation"
                                           data-id="password_fn3"
                                           placeholder="再入力 | Re-enter">
                                    <span class="EysPasswordClose password_fn3"></span>
                                </p>
                                @error('new_password')
                                <p class="Red FontJP SmallFont13">入力されたパスワードは過去に使用しているため設定できません。他のパスワードをご使用ください。</p>
                                <p class="Red SmallFont13">กรุณาตั้งรหัสที่คุณยังไม่เคยใช้ในเว็บไซต์นี้</p>
                                @enderror
                                <p class="FontJP" style="margin-top:20px;">
                                    半角英文字・半角数字・半角記号（!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~）の3種類組み合わせて設定してください。<br>12文字以上で設定してください。<br>過去に使用したパスワードは使用できません。
                                </p>
                                <p>กรุณาตั้งรหัสผ่านที่มี ตัวอักษรภาษาอังกฤษ ตัวเลข และอักขระพิเศษ (!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~)<br>รหัสผ่านต้องมีความยาวอย่างน้อย 12 ตัวอักษร<br>กรุณาตั้งรหัสที่คุณยังไม่เคยใช้ในเว็บไซต์นี้
                                </p>
                                <p class="BTNAccept"><input type="submit" value="保存 | Save"></p>
                            </div>
                        </fieldset>

                    </div>
                </div>
            </form>
            <!-- End Topic form -->


        </div>
        <!-- End RightContent -->
    </div>
    <!-- MyPageTemplate -->
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {

            $(".password_fn1").click(function () {
                var input = $("input[data-id='password_fn1']");
                if ($(this).hasClass('EysPasswordClose')) {
                    input.attr('type' , 'text');
                    $(".password_fn1").removeClass('EysPasswordClose');
                    $(".password_fn1").addClass('EysPasswordOpen');
                } else {
                    input.attr('type' , 'password');
                    $(".password_fn1").removeClass('EysPasswordOpen');
                    $(".password_fn1").addClass('EysPasswordClose');
                }
            });
            $(".password_fn2").click(function () {
                var input = $("input[data-id='password_fn2']");
                if ($(this).hasClass('EysPasswordClose')) {
                    input.attr('type' , 'text');
                    $(".password_fn2").removeClass('EysPasswordClose');
                    $(".password_fn2").addClass('EysPasswordOpen');
                } else {
                    input.attr('type' , 'password');
                    $(".password_fn2").removeClass('EysPasswordOpen');
                    $(".password_fn2").addClass('EysPasswordClose');
                }
            });
            $(".password_fn3").click(function () {
                var input = $("input[data-id='password_fn3']");
                if ($(this).hasClass('EysPasswordClose')) {
                    input.attr('type' , 'text');
                    $(".password_fn3").removeClass('EysPasswordClose');
                    $(".password_fn3").addClass('EysPasswordOpen');
                } else {
                    input.attr('type' , 'password');
                    $(".password_fn3").removeClass('EysPasswordOpen');
                    $(".password_fn3").addClass('EysPasswordClose');
                }
            });

            $(".ValidateDisplay").hide();

            $.validator.addMethod("strong_password" , function (value , element) {
                let password = value;
                if (!(/^(?=.*[A-Za-z])(?=.*[0-9])(?=.*[!"#$%&'()*+,\-./:;<=>?@[\]^_`{|}~])(.{12,}$)/.test(password))) {
                    return false;
                }
                return true;
            } , function (value , element) {
                let password = $(element).val();
                if (!(/^(?=.*[A-Za-z])/.test(password))) {
                    return "半角英文字を組み合わせて設定してください。 กรุณาตั้งรหัสที่มี ตัวอักษรภาษาอังกฤษ อย่างน้อย 1 ตัวอักษร";
                } else if (!(/^(?=.*[0-9])/.test(password))) {
                    return "半角数字を組み合わせて設定してください。 กรุณาตั้งรหัสที่มี ตัวเลข อย่างน้อย 1 ตัวอักษร";
                } else if (!(/^(?=.*[!"#$%&'()*+,\-./:;<=>?@[\]^_`{|}~])/.test(password))) {
                    return "半角記号を組み合わせて設定してください。กรุณาตั้งรหัสที่มี อักขระพิเศษ (!\"#$%&'()*+,-./:;<=>?@[\]^_`{|}~) อย่างน้อย 1 ตัวอักษร";
                }
                return false;
            });

            $("#reset-password").validate({
                rules:
                    {
                        password: {
                            required: true ,
                            minlength: 12 ,
                            strong_password: true ,
                        } ,
                        new_password: {
                            required: true ,
                            minlength: 12 ,
                            strong_password: true ,
                        } ,
                        new_password_confirmation: {
                            equalTo: "#new_password"
                        }
                    } ,
                messages: {
                    password: {
                        required: "パスワードを入力してください。กรุณาระบุรหัสผ่าน" ,
                        minlength: "12文字以上で設定してください。กรุณาตั้งรหัสความยาวอย่างน้อย 12 ตัวอักษร"
                    } ,
                    new_password: {
                        required: "パスワードを設定してください。กรุณาตั้งรหัสผ่าน" ,
                        minlength: "12文字以上で設定してください。กรุณาตั้งรหัสความยาวอย่างน้อย 12 ตัวอักษร"
                    } ,
                    new_password_confirmation: {
                        equalTo: "同じ内容を入力してください。กรุณากรอกข้อมูลให้ตรงกัน" ,
                        minlength: "12文字以上で設定してください。กรุณาตั้งรหัสความยาวอย่างน้อย 12 ตัวอักษร"
                    }
                }
            });
        });
    </script>
@endpush