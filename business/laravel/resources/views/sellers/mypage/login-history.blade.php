@extends('sellers.app')

@section('content')
    <!-- MyPageTemplate -->
    <div class="MyPageTemplate">
        <!-- LeftMenu -->
        <div class="LeftMenu FontJP">
            <!-- LeftBox -->
            <div class="LeftBox clr">
                <!-- Mobile Left Menu -->
                <nav class="mobile-menu-LeftSide">
                    <input type="checkbox" id="checkboxleft" class="mobile-menu-LeftSide__checkbox">
                    <label for="checkboxleft" class="mobile-menu-LeftSide__btn">
                        <div class="mobile-menu-LeftSide__icon"></div>
                    </label>
                    <!-- menu__container -->
                    <div class="mobile-menu-LeftSide__container">
                        @include('sellers.includes.left-side',['menu' => 'login-history'])
                    </div>
                    <!-- End menu__container -->
                </nav>
                <!-- End Mobile Left Menu -->
            </div>
            <!-- End LeftBox -->
        </div>
        <!-- End LeftMenu -->

        <!-- RightContent -->
        <div class="RightContent">

            <!-- OtherTemplate -->
            <div class="OtherTemplate clr">
                <h2 class="Title">ログイン履歴 | Login History</h2>
                <div class="LoginHistoryList clr">
                    <ul>
                        <li class="LHHeader"><p>Date/time</p>
                            <p>IP Address</p></li>
                        @forelse($histories as $history)
                            <li>
                                <p>{{ $history->created_at->format('Y/m/d H:i') }}</p>
                                <p>{{ $history->properties['ip'] }}</p></li>
                        @empty
                            <li>ไม่พบประวัติการ login</li>
                        @endforelse
                    </ul>
                </div>
                {{ $histories->links('pagination::custom') }}
            </div>
            <!-- End OtherTemplate -->

        </div>
        <!-- End RightContent -->
    </div>
    <!-- MyPageTemplate -->
@endsection