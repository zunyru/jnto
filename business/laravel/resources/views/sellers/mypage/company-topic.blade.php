@extends('sellers.app')

@push('css')
    <style>
        button[type="submit"]:disabled {
            background-color: #9E9E9E !important;
            cursor: no-drop !important;
        }
    </style>
@endpush
@section('content')
    <!-- MyPageTemplate -->
    <div class="MyPageTemplate">
        <!-- LeftMenu -->
        <div class="LeftMenu FontJP">
            <!-- LeftBox -->
            <div class="LeftBox clr">
                <!-- Mobile Left Menu -->
                <nav class="mobile-menu-LeftSide">
                    <input type="checkbox" id="checkboxleft" class="mobile-menu-LeftSide__checkbox">
                    <label for="checkboxleft" class="mobile-menu-LeftSide__btn">
                        <div class="mobile-menu-LeftSide__icon"></div>
                    </label>
                    <!-- menu__container -->
                    <div class="mobile-menu-LeftSide__container">
                        @include("sellers.includes.left-side",['menu' => 'toppic'])
                    </div>
                    <!-- End menu__container -->
                </nav>
                <!-- End Mobile Left Menu -->
            </div>
            <!-- End LeftBox -->
        </div>
        <!-- End LeftMenu -->
        <form id="topic" action="{{ route('company-topic.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <!-- ToolsPanel -->
            <div class="ToolsPanel">
                <div class="ToolsPanelBox">
                    <div class="TemplateTools">
                        <div class="Left">
                            <p class="SelectStatus"><span>Status</span>
                                <label>
                                    <input type="radio"
                                           value="editing"
                                           name="status"
                                            {{ @$topic->status == 'editing' ? 'checked' : 'checked' }}>
                                    Editing</label>
                                <label>
                                    <input
                                            type="radio"
                                            name="status"
                                            value="done"
                                            {{ @$topic->status == 'done' ? 'checked' : '' }}
                                    > Done
                                </label>
                            </p>
                        </div>
                        <div class="Right">
                            <p class="BTNSave TopPicPage">
                                <button type="submit">
                                    <img src="{{ asset('assets/images/icon/icon-save.svg') }}"> Save
                                </button>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ToolsPanel -->

            <!-- RightContent -->
            <div class="RightContent">
                <input type="hidden" name="id" value="{{ @$topic->id }}">
                <input type="hidden" name="company_id" value="{{ Auth::guard('seller')->user()->id }}">
                <!-- Topic form -->
                <div class="FormBox clr" style="margin-top:60px;">
                    <div class="FormList">
                        <fieldset style="background-color: #e2ece8; border: 1px solid #dfdfdf;">
                            <legend style="color: #008084;">5 Event & Latest information</legend>

                            <ul class="StyleMP0">
                                <li class="LabelMarTop RadioBox">
                                    <p class="FloatLeft">5-1 カテゴリー | <span class="FontNormal">Category </span><span
                                                class="NeedRed">*</span></p>
                                    <label><input type="radio" name="category"
                                                  value="event"
                                                  {{ @$topic->category == 'event' ? 'checked' : '' }}
                                                  id="1"
                                                  onclick="TopicCheck(1);">
                                        イベント | Event
                                    </label>
                                    <div>
                                        <span class="FontNormal2">
                                        開催期間が確定しているイベントで、開催期間が1ヶ月以内のイベント。また登録日から6ヶ月以内に実施されるイベントは「イベント」としてご登録ください。
                                        </span>
                                        <br>
                                        <span class="FontNormal2">
                                            Please post as event in case of the event has a set duration and will occur within one month of the event's start date and the event will occur within six months of the post date.
                                        </span>
                                    </div>
                                    <label><input type="radio" name="category"
                                                  value="information"
                                                  id="2"
                                                  {{ @$topic->category == 'information' ? 'checked' : '' }}
                                                  onclick="TopicCheck(2);">
                                        お知らせ | Latest information
                                    </label>
                                    <div>
                                        <span class="FontNormal2">
                                        日本側セラーからのお知らせや開催期間が決まってないイベントなど。また開催期間が1ヶ月以上のイベント、登録日から6ヶ月以降に実施されるイベントは「お知らせ」としてご登録ください。
                                        </span>
                                        <br>
                                        <span class="FontNormal2">
                                            Please post as “Latest Information” in case that There is no set the time for the event and the Event period is more than 1 month.  Additionally it will be held after 6 months.
                                        </span>
                                    </div>
                                </li>
                                <label id="category-error" class="error" for="category"></label>
                                <li id="TopicCheck"
                                    style="display: {{ @$topic->category == 'event' ? 'block' : 'none' }}">
                                    <label>
                                        <p>5-1-1 イベント開始日 <span
                                                    class="NeedRed">*</span></p>

                                        <span
                                                class="FontNormal">Start date </span>
                                        <input type="text"
                                               class="TxtBoxDate"
                                               name="event_date_start"
                                               id="from"
                                               value="{{ !empty(@$topic) ? is_null($topic->event_date_start) ? '' : $topic->event_date_start->format('Y-m-d') : '' }}"
                                               placeholder="2021-08-00" style="display: inline-block;">
                                        <span
                                                class="FontNormal">End date </span><span
                                                class="NeedRed"></span>
                                        <input type="text"
                                               class="TxtBoxDate"
                                               name="event_date_end"
                                               id="to"
                                               value="{{ !empty(@$topic) ? is_null($topic->event_date_end) ? '' : $topic->event_date_end->format('Y-m-d') : '' }}"
                                               placeholder="2021-08-00" style="display: inline-block;">
                                    </label>

                                </li>
                                <li>
                                    <p>5-2 画像 | <span class="FontNormal">Photo</span><br>
                                        <span class="FontNormal2">画像サイズの目安：W830×H550ピクセル／解像度200dpi／1MB以下／PNGまたはJPG  <br>
ขนาดภาพที่แนะนำ W830×H550 pixels / ความละเอียด 200 dpi / ความจุต่ำกว่า 1MB / PNG หรือ JPG</span>
                                    </p>
                                    @if(@$topic->photo)
                                        <img width="350"
                                             id="main_photo_preview"
                                             src="{{ asset($topic->photo) }}" alt="photo">
                                        <br>
                                        <p class="DelPicture2">
                                            <a href="javascript:void(0);"
                                               data-filters="photo"
                                               data-db="topics"
                                               data-where="id"
                                               data-id="{{ $topic->id }}"
                                               class="delete-img bg-red-600 text-white text-center p-2 rounded-md">
                                                x Delete
                                            </a>
                                        </p>
                                    @else
                                        <img width="350"
                                             id="main_photo_preview"
                                             src=""
                                             alt="photo">
                                        <br>
                                    @endif
                                    <label id="main_photo-error" class="error" for="main_photo"></label>
                                    <br>
                                    <span>
										<label for="main_photo" class="FileUploadCSS"> Choose a file <input
                                                    type="file"
                                                    name="photo"
                                                    accept="image/*"
                                                    id="main_photo"/>
										</label>
									</span>
                                </li>
                                <li>
                                    <label>
                                        <p>5-3 タイトル | <span class="FontNormal">Title  </span><span
                                                    class="NeedRed">*</span>
                                        </p>
                                        <input type="text"
                                               maxlength="100"
                                               name="title"
                                               value="{{ old('title') ?? @$topic->title }}"
                                               placeholder="英語またはタイ語100文字以内 ภาษาอังกฤษหรือภาษาไทย สูงสุด 100 ตัวอักษร">
                                    </label>
                                    <p><span class="title">0</span>/100</p>
                                </li>
                                <li>
                                    <label>
                                        <p>5-4 本文 | <span class="FontNormal">Detail </span> <span
                                                    class="NeedRed">*</span>
                                        </p>
                                        <textarea required
                                                  name="detail"
                                                  id="detail"
                                                  maxlength="1000"
                                                  placeholder="英語またはタイ語1,000文字以内、URLも記載可能 | ภาษาอังกฤษหรือภาษาไทย สูงสุด 1,000 ตัวอักษร และสามารถใส่ URL Website ได้">{!! old('detail') ?? nl2br(preg_replace('/<p[^>]*>/', '', @$topic->detail)) !!}</textarea>
                                        <p id="character_count"></p>
                                    </label>
                                    @error('detail')
                                    <p class="error">
                                        {{ $errors->first() }}
                                    </p>
                                @enderror
                            </ul>
                        </fieldset>

                    </div>
                </div>
                <!-- End Topic form -->


            </div>
            <!-- End RightContent -->
        </form>
    </div>
    <!-- MyPageTemplate -->
@endsection
@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <link rel="stylesheet" href="{{ asset('assets/jquery-ui/jquery-ui.min.css') }}">
    <script src="{{ asset('assets/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
    <script>
        $(document).ready(function () {
            @error('detail')
            toastr.error(`{{ $errors->first() }}`);
            @enderror

            let title = $('input[name="title"]');
            $('span.title').html(title.val().length);
            title.on('keyup', function () {
                $('span.title').html($(this).val().length);
            });

            $(function () {

                $("#from")
                    .datepicker({
                        dateFormat: "yy-mm-dd",
                        changeMonth: true,
                        changeYear: true,
                        monthNamesShort: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                    })
                    .on("change", function () {
                        $("#to").datepicker("option", "minDate", getDate(this));
                        let nextDay = new Date(this.value);
                        nextDay.setDate(nextDay.getDate() + 30);
                        $("#to").datepicker("option", "maxDate", nextDay);
                    });

                $("#to").datepicker({
                    dateFormat: "yy-mm-dd",
                    changeMonth: true,
                    changeYear: true,
                    monthNamesShort: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                }).on("change", function () {
                    $("#from").datepicker("option", "maxDate", getDate(this));
                    let lastDay = new Date(this.value);
                    lastDay.setDate(lastDay.getDate() - 30);
                    $("#from").datepicker("option", "minDate", lastDay);
                });

                function getDate(element) {
                    var date;

                    var dateFormat = "yy-mm-dd";
                    try {
                        date = $.datepicker.parseDate(dateFormat, element.value);
                    } catch (error) {
                        date = null;
                    }
                    console.log(element.value);
                    return date;
                }
            });

            tinymce.init({
                selector: 'textarea#detail',
                menubar: false,
                width: '100%',
                height: 500,
                forced_root_block: false,
                force_br_newlines: true,
                force_p_newlines: false,
                toolbar: 'bold customBr link underline',
                plugins: 'nonbreaking link',
                convert_urls: false,
                setup: function (editor) {
                    editor.ui.registry.addButton('customBr', {
                        text: 'Enter',
                        onAction: function () {
                            editor.insertContent('<br/> ');
                        },
                    });
                    editor.on('keyup', function (e) {
                        var count = CountCharacters();
                        ValidateCharacterLength(count);
                        document.getElementById("character_count").innerHTML = count.toLocaleString() + "/1,000";
                    });
                    editor.on('init', function (e) {
                        //var body = editor.getContent();
                        var body = tinymce.get("detail").getContent({format: "text"});
                        $('#character_count').html(body.length.toLocaleString() + "/1,000");
                    });
                },
                onchange_callback: function (editor) {
                    tinyMCE.triggerSave();
                    $("#" + editor.id).valid();
                }
            });

            $.validator.addMethod('filesize', function (value, element, param) {
                var param = param * 1000000;
                return this.optional(element) || (element.files[0].size <= param)
            }, function (param, element) {
                return "File size must be less than " + param + "MB"
            });

            $("#topic").submit(function () {
                tinyMCE.triggerSave();
            }).validate({
                rules:
                    {
                        category: {
                            required: true
                        },
                        title: {
                            required: true,
                            maxlength: 255,
                        },
                        detail: {
                            required: true,
                        },
                        event_date_start: {
                            required: function (element) {
                                return $("input[name$='category']").val() == 'event';
                            }
                        },
                        event_date_end: {
                            required: function (element) {
                                return $("input[name$='category']").val() == 'event';
                            }
                        },
                        photo: {
                            extension: "png|jpg|jpeg",
                            filesize: 1
                        }
                    },
                messages: {
                    category: {
                        required: "Please input Category",
                    },
                    title: {
                        required: "Please input Title ",
                    },
                    detail: {
                        required: "Please input Detail "
                    },
                    event_date_start: {
                        required: "Please choose event start date",
                    },
                    event_date_end: {
                        required: "Please choose event end date",
                    }
                }
            });

            $("#main_photo").change(function () {
                readURL(this);
            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var preview = $(input).attr('id') + '_preview';
                        $('#' + preview).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                } else {
                    $('#previewHolder').attr('src', '');
                }
            }

            function CountCharacters() {
                var body = tinymce.get("detail").getBody();
                var content = tinymce.trim(body.innerText || body.textContent);
                return content.length;
            };

            function ValidateCharacterLength(count) {
                var max = 1000;
                if (count > max) {
                    $('button[type="submit"]').prop('disabled', true);
                    toastr.error("Maximum " + max + " characters allowed.")
                    return false;
                }
                $('button[type="submit"]').prop('disabled', false);
                return true;
            }

            $('.delete-img').click(function () {
                var data_value = $(this).attr('data-filters');
                var data_db = $(this).attr('data-db');
                var data_id = $(this).attr('data-id');
                var data_where = $(this).attr('data-where');
                Swal.fire({
                    title: 'Are you sure to delete ?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            method: "POST",
                            headers: {
                                'X-CSRF-TOKEN': `{{ csrf_token() }}`
                            },
                            data: {
                                id: data_id,
                                field: data_value,
                                db: data_db,
                                where: data_where,
                            },
                            url: `{{ route('file.delete') }}`,
                        }).done(function (msg) {
                            toastr.success(msg.message);
                            setTimeout(function () {
                                location.reload();
                            }, 500);

                        }).fail(function (msg) {
                            toastr.error(msg.message)
                        });
                    }
                });
            });
        });
    </script>
@endpush