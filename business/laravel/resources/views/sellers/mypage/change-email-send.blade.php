@extends('sellers.app')

@section('content')
    <!-- MyPageTemplate -->
    <div class="MyPageTemplate">
        <!-- LeftMenu -->
        <div class="LeftMenu FontJP">
            <!-- LeftBox -->
            <div class="LeftBox clr">
                <!-- Mobile Left Menu -->
                <nav class="mobile-menu-LeftSide">
                    <input type="checkbox" id="checkboxleft" class="mobile-menu-LeftSide__checkbox">
                    <label for="checkboxleft" class="mobile-menu-LeftSide__btn">
                        <div class="mobile-menu-LeftSide__icon"></div>
                    </label>
                    <!-- menu__container -->
                    <div class="mobile-menu-LeftSide__container">
                        @include("sellers.includes.left-side",['menu' => 'change-email'])
                    </div>
                    <!-- End menu__container -->
                </nav>
                <!-- End Mobile Left Menu -->
            </div>
            <!-- End LeftBox -->
        </div>
        <!-- End LeftMenu -->

        <!-- RightContent -->
        <div class="RightContent">

            <!-- FormBox -->
            <div class="FormBox clr">

                <!-- RegisterStep -->
                <div class="RegisterStep clr">
                    <div class="RegisterStepBC">
                        <a>
					<span class="RegisterStepBC__inner">
						<span class="RegisterStepBC__title">メールアドレスの入力</span>
						<span class="RegisterStepBC__desc">Enter <br>e-mail</span>
					</span>
                        </a>
                        <a class="active">
					<span class="RegisterStepBC__inner">
						<span class="RegisterStepBC__title">メールの確認</span>
						<span class="RegisterStepBC__desc">Check <br>e-mail</span>
					</span>
                        </a>
                        <a>
					<span class="RegisterStepBC__inner">
						<span class="RegisterStepBC__title">パスワード設定</span>
						<span class="RegisterStepBC__desc">Set <br>Password</span>
					</span>
                        </a>
                        <a>
					<span class="RegisterStepBC__inner">
						<span class="RegisterStepBC__title">完了</span>
						<span class="RegisterStepBC__desc">Finish</span>
					</span>
                        </a>
                    </div>
                </div>
                <!-- End RegisterStep -->

                <div class="OthersPage ChangeEmailPass RegisterBox SetPassword">
                    <fieldset>
                        <legend>ログインメールを変更する | Change E-mail</legend>
                        <div class="OthersPageBox">
                            <p class="FontJP DarkBlue19">新しいメールアドレスにメールを送信しました。<br>
                                メールに記載されているパスワードリセット用URLをクリックし、<br>
                                新しいパスワードに設定後、ログインメールの変更は完了します。</p>
                            <p class="FontJP">10分経っても返信がない場合、スパム・迷惑メールをご確認の上、<br>下記までお問い合わせください。</p>
                            <p class="FontJP">
                                {!! nl2br(setting('admin.contact-jp')) !!}
                            </p>
                            <p class="DarkBlue22" style="margin-top:30px;">โปรดตรวจสอบอีเมลของท่าน<br>และคลิก URL
                                รีเซ็ตรหัสผ่านภายในอีเมล<br>การเปลี่ยนอีเมลจะเสร็จสมบูรณ์ หลังจากท่านตั้งรหัสผ่านใหม่
                            </p>
                            <p>
                                {!! nl2br(setting('admin.contact-th')) !!}
                            </p>
                        </div>
                    </fieldset>
                </div>
                <!-- End FormBox -->

            </div>
            <!-- End RightContent -->
        </div>
        <!-- MyPageTemplate -->
@endsection