@include('sellers.includes.header')
@yield('content')
{{--
@include('sellers.includes.footer')--}}
@include('layouts.script')
@stack('scripts')

<script>

    /* Phase2 */
    function TopicCheck(id){
        var TopicCheck = document.getElementById('TopicCheck');
        if(id==1){ TopicCheck.style.display='block'; }
        if(id==2){ TopicCheck.style.display='none'; }
    }
    function PrivacyCheck(id){
        var PublicDisplay = document.getElementById('PublicDisplay');
        var PrivateDisplay = document.getElementById('PrivateDisplay');
        if(id==1){ PublicDisplay.style.display='block'; PrivateDisplay.style.display='none'; }
        if(id==2){ PublicDisplay.style.display='none'; PrivateDisplay.style.display='block'; }
    }
    /* End Phase2 */
</script>

</body>
</html>
