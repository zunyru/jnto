@extends('sellers.app-register')

@section('content')
    <!-- HeaderRegister -->
    <div class="HeaderRegister clr">
        <h1>パスワードをリセットする｜Reset Password</h1>
    </div>
    <!-- End HeaderRegister -->

    <!-- RegisterStep -->
    <div class="RegisterStep clr">
        <div class="RegisterStepBC">
            <a class="active">
			<span class="RegisterStepBC__inner">
				<span class="RegisterStepBC__title">メールアドレスの入力</span>
				<span class="RegisterStepBC__desc">Enter <br>e-mail</span>
			</span>
            </a>
            <a>
			<span class="RegisterStepBC__inner">
				<span class="RegisterStepBC__title">メールの確認</span>
				<span class="RegisterStepBC__desc">Check <br>e-mail</span>
			</span>
            </a>
            <a>
			<span class="RegisterStepBC__inner">
				<span class="RegisterStepBC__title">パスワード設定</span>
				<span class="RegisterStepBC__desc">Set <br>Password</span>
			</span>
            </a>
            <a>
			<span class="RegisterStepBC__inner">
				<span class="RegisterStepBC__title">完了</span>
				<span class="RegisterStepBC__desc">Finish</span>
			</span>
            </a>
        </div>
    </div>
    <!-- End RegisterStep -->

    <form action="{{ route('seller.forgot-password') }}"
          id="register" method="post">
    @csrf
    <!-- RegisterBox -->
        <div class="RegisterBox SetPassword clr">
            <p><input type="email"
                      id="email"
                      name="email"
                      value="{{ old('email') }}"
                      placeholder="メールアドレス | E-mail"></p>
            <p><input type="email"
                      name="re_email"
                      value="{{ old('re_email') }}"
                      placeholder="再入力 | Re-enter"></p>

            <div>
                <p class="FontJP">登録しているメールアドレスを入力し、送信ボタンを押してください。</p>
                <p>กรุณากรอกอีเมลที่ท่านใช้ในการสมัครสมาชิก และกดปุ่ม Send</p>
            </div>

            <!-- ValidateDisplay -->
            <div class="ValidateDisplay">
                <p class="Red FontJP">
                    入力されたメールアドレスは登録されていません。入力内容に間違いがないかご確認の上、もう一度をお試しください。上記で解決しない場合は、事務局までご連絡ください。</p>
                <p class="Red">
                    ไม่พบอีเมลในระบบ กรุณาตรวจสอบความถูกต้องและทดลองอีกครั้ง
                    <br>
                    หากไม่สำเร็จกรุณาติดต่อบริษัทตัวแทนดำเนินงาน
                </p>
                <div class="AddressBox">
                    <p class="FontJP">
                        {!! nl2br(setting('admin.contact-jp')) !!}
                    </p>
                    <p style="margin-top:10px;">
                        {!! nl2br(setting('admin.contact-th') ) !!}
                    </p>
                </div>
            </div>

            <!-- End ValidateDisplay -->

            <p class="BTNAccept"><input type="submit" value="送信｜Send"></p>

        </div>
    </form>
    <!-- End RegisterBox -->
@endsection
@push('script')
    <script>
        $(document).ready(function () {
            $(".ValidateDisplay").hide();
            $("#register").validate({
                submitHandler: function (form) {
                    const email = $("input[name='email']").val();
                    $.ajax({
                        url: `{{ route('check-has-email') }}` ,
                        method: "POST" ,
                        data: {
                            _token: `{{ csrf_token() }}` ,
                            email: email
                        } ,
                        success: function (data) {
                            if (data >= 1) {
                                form.submit();
                            } else {
                                $(".ValidateDisplay").show();
                            }
                        } ,
                        error: function () {
                            return false;
                        } ,
                    });
                    return false;
                } ,
                rules:
                    {
                        email: {
                            required: true ,
                            email: true
                        } ,
                        re_email: {
                            equalTo: "#email"
                        }
                    } ,
                messages: {
                    email: {
                        required: "情報を入力してください。กรุณากรอกข้อมูล" ,
                        email: "メールアドレスを入力してください。กรุณากรอก E-mail"
                    } ,
                    re_email: {
                        equalTo: "確認のためにもう一度入力ください。กรุณากรอก E-mail อีกครั้ง"
                    }
                }
            });
        });
    </script>
@endpush