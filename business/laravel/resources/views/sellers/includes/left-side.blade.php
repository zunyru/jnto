<!-- End Mobile Menu -->
<div class="GroupBox clr">
    <p class="GroupHeader">ウェブ表示情報の編集<span class="FontEN">Edit your web page</span></p>
    <ul>
        <li><a href="{{ route('seller.mypage') }}" class="{{ $menu == 'mypage' ? 'Active' : '' }}">セラー基本情報<span
                        class="FontEN">Company information</span></a></li>
        <li><a href="{{ route('company-topic') }}" class="{{ $menu == 'toppic' ? 'Active' : '' }}">イベント・お知らせ<span
                        class="FontEN">Event & Latest information</span></a></li>
        <li><a href="{{ route('privicy-setting') }}" class="{{ $menu == 'privicy-setting' ? 'Active' : '' }}">日本側セラーへの公開設定<span
                        class="FontEN">Privacy Setting</span></a></li>
    </ul>
</div>
<div class="GroupBox clr">
    <p class="GroupHeader">ログイン情報の変更<span class="FontEN">Change login details</span></p>
    <ul>
        <li><a href="{{ route('change-email') }}" class="{{ $menu == 'change-email' ? 'Active' : '' }}">ログインメール変更<span
                        class="FontEN">Change Login E-mail</span></a></li>
        <li><a href="{{ route('change-password') }}"
               class="{{ $menu == 'change-password' ? 'Active' : '' }}">パスワード変更<span
                        class="FontEN">Change Password </span></a></li>
    </ul>
</div>
<div class="GroupBox clr">
    <p class="GroupHeader">その他｜Others</p>
    <ul>
        <li><a href="{{ route('login-history') }}" class="{{ $menu == 'login-history' ? 'Active' : '' }}">ログイン履歴<span
                        class="FontEN">Login History</span></a></li>
        <li><a href="{{ route('contact-staff') }}" class="{{ $menu == 'contact' ? 'Active' : '' }}">事務局へのお問い合わせ<span
                        class="FontEN">Contact Staff</span></a></li>
        <li><a href="{{ route('seller.privacy-policy.page') }}" class="{{ $menu == 'policy' ? 'Active' : '' }}">個人情報保護方針<span
                        class="FontEN">Privacy Policy</span></a></li>
    </ul>
</div>