<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Resources for Travel Agents</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css-new/style.css') }}?v=5">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css-new/style-responsive.css') }}?v=5">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css-new/style-menu.css') }}?v=5">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css-new/style-popup.css') }}?v=5">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css-new/style-mypage.css') }}?v=5">
    <!-- SlickSlider -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/slick/slick.css') }}?v=5"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/slick/slick-theme.css') }}?v=5"/>
    <!-- End SlickSlider -->
    <link rel="icon" href="{{ asset('favicon-32x32.png') }}" type="image" sizes="32x32">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Montserrat:wght@300;400;500;600;700&family=Sarabun:wght@100;500&display=swap"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=M+PLUS+1p:wght@100;300;400;500;700;800;900&display=swap"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <style>
        .block {
            display: block;
        }

        form label.error {
            display: none;
        }

        .top--20 {
            position: relative;
            top: -20px;
        }

        .error {
            color: red;
            margin-top: unset;
            margin-bottom: unset;
        }

        #toast-container {
            z-index: 9999999999;
        }
    </style>
    @stack('css')

    @include('includes.google-tag-manager-header')

</head>
<body>

@php
    $seminar_repo = new \App\Repositories\SeminarRepository();
    $seminar_all = $seminar_repo->all_published();
    $grouped_seminars = $seminar_all->groupBy('year');
    $seminars = $grouped_seminars->all();

    $useful_links_repo = new \App\Repositories\UsefulLinkRepository();
    $nav_groups = $useful_links_repo->group_category();

    $route_list = Illuminate\Support\Facades\Route::getCurrentRoute()->getName();
    $routes = explode(".",$route_list);
@endphp


@include('includes.google-tag-manager-body')


<div id="BackToTop"></div>
<!-- Mobile Menu -->
<nav class="mobile-menu MenuSeller">
    <!-- HeaderLogo -->
    <div class="HeaderLogo">
        <a href="{{ route('home') }}"
           @if(isset($routes[0]) && @$routes[0] == 'seller')
           target="_blank"
           @endif
        >
            <img src="{{ asset('assets/images/logo.png') }}">
            <span>JNTO B2B Website</span>
        </a>
    </div>
    <!-- End HeaderLogo -->

    <!-- Phase2 MemberHeader -->
    <div class="MemberHeader">
        <p class="TopEmail">
            <a href="{{ route('seller.mypage') }}">
                {{ Auth::guard('seller')->user()->user_login }} (ID{{ Auth::guard('seller')->user()->ref_id }})
            </a>
        </p>
        <p><a href="{{ route('home') }}" class="BTNTopPage">TOP</a></p>
        <p><a href="#"
              onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
              class="BTNLogout">Logout</a>
        </p>
    </div>
    <!-- End Phase2 MemberHeader -->

    <input type="checkbox" id="checkbox" class="mobile-menu__checkbox">
    <label for="checkbox" class="mobile-menu__btn">
        <div class="mobile-menu__icon"></div>
    </label>
    <!-- menu__container -->
    <div class="mobile-menu__container">
        <!-- Header -->
        <header>
            <div class="Header">
                <!-- FooterBox -->
                <div class="HeaderBox clr">
                    <h2><a href="{{ route('home') }}">B2B Website</a></h2>

                    <div class="FooterBoxListAll">

                        <div class="FooterBoxLeft">
                            <p><span>Discover new partners!</span>ค้นหาพาร์ทเนอร์ฝ่ายญี่ปุ่น</a></p>
                            @if((Auth::guard('seller')->check() && Auth::guard('seller')->user()->privacy != 'private') || Auth::guard('web')->check())
                            <p class="Links">
                                <a href="{{ route('seller-info.search') }}">> ค้นหา</a>
                            </p>
                            <p class="Links">
                                <a href="{{ route('topic.all') }}">> อีเวนต์
                                    ข่าวสาร</a>
                            </p>
                            @endif
                        </div>

                        <div class="FooterBoxRight">
                            <p><span>News</span>ข่าวสาร</a></p>
                            <p class="Links"><a href="{{ route('news-jnto.all') }}">> ข่าวสารจาก JNTO</a></p>
                            <p class="Links"><a href="{{ route('news-jnto-partner.all') }}">> ข่าวสารจากองค์กรท่องเที่ยว</a></p>
                        </div>

                        <div class="FooterBoxRight clr">
                            <p><a href="{{ route('seminar-list') }}"><span>Online Seminar</span>สัมมนาออนไลน์</a>
                            </p>

                        </div>

                    </div>

                    <!-- ULTail -->
                    {{--<div class="ULTail clr" style="clear: both;">

                        <h2><span>Useful Links</span>เว็บไซต์ที่เป็นประโยชน์</h2>
                        <div class="Boxlist">

                        <div class="ULTailList">
                            <h3>ข้อมูลท่องเที่ยว</h3>
                            <ul>
                                <li>
                                    <a href="https://www.jnto.or.th/contactus/download/" target="_blank">
                                        ดาวน์โหลดเอกสารเที่ยวญี่ปุ่น (ภาษาไทย)
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.jnto.go.jp/brochures/eng/index.php" target="_blank">
                                        ดาวน์โหลดเอกสารเที่ยวญี่ปุ่น (ภาษาอังกฤษ)
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.japan.travel/adventure/en/" target="_blank">
                                        เที่ยวญี่ปุ่นแนวแอดเวนเจอร์
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.japan.travel/snow/en/" target="_blank">
                                        ฐานข้อมูลเกี่ยวกับกิจกรรมในฤดูหนาวที่ญี่ปุ่น
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.japan.travel/luxury/" target="_blank">
                                        เที่ยวญี่ปุ่นแบบหรูหรา
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.japan.travel/diving/en/" target="_blank">
                                        ดำน้ำที่ญี่ปุ่น
                                    </a>
                                </li>
                                <li>
                                    <a href="https://business.jnto.go.jp/" target="_blank">
                                        รูปภาพและวีดีโอโปรโมทเที่ยวญี่ปุ่น
                                    </a>
                                </li>
                            </ul>
                        </div>


                        <div class="ULTailList">
                            <h3>ข้อมูลอินเซ็นทีฟ และ MICE</h3>
                            <ul>
                                <li>
                                    <a href="https://www.japanmeetings.org/" target="_blank">
                                        อินเซ็นทีฟ MICE และการจัดอีเว้นท์ที่ญี่ปุ่น
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.jetro.go.jp/en/ind_tourism/" target="_blank">
                                        การท่องเที่ยวเชิงอุตสาหกรรมที่ญี่ปุ่น
                                    </a>
                                </li>
                                <li>
                                    <a href="https://education.jnto.go.jp/en/" target="_blank">
                                        การท่องเที่ยวเชิงการศึกษาที่ญี่ปุ่น
                                    </a>
                                </li>
                            </ul>
                        </div>


                        <div class="ULTailList">
                            <h3>ข้อมูลพื้นฐาน</h3>
                            <ul>
                                <li>
                                    <a href="https://statistics.jnto.go.jp/en/" target="_blank">
                                        สถิติการท่องเที่ยวญี่ปุ่น
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.th.emb-japan.go.jp/itpr_th/visaindex.html" target="_blank">
                                        ข่าวสารเกี่ยวกับวีซ่า
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </div>--}}
                    @if($nav_groups->count() > 0)
                        <div class="ULTail clr" style="clear: both;">
                            {{--<h2>
                                <a href="{{ route('home') }}#usefullinks">เว็บไซต์ที่เป็นประโยชน์ Useful
                                    Links <span></span></a>
                            </h2>--}}
                            <h2><span>Useful Links</span>เว็บไซต์ที่เป็นประโยชน์</h2>
                            <div class="Boxlist">
                                @foreach($nav_groups as $nav_group)

                                    <div class="ULTailList">
                                        <h3>{{ $nav_group->title }}</h3>
                                        <ul>
                                            @foreach($nav_group->useful_links()->published()->get() as $nav_useful_link)
                                                <li>
                                                    <a href="{{ $nav_useful_link->url }}" target="_blank">
                                                        {{ $nav_useful_link->title }}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>

                                @endforeach
                            </div>
                        </div>
                    @endif
                    <!-- End ULTail -->

                </div>
                <!-- End FooterBox -->
            </div>
        </header>
        <!-- End Header -->
    </div>
    <!-- End menu__container -->

</nav>
<form id="logout-form" action="{{ route('logout-seller') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
<!-- End Mobile Menu -->
