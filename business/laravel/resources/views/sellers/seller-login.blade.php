@extends('sellers.app-register')

@section('content')
    <!-- HeaderRegister -->
    <div class="HeaderRegister">
        <h1>JNTO<br>タイ旅行会社向け訪日情報発信サイト</h1>
        <p>B2B Website<br>Login</p>
    </div>
    <!-- End HeaderRegister -->

    <!-- RegisterBox -->
    <div class="RegisterBox SetPassword clr">
        <form action="{{ route('seller.login') }}"
              id="login"
              method="post">
            @csrf
            <p>E-mail
                <input type="text"
                       name="user_login"
                       value="{{ old('user_login') }}"
                       placeholder="E-mail">
            </p>
            <p class="EysPasswordBox">
                <input type="password"
                       name="password"
                       id="password"
                       data-id="password_fn"
                       placeholder="Password">
                <span class="EysPasswordClose password_fn"></span>
            </p>
            <p>
                <label>
                    <input type="checkbox"
                           name="remember" {{ old('remember') ? 'checked' : '' }}> Remember password</label>
            </p>

            @if($errors->any())
                @error('password')
                    <p class="Red">パスワードが間違っています。รหัสผ่านไม่ถูกต้อง</p>
                @else
                <p class="Red FontJP">
                    入力されたメールアドレスは登録されていません。入力内容に間違いがないかご確認の上、もう一度をお試しください。上記で解決しない場合は、事務局までご連絡ください。</p>
                <p class="Red">
                    ไม่พบอีเมลในระบบ กรุณาตรวจสอบความถูกต้องและทดลองอีกครั้ง
                    <br>
                    หากไม่สำเร็จกรุณาติดต่อบริษัทตัวแทนดำเนินงาน
                </p>
                @endif
           @endif

        @if ($errors->first('no_email'))
            <!-- ValidateDisplay -->
                <div class="ValidateDisplay">
                    <div class="AddressBox">
                        <p class="FontJP">
                            {!! setting('admin.contact-jp') !!}</p>
                        <p style="margin-top:10px;">
                            {!! setting('admin.contact-th') !!}
                        </p>
                    </div>
                </div>
        @endif
        <!-- End ValidateDisplay -->

            <p class="BTNAccept">
                <input type="submit" value="Login">
            </p>

            <div class="MemberLink">
                <p class="FontJP">パスワードを忘れた方は、<a href="{{ route('forgot-password') }}">こちら</a></p>
                <p class="FontJP"><a href="{{ route('forgot-password') }}">Forgot password?</a></p>
            </div>
        </form>
    </div>
    <!-- End RegisterBox -->
@endsection
@push('script')
    <script>
        $(document).ready(function () {
            $("#login").validate({
                rules:
                    {
                        user_login: {
                            required: true ,
                            email: true
                        } ,
                        password: {
                            required: true
                        }
                    },
                messages:{
                    user_login:{
                        required: "メールアドレスを入力してください。กรุณากรอก E-mail" ,
                        email: "メールアドレスを入力してください。กรุณากรอก E-mail"
                    },
                    password:{
                        required:"パスワードを入力してください。กรุณากรอกรหัสผ่าน",
                    }
                }
            });

            $(".password_fn").click(function () {
                var input = $("input[data-id='password_fn']");
                if ($(this).hasClass('EysPasswordClose')) {
                    input.attr('type' , 'text');
                    $(".password_fn").removeClass('EysPasswordClose');
                    $(".password_fn").addClass('EysPasswordOpen');
                } else {
                    input.attr('type' , 'password');
                    $(".password_fn").removeClass('EysPasswordOpen');
                    $(".password_fn").addClass('EysPasswordClose');
                }
            });
        });

    </script>
@endpush
