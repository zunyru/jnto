<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Resources for Travel Agents</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style-mypage.css') }}">
    <link rel="icon" href="{{ asset('favicon-32x32.png') }}" type="image" sizes="32x32">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Montserrat:wght@300;400;500;600;700&family=Sarabun:wght@100;500&display=swap"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=M+PLUS+1p:wght@100;300;400;500;700;800;900&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    @stack('css')
    <style>
        .error {
            color: red;
            font-size:14px;
        }
    </style>

    @include('includes.google-tag-manager-header')

</head>
<body>

@include('includes.google-tag-manager-body')

@yield('content')

<!-- FooterRegister -->
<div class="FooterRegister clr">
    Copyright © Japan National Tourism Organization. All Rights Reserved.
</div>
<!-- End FooterRegister -->
@include('layouts.script')
@stack('script')
</body>
</html>
