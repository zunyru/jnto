@extends('sellers.app-register')

@section('content')
    <!-- HeaderRegister -->
    <div class="HeaderRegister">
        <h1>パスワードをリセットする｜Reset Password</h1>
    </div>
    <!-- End HeaderRegister -->

    <!-- RegisterStep -->
    <div class="RegisterStep clr">
        <div class="RegisterStepBC">
            <a>
			<span class="RegisterStepBC__inner">
				<span class="RegisterStepBC__title">メールアドレスの入力</span>
				<span class="RegisterStepBC__desc">Enter <br>e-mail</span>
			</span>
            </a>
            <a class="active">
			<span class="RegisterStepBC__inner">
				<span class="RegisterStepBC__title">メールの確認</span>
				<span class="RegisterStepBC__desc">Check <br>e-mail</span>
			</span>
            </a>
            <a>
			<span class="RegisterStepBC__inner">
				<span class="RegisterStepBC__title">パスワード設定</span>
				<span class="RegisterStepBC__desc">Set <br>Password</span>
			</span>
            </a>
            <a>
			<span class="RegisterStepBC__inner">
				<span class="RegisterStepBC__title">完了</span>
				<span class="RegisterStepBC__desc">Finish</span>
			</span>
            </a>
        </div>
    </div>
    <!-- End RegisterStep -->

    <!-- RegisterBox -->
    <div class="RegisterBox SetPassword">
        <p class="FontJP DarkBlue19">メールを送信しました。<br>
            メールに記載されているパスワード設定用URLをクリックし、<br>
            パスワードを設定してください。</p>
        <p class="FontJP">
            {!! nl2br(setting('admin.contact-jp')) !!}
        </p>
        <p class="DarkBlue22" style="margin-top:30px;">โปรดตรวจสอบอีเมลของท่าน<br>คลิก URL ตั้งค่ารหัสผ่านภายในอีเมล
            และตั้งรหัสผ่าน</p>
        <p>หากผ่านไป 10 นาทีแล้วไม่พบอีเมลในกล่องจดหมายเข้า กรุณาตรวจสอบโฟลเดอร์สแปม จดหมายขยะ
            หากคุณไม่ได้รับอีเมลใดๆ โปรดติดต่อ
        </p>
        <p>
            {!! nl2br(setting('admin.contact-th') ) !!}
        </p>
        <p>

        </p>
    </div>
    <!-- End RegisterBox -->
@endsection