@extends('sellers.app-register')

@section('content')
    <!-- HeaderRegister -->
    <div class="HeaderRegister clr">
        <h1>パスワードをリセットする｜Reset Password</h1>
    </div>
    <!-- End HeaderRegister -->

    <!-- RegisterStep -->
    <div class="RegisterStep clr">
        <div class="RegisterStepBC">
            <a>
			<span class="RegisterStepBC__inner">
				<span class="RegisterStepBC__title">メールアドレスの入力</span>
				<span class="RegisterStepBC__desc">Enter <br>e-mail</span>
			</span>
            </a>
            <a>
			<span class="RegisterStepBC__inner">
				<span class="RegisterStepBC__title">メールの確認</span>
				<span class="RegisterStepBC__desc">Check <br>e-mail</span>
			</span>
            </a>
            <a class="active">
			<span class="RegisterStepBC__inner">
				<span class="RegisterStepBC__title">パスワード設定</span>
				<span class="RegisterStepBC__desc">Set <br>Password</span>
			</span>
            </a>
            <a>
			<span class="RegisterStepBC__inner">
				<span class="RegisterStepBC__title">完了</span>
				<span class="RegisterStepBC__desc">Finish</span>
			</span>
            </a>
        </div>
    </div>
    <!-- End RegisterStep -->

    <!-- RegisterBox -->
    <form action="{{ route('seller.reset-password') }}"
          id="reset-password" method="POST">
        @csrf
        <div class="RegisterBox SetPassword">
            <input type="hidden" name="remember_token" value="{{ $token }}">
            <p class="EysPasswordBox">
                <input type="password" name="password"
                       id="password"
                       minlength="12"
                       data-id="password_fn"
                       placeholder="新しいパスワード | New password">
                <span class="EysPasswordClose password_fn"></span>
            </p>
            @error('password')
            <label id="password-error" class="error" for="password">{{ $message }}</label>
            @enderror
            <p class="EysPasswordBox">
                <input type="password"
                       minlength="12"
                       name="password_confirmation"
                       data-id="password_fn2"
                       placeholder="再入力 | Re-enter">
                <span class="EysPasswordClose password_fn2"></span>
            </p>
            <label id="password-error" class="error" for="password"></label>
            <p class="FontJP">
                半角英文字・半角数字・半角記号（!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~）の3種類組み合わせて設定してください。<br>12文字以上で設定してください。<br>過去に使用したパスワードは使用できません。
            </p>
            <p>กรุณาตั้งรหัสที่มี ตัวอักษรภาษาอังกฤษ ตัวเลข และอักขระพิเศษ (!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~)<br>รหัสความยาวอย่างน้อย
                12 ตัวอักษร<br>กรุณาตั้งรหัสที่คุณยังไม่เคยใช้ในเว็บไซต์นี้</p>
            <p class="BTNAccept"><input type="submit" value="保存 | Save"></p>

        </div>
    </form>
    <!-- End RegisterBox -->
@endsection
@push('script')
    <script>
        $(document).ready(function () {

            $(".password_fn").click(function () {
                var input = $("input[data-id='password_fn']");
                if ($(this).hasClass('EysPasswordClose')) {
                    input.attr('type' , 'text');
                    $(".password_fn").removeClass('EysPasswordClose');
                    $(".password_fn").addClass('EysPasswordOpen');
                } else {
                    input.attr('type' , 'password');
                    $(".password_fn").removeClass('EysPasswordOpen');
                    $(".password_fn").addClass('EysPasswordClose');
                }
            });
            $(".password_fn2").click(function () {
                var input = $("input[data-id='password_fn2']");
                if ($(this).hasClass('EysPasswordClose')) {
                    input.attr('type' , 'text');
                    $(".password_fn2").removeClass('EysPasswordClose');
                    $(".password_fn2").addClass('EysPasswordOpen');
                } else {
                    input.attr('type' , 'password');
                    $(".password_fn2").removeClass('EysPasswordOpen');
                    $(".password_fn2").addClass('EysPasswordClose');
                }
            });

            $(".ValidateDisplay").hide();

            $.validator.addMethod("strong_password" , function (value , element) {
                let password = value;
                if (!(/^(?=.*[A-Za-z])(?=.*[0-9])(?=.*[!"#$%&'()*+,\-./:;<=>?@[\]^_`{|}~])(.{12,}$)/.test(password))) {
                    return false;
                }
                return true;
            } , function (value , element) {
                let password = $(element).val();
                if (!(/^(?=.*[A-Za-z])/.test(password))) {
                    return "半角英文字を組み合わせて設定してください。 กรุณาตั้งรหัสที่มี ตัวอักษรภาษาอังกฤษ อย่างน้อย 1 ตัวอักษร";
                } else if (!(/^(?=.*[0-9])/.test(password))) {
                    return "半角数字を組み合わせて設定してください。 กรุณาตั้งรหัสที่มี ตัวเลข อย่างน้อย 1 ตัวอักษร";
                } else if (!(/^(?=.*[!"#$%&'()*+,\-./:;<=>?@[\]^_`{|}~])/.test(password))) {
                    return "半角記号を組み合わせて設定してください。กรุณาตั้งรหัสที่มี อักขระพิเศษ (!\"#$%&'()*+,-./:;<=>?@[\]^_`{|}~) อย่างน้อย 1 ตัวอักษร";
                }
                return false;
            });

            $("#reset-password").validate({
                rules:
                    {
                        password: {
                            required: true ,
                            minlength: 12 ,
                            strong_password: true ,
                        } ,
                        password_confirmation: {
                            equalTo: "#password"
                        }
                    } ,
                messages: {
                    password: {
                        required: "パスワードを設定してください。กรุณาตั้งรหัสผ่าน" ,
                        minlength: "12文字以上で設定してください。 รหัสความยาวอย่างน้อย 12 ตัวอักษร"
                    } ,
                    password_confirmation: {
                        equalTo: "同じ内容を入力してください。กรุณากรอกข้อมูลให้ตรงกัน"
                    }
                }
            });
        });
    </script>
@endpush