@extends('sellers.app-register')

@section('content')
    <!-- HeaderRegister -->
    <div class="HeaderRegister">
        <h1>新しいパスワードに設定後、ログインメールの変更は完了します</h1>
        <p class="FontTH">การเปลี่ยนอีเมลจะเสร็จสมบูรณ์ หลังจากท่านตั้งรหัสผ่านใหม่</p>
    </div>
    <!-- End HeaderRegister -->

    <!-- RegisterStep -->
    <div class="RegisterStep clr">
        <div class="RegisterStepBC">
            <a>
			<span class="RegisterStepBC__inner">
				<span class="RegisterStepBC__title">メールアドレスの入力</span>
				<span class="RegisterStepBC__desc">Enter <br>e-mail</span>
			</span>
            </a>
            <a>
			<span class="RegisterStepBC__inner">
				<span class="RegisterStepBC__title">メールの確認</span>
				<span class="RegisterStepBC__desc">Check <br>e-mail</span>
			</span>
            </a>
            <a class="active">
			<span class="RegisterStepBC__inner">
				<span class="RegisterStepBC__title">パスワード設定</span>
				<span class="RegisterStepBC__desc">Set <br>Password</span>
			</span>
            </a>
            <a>
			<span class="RegisterStepBC__inner">
				<span class="RegisterStepBC__title">完了</span>
				<span class="RegisterStepBC__desc">Finish</span>
			</span>
            </a>
        </div>
    </div>
    <!-- End RegisterStep -->

    <form action="{{ route('change-email-confirm.store') }}"
          id="register-password" method="POST">
    @csrf
    <!-- RegisterBox -->
        <div class="RegisterBox SetPassword">
            <input type="hidden" name="token" value="{{ $token }}">
            <p class="EysPasswordBox">
                <input type="password" name="password"
                       data-id="password_fn1"
                       placeholder="現在のパスワード | Password">
                <span class="EysPasswordClose password_fn1"></span>
            </p>
            @error('password')
            <label id="password-error" class="error" for="password">{{ $message }}</label>
            @enderror
            <p class="EysPasswordBox" style="margin-top:25px;">
                <input type="password" name="new_password"
                       id="new_password"
                       data-id="password_fn2"
                       placeholder="新しいパスワード | New password">
                <span class="EysPasswordClose password_fn2"></span>
            </p>
            @error('new_password')
            <label id="new_password-error" class="error" for="new_password">{{ $message }}</label>
            @enderror
            <p class="EysPasswordBox">
                <input type="password"
                       data-id="password_fn3"
                       name="new_password_confirmation" placeholder="再入力 | Re-enter">
                <span class="EysPasswordClose password_fn3"></span>
            </p>
            <p class="FontJP">
                半角英文字・半角数字・半角記号（!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~）の3種類組み合わせて設定してください。<br>12文字以上で設定してください。<br>過去に使用したパスワードは使用できません。
            </p>
            <p>กรุณาตั้งรหัสที่มี ตัวอักษรภาษาอังกฤษ ตัวเลข และอักขระพิเศษ (!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~)<br>รหัสความยาวอย่างน้อย
                12 ตัวอักษร<br>กรุณาตั้งรหัสที่คุณยังไม่เคยใช้ในเว็บไซต์นี้</p>
            <p class="BTNAccept"><input type="submit" value="保存 | Save"></p>
        </div>
    </form>
    <!-- End RegisterBox -->
@endsection
@push('script')
    <script>
        $(document).ready(function () {

            $(".password_fn1").click(function () {
                var input = $("input[data-id='password_fn1']");
                if ($(this).hasClass('EysPasswordClose')) {
                    input.attr('type' , 'text');
                    $(".password_fn1").removeClass('EysPasswordClose');
                    $(".password_fn1").addClass('EysPasswordOpen');
                } else {
                    input.attr('type' , 'password');
                    $(".password_fn1").removeClass('EysPasswordOpen');
                    $(".password_fn1").addClass('EysPasswordClose');
                }
            });
            $(".password_fn2").click(function () {
                var input = $("input[data-id='password_fn2']");
                if ($(this).hasClass('EysPasswordClose')) {
                    input.attr('type' , 'text');
                    $(".password_fn2").removeClass('EysPasswordClose');
                    $(".password_fn2").addClass('EysPasswordOpen');
                } else {
                    input.attr('type' , 'password');
                    $(".password_fn2").removeClass('EysPasswordOpen');
                    $(".password_fn2").addClass('EysPasswordClose');
                }
            });
            $(".password_fn3").click(function () {
                var input = $("input[data-id='password_fn3']");
                if ($(this).hasClass('EysPasswordClose')) {
                    input.attr('type' , 'text');
                    $(".password_fn3").removeClass('EysPasswordClose');
                    $(".password_fn3").addClass('EysPasswordOpen');
                } else {
                    input.attr('type' , 'password');
                    $(".password_fn3").removeClass('EysPasswordOpen');
                    $(".password_fn3").addClass('EysPasswordClose');
                }
            });

            $(".ValidateDisplay").hide();

            $.validator.addMethod("strong_password" , function (value , element) {
                let password = value;
                if (!(/^(?=.*[A-Za-z])(?=.*[0-9])(?=.*[!"#$%&'()*+,\-./:;<=>?@[\]^_`{|}~])(.{12,}$)/.test(password))) {
                    return false;
                }
                return true;
            } , function (value , element) {
                let password = $(element).val();
                if (!(/^(?=.*[A-Za-z])/.test(password))) {
                    return "กรุณาตั้งรหัสที่มี ตัวอักษรภาษาอังกฤษ อย่างน้อย 1 ตัวอักษร";
                } else if (!(/^(?=.*[0-9])/.test(password))) {
                    return "กรุณาตั้งรหัสที่มี ตัวเลข อย่างน้อย 1 ตัวอักษร";
                } else if (!(/^(?=.*[!"#$%&'()*+,\-./:;<=>?@[\]^_`{|}~])/.test(password))) {
                    return "กรุณาตั้งรหัสที่มี อักขระพิเศษ (!\"#$%&'()*+,-./:;<=>?@[\]^_`{|}~) อย่างน้อย 1 ตัวอักษร";
                }
                return false;
            });

            $("#register-password").validate({
                rules:
                    {
                        password: {
                            required: true ,
                            minlength: 12 ,
                            strong_password: true ,
                        } ,
                        new_password: {
                            required: true ,
                            minlength: 12 ,
                            strong_password: true ,
                        } ,
                        password_confirmation: {
                            equalTo: "#new_password"
                        }
                    } ,
                messages: {
                    password: {
                        required: "กรุณาตั้งรหัสผ่าน" ,
                        minlength: "รหัสความยาวอย่างน้อย 12 ตัวอักษร"
                    }
                }
            });
        });
    </script>
@endpush