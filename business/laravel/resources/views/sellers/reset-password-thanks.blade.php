@extends('sellers.app-register')

@section('content')
    <!-- HeaderRegister -->
    <div class="HeaderRegister">
        <h1>パスワードをリセットする｜Reset Password</h1>
    </div>
    <!-- End HeaderRegister -->

    <!-- RegisterStep -->
    <div class="RegisterStep clr">
        <div class="RegisterStepBC">
            <a>
			<span class="RegisterStepBC__inner">
				<span class="RegisterStepBC__title">メールアドレスの入力</span>
				<span class="RegisterStepBC__desc">Enter <br>e-mail</span>
			</span>
            </a>
            <a>
			<span class="RegisterStepBC__inner">
				<span class="RegisterStepBC__title">メールの確認</span>
				<span class="RegisterStepBC__desc">Check <br>e-mail</span>
			</span>
            </a>
            <a>
			<span class="RegisterStepBC__inner">
				<span class="RegisterStepBC__title">パスワード設定</span>
				<span class="RegisterStepBC__desc">Set <br>Password</span>
			</span>
            </a>
            <a class="active">
			<span class="RegisterStepBC__inner">
				<span class="RegisterStepBC__title">完了</span>
				<span class="RegisterStepBC__desc">Finish</span>
			</span>
            </a>
        </div>
    </div>
    <!-- End RegisterStep -->

    <!-- RegisterBox -->
    <div class="RegisterBox SetPassword">
        <div class="ThanksRegis">
            <div class="Left"><img src="{{ asset('assets/images/icon/icon-thanks.svg') }}"></div>
            <div class="Right">
                <p class="FontJP DarkBlue19" style="margin-bottom:0px!important;">パスワードリセットが完了しました<br>マイページに画面が切り替わります。</p>
                <p class="FontJP">画面が自動で切り替わらない場合は<a href="{{ route('seller.mypage') }}">こちら</a>をクリックしてください。</p>
                <p class="DarkBlue22" style="margin-top:20px;margin-bottom:0px!important;">รีเซ็ตรหัสผ่านเรียบร้อยแล้ว<br>ระบบจะพาท่านไปยังหน้า My page</p>
                <p>หรือกด <a href="{{ route('seller.mypage') }}">ที่นี่</a> เพื่อไปยังหน้า My page</p>
            </div>
        </div>
    </div>
    <!-- End RegisterBox -->
@endsection
@push('script')
    <script>
        window.onload = function () {
            setInterval(function () {
                window.location.href = `{{ route('seller.mypage') }}`;
            } , 4000);
        };
    </script>
@endpush