@extends('admin.app')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
@section('content')
    <div class="container mx-auto w-1/2 p-4 h-full flex flex-col justify-center">
        <div class="items-center">
            <h1 class="text-2xl text-gray-700 text-center mt-6">
                Verify OTP
            </h1>

            <form class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" method="POST"
                  id="form-otp"
                  action="{{ route('admin.otp.verify') }}">
                @csrf

                <div class="mb-4">
                    <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                        Code
                    </label>
                    <input type="hidden" name="id" value="{{ $id }}">
                    <input class="form-control focus:outline-none border-gold-300"
                           id="otp" type="text" placeholder="Code OTP"
                           name="otp"
                           value="{{ old('otp') }}"
                           autofocus>
                    @error('otp')
                    <p class="text-red-500 text-xs italic">{{ $message }}</p>
                    @enderror
                </div>
                <div class="items-center justify-center">
                    <button class="bg-gold-500 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                            type="submit">
                        Verify
                    </button>
                </div>
            </form>

        </div>

    </div>
    <p class="text-center text-gray-500 text-xs">
        Copyright © Japan National Tourism Organization. All Rights Reserved.
    </p>
@endsection
@push('scripts')
    <script>

        $("#form-otp").validate({
            rules:
                {
                    otp: {
                        required: true,
                    },
                },
        });
    </script>
@endpush
