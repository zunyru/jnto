<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>JNTO Japan Tourism New Posibilities</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css-new/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css-new/style-responsive.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css-new/style-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css-new/style-popup.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css-new/style-mypage.css') }}">
    <!-- SlickSlider -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/slick/slick.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/slick/slick-theme.css') }}"/>
    <!-- End SlickSlider -->
    <link rel="icon" href="{{ asset('favicon-32x32.png') }}" type="image" sizes="32x32">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Montserrat:wght@300;400;500;600;700&family=Sarabun:wght@100;500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@stack('css')
<!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-TCL2WC4');</script>
    <!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TCL2WC4"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="BackToTop"></div>
@php
    $seminar_repo = new \App\Repositories\SeminarRepository();
    $seminar_all = $seminar_repo->all_published();
    $grouped_seminars = $seminar_all->groupBy('year');
    $seminars = $grouped_seminars->all();

    $useful_links_repo = new \App\Repositories\UsefulLinkRepository();
    $nav_groups = $useful_links_repo->group_category();
@endphp
<!-- Mobile Menu -->
@include('frontend.includes.navbar')
<!-- End Mobile Menu -->
<div>
    @yield('message')
</div>
<!-- Footer -->
@include('frontend.includes.footer')

