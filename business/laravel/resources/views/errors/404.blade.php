@extends('errors::custom')

@section('title', __('Not Found'))
@section('code', '404')
@section('message')
    <!--Breadcrumbs-->
    <div class="Breadcrumbs clr">
        <a href="{{ route('home') }}">หน้าหลัก</a> > <span>404 page not found</span>
    </div>
    <!--Breadcrumbs-->

    <!-- PageError -->
    <div class="PageError clr">
        <p class="NumberError">404</p>
        <p class="Title">page not found!</p>
        <p>ไม่พบหน้าเว็บนี้</p>
    </div>
    <!-- End PageError -->
@endsection
