@extends('errors::custom')

@section('title', 'Session has been expired')
@section('code', '419')
@section('message')
    <!--Breadcrumbs-->
    <div class="Breadcrumbs clr">
        <a href="{{ route('home') }}">หน้าหลัก</a> > <span>Session has been expired.</span>
    </div>
    <!--Breadcrumbs-->

    <!-- PageError -->
    <div class="PageError clr">
        <p class="NumberError">419</p>
        <p class="Title">Session has been expired.</p>
        <p>เซสชั่นของคุณหมดอายุ เนื่องจากคุณใช้เวลานานเกินไป</p>
    </div>
    <!-- End PageError -->
@endsection
