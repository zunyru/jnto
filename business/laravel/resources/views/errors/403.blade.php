@extends('errors::custom')

@section('title', 'You are not permission to access this page')
@section('code', '403')
@section('message')
    <!--Breadcrumbs-->
    <div class="Breadcrumbs clr">
        <a href="{{ route('home') }}">หน้าหลัก</a> > <span>You are not permission to access this page.</span>
    </div>
    <!--Breadcrumbs-->

    <!-- PageError -->
    <div class="PageError clr">
        <p class="NumberError">403</p>
        <p class="Title">You are not permission to access this page.</p>
        <p>ไม่ได้รับสิทธิ์ในการเข้าถึงหน้านี้</p>
    </div>
    <!-- End PageError -->
@endsection
