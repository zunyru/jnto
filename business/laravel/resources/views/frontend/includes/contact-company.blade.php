@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
@endpush
@if(!empty($company))
    <div id="ContactUsPopupDisplay" class="ModelContactUs">
        <!-- ModelContactUsBox -->
        <div class="ModelContactUsBox clr">
                <span class="close">
                    <img src="{{ asset('frontend/assets/images/icon/icon-close.svg') }}">
                </span>
            <div class="ModelContactUsBoxInner clr">
                <!-- Form -->
                <form id="ContacForm" action="{{ route('contact-form-store') }}" method="post">
                    <div class="FormPopup">
                        <p class="Title">แบบฟอร์มติดต่อ</p>
                        <p>Send to: {{ $company->name_en }}</p>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="company_id" value="{{ $company->id }}">
                        <input type="hidden" name="type" value="JAPAN">
                        <hr>
                        <p>โปรดใช้ภาษาอังกฤษ หรือภาษาญี่ปุ่นในการกรอกข้อมูล</p>
                        <ul>
                            <li>
                                <label>Your Company Name <font color="red">*</font>
                                    <input type="text"
                                           placeholder="Company Name Co., Ltd."
                                           name="company_name"
                                    >
                                </label>
                            </li>
                            <li>
                                <label>Offcial website URL <font color="red">*</font>
                                    <input type="text"
                                           name="offcial_website_url"
                                    >
                                </label>
                            </li>
                            <li>
                                <label>Name <font color="red">*</font>
                                    <input type="text"
                                           name="name"
                                    >
                                </label>
                            </li>
                            <li>
                                <label>Position <font color="red">*</font>
                                    <input type="text"
                                           name="position"
                                    >
                                </label>
                            </li>
                            <li>
                                <label>E-mail <font color="red">*</font>
                                    <input type="text"
                                           name="email"
                                    >
                                </label>
                            </li>
                            <li>
                                <label>Message <font color="red">*</font>
                                    <textarea name="message"></textarea>
                                </label>
                            </li>
                            <li style="text-align: center; margin-bottom: 0px;">
                                <button class="BTNN BTNN-4">SEND
                                    <img src="{{ asset('frontend/assets/images/icon/icon-send.svg') }}">
                                </button>
                            </li>
                        </ul>
                    </div>
                </form>
                <!-- End Form -->
                <!-- THanksClass -->
                <div class="THanksClass clr">
                    <p>
                        <img src="{{ asset('frontend/assets/images/icon/icon-thank.svg') }}">
                    </p>
                    <p>ส่งคำถามของท่านเรียบร้อยแล้ว</p>
                    <p>โปรดรอการติดต่อกลับจาก <b>{{ $company->name_en }}</b></p>
                </div>
                <!-- End THanksClass -->
            </div>
            <!-- ModelContactUsBoxInner -->
        </div>
        <!-- End ModelContactUsBox -->
    </div>
@endif
@push('scripts')
    <script type="text/javascript" src="{{ asset('frontend/assets/js/jquery-2.1.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('frontend/assets/js/plugins.js') }}"></script>
    <script type="text/javascript" src="{{ asset('frontend/assets/slick/slick.min.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script type="text/javascript"
            src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script>
    <script type="text/javascript"
            src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/additional-methods.min.js"></script>
    <script type="application/javascript" src="{{ asset('js/validate-contact-form.js') }}"></script>
@endpush