@push('css')
    <link href="{{ asset('css/placeholder.css') }}?v=3" rel="stylesheet">
    <style>
        .CalendarRight {
            background-color: #eaecec;
        }

        .TitleEvent {
            padding: 5px;
            margin: 0;
            text-align: center;
            text-transform: uppercase;
            font-size: 15px;
            font-weight: 500;
        }

        .calendar * {
            box-sizing: border-box;
            font-size: 14px;
        }

        .calendar-sm {
            cursor: default;
            width: 100%;
            height: 370px;
        }

        .calendar {
            cursor: default;
            width: 100%;
            height: 350px;
        }

        .calendar-sm .c-pad-top {
            padding-top: 2%;
        }

        .calendar .c-pad-top {
            padding-top: 3%;
        }

        .c-grid {
            box-shadow: 2px 2px 5px #d5d5d5;
            height: inherit;
            border: 1px solid #d5d5d5;
        }

        .c-day {
            width: 14.28%;
            height: 13%;
            background-color: #FFF;
            float: left;
            text-align: center;
            position: relative;
        }

        .c-day-previous-month {
            width: 14.28%;
            height: 13%;
            background-color: #FFF;
            float: left;
            text-align: center;
            color: #e5e5e5;
        }

        .c-day-next-month {
            width: 14.28%;
            height: 13%;
            color: #e5e5e5;
            background-color: #FFF;
            float: left;
            text-align: center;
        }

        .c-week-day {
            width: 14.28%;
            height: 10.38%;
            background-color: #FFFF;
            color: #9ba3ac;
            float: left;
            text-align: center;
            font-weight: bold;
            padding-top: 1%;
            border-bottom: 1px solid #9ba3ac;
        }

        .c-next {
            width: 12.5%;
            height: 12%;
            padding: 2% 2% 0 2%;
            text-align: right;
            cursor: pointer;
        }

        .c-previous {
            width: 12.5%;
            height: 12%;
            padding: 2% 2% 0 2%;
            text-align: left;
            cursor: pointer;
        }

        .c-month {
            width: 75%;
            height: 12%;
            text-align: center;
        }

        .c-nav-btn-over {
            background-color: rgb(137, 163, 192) !important;
            font-weight: bold;
        }

        .c-today {
            background-color: #D8EAF1;
        }

        .c-event {
            background-color: rgb(166, 166, 166);
            color: white;
            font-weight: bold;
            cursor: pointer;
        }

        .c-grid {
            float: left;
            width: 100%;
        }

        .c-event-grid {
            margin-left: 1px;
            height: 150px;
            min-height: 150px;
            /*height: inherit;*/
            width: 100%;
            float: left;
            /*box-shadow: 2px 2px 5px #888888;*/
            margin-bottom: 10px;
        }

        .c-grid-title {
            font-weight: bold;
            float: left;
            background-color: #FFF;
        }

        .c-event-title {
            width: auto;
            text-align: left;
            color: gray;
            font-size: 12px;
        }

        .c-event-body {
            background-color: #EFF4F9;
            height: 88.1%;
        }

        .c-event-list {
            padding: 7 0 0 0;
            overflow: auto;
            height: 95%;
        }

        .c-event-item > .title {
            font-weight: bold;
        }

        .c-event-item > div {
            /*text-overflow: ellipsis;
            width: inherit;*/
            /*overflow: hidden;
            white-space: nowrap;*/
        }

        .c-event-item > div:nth-child(1) {
            width: 80%;
        }

        .c-event-item > div:nth-child(1) div:nth-child(1) {
            font-size: 13px;
            padding-right: 10px;
        }

        .c-event-item > div:nth-child(1) div:nth-child(2) {
            color: #666;
            font-size: 13px;
            font-weight: 200;
        }

        .c-event-item > div:nth-child(2) {
            width: 20%;
        }

        .c-event-over {
            background-color: lightgray;
            font-weight: bold;
            color: black;
        }

        .c-event-over > .description {
            font-weight: normal;
        }

        .event-info, .event-partner {
            cursor: pointer;
        }

        .c-day.c-pad-top.event-info::before {
            content: '';
            width: 100%;
            height: 7px;
            background: #ea9c4e;
            position: absolute;
            bottom: 0;
            left: 0;

        }

        .c-day.c-pad-top.event-partner::after {
            content: '';
            width: 100%;
            height: 7px;
            background: #558ca4;
            position: absolute;
            bottom: 7px;
            left: 0;
            cursor: pointer;
        }

        .justify-between {
            justify-content: space-between;
        }

        .flex {
            display: flex;
        }

        .align-items-center {
            align-items: center;
        }

        .box-partner {
            width: 15px;
            height: 7px;
            background: #418da5;
        }

        .box-jnto {
            width: 15px;
            height: 7px;
            background: #ea9c4e;
        }

        .m-left {
            margin-left: 4px;
        }

        .m-right {
            margin-right: 4px;
        }

        .font-sm {
            font-size: 11px;
        }

        .p-10 {
            padding: 10px;
        }

        .scroll {
            overflow-y: scroll;
            height: 75%;
        }

        .c-event-item {
            margin: 0 10px;
            background: white;
            box-shadow: 2px 2px 5px #d5d5d5;
            align-items: center;
        }

        .line-event-partner, .line-event-jnto {
            position: relative;
        }

        .line-event-partner::before {
            content: '';
            width: 6px;
            height: 100%;
            position: absolute;
            background: #418da5;
        }

        .line-event-jnto::before {
            content: '';
            width: 6px;
            height: 100%;
            position: absolute;
            background: #ea9c4e;
        }

        a.view-jnto {
            background: #ea9c4e;
            padding: 0 10px;
            color: white;
            margin-right: 0px;
            border-radius: 100px;
            border: 1px solid #fff;
            box-shadow: 2px 2px 10px rgba(0, 0, 0, 0.3);
            font-size: 13px;
        }

        a.view-partner {
            border-radius: 9999px;
            background: #418da5;
            padding: 0 10px;
            color: white;
            margin-right: 0px;
            border-radius: 100px;
            border: 1px solid #fff;
            box-shadow: 2px 2px 10px rgba(0, 0, 0, 0.3);
            font-size: 13px;
        }

        .m-14 {
            margin-left: 14px;
        }

        .space-y-4 > :not([hidden]) ~ :not([hidden]) {
            --tw-space-y-reverse: 0;
            margin-top: 6px;
            margin-bottom: 6px;
        }

        .pt-4 {
            padding-top: 4px;
        }

        .pb-4 {
            padding-bottom: 4px;
        }

        a.box-view-all {
            float: right;
            margin-right: 10px;
            margin-bottom: 10px;
            font-family: 'Caveat', cursive;
            background-color: #663300;
            box-shadow: 2px 2px 10px rgb(0 0 0 / 50%);
            font-size: 15px;
            font-weight: 500;
            padding: 0px 10px 2px 10px;
            color: #fff;
            border-radius: 100px;
            border: 1px solid #fff;
        }
    </style>
@endpush
<div class="CalendarRight">
    <p class="TitleEvent">Event Schedule</p>
    <div id="calendar"></div>
    <div class="box-view-all">
        <div>
            <a class="box-view-all" href="javascript:void(0)">
                View All
            </a>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        (function ($) {

            var eCalendar = function (options, object) {
                // Initializing global variables
                var adDay = new Date().getDate();
                var adMonth = new Date().getMonth();
                var adYear = new Date().getFullYear();
                var dDay = adDay;
                var dMonth = adMonth;
                var dYear = adYear;
                var instance = object;
                var isDate = new Date().toISOString().slice(0, 10);

                var settings = $.extend({}, $.fn.eCalendar.defaults, options);

                function lpad(value, length, pad) {
                    if (typeof pad == 'undefined') {
                        pad = '0';
                    }
                    var p;
                    for (var i = 0; i < length; i++) {
                        p += pad;
                    }
                    return (p + value).slice(-length);
                }

                var mouseOver = function () {
                    $(this).addClass('c-nav-btn-over');
                };
                var mouseLeave = function () {
                    $(this).removeClass('c-nav-btn-over');
                };

                var nextMonth = function () {
                    if (dMonth < 11) {
                        dMonth++;
                    } else {
                        dMonth = 0;
                        dYear++;
                    }
                    var d = new Date(dYear, dMonth + 1, 1);
                    var date = d.toISOString().slice(0, 10);
                    isDate = date;
                    adMonth = dMonth;
                    adYear = dYear;
                    print();
                    callCalendar(date, true);
                };
                var previousMonth = function () {
                    if (dMonth > 0) {
                        dMonth--;
                    } else {
                        dMonth = 11;
                        dYear--;
                    }
                    var d = new Date(dYear, dMonth + 1, 1);
                    var date = d.toISOString().slice(0, 10);
                    isDate = date;
                    adMonth = dMonth;
                    adYear = dYear;
                    print();
                    callCalendar(date, true);
                };


                function print() {
                    var dWeekDayOfMonthStart = new Date(dYear, dMonth, 1).getDay();
                    var dLastDayOfMonth = new Date(dYear, dMonth + 1, 0).getDate();

                    var dLastDayOfPreviousMonth = new Date(dYear, dMonth + 1, 0).getDate() - dWeekDayOfMonthStart + 1;

                    var cBody = $('<div/>').addClass('c-grid');
                    var cEvents = $('<div/>').addClass('c-event-grid');

                    var cNext = $('<div/>').addClass('c-next c-grid-title c-pad-top');
                    var cMonth = $('<div/>').addClass('c-month c-grid-title c-pad-top');
                    var cPrevious = $('<div/>').addClass('c-previous c-grid-title c-pad-top');
                    cPrevious.html(settings.textArrows.previous);
                    cMonth.html(settings.months[dMonth] + ' ' + dYear);
                    cNext.html(settings.textArrows.next);

                    cPrevious.on('mouseover', mouseOver).on('mouseleave', mouseLeave).on('click', previousMonth);
                    cNext.on('mouseover', mouseOver).on('mouseleave', mouseLeave).on('click', nextMonth);

                    cBody.append(cPrevious);
                    cBody.append(cMonth);
                    cBody.append(cNext);

                    for (var i = 0; i < settings.weekDays.length; i++) {
                        var cWeekDay = $('<div/>').addClass('c-week-day c-pad-top');
                        cWeekDay.html(settings.weekDays[i]);
                        cBody.append(cWeekDay);
                    }
                    var day = 1;
                    var dayOfNextMonth = 1;
                    for (var i = 0; i < 42; i++) {
                        var cDay = $('<div/>');
                        if (i < dWeekDayOfMonthStart) {
                            cDay.addClass('c-day-previous-month c-pad-top');
                            cDay.html(dLastDayOfPreviousMonth++);
                        } else if (day <= dLastDayOfMonth) {
                            cDay.addClass('c-day c-pad-top');
                            var _date = new Date(Date.UTC(adYear, adMonth, day)).toISOString().slice(0, 10);


                            if (adMonth == dMonth && adYear == dYear) {
                                const event_infos = @json($event_info_date);
                                if ($.inArray(_date, event_infos) !== -1) {
                                    cDay.addClass('event');
                                    cDay.addClass('event-info');
                                    cDay.attr('data-date', _date);
                                }
                                const events_partner = @json($event_partner_date);
                                if ($.inArray(_date, events_partner) !== -1) {
                                    cDay.addClass('event');
                                    cDay.addClass('event-partner');
                                    cDay.attr('data-date', _date);
                                }
                            }

                            /*
                            if (events.includes(_date)) {
                                cDay.addClass('event-partner');
                            }*/
                            /*if (day == dDay && adMonth == dMonth && adYear == dYear) {
                                cDay.addClass('c-today');
                            }*/

                            cDay.html(day++);
                        } else {
                            cDay.addClass('c-day-next-month c-pad-top');
                            cDay.html(dayOfNextMonth++);
                        }
                        cBody.append(cDay);
                    }
                    var eventList = $('<div/>').addClass('c-event-list');

                    $(instance).addClass('calendar');
                    $(instance).html(cBody).append(cEvents);


                    $('.event').on('click', function () {
                        let date = $(this).attr('data-date');
                        callCalendar(date);
                    });

                    $('.box-view-all').on('click', function () {
                        callCalendar(isDate, true);
                    });
                }

                return print();
            }

            $.fn.eCalendar = function (oInit) {
                return this.each(function () {
                    return eCalendar(oInit, $(this));
                });
            };

        }(jQuery));

        $(document).ready(function () {
            $('#calendar').eCalendar({
                weekDays: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                textArrows: {previous: '<', next: '>'},
                eventTitle: 'Events',
                url: '',
                events: []
            });

            callCalendar(null, true);

        });

        function callCalendar(date = null, next = false) {
            $.ajax({
                method: "POST",
                beforeSend: function () {
                    $('.c-event-grid').html('');
                    $('.c-event-grid').append("<div class='box-loader' style='margin-top: 10%;'><div class='loader'> <span></span> <span></span> <span></span> <span></span> <span></span></div> </div>");
                },
                data: {
                    "_token": "{{ csrf_token() }}",
                    "date": date,
                    "next": next
                },
                url: `{{ route('get-event-list') }}`,
            }).done(function (html) {
                $('.c-event-grid').html('');
                $('.c-event-grid').append(html);
            }).fail(function (msg) {
                console.log(msg);
            });
        }
    </script>
@endpush