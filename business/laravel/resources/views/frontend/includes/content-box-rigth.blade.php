@if(!empty($company))
    <div class="ContentBoxRight">
        <!-- SupportMenuLink -->
        <!--<div class="SupportMenuLink clr"><a href="#GotoSupportMenu"><img src="assets/images/support-menu-banner.png"></a></div>-->
        <!-- SupportMenuMobile -->
        @if(!empty($company->support_menu->support_menu1)
          || !empty($company->support_menu->support_menu2)
          || !empty($company->support_menu->support_menu3)
          || !empty($company->support_menu->support_menu4)
          || !empty($company->support_menu->support_menu5))
            <div class="SupportMenuPCPartner clr">
                <div class="SupportMenuPCBoxPartner BTNSupportMenuPCPartner">
                    <a href="{{ route('seller-info-detail',$company->ref_id) }}#GotoSupportMenu"
                       class="BTNSM BTNSM-4">
                        <span>มีโปรแกรมสนับสนุน</span>
                    </a>
                </div>
            </div>
        @endif
        <!-- End SupportMenuMobile -->
        <!-- ENd SupportMenuLink -->
        @if(empty($page))
            <!-- CategoryList -->
            <div class="CategoryList">
                <h2>Category</h2>
                <p class="ProvinceDisplay">{{ $provinces_implode }}</p>
                {{--@if(!empty($season_implode))
                    <p class="SeasonDisplay">{{ $season_implode }}</p>
                @endif--}}
                @if(!empty($special_features_implode))
                    <p class="SpecialFeaturesDisplay">{!! $special_features_implode !!}
                @endif
                @if(!empty($type_of_travels_implode))
                    <p class="TypeOfTravelDisplay">{!! $type_of_travels_implode !!}</p>
                @endif
            </div>
            <!-- End CategoryList -->
        @endif

        @if(!empty($company))
            <!-- CompanyInfoList -->
            <div class="CompanyInfoList">
                @if(!empty($company->logo))
                    <p class="Logo">
                        <img src="{{ asset($company->logo) }}">
                    </p>
                @endif
                @if($company->is_partner)
                    @if(Auth::guard('web')->check() || @$preview)
                        <p class="JNTOPartner">
                            <span>JNTO Partners</span>
                        </p>
                    @endif
                @endif
                <h2>{{ $company->name_en }}</h2>
                <p class="BusinessType">{{ $business_types_implode }}</p>

                @if(!empty($company->registered) && $company->registered != '3')
                    {{--<p class="SellerIDDisplay">
                        {{ $company->registered_text }}
                    </p>--}}
                    <p class="SellerIDDisplayText"><strong>Japan travel agency registration number :</strong> <br>
                        {!! $company->licens_no !!}
                    </p>
                @endif

                @if(!empty($company->address))
                    <p class="AddressDisplay">
                        @if(!empty($company->google_map_url))
                            <a href="{{ $company->google_map_url }}"
                               target="_blank">
                                @endif
                                {!! $company->address !!}
                                @if(!empty($company->google_map_url))
                            </a>
                        @endif
                    </p>
                @endif
                @if(!empty($company->official_website_url))
                    <p class="WebsiteDisplay">
                        <a href="{{ $company->official_website_url }}"
                           target="_blank">{{ $company->official_website_url }}
                        </a>
                    </p>
                @endif
                <p class="SocialNetwork">

                        <span class="{{ empty($company->facebook_url) ? 'InActiveSN' : '' }}">
                        @if(!empty($company->facebook_url))
                                <a href="{{ $company->facebook_url }}"
                                   target="_blank">
                                <img src="{{ asset('frontend/assets/images/icon/icon-facebook.svg') }}">
                                </a>
                            @else
                                <img src="{{ asset('frontend/assets/images/icon/icon-facebook.svg') }}">
                            @endif
                        </span>

                    <span class="{{ empty($company->instagram_url) ? 'InActiveSN' : '' }}">
                        @if(!empty($company->instagram_url))
                            <a href="{{ $company->instagram_url }}"
                               target="_blank">
                            <img src="{{ asset('frontend/assets/images/icon/icon-instagram.svg') }}">
                         </a>
                        @else
                            <img src="{{ asset('frontend/assets/images/icon/icon-instagram.svg') }}">
                        @endif
                        </span>

                    <span class="{{ empty($company->youtube_channel_url) ? 'InActiveSN' : '' }}">
                        @if(!empty($company->youtube_channel_url))
                            <a href="{{ $company->youtube_channel_url }}"
                               target="_blank">
                            <img src="{{ asset('frontend/assets/images/icon/icon-youtube.svg') }}">
                        </a>
                        @else
                            <img src="{{ asset('frontend/assets/images/icon/icon-youtube.svg') }}">
                        @endif
                        </span>

                    <span class="{{ empty($company->twitter) ? 'InActiveSN' : '' }}">
                        @if(!empty($company->twitter))
                            <a href="{{ $company->twitter }}"
                               target="_blank">
                            <img src="{{ asset('frontend/assets/images/icon/icon-twitter.svg') }}">
                        </a>
                        @else
                            <img src="{{ asset('frontend/assets/images/icon/icon-twitter.svg') }}">
                        @endif
                        </span>

                    <span class="{{ empty($company->line) ? 'InActiveSN' : '' }}">
                        @if(!empty($company->line))
                            <a href="{{ $company->line }}" target="_blank">
                            <img src="{{ asset('frontend/assets/images/icon/icon-line.svg') }}">
                        </a>
                        @else
                            <img src="{{ asset('frontend/assets/images/icon/icon-line.svg') }}">
                        @endif
                        </span>
                </p>
                @if(Auth::guard('web')->check() || @$preview)
                    <p class="DepartmentDisplay">{{ $company->department }}</p>
                    @if(!empty($company->contact_person_name))
                        <p class="ContactPersonDisplay">{{ $company->contact_person_name }}</p>
                    @endif
                    @if(!empty($company->email))
                        <p class="EmailDisplay">{{ $company->email }}</p>
                    @endif
                    @if(!empty($company->tel))
                        <p class="TelDisplay">{{ $company->tel }}</p>
                    @endif
                    @if(!empty($languages_implode))
                        <p class="LanguageDisplay">ทักษะภาษา: {{ $languages_implode }}</p>
                    @endif
                    {{--<div class="BTNContact">
                        <a href="javascript:void(0);"
                           id="ContactUsPopup"
                           class="BTN BTN-4">
                            Contact us >
                        </a>
                    </div>--}}
                @endif
                @if(Auth::guard('web')->check() || @$preview)
                    @if($company->is_branch_thai == 1)
                        <div class="CompanyInfoListTH">Thai Branch/Representative</div>
                        <h2>{{ $company->branch_company_name_en }}
                            @if(!empty($company->branch_company_name_th))

                            @endif
                        </h2>
                        @if(!empty($company->branch_address ))
                            <p class="AddressDisplay">
                                @if(!empty($company->branch_google_map_url))
                                    <a href="{{ $company->branch_google_map_url }}" target="_blank">
                                        @endif
                                        {!! $company->branch_address !!}
                                        @if(!empty($company->branch_google_map_url))
                                    </a>
                                @endif
                            </p>
                        @endif
                        @if(!empty($company->branch_thai_website_url))
                            <p class="WebsiteDisplay">
                                <a href="{{ $company->branch_thai_website_url }}" target="_blank">
                                    {{ $company->branch_thai_website_url }}
                                </a>
                            </p>
                        @endif
                        @if(!empty($company->branch_department))
                            <p class="DepartmentDisplay">{{ $company->branch_department }}</p>
                        @endif
                        @if(!empty($company->branch_contact_person_name))
                            <p class="ContactPersonDisplay">{{ $company->branch_contact_person_name }}</p>
                        @endif
                        <p class="EmailDisplay">{{ $company->branch_email }}</p>
                        @if(!empty($company->branch_tel))
                            <p class="TelDisplay">{{ $company->branch_tel }}</p>
                        @endif
                        @if(!empty($languages_branch_implode))
                            <p class="LanguageDisplay">ทักษะภาษา: {{ $languages_branch_implode }}</p>
                        @endif
                        {{--<div class="BTNContact">
                            <a href="javascript:void(0);" id="ContactUsBranchPopup" class="BTN BTN-4">
                                Contact us >
                            </a>
                        </div>--}}
                    @endif
                @endif

                {{--@if(!empty($company->licens_no))
                    <!-- LicensNo -->
                    <p class="LicensNo"><span>Licens no. : </span>{{ $company->licens_no }}</p>
                    <!-- End LicensNo -->
                @endif--}}

            </div>
            <!-- End CompanyInfoList -->

            <!-- NewsBannerRight -->
            @if($company->is_show_banner == 1 && empty($page))
                <div class="NewsBannerRight">
                    <div class="BTNNews">
                        <a href="{{ $company->url_banner }}" class="BTNN BTNN-4">
                            <img src="{{ asset('frontend/assets/images/JNTO-Partner-News-Banner.png') }}">
                        </a>
                    </div>
                </div>
            @endif
            @if(!empty($page))
                <div class="NewsBannerRight">
                    <div class="BTNNews">
                        <a href="{{ route('seller-info-detail',$company->ref_id) }}" class="BTNN BTNN-4"
                        >
                            <img src="{{ asset('frontend/assets/images/JNTO-Partner-News.png') }}">
                        </a>
                    </div>
                </div>
            @endif
        @endif
        <!-- End NewsBannerRight -->
    </div>
@endif
