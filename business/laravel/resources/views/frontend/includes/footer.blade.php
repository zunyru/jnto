@if(!is_null(Route::getCurrentRoute()))
    @if(Route::getCurrentRoute()->getName() != 'privacy-policy')
        {{--@include('cookieConsent::index')--}}
        @include('cookie-consent::index')
    @endif
@endif
<!-- Footer -->
<footer>
    <!-- Footer -->
    <div class="Footer">
        <!-- BackToTop -->
        <div class="BackToTop"><a href="#BackToTop">
                <img src="{{ asset('frontend/assets/images/icon/icon-back-to-top.svg') }}"></a></div>
        <!-- End BackToTop -->

        <!-- Phase2 FooterBox -->
        <div class="FooterBox clr">
            <h2><a href="{{ route('home') }}">B2B Website</a></h2>

            <div class="FooterBoxListAll">

                <div class="FooterBoxLeft">
                    <p><span>Discover new partners!</span>ค้นหาพาร์ทเนอร์ฝ่ายญี่ปุ่น</a></p>
                    @if((Auth::guard('seller')->check() && Auth::guard('seller')->user()->privacy != 'private') || Auth::guard('web')->check())
                        <p class="Links"><a href="{{ route('seller-info.search') }}">> ค้นหา</a></p>
                    @endif
                    @if((Auth::guard('seller')->check() && Auth::guard('seller')->user()->privacy != 'private') || Auth::guard('web')->check())
                        <p class="Links"><a href="{{ route('topic.all') }}">> อีเวนต์ ข่าวสาร</a></p>
                    @endif

                </div>

                <div class="FooterBoxRight">
                    <p><span>News</span>ข่าวสาร</a></p>
                    <p class="Links"><a href="{{ route('news-jnto.all') }}">> ข่าวสารจาก JNTO</a></p>
                    <p class="Links"><a href="{{ route('news-jnto-partner.all') }}">> ข่าวสารจากองค์กรท่องเที่ยว</a></p>
                </div>

                {{--@if(count($seminars) > 0)--}}
                <div class="FooterBoxRight clr">
                    <p><a href="{{ route('seminar-list') }}"><span>Online Seminar</span>สัมมนาออนไลน์</a></p>
                    {{--@foreach($seminars as $key => $item_seminars)
                        <div class="MBFont"><label class="YearsBox">{{ $key }}</label>|
                            @foreach($item_seminars as $item_seminar)
                                <a href="{{ route('seminar.show',$item_seminar->ref_id) }}">
                                    {{ $item_seminar->list ?? "ครั้งที่ $item_seminar->id" }}
                                </a>
                            @endforeach
                        </div>
                    @endforeach
                </div>--}}
                    {{--@endif--}}

                </div>
            </div>
            <!-- Phase2 End FooterBox -->

            <!-- ULTail -->
            @if($nav_groups->count() > 0)
                <div class="ULTail clr">
                    <div class="ULTail clr">
                        <h2><span>Useful Links</span>เว็บไซต์ที่เป็นประโยชน์</h2>
                        <div class="Boxlist">
                            @foreach($nav_groups as $nav_group)

                                <div class="ULTailList">
                                    <h3>{{ $nav_group->title }}</h3>
                                    <ul>
                                        @foreach($nav_group->useful_links()->published()->get() as $nav_useful_link)
                                            <li>
                                                <a href="{{ $nav_useful_link->url }}" target="_blank">
                                                    {{ $nav_useful_link->title }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>

                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
            <!-- End ULTail -->

            {{--<div class="JNTOEmail"><a href="mailto:vj_business_th@mediator.co.th">vj_business_th@mediator.co.th</a></div>
            <p class="Copyright">Copyright © Japan National Tourism Organization. All Rights Reserved.</p>--}}
            @if(!empty(setting('admin.footer-text')))
                <div class="FooterTextBox">{!! setting('admin.footer-text') !!}</div>
            @endif
        </div>
        <!-- End Footer -->


</footer>
<!-- End Footer -->

<!-- TailLogoandLink -->
<div class="TailLogoandLink clr">
    <!-- TailLogoandLinkBox -->
    <div class="TailLogoandLinkBox">
        <div class="LogoTail"><a href="https://www.jnto.or.th/" target="_blank">
                <img
                    src="{{ asset('frontend/assets/images/logo-tail.png') }}">{{--<span>องค์การส่งเสริมการท่องเที่ยวแห่งประเทศญี่ปุ่น</span>--}}
            </a>
        </div>

        <div class="PrivacyTailBox">
            <div class="PrivacyTailBox1"><a href="{{ setting('admin.privacy-policy-url-th') }}" target="_blank">Privacy
                    Policy</a></div>
            <div class="PrivacyTailBox2"><a href="{{ setting('admin.cookie-policy-url') }}" target="_blank">Cookie
                    Policy</a></div>
        </div>

        <div class="JNTOEmail2"><a href="mailto:{{ setting('admin.email') }}">{{ setting('admin.email') }}</a></div>

        <div class="JNTOWebsiteH">
            <span class="JNTOWebsite"><a href="https://www.jnto.or.th" target="_blank">www.jnto.or.th</a></span><br>
            <span class="JNTOWebsite"><a href="https://www.japan.travel/th/th/"
                                         target="_blank">www.japan.travel/th/th/</a></span>
        </div>
        <div class="JNTOSocial">
            <a href="https://www.facebook.com/visitjapanth/" target="_blank"><img
                    src="{{ asset('frontend/assets/images/icon/icon-facebook.svg') }}"></a>
            <a href="https://www.instagram.com/visitjapanth/" target="_blank"><img
                    src="{{ asset('frontend/assets/images/icon/icon-instagram.svg') }}"></a>
            <a href="https://bddy.me/3oZuO3Y" target="_blank"><img
                    src="{{ asset('frontend/assets/images/icon/icon-line.svg') }}"></a>
        </div>
    </div>
    <!-- End TailLogoandLinkBox -->
</div>
<!-- End TailLogoandLink -->

<!-- CookiePopup -->

<!-- End CookiePopup -->

<script type="text/javascript" src="{{ asset('frontend/assets/js/jquery-2.1.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/assets/js/plugins.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/assets/slick/slick.min.js') }}"></script>
<script type="text/javascript">
    $(function () {

        $('.SectionShow').hide();
        $('.SectionClick').on('click', function () {
            $('.SectionShow').not($($(this).attr('href'))).hide();
            $($(this).attr('href')).fadeToggle(200);
        });

        $('.Tab1').on('click', function () {
            $('.JNTO_News').show();
            $('.JNTO_Partner').hide();
            $('.Tab1').addClass("Active");
            $('.Tab2').removeClass("Active");
        });
        $('.Tab2').on('click', function () {
            $('.JNTO_News').hide();
            $('.JNTO_Partner').show();
            $('.Tab1').removeClass("Active");
            $('.Tab2').addClass("Active");
        });


        $('#AcceptCookie').on('click', function () {
            $('.CookiePopup').hide();
        });

    });

</script>
@stack('scripts')
</body>
</html>
