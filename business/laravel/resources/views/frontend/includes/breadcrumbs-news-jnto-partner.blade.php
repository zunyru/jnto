<!--Breadcrumbs-->
<div class="Breadcrumbs clr">
    <a href="{{ route('home') }}">หน้าหลัก</a> > <a href="{{ route('news-jnto-partner.all') }}">ข่าวสารจากองค์กรท่องเที่ยวต่างๆ</a>
    @if(!empty($detail))
    > <span>{{ @$news->title }}</span>
    @endif
</div>
<!--Breadcrumbs-->