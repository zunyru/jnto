<!--Breadcrumbs-->
<div class="Breadcrumbs clr">
    <a href="{{ route('home') }}">หน้าหลัก</a> > <a href="{{ route('news-jnto.all') }}">ข่าวสารจาก JNTO</a>
    @if(!empty($detail))
    > <span>{{ @$news->title }}</span>
    @endif
</div>
<!--Breadcrumbs-->