@if(!empty($company))
    <div id="ContactUsBranchPopupDisplay" class="ModelContactUs">
        <!-- ModelContactUsBox -->
        <div class="ModelContactUsBox clr">
                <span class="closeBranch">
                    <img src="{{ asset('frontend/assets/images/icon/icon-close.svg') }}">
                </span>

            <div class="ModelContactUsBoxInner clr">
                <!-- Form -->
                <form id="ContacFormThai" action="{{ route('contact-form-store') }}" method="post">
                    <div class="FormPopup">
                        <p class="Title">แบบฟอร์มติดต่อ</p>
                        <p>ส่งอีเมล์ถึง : {{ $company->branch_company_name_en }}</p>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="company_id" value="{{ $company->id }}">
                        <input type="hidden" name="type" value="THAI">
                        <hr>
                        <ul>
                            <li>
                                <label>ชื่อบริษัทของท่าน <font color="red">*</font>
                                    <input type="text"
                                           placeholder="Company Name Co., Ltd."
                                           name="company_name"
                                    >
                                </label>
                            </li>
                            <li>
                                <label>Offcial website URL <font color="red">*</font>
                                    <input type="text"
                                           name="offcial_website_url"
                                    >
                                </label>
                            </li>
                            <li>
                                <label>ชื่อและนามสกุล <font color="red">*</font>
                                    <input type="text"
                                           name="name"
                                    >
                                </label>
                            </li>
                            <li>
                                <label>ตำแหน่ง <font color="red">*</font>
                                    <input type="text"
                                           name="position"
                                    >
                                </label>
                            </li>
                            <li>
                                <label>E-mail <font color="red">*</font>
                                    <input type="text"
                                           name="email"
                                    >
                                </label>
                            </li>
                            <li>
                                <label>ข้อความ <font color="red">*</font>
                                    <textarea name="message"></textarea>
                                </label>
                            </li>
                            <li style="text-align: center; margin-bottom: 0px;">
                                <button class="BTNN BTNN-4">SEND
                                    <img src="{{ asset('frontend/assets/images/icon/icon-send.svg') }}">
                                </button>
                            </li>
                        </ul>
                    </div>
                </form>
                <!-- End Form -->
                <!-- THanksClass -->
                <div class="THanksClassBranch clr">
                    <p>
                        <img src="{{ asset('frontend/assets/images/icon/icon-thank.svg') }}">
                    </p>
                    <p>ส่งคำถามของท่านเรียบร้อยแล้ว</p>
                    <p>โปรดรอการติดต่อกลับจาก <b>{{ $company->branch_company_name_en }}</b></p>
                </div>
                <!-- End THanksClass -->
            </div>
            <!-- ModelContactUsBoxInner -->
        </div>
        <!-- End ModelContactUsBox -->
    </div>
@endif