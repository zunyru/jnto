@push('css')
    <link rel="stylesheet" href="{{ asset('assets/jquery-ui/jquery-ui.min.css') }}">
    <style>
        #datepicker .ui-datepicker td {
            padding: 0;
        }

        #datepicker .ui-datepicker-inline {
            width: 100%;
        }

        #datepicker .ui-widget-header {
            background: #fff;
            border: none;
        }

        #datepicker .ui-datepicker-calendar thead tr:first-child {
            border-bottom: 1px solid #e2e2e2;
        }

        #datepicker .ui-state-hover, .ui-widget-content .ui-state-hover, .ui-widget-header .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus, .ui-widget-header .ui-state-focus, .ui-button:hover, .ui-button:focus {
            border: none;
            background: #ccc;
            color: #2b2b2b;
        }

        #datepicker .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default, .ui-button, html .ui-button.ui-state-disabled:hover, html .ui-button.ui-state-disabled:active {
            border: none;
            color: #000000;
            padding: 10px 5px;
            text-align: center;
        }

        #datepicker .ui-state-default, .ui-widget-content .ui-state-default {
            background: #cc7046;
            color: #FFF;
        }

        #datepicker .ui-state-disabled .ui-state-default, .ui-widget-content .ui-state-disabled {
            opacity: 1;
            border: none;
            background: #FFF;
            color: #000;
        }

        #datepicker .ui-state-default:first-child,
        #datepicker .ui-widget-content .ui-state-default:first-child
        {
            border-radius-topleft: 40px;
        }
    </style>
@endpush
<div class="CalendarRight">
    <div id="datepicker"></div>
</div>
@push('scripts')
    <script src="{{ asset('assets/jquery-ui/jquery-ui.min.js') }}"></script>
    <script>
        $(function () {
            var holidays = @json($holidays);
            $("#datepicker").datepicker({
                beforeShowDay: function (date) {
                    var datestring = jQuery.datepicker.formatDate('yy-mm-dd', date);
                    return [holidays.indexOf(datestring) == -1]
                },
                beforeShow: function(input, inst) {
                    $('#datepicker').attr("inputId",this.id);
                },
                dateFormat: "yy-mm-dd",
                @if(!empty($event_mount_start))
                minDate: `{{ $event_mount_start }}`,
                maxDate: `{{ $event_mount_end }}`,
                @endif
            });
        });
    </script>
@endpush