@if(sizeof($others) > 0)
    <!-- RelatedCompanyInfo -->
    <div class="RelatedCompanyInfo clr">
        <h2>บริษัทอื่นๆ</h2>
        <ul class="RelatedSlideBox">
            @foreach($others as $item)
                <li>
                    <a href="{{ route('seller-info-detail',$item->ref_id) }}" target="_blank">
                        <div class="PictureDisplayRe">
                            @if($item->is_partner && Auth::guard('web')->check())
                                <span>JNTO Partners</span>
                            @endif
                            @if(!empty($item->service_information->main_photo))
                                <div class="inner"
                                     style="background-image:url('{{ asset($item->service_information->main_photo) }}');background-size: cover; background-position: center center;"></div>
                        </div>
                        @endif
                        <div class="RightCaptions">
                            @if(!empty($item->name_en))
                                <p class="CompanyName">{{ $item->name_en }}</p>
                            @endif
                            @if(!empty($item->service_information->title))
                                <p class="Title">{{ $item->service_information->title}}</p>
                            @endif
                            @if(!empty($provinces_other_implode))
                                <p class="ProvinceDisplay">{{ $provinces_other_implode }}</p>
                            @endif
                            @if(!empty($item->support_menu->support_menu1)
                             || !empty($item->support_menu->support_menu2)
                             || !empty($item->support_menu->support_menu3)
                             || !empty($item->support_menu->support_menu4)
                             || !empty($item->support_menu->support_menu5))
                                <p class="SupportMenuDisplay">มีโปรแกรมสนับสนุน</p>
                            @endif
                            @if(!empty($item->topic))
                                @if($item->topic->category == 'event')
                                    <p class="ToppicEvent">มีอีเวนท์ใหม่</p>
                                @else
                                    <p class="ToppicNews">ข่าวสาร</p>
                                @endif
                            @endif
                        </div>
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
    <!-- RelatedCompanyInfo -->
@endif
