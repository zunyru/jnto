@if(Auth::guard('seller')->check())
    <nav class="mobile-menu MenuSeller">
        @else
            <nav class="mobile-menu">
                @endif

                @if(Auth::guard('web')->check())
                    <di class="AllMargin">@endif
                        <!-- HeaderLogo -->
                        <div class="HeaderLogo">
                            <a href="{{ route('home') }}">
                                <img src="{{ asset('frontend/assets/images/logo.png') }}">
                                <span>JNTO B2B Website</span>
                            </a>
                        </div>
                        <!-- End HeaderLogo -->

                        <!-- Phase2 MemberHeader -->
                        <div class="MemberHeader">
                            @if(Auth::guard('web')->check())
                                <div class="ListBox">
                                    <ul>
                                        <li>
                                            <a href="{{ route('seller-info.search') }}"><span>Discover new partners!</span>พาร์ทเนอร์ฝ่ายญี่ปุ่น</a>
                                        </li>
                                        <li><a href="{{ route('news-jnto.all') }}"><span>News</span>ข่าวสาร</a></li>
                                        <li><a href="{{ route('seminar-list') }}"><span>Online Seminar</span>สัมมนาออนไลน์</a>
                                        </li>
                                        <li><a href="{{ route('home') }}#usefullinks"><span>Useful Links</span>ข้อมูลที่น่าสนใจอื่นๆ</a>
                                        </li>
                                    </ul>
                                </div>
                            @endif
                            <div>
                                @if(Auth::guard('seller')->check())
                                    <p class="TopEmail"><a
                                                href="{{ route('seller.mypage') }}">{{ Auth::guard('seller')->user()->user_login }}
                                            <span class="IDSeller">(ID{{ Auth::guard('seller')->user()->ref_id }})</span></a>
                                    </p>
                                    <p>
                                        <a href="{{ route('seller.mypage') }}" class="BTNTopPage">My
                                            page</a>
                                        <a href="#"
                                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                           class="BTNLogout">Logout</a>
                                    </p>
                                @elseif(Auth::guard('web')->check())
                                    <p><a href="#"
                                          onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                          class="BTNLogout">Logout</a></p>
                                    <p class="TopEmail"><a
                                                href="{{ route('dashboard') }}">{{ Auth::guard('web')->user()->name }}</a>
                                    </p>
                                @endif
                            </div>
                        </div>
                        <!-- End Phase2 MemberHeader -->

                        @if(Auth::guard('web')->check())</div>@endif


                        @if(Auth::guard('web')->check())
                            <!-- MobileMenuRight -->
                            <div class="MobileMenuRight">

                                <input type="checkbox" id="checkbox" class="mobile-menu__checkbox">
                                <label for="checkbox" class="mobile-menu__btn">
                                    <div class="mobile-menu__icon"></div>
                                </label>
                                <!-- menu__container -->
                                <div class="mobile-menu__container">
                                    <!-- Header -->
                                    <header>
                                        <div class="Header">
                                            <!-- HeaderBox -->
                                            <div class="HeaderBox clr">

                                                <h2><a href="{{ url('/') }}">B2B Website</a></h2>

                                                <div class="FooterBoxLeft">
                                                    @if((Auth::guard('seller')->check() && Auth::guard('seller')->user()->privacy != 'private') || Auth::guard('web')->check())
                                                        <p><a href="{{ route('seller-info.search') }}"><span>Discover new partners!</span>พาร์ทเนอร์ฝ่ายญี่ปุ่น</a>
                                                        </p>
                                                    @endif
                                                    @if((Auth::guard('seller')->check() && Auth::guard('seller')->user()->privacy != 'private') || Auth::guard('web')->check())
                                                        <p><a href="{{ route('topic.all') }}"><span>Event & Latest information</span>ข่าวสารจากพาร์ทเนอร์ฝ่ายญี่ปุ่น</a>
                                                        </p>
                                                    @endif
                                                    <p><a href="{{ route('news-jnto-partner.all') }}"><span>News</span>ข่าวสาร</a></p>
                                                    <p><a href="{{ route('seminar-list') }}"><span>Online Seminar</span>สัมมนาออนไลน์</a>
                                                    </p>
                                                    <p>
                                                        <a href="{{ route('home') }}#usefullinks"><span>Useful Links</span>ข้อมูลที่น่าสนใจอื่นๆ</a>
                                                    </p>
                                                </div>

                                            </div>
                                            <!-- End HeaderBox -->
                                        </div>
                                    </header>
                                    <!-- End Header -->
                                </div>
                                <!-- End menu__container -->

                            </div>
                            <!-- End MobileMenuRight -->
                        @endif



                        @if(Auth::guard('seller')->check())
                            <input type="checkbox" id="checkbox" class="mobile-menu__checkbox">
                            <label for="checkbox" class="mobile-menu__btn">
                                <div class="mobile-menu__icon"></div>
                            </label>
                            <!-- menu__container -->
                            <div class="mobile-menu__container">
                                <!-- Header -->
                                <header>
                                    <div class="Header">
                                        <!-- FooterBox -->
                                        <div class="HeaderBox clr">
                                            <h2><a href="{{ url('/') }}">B2B Website</a></h2>

                                            <div class="FooterBoxListAll">

                                                <div class="FooterBoxLeft">
                                                    <p><span>Discover new partners!</span>ค้นหาพาร์ทเนอร์ฝ่ายญี่ปุ่น</a>
                                                    </p>
                                                    @if((Auth::guard('seller')->check() && Auth::guard('seller')->user()->privacy != 'private') || Auth::guard('web')->check())
                                                        <p class="Links"><a href="{{ route('seller-info.search') }}">> ค้นหา</a></p>
                                                    @endif
                                                    @if((Auth::guard('seller')->check() && Auth::guard('seller')->user()->privacy != 'private') || Auth::guard('web')->check())
                                                        <p class="Links"><a href="{{ route('topic.all') }}">> อีเวนต์ ข่าวสาร</a></p>
                                                    @endif

                                                </div>

                                                <div class="FooterBoxRight">
                                                    <p><span>News</span>ข่าวสาร</a></p>
                                                    <p class="Links"><a href="{{ route('news-jnto.all') }}">> ข่าวสารจาก JNTO</a></p>
                                                    <p class="Links"><a href="{{ route('news-jnto-partner.all') }}">> ข่าวสารจากองค์กรท่องเที่ยว</a>
                                                    </p>
                                                </div>

                                                {{--@if(count($seminars) > 0)--}}
                                                <div class="FooterBoxRight clr">
                                                    <p><a href="{{ route('seminar-list') }}"><span>Online Seminar</span>สัมมนาออนไลน์</a>
                                                    </p>
                                                    {{--@foreach($seminars as $key => $item_seminars)
                                                        <div class="MBFont"><label class="YearsBox">{{ $key }}</label>|
                                                            @foreach($item_seminars as $item_seminar)
                                                                <a href="{{ route('seminar.show',$item_seminar->ref_id) }}">
                                                                    {{ $item_seminar->list ?? "ครั้งที่ $item_seminar->id" }}
                                                                </a>
                                                            @endforeach
                                                        </div>
                                                    @endforeach--}}
                                                </div>
                                                {{--@endif--}}

                                            </div>

                                            <!-- ULTail -->
                                            @if($nav_groups->count() > 0)
                                                <div class="ULTail clr" style="clear: both;">
                                                    {{--<h2>
                                                        <a href="{{ route('home') }}#usefullinks">เว็บไซต์ที่เป็นประโยชน์ Useful
                                                            Links <span></span></a>
                                                    </h2>--}}
                                                    <h2><span>Useful Links</span>เว็บไซต์ที่เป็นประโยชน์</h2>
                                                    <div class="Boxlist">
                                                        @foreach($nav_groups as $nav_group)

                                                            <div class="ULTailList">
                                                                <h3>{{ $nav_group->title }}</h3>
                                                                <ul>
                                                                    @foreach($nav_group->useful_links()->published()->get() as $nav_useful_link)
                                                                        <li>
                                                                            <a href="{{ $nav_useful_link->url }}"
                                                                               target="_blank">
                                                                                {{ $nav_useful_link->title }}
                                                                            </a>
                                                                        </li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>

                                                        @endforeach
                                                    </div>
                                                </div>
                                            @endif
                                            <!-- End ULTail -->

                                        </div>
                                        <!-- End FooterBox -->
                                    </div>
                                </header>
                                <!-- End Header -->
                            </div>
                            <!-- End menu__container -->
                @endif


            </nav>
            <form id="logout-form"
                  action="{{ Auth::guard('seller')->check() ? route('logout-seller') : route('logout')  }}"
                  method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>