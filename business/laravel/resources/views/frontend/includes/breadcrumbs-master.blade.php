<!--Breadcrumbs-->
<div class="Breadcrumbs clr">
    <a href="{{ route('home') }}">หน้าหลัก</a> >
    
    @if(Route::getCurrentRoute()->getName() == 'privacy-policy')
        <span>Cookie policy</span>
    @endif
    @if(Route::getCurrentRoute()->getName() == 'seminar.show')
        <span>{{ $seminar->title }}</span>
    @endif
    @if(Route::getCurrentRoute()->getName() == 'seller-info.search')
        <span>{{ 'ค้นหาพาร์ทเนอร์ฝ่ายญี่ปุ่น' }}</span>
    @endif
</div>
<!--Breadcrumbs-->