<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>JNTO Japan Tourism New Posibilities</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css-new/style.css') }}?v=5">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css-new/style-responsive.css') }}?v=5">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css-new/style-menu.css') }}?v=5">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css-new/style-popup.css') }}?v=5">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css-new/style-mypage.css') }}?v=5">
    <!-- SlickSlider -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/slick/slick.css') }}?v=5"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/slick/slick-theme.css') }}?v=5"/>
    <!-- End SlickSlider -->
    <link rel="icon" href="{{ asset('favicon-32x32.png') }}" type="image" sizes="32x32">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link
        href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Montserrat:wght@300;400;500;600;700&family=Sarabun:wght@100;500&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    @stack('css')

    @include('includes.google-tag-manager-header')

</head>
<body>

@include('includes.google-tag-manager-body')

<div id="BackToTop"></div>
@php
    $seminar_repo = new \App\Repositories\SeminarRepository();
    $seminar_all = $seminar_repo->all_published();
    $grouped_seminars = $seminar_all->groupBy('year');
    $seminars = $grouped_seminars->all();

    $useful_links_repo = new \App\Repositories\UsefulLinkRepository();
    $nav_groups = $useful_links_repo->group_category();
@endphp

<!-- Mobile Menu -->

@include('frontend.includes.navbar')
<!-- End Mobile Menu -->
<div>
    @yield('content')
</div>
<!-- Footer -->
@include('frontend.includes.footer')
