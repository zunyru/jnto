@extends('frontend.app')

@section('content')
    <div class="HeaderTopicPage">
        <div class="Breadcrumbs clr">
            <a href="{{ route('home') }}">หน้าหลัก</a> &gt; พาร์ทเนอร์ฝ่ายญี่ปุ่น อีเวนต์ ข่าวสาร
        </div>
        <!--<h2>Partners’ Event & Latest information!</h2>-->
        <div class="BixTXT">
            <p><span class="txt1">Partners’ Event & Latest information!</span>อีเวนต์ ข่าวสาร<span class="Sub">จาก</span>พาร์ทเนอร์ฝ่ายญี่ปุ่น</p>
        </div>
    </div>

    <!-- ToppageLastToppic -->
    <div class="ToppageLastToppic MarginTop50 clr">
        <div class="CategoryToppic clr">
            <ul class="CategoryToppicBox">
                <li class="ToppicAll"><a href="{{ route('topic.all') }}" class="{{ $menu == 'all' ? 'Active' : '' }}">ทั้งหมด</a></li>
                <li class="ToppicEvent"><a href="{{ route('topic.event') }}" class="{{ $menu == 'event' ? 'Active' : '' }}">อีเวนต์</a></li>
                <li class="ToppicNews"><a href="{{ route('topic.news') }}" class="{{ $menu == 'news' ? 'Active' : '' }}">ข่าวสาร</a></li>
            </ul>
        </div>
        <div class="ToppageLastToppicBox ToppageLastToppicPage clr">
            <ul>
                @foreach($topics as $topic)
                    <li>
                        <a href="{{ route('seller-info-detail',$topic->company->ref_id) }}#Toppic" target="_blank">
                            @if($topic->category == 'event')
                                <div class="PictureEventList">
                                    @if(!empty($topic->photo))
                                        <img src="{{ asset($topic->photo) }}">
                                    @else
                                        <img src="{{ asset('assets/images/no-image-event.png') }}">
                                    @endif
                                </div>
                                <p class="EventDate">เริ่มวันที่ {{ formatDateThai($topic->event_date_start) }} - {{ formatDateThai($topic->event_date_end) }} </p>
                            @else
                                <div class="PictureEventList">
                                    @if(!empty($topic->photo))
                                        <img src="{{ asset($topic->photo) }}">
                                    @else
                                        <img src="{{ asset('assets/images/no-image-news.png') }}">
                                    @endif
                                </div>
                                <p class="NewsToppic">ข่าวสาร</p>
                            @endif
                            <p class="Title">
                                {{ $topic->title }}
                            </p>
                            <div class="CompayandDate">
                                <p><span><img src="{{ asset($topic->company->logo) }}"></span>
                                    <label>{{ $topic->company->name_en }}</label></p>
                                <p>อัพเดท {{ formatDateThai($topic->updated_at) }}</p>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>

        <!-- NavigatorList -->
        {{ $topics->links('pagination::custom') }}
        <!-- End NavigatorList -->

    </div>
    <!-- ToppageLastToppic -->
@endsection