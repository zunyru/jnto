@extends('frontend.app')
@push('css')
    <style>
        .error {
            color: red;
        }
    </style>
@endpush
@section('content')
    @include('frontend.includes.breadcrumbs-news-jnto-partner')

    <!-- ContentBoxFull -->
    <div class="ContentBoxFull clr">
        <!-- SellerInfoPage -->
        <div class="SellerInfoPage clr">
            <h1 class="NewsTitle">{{ $news->title }}</h1>
        <!-- End SellerInfoPage -->
        </div>
    </div>
    <!-- End ContentBoxFull -->


    <!-- ContentBox -->
    <div class="ContentBox clr">

    @if(!empty($company))
        <!-- เริ่ม => ถ้ามี Seller info ให้เปิดใช้ div นี้ <div class="ContentBoxLeft clr"> / ถ้าไม่มีก็ไม่ต้องแสดง -->
            <div class="ContentBoxLeft clr">
            @endif

            <!-- SlidePhoto -->
            @if(!empty($news->photo1) || !empty($news->photo2) || !empty($news->photo3) || !empty($news->photo4))
            <!-- SlidePhoto -->
                <div id="gal-results" class="containerPhotoSlide clr">
                    <div id="gallery-view">
                        <div id="gal-slide">
                            @if(!empty($news->photo1))
                                <div class="gal-slide-img"
                                     style="background: url('{{ asset($news->photo1) }}') no-repeat; background-size: contain; background-repeat: no-repeat; background-position: center center;">
                                </div>
                            @endif
                            @if(!empty($news->photo2))
                                <div class="gal-slide-img"
                                     style="background: url('{{ asset($news->photo2) }}') no-repeat; background-size: contain; background-repeat: no-repeat; background-position: center center;">
                                </div>
                            @endif
                            @if(!empty($news->photo3))
                                <div class="gal-slide-img"
                                     style="background: url('{{ asset($news->photo3) }}') no-repeat; background-size: contain; background-repeat: no-repeat; background-position: center center;">
                                </div>
                            @endif
                            @if(!empty($news->photo4))
                                <div class="gal-slide-img"
                                     style="background: url('{{ asset($news->photo4) }}') no-repeat; background-size: contain; background-repeat: no-repeat; background-position: center center;">
                                </div>
                            @endif
                        </div>
                    </div>
                    <div id="gallery-thumbs">
                        <div id="gal-thumb">
                            @if(!empty($news->photo1))
                                <div class="gal-slide-thumb"
                                     style=" background: url('{{ asset($news->photo1) }}')no-repeat; background-size: cover; background-repeat: no-repeat; background-position: center center;">
                                </div>
                            @endif
                            @if(!empty($news->photo2))
                                <div class="gal-slide-thumb"
                                     style=" background: url('{{ asset($news->photo2) }}')no-repeat; background-size: cover; background-repeat: no-repeat; background-position: center center;">
                                </div>
                            @endif
                            @if(!empty($news->photo3))
                                <div class="gal-slide-thumb"
                                     style=" background: url('{{ asset($news->photo3) }}')no-repeat; background-size: cover; background-repeat: no-repeat; background-position: center center;">
                                </div>
                            @endif
                            @if(!empty($news->photo4))
                                <div class="gal-slide-thumb"
                                     style=" background: url('{{ asset($news->photo4) }}')no-repeat; background-size: cover; background-repeat: no-repeat; background-position: center center;">
                                </div>
                            @endif
                        </div>
                    </div>
                    <!-- End SlidePhoto -->
                </div>
                <!-- End SlidePhoto -->
             @endif

            <!-- SellerInfoDetail -->
                <div class="NewsBoxDetail clr">
                    <!-- NewsBoxDetailBox -->
                    <div class="NewsBoxDetailBox">
                        {!! nl2br($news->detail) !!}
                    </div>
                    <!-- End NewsBoxDetailBox -->
                    <!-- CompanyProfileList -->

                @if(!empty($news->pdf1) || !empty($news->pdf2) || !empty($news->pdf3))
                    <!-- CompanyProfileList -->
                        <div class="CompanyProfileList clr">
                            <h2>รายละเอียดเพิ่มเติม</h2>
                            @if(!empty($news->pdf1))
                                <p class="DownloadPDF">
                                    <a target="_blank" href="{{ asset($news->pdf1) }}">
                                        <span>Download file</span>
                                    </a><br> {{ $news->file_description1 }}
                                    <label>{{ formatSizeUnits($news->pdf_size1) }}</label></p>
                            @endif
                            @if(!empty($news->pdf2))
                                <p class="DownloadPDF">
                                    <a target="_blank" href="{{ asset($news->pdf2) }}">
                                        <span>Download file</span>
                                    </a><br> {{ $news->file_description2 }}
                                    <label>{{ formatSizeUnits($news->pdf_size2) }}</label></p>
                            @endif
                            @if(!empty($news->pdf3))
                                <p class="DownloadPDF">
                                    <a target="_blank" href="{{ asset($news->pdf3) }}">
                                        <span>Download file</span>
                                    </a><br> {{ $news->file_description3 }}
                                    <label>{{ formatSizeUnits($news->pdf_size3) }}</label></p>
                            @endif
                        </div>
                        <!-- End CompanyProfileList -->
                        <p class="LinktoAll" style="text-align: left!important;">
                            <a href="{{ route('news-jnto-partner.all') }}">กลับไปที่หน้ารวมข่าวสารจากองค์กรท่องเที่ยวต่างๆ</a>
                        </p>
                        <!-- End NewsBoxDetail -->
                    @endif
                </div>
                <!-- End NewsBoxDetail -->
            @if(!empty($company))
                <!-- สิ้นสุด => ถ้ามี Seller info ให้เปิดใช้ div นี้ </div> / ถ้าไม่มีก็ไม่ต้องแสดง -->
            </div>
    @endif

    <!-- เริ่ม => ถ้ามี Seller info Box ด้านขวาจะโชว์  -->
        <!-- ContentBoxRight -->
    @include('frontend.includes.content-box-rigth')
    <!-- End NewsBannerRight -->

        <!-- ContactUsPopupDisplay -->
    @include('frontend.includes.contact-company')
    <!-- End ContactUsPopupDisplay -->

        <!-- ContactUsBranchPopupDisplay -->
    @include('frontend.includes.contact-company-branch')
    <!-- End ContactUsPopupDisplay -->

    </div>
    <!-- End ContentBoxRight -->
    <!--  สิ้นสุด => ถ้ามี Seller info Box ด้านขวาจะโชว์  -->

    <!-- End ContentBox -->

@endsection