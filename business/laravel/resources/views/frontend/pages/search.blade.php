<div class="ToppageSearch clr">
    <div class="ToppageSearchBox">
        <p class="SearchTitle"><span class="FontItalic">Discover New Partners!</span>ค้นหาพาร์ทเนอร์ฝ่ายญี่ปุ่น</p>
        <form action="{{ route('seller-info.search') }}" method="GET">
            <div class="FormBox">
                <!-- FormBoxLeft -->
                <div class="FormBoxLeft">
                    <div class="PaddingMap">
                        <!-- MapRegion -->
                        <div class="MapRegion">
                            <p class="MapSearchTxt" style="margin-top: -5px;">ค้นหาจากสถานที่ <span>คลิกภูมิภาคที่ต้องการ</span>
                            </p>
                            <img src="{{ asset('frontend/assets/images/bg-map/bg-map-cover.png') }}">
                            <a href="#HokkaidoBox" class='SectionClick HokkaidoBox'></a>
                            <a href="#TohokuBox" class='SectionClick TohokuBox'></a>
                            <a href="#KantoBox" class='SectionClick KantoBox'></a>
                            <a href="#HokurikuShinetsuBox" class='SectionClick HokurikuShinetsuBox'><span></span></a>
                            <a href="#ChubuBox" class='SectionClick ChubuBox'><span></span></a>
                            <a href="#KansaiBox" class='SectionClick KansaiBox'><span></span></a>
                            <a href="#ChugokuBox" class='SectionClick ChugokuBox'></a>
                            <a href="#ShikokuBox" class='SectionClick ShikokuBox'></a>
                            <a href="#KyushuBox" class='SectionClick KyushuBox'></a>
                            <a href="#OkinawaBox" class='SectionClick OkinawaBox'></a>

                            <!-- HokkaidoBox -->
                            <div id="HokkaidoBox" class="SectionShow HokkaidoDisplay">
                                <div class="MapProvince">
                                    <p class="MapSearchTxt">ค้นหาจากสถานที่</p>
                                    <img src="{{ asset('frontend/assets/images/bg-map/bg-map-cover.png') }}">
                                    <p class="BacktoRegion">
                                        <a href="javascript:void(0)" class="back SectionClick">< กลับไป</a>
                                    </p>
                                    <div class="HokkaidoCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Hokkaido"
                                               value="Hokkaido">
                                        <label for="Hokkaido"></label>
                                    </div>
                                </div>
                            </div>
                            <!-- End HokkaidoBox -->

                            <!-- TohokuBox -->
                            <div id="TohokuBox" class="SectionShow TohokuDisplay">
                                <div class="MapProvince">
                                    <p class="MapSearchTxt">ค้นหาจากสถานที่</p>
                                    <img src="{{ asset('frontend/assets/images/bg-map/bg-map-cover.png') }}">
                                    <p class="BacktoRegion">
                                        <a href="javascript:void(0)" class="back SectionClick">< กลับไป</a></p>
                                    <div class="AomoriCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               id="Aomori"
                                               class="input_search"
                                               value="Aomori">
                                        <label for="Aomori"></label>
                                    </div>
                                    <div class="AkitaCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               id="Akita"
                                               class="input_search"
                                               value="Akita">
                                        <label for="Akita"></label>
                                    </div>
                                    <div class="IwateCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               id="Iwate"
                                               class="input_search"
                                               value="Iwate">
                                        <label for="Iwate"></label>
                                    </div>
                                    <div class="YamagataCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Yamagata"
                                               value="Yamagata">
                                        <label for="Yamagata"></label>
                                    </div>
                                    <div class="MiyagiCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Miyagi"
                                               value="Miyagi">
                                        <label for="Miyagi"></label>
                                    </div>
                                    <div class="FukushimaCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Fukushima"
                                               value="Fukushima">
                                        <label for="Fukushima"></label>
                                    </div>
                                </div>
                            </div>
                            <!-- End TohokuBox -->

                            <!-- KantoBox -->
                            <div id="KantoBox" class="SectionShow KantoDisplay">
                                <div class="MapProvince">
                                    <p class="MapSearchTxt">ค้นหาจากสถานที่</p>
                                    <img src="{{ asset('frontend/assets/images/bg-map/bg-map-cover.png') }}">
                                    <p class="BacktoRegion">
                                        <a href="javascript:void(0)" class="back SectionClick">< กลับไป</a></p>
                                    <div class="GunmaCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               id="Gunma"
                                               class="input_search"
                                               value="Gunma">
                                        <label for="Gunma"></label>
                                    </div>
                                    <div class="TochigiCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Tochigi"
                                               value="Tochigi">
                                        <label for="Tochigi"></label>
                                    </div>
                                    <div class="IbarakiCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               id="Ibaraki"
                                               class="input_search"
                                               value="Ibaraki">
                                        <label for="Ibaraki"></label>
                                    </div>
                                    <div class="SaitamaCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Saitama"
                                               value="Saitama">
                                        <label for="Saitama"></label>
                                    </div>
                                    <div class="YamanashiCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               id="Yamanashi"
                                               class="input_search"
                                               value="Yamanashi">
                                        <label for="Yamanashi"></label>
                                    </div>
                                    <div class="TokyoCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Tokyo"
                                               value="Tokyo">
                                        <label for="Tokyo"></label>
                                    </div>
                                    <div class="ChibaCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Chiba"
                                               value="Chiba">
                                        <label for="Chiba"></label>
                                    </div>
                                    <div class="KanagawaCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               id="Kanagawa"
                                               class="input_search"
                                               value="Kanagawa">
                                        <label for="Kanagawa"></label>
                                    </div>
                                </div>
                            </div>
                            <!-- End KantoBox -->

                            <!-- HokurikuShinetsuBox -->
                            <div id="HokurikuShinetsuBox" class="SectionShow HokurikuShinetsuDisplay">
                                <div class="MapProvince">
                                    <p class="MapSearchTxt">ค้นหาจากสถานที่</p>
                                    <img src="{{ asset('frontend/assets/images/bg-map/bg-map-cover.png') }}">
                                    <p class="BacktoRegion"><a href="javascript:void(0)" class="back SectionClick">< กลับไป</a></p>
                                    <div class="IshikawaCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Ishikawa"
                                               value="Ishikawa">
                                        <label for="Ishikawa"></label>
                                    </div>
                                    <div class="ToyamaCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Toyama"
                                               value="Toyama">
                                        <label for="Toyama"></label>
                                    </div>
                                    <div class="NiigataCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Niigata"
                                               value="Niigata">
                                        <label for="Niigata"></label>
                                    </div>
                                    <div class="NaganoCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Nagano"
                                               value="Nagano">
                                        <label for="Nagano"></label>
                                    </div>
                                </div>
                            </div>
                            <!-- End HokurikuShinetsuBox -->

                            <!-- ChubuBox -->
                            <div id="ChubuBox" class="SectionShow ChubuDisplay">
                                <div class="MapProvince">
                                    <p class="MapSearchTxt">ค้นหาจากสถานที่</p>
                                    <img src="{{ asset('frontend/assets/images/bg-map/bg-map-cover.png') }}">
                                    <p class="BacktoRegion"><a href="javascript:void(0)" class="back SectionClick">< กลับไป</a></p>
                                    <div class="FukuiCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Fukui"
                                               value="Fukui">
                                        <label for="Fukui"></label>
                                    </div>
                                    <div class="GifuCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Gifu"
                                               value="Gifu">
                                        <label for="Gifu"></label>
                                    </div>
                                    <div class="ShizuokaCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Shizuoka"
                                               value="Shizuoka">
                                        <label for="Shizuoka"></label>
                                    </div>
                                    <div class="AichiCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Aichi"
                                               value="Aichi">
                                        <label for="Aichi"></label>
                                    </div>
                                    <div class="MieCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Mie"
                                               value="Mie">
                                        <label for="Mie"></label>
                                    </div>
                                </div>
                            </div>
                            <!-- End ChubuBox -->

                            <!-- KansaiBox -->
                            <div id="KansaiBox" class="SectionShow KansaiDisplay">
                                <div class="MapProvince">
                                    <p class="MapSearchTxt">ค้นหาจากสถานที่</p>
                                    <img src="{{ asset('frontend/assets/images/bg-map/bg-map-cover.png') }}">
                                    <p class="BacktoRegion"><a href="javascript:void(0)" class="back SectionClick">< กลับไป</a></p>
                                    <div class="HyogoCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               id="Hyogo"
                                               class="input_search"
                                               value="Hyogo">
                                        <label for="Hyogo"></label>
                                    </div>
                                    <div class="KyotoCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               id="Kyoto"
                                               class="input_search"
                                               value="Kyoto">
                                        <label for="Kyoto"></label>
                                    </div>
                                    <div class="ShigaCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Shiga"
                                               value="Shiga">
                                        <label for="Shiga"></label>
                                    </div>
                                    <div class="NaraCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Nara"
                                               value="Nara">
                                        <label for="Nara"></label>
                                    </div>
                                    <div class="OsakaCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Osaka"
                                               value="Osaka">
                                        <label for="Osaka"></label>
                                    </div>
                                    <div class="WakayamaCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Wakayama"
                                               value="Wakayama">
                                        <label for="Wakayama"></label>
                                    </div>
                                </div>
                            </div>
                            <!-- End KansaiBox -->

                            <!-- ChugokuBox -->
                            <div id="ChugokuBox" class="SectionShow ChugokuDisplay">
                                <div class="MapProvince">
                                    <p class="MapSearchTxt">ค้นหาจากสถานที่</p>
                                    <img src="{{ asset('frontend/assets/images/bg-map/bg-map-cover.png') }}">
                                    <p class="BacktoRegion"><a href="javascript:void(0)" class="back SectionClick">< กลับไป</a></p>
                                    <div class="TottoriCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Tottori"
                                               value="Tottori">
                                        <label for="Tottori"></label>
                                    </div>
                                    <div class="ShimaneCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Shimane"
                                               value="Shimane">
                                        <label for="Shimane"></label>
                                    </div>
                                    <div class="YamaguchiCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Yamaguchi"
                                               value="Yamaguchi">
                                        <label for="Yamaguchi"></label>
                                    </div>
                                    <div class="OkayamaCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Okayama"
                                               value="Okayama">
                                        <label for="Okayama"></label>
                                    </div>
                                    <div class="HiroshimaCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Hiroshima"
                                               value="Hiroshima">
                                        <label for="Hiroshima"></label>
                                    </div>
                                </div>
                            </div>
                            <!-- End ChugokuBox -->

                            <!-- ShikokuBox -->
                            <div id="ShikokuBox" class="SectionShow ShikokuDisplay">
                                <div class="MapProvince">
                                    <p class="MapSearchTxt">ค้นหาจากสถานที่</p>
                                    <img src="{{ asset('frontend/assets/images/bg-map/bg-map-cover.png') }}">
                                    <p class="BacktoRegion"><a href="javascript:void(0)" class="back SectionClick">< กลับไป</a></p>
                                    <div class="KagawaCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Kagawa"
                                               value="Kagawa">
                                        <label for="Kagawa"></label>
                                    </div>
                                    <div class="TokushimaCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Tokushima"
                                               value="Tokushima">
                                        <label for="Tokushima"></label>
                                    </div>
                                    <div class="EhimeCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Ehime"
                                               value="Ehime">
                                        <label for="Ehime"></label>
                                    </div>
                                    <div class="KochiCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Kochi"
                                               value="Kochi">
                                        <label for="Kochi"></label>
                                    </div>
                                </div>
                            </div>
                            <!-- End ShikokuBox -->

                            <!-- KyushuBox -->
                            <div id="KyushuBox" class="SectionShow KyushuDisplay">
                                <div class="MapProvince">
                                    <p class="MapSearchTxt">ค้นหาจากสถานที่</p>
                                    <img src="{{ asset('frontend/assets/images/bg-map/bg-map-cover.png') }}">
                                    <p class="BacktoRegion"><a href="javascript:void(0)" class="back SectionClick">< กลับไป</a></p>
                                    <div class="OitaCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Oita"
                                               value="Oita">
                                        <label for="Oita"></label>
                                    </div>
                                    <div class="FukuokaCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Fukuoka"
                                               value="Fukuoka">
                                        <label for="Fukuoka"></label>
                                    </div>
                                    <div class="SagaCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Saga"
                                               value="Saga">
                                        <label for="Saga"></label>
                                    </div>
                                    <div class="NagasakiCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Nagasaki"
                                               value="Nagasaki">
                                        <label for="Nagasaki"></label>
                                    </div>
                                    <div class="MiyazakiCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Miyazaki"
                                               value="Miyazaki">
                                        <label for="Miyazaki"></label>
                                    </div>
                                    <div class="KumamotoCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Kumamoto"
                                               value="Kumamoto">
                                        <label for="Kumamoto"></label>
                                    </div>
                                    <div class="KagoshimaCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Kagoshima"
                                               value="Kagoshima">
                                        <label for="Kagoshima"></label>
                                    </div>
                                </div>
                            </div>
                            <!-- End KyushuBox -->

                            <!-- OkinawaBox -->
                            <div id="OkinawaBox" class="SectionShow OkinawaDisplay">
                                <div class="MapProvince">
                                    <p class="MapSearchTxt">ค้นหาจากสถานที่</p>
                                    <img src="{{ asset('frontend/assets/images/bg-map/bg-map-cover.png') }}">
                                    <p class="BacktoRegion"><a href="javascript:void(0)" class="back SectionClick">< กลับไป</a></p>
                                    <div class="OkinawaCheckbox">
                                        <input type="checkbox"
                                               name="search[]"
                                               class="input_search"
                                               id="Okinawa"
                                               value="Okinawa">
                                        <label for="Okinawa"></label>
                                    </div>
                                </div>
                            </div>
                            <!-- End OkinawaBox -->
                        </div>
                    </div>
                    <!-- End MapRegion -->
                </div>
                <!-- End FormBoxLeft -->
                <!-- FormBoxRight -->
                <div class="FormBoxRight">
                    <div class="BusinessTypeList clr">
                        <p class="BType">ประเภทธุรกิจ</p>
                        <ul>
                            @foreach($business_types as $business_type)
                                <li><label>
                                        <input
                                                name="business_type[]"
                                                class="business_type"
                                                value="{{ $business_type->id }}"
                                                type="checkbox"> {{ $business_type->name_th }}</label>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- End FormBoxRight -->
                    <!-- SupportMenuListSearch -->
                    <div class="SupportMenuListSearch">
                        <select
                                name="support_menus"
                                class="support_menu"
                        >
                            <option value="">คลิกเลือกโปรแกรมสนับสนุน</option>
                            {{--                            <option value="none">ไม่มี</option>--}}
                            @foreach($support_menus as $support_menu)
                                <option value="{{ $support_menu->id }}">{{ $support_menu->name_th }}</option>
                            @endforeach
                        </select>
                    </div>
                    <!-- End SupportMenuListSearch -->
                    <!-- KeywordS -->
                    <div class="KeywordS clr"><label>
                            <input class="search_txt" name="search_txt" type="text" placeholder="ค้นหาด้วยชื่อบริษัท">
                        </label></div>
                    <!-- End KeywordS -->
                </div>
                <!-- End FormBoxRight -->
            </div>
            <input type="hidden" name="section">
            <div class="BTNSearchPage clr">
                <button type="submit" id="search" class="BTNN BTNN-4"><span>ค้นหา <img
                                src="{{ asset('frontend/assets/images/icon/icon-search-white.svg') }}"></span>
                </button>
            </div>
        </form>

    </div>
</div>
@push('scripts')
    <script>
        $('.back').click(function () {
            $('.input_search').prop("checked", false);
            $("input[name='section']").val('');
        });
        $('.SectionClick').click(function () {
            $("input[name='section']").val($(this).attr('href'));
        });
    </script>
@endpush