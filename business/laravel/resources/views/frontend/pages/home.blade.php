@extends('frontend.app')
@section('content')
    {{--<!-- FeatureToppage -->
    <div class="FeatureToppage clr">
        <div class="FeatureToppageLeft"></div>
        <div class="FeatureToppageRight">
            <div class="FeatureToppageRightBox"><h1>รวมข้อมูลที่เป็นประโยชน์เพื่อผู้ประกอบการท่องเที่ยว<span>เตรียมความพร้อมรอวันน่านฟ้าเปิด</span>
                </h1></div>
        </div>
    </div>
    <!-- End FeatureToppage -->


    <!-- AnchorToppage -->
    <div class="AnchorToppage">
        <ul>

            <li class="AnchorPartnerTP">
                <a href="#searchpartner"><p>
                        <img src="{{ asset('frontend/assets/images/icon/icon-search-partner.svg') }}">
                        ค้นหาพาร์ทเนอร์<br>ฝ่ายญี่ปุ่น</p>
                </a>
            </li>


            <li class="AnchorNewsUpdateTP"><a href="#newsupdate"><p>
                        <img src="{{ asset('frontend/assets/images/icon/icon-news-toppage.svg') }}"> ข่าวสารล่าสุด
                    </p>
                </a></li>

            <li class="AnchorSeminarTP"><a href="#seminarupdate"><p>
                        <img src="{{ asset('frontend/assets/images/icon/icon-seminar-toppage.svg') }}">
                        ดูสัมมนาออนไลน์<br>ย้อนหลัง</p></a></li>
            <li class="AnchorUseFullTP"><a href="#usefullinks"><p>
                        <img src="{{ asset('frontend/assets/images/icon/icon-wbsite-other.svg') }}"> เว็บไซต์<br>ที่เป็นประโยชน์
                    </p></a></li>
        </ul>
    </div>
    <!-- End AnchorToppage -->
    --}}

    <!-- BannerandEvent -->
    <div class="BannerandEvent">

        <!-- Phase2 FeatureSlideToppage -->
        <div class="FeatureSlideToppage">
            @if(sizeof($banners) > 0)
                <ul class="FeatureSlideBox">
                    @foreach($banners as $banner)
                        <li>
                            @if(!empty($banner->url))
                                <a target="_blank" href="{{ $banner->url }}">
                                    @endif
                                    <img src="{{ asset($banner->thumbnail) }}">
                                    @if(!empty($banner->url))
                                </a>
                            @endif
                        </li>
                    @endforeach
                </ul>
            @endif
        </div>
        <!-- Phase2 End FeatureSlideToppage -->

        <!-- CalendarRight -->
        @include('frontend.includes.calendar-custom')
        <!-- End CalendarRight -->

    </div>
    <!-- End BannerandEvent -->


    @if((Auth::guard('seller')->check() && Auth::guard('seller')->user()->privacy != 'private') || Auth::guard('web')->check())
        <!-- UpdateInfo -->
        @if(sizeof($seller_revision_histories) > 0)
            <div class="UpdateInfoTitle">Latest Update</div>
            <div class="UpdateInfo clr">
                <ul>
                    @foreach($seller_revision_histories as $seller_revision_history)
                        <li>
                            <p>{{ formatDateThai($seller_revision_history['created_at']) }}</p>
                            @if($seller_revision_history['type'] == 'seller')
                                <p class="UpdateBox"><span>UPDATE!</span></p>
                            @else
                                <p class="NewBox"><span>NEW!</span></p>
                            @endif
                            <p class="Title">
                                <a href="{{ $seller_revision_history['link'] }}">
                                    {{ $seller_revision_history['title'] }}
                                </a>
                            </p>
                        </li>
                    @endforeach
                </ul>
                <!--<p class="LinkViewAll"><a href="#">ดูทั้งหมด</a></p>-->
            </div>
        @endif
        <!-- End UpdateInfo -->
    @endif

    {{--<!-- ToppageCompanyInfo -->
    <div class="ToppageCompanyInfo clr">
        <!-- ToppageCompanyInfoBox -->
        <div class="ToppageCompanyInfoBox clr" id="searchpartner">
            <!-- RelatedCompanyInfo -->
            <div class="RelatedCompanyInfo clr">
                <h2>ข้อมูลพาร์ทเนอร์ฝ่ายญี่ปุ่นมากมาย พร้อมช่องทางการติดต่อ</h2>
                <ul class="RelatedSlideBox">
                    @if(!empty($companies))
                        @foreach($companies as $key => $company)
                            <li>
                                <a href="{{ route('seller-info-detail',$company->ref_id) }}" target="_blank">
                                    <div class="PictureDisplayRe">
                                        @if(!empty($company->is_partner))
                                            @if(Auth::guard('web')->check())
                                                <span>JNTO Partners</span>
                                            @endif
                                        @endif
                                        @if(!empty($company->service_information->main_photo))
                                            <div class="inner"
                                                 style="background-image:url('{{ $company->service_information->main_photo }}');background-size: cover; background-position: center center;">
                                            </div>
                                        @endif
                                    </div>
                                    <div class="RightCaptions">
                                        <p class="CompanyName">{{ $company->name_en }}</p>
                                        <p class="Title">{{ optional($company->service_information)->title }}</p>
                                        <p class="ProvinceDisplay">{{ @$provinces_implode[$company->id]}}</p>
                                        @if(!empty($company->support_menu->support_menu1)
                                              || !empty($company->support_menu->support_menu2)
                                              || !empty($company->support_menu->support_menu3)
                                              || !empty($company->support_menu->support_menu4)
                                              || !empty($company->support_menu->support_menu5))
                                            <p class="SupportMenuDisplay">มีโปรแกรมสนับสนุน</p>
                                        @endif
                                        @if(!empty($company->topic))
                                            @if($company->topic->category == 'event')
                                                <p class="ToppicEvent">มีอีเวนท์ใหม่</p>
                                            @else
                                                <p class="ToppicNews">ข่าวสาร</p>
                                            @endif
                                        @endif
                                    </div>
                                </a>
                            </li>
                        @endforeach
                    @endif
                    @if(Auth::guard('seller')->check())
                        @if(Auth::guard('seller')->user()->privacy == 'private')
                            <li>
                                <img src="{{ asset('assets/images/banner-locked2.png') }}"
                                     class="BannerLocked2">
                            </li>
                            <li>
                                <img src="{{ asset('assets/images/banner-locked2.png') }}"
                                     class="BannerLocked2">
                            </li>
                            <li>
                                <img src="{{ asset('assets/images/banner-locked2.png') }}"
                                     class="BannerLocked2">
                            </li>
                        @endif
                    @endif
                </ul>
            </div>
            <!-- RelatedCompanyInfo -->
        </div>
        <!-- End ToppageCompanyInfoBox -->
        @if((Auth::guard('seller')->check() && Auth::guard('seller')->user()->privacy != 'private') || Auth::guard('web')->check())
            <div class="BTNLink">
                <a href="{{ route('seller-info.search') }}" class="LinkTOSearch">ค้นหาจากสถานที่และประเภทธุรกิจ
                    <img src="{{ asset('frontend/assets/images/icon/icon-send-arrow-right.svg') }}">
                </a>
            </div>
        @endif

    </div>
    <!-- End ToppageCompanyInfo -->--}}


    <!-- Phase2 ToppageLastToppic -->
    @if(sizeof($data_topics) > 0)
        <div class="ToppageLastToppic clr">
            <div class="ToppageLastToppicBox">
                <!--<h2>อัพเดทล่าสุด</h2>
                <p class="Captions"><span>จาก</span>พาร์ทเนอร์ฝ่ายญี่ปุ่น</p>-->
                <p class="EventTitle"><span class="FontItalic">Partners’ Events!</span>อีเวนต์<span class="FontSmall">จัดโดย</span>พาร์ทเนอร์ฝ่ายญี่ปุ่น
                </p>
                <ul class="EventSlideBox">
                    @foreach($data_topics as $topics)
                        <li>
                            @foreach($topics as $topic)
                                <div class="PartnerTopBox">
                                    <a href="{{ route('seller-info-detail',$topic->company->ref_id) }}#Toppic"
                                       target="_blank">
                                        <div class="PictureEventList">
                                            @if(!empty($topic->photo))
                                                <img src="{{ asset($topic->photo) }}">
                                            @else
                                                @if($topic->category == 'event')
                                                    <img src="{{ asset('assets/images/no-image-event.png') }}">
                                                @else
                                                    <img src="{{ asset('assets/images/no-image-news.png') }}">
                                                @endif
                                            @endif
                                        </div>
                                        <p class="EventDate">เริ่มวันที่ {{ formatDateThai($topic->event_date_start) }}
                                            - {{ formatDateThai($topic->event_date_end) }}</p>
                                        <p class="Title">
                                            {{ $topic->title }}
                                        </p>
                                        <div class="CompayandDate">
                                            <p><span>
                                                    @if(!empty($topic->company->logo))
                                                        <img src="{{ asset($topic->company->logo) }}">
                                                    @else
                                                        <img src="{{ asset('frontend/assets/images/logo.png') }}">
                                                    @endif
                                                </span><label>
                                                    {{ $topic->company->name_en }}</label></p>
                                            <p>อัพเดท {{ formatDateThai($topic->updated_at) }}</p>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                            @if(Auth::guard('seller')->check())
                                @if(Auth::guard('seller')->user()->privacy == 'private')
                                    <div class="PartnerTopBox PartnerTopLock">
                                        <img src="{{ asset('assets/images/banner-locked.png') }}" class="BannerLocked">
                                    </div>
                                @endif
                            @endif
                        </li>
                    @endforeach
                </ul>
                @if((Auth::guard('seller')->check() && Auth::guard('seller')->user()->privacy != 'private') || Auth::guard('web')->check())
                    <div class="BTNLink BTNLink2">
                        <a href="{{ route('topic.all') }}">ดูอีเวนต์และข่าวสารทั้งหมด
                            <img src="{{ asset('assets/images/icon/icon-arrow-right-white2.svg') }}">
                        </a>
                    </div>
                @endif
            </div>
        </div>
    @endif
    <!-- Phase2 End ToppageLastToppic -->

    @if((Auth::guard('seller')->check() && Auth::guard('seller')->user()->privacy != 'private') || Auth::guard('web')->check())
        <!-- ToppageSearch -->
        @include('frontend.pages.search')
        <!-- End ToppageSearch -->
    @endif

    <!-- NewsTopPage -->
    <div class="NewsTopPage clr" id="newsupdate">
        <!-- NewsTopPageLeft -->
        <div class="NewsTopPageLeft">
            <!-- NewsTopPageLeftHead -->
            <div class="NewsTopPageLeftHead">
                <ul>
                    <li class="Active Tab1" id="Tab1"><a href="javascript:void(0);"><span>JNTO News</span>ข่าวสารจาก
                            JNTO</a></li>
                    <li class="Tab2" id="Tab2"><a href="javascript:void(0);"><span>JNTO Partners’ News</span>ข่าวสารจากองค์กรท่องเที่ยว</a>
                    </li>
                </ul>
            </div>
            <!-- End NewsTopPageLeftHead -->
            <!-- NewsTopPageLeftBody -->
            <div class="NewsTopPageLeftBody">
                @if($news_jnto->count() > 0)
                    <span id="JNTO_News" class="JNTO_News">
        <ul>
            @foreach($news_jnto as $news)
                <li>
                <span>{{ formatDateThai($news->created_at) }}</span>
                    @if($news->template == 'full-content')
                        <a 
                           href="{{ route('news-jnto.show',$news->ref_id) }}">{{ $news->title }}</a>
                    @endif
                    @if($news->template == 'link-only')
                        <a target="_blank"
                           href="{{ $news->link }}">{{ $news->title }}</a>
                    @endif
                    @if($news->template == 'PDF-only')
                        <a target="_blank"
                           href="{{ $news->pdf_only }}">{{ $news->title }}</a>
                    @endif
            </li>
            @endforeach
        </ul>
        <div class="LinkNews"><a href="{{ route('news-jnto.all') }}">ดูทั้งหมด <img
                        src="{{ asset('frontend/assets/images/icon/icon-send-arrow-right.svg') }}"></a></div>
    </span>
                @else
                    <span id="JNTO_News" class="JNTO_News">
       <p style="color: red; text-align: center; padding: 50px 0px;">ยังไม่มีข่าวสารจาก JNTO</p>
    </span>
                @endif
                @if($news_jnto_partner->count() > 0)
                    <span id="JNTO_Partner" class="JNTO_Partner" style="display: none;">
        <ul>
            @foreach($news_jnto_partner as $news_partner)
                <li>
                  <span>{{ formatDateThai($news_partner->created_at) }}</span>
                  @if($news_partner->template == 'full-content')
                        <a 
                           href="{{ route('news-jnto-partner.show',$news_partner->ref_id) }}">{{ $news_partner->title }}</a>
                    @endif
                    @if($news_partner->template == 'link-only')
                        <a target="_blank"
                           href="{{ $news_partner->link }}">{{ $news_partner->title }}</a>
                    @endif
                    @if($news_partner->template == 'PDF-only')
                        <a target="_blank"
                           href="{{ $news_partner->pdf_only }}">{{ $news_partner->title }}</a>
                    @endif
               </li>
            @endforeach
        </ul>
        <div class="LinkNews">
            <a href="{{ route('news-jnto-partner.all') }}">ดูทั้งหมด <img
                        src="{{ asset('frontend/assets/images/icon/icon-send-arrow-right.svg') }}"></a></div>
    </span>
                @else
                    <span id="JNTO_Partner" class="JNTO_Partner" style="display: none">
       <p style="color: red; text-align: center; padding: 50px 0px;">ยังไม่มีข่าวสารจากองค์กรท่องเที่ยวต่างๆ</p>
    </span>
                @endif
            </div>
            <!-- End NewsTopPageLeftBody -->
            <div class="NewsTopPageLeftTail"></div>
        </div>
        <!-- End NewsTopPageLeft -->


        <!-- NewsTopPageRight -->
        <div class="NewsTopPageRight">
            <h3 class="Title">COVID-19 information </h3>
            <div class="Covid19Box">
                @if($covids->count() > 0)
                    <ul>
                        @foreach($covids as $covid)
                            <li>
                                <a href="{{ $covid->url }}" target="_blank">{{ $covid->title }}</a>
                            </li>
                        @endforeach
                    </ul>
                @else
                    <p style="color: red; text-align: center; padding: 50px 0px;">ยังไม่มีรายการ COVID-19
                        information</p>
                @endif
            </div>
        </div>

        <!-- End NewsTopPageRight -->
    </div>
    <!-- End NewsTopPage -->


    <!-- SeminarPageListSlide -->
    <div class="SeminarPageListSlideForBuyer clr" style="background-color: #fff;">

        <div class="MarTitleSeminar"><p class="TitleTail TitleToppage"><span
                        class="FontItalic">Online Seminar</span><span id="seminarupdate">สัมมนาออนไลน์
            <img src="{{ asset('frontend/assets/images/th-top-page.svg') }}"></span></p>
        </div>
        @if(!empty($seminar_th_first))

            <div class="LatestSeminarBig">
                <div class="RightWidthBox">
                    <div class="PictureDisplay"><a
                                href="{{ route('seminar.show',$seminar_th_first->ref_id) }}"><span></span>
                            <div class="inner"
                                 style="background-image:url('{{ asset($seminar_th_first->thumbnail) }}');background-size: cover; background-position: center center;">
                                <div class="BGS"></div>
                        </a>
                    </div>
                </div>
                <div class="RightCaptions">
                    <div class="New"><img src="{{ asset('assets/images/icon/icon-new.svg') }}"></div>
                    <span>{{ $seminar_th_first->list ?? "ครั้งที่ $seminar_th_first->id"  }}</span>
                    <p class="Title clr">{{ $seminar_th_first->sub_title }}</p>
                    <a href="{{ route('seminar.show',$seminar_th_first->ref_id) }}" class="DetailLink">
                        ดูรายละเอียด >
                    </a>
                </div>
            </div>
    </div>

    <ul class="TopPageSeminarSlideBoxForBuyer">
        @foreach($seminars_th as $item_seminar)
            <li>
                <a href="{{ route('seminar.show',$item_seminar->ref_id) }}">
                    <div class="PictureDisplay"><span></span>
                        <div class="inner"
                             style="background-image:url('{{ asset($item_seminar->thumbnail) }}');background-size: cover; background-position: center center;">
                            <div class="BGS"></div>
                        </div>
                    </div>
                    <div class="RightCaptions">
                        @if(!empty($item_seminar->list))
                            <span>{{ $item_seminar->list ?? "ครั้งที่ $item_seminar->id"  }}</span>
                        @endif
                        <p class="Title clr">{{ $item_seminar->sub_title }}</p>
                        <a href="{{ route('seminar.show',$item_seminar->ref_id) }}"
                           class="DetailLink">ดูรายละเอียด
                            ></a>
                    </div>
                </a>
            </li>
        @endforeach
        {{--@if($seminars_th->count() > 0)
            @for ($i = 6; $i > $seminars_th->count(); $i--)
                <li class="ComingSoon"></li>
            @endfor
        @endif--}}

    </ul>
    @else
        <p style="color: red; text-align: center; padding: 50px 0px;">
            ยังไม่มีรายการสัมมนาออนไลน์ย้อนหลัง
        </p>
    @endif

    <div class="BTNLink BTNLink2">
        <a href="{{ route('seminar-list') }}">ดูสัมมนาออนไลน์อื่นๆ <img
                    src="{{ asset('assets/images/icon/icon-arrow-right-white2.svg') }}">
        </a>
    </div>

    </div>
    <!-- SeminarPageListSlide -->


    @if(Auth::guard('seller')->check())
        <!-- SeminarPageList สำหรับ Seller -->
        <div class="SeminarPageListSlide clr" style="background-color: #fff;">
            <div class="MarTitleSeminar"><p class="TitleTail TitleToppage TitleToppageSeller"><span class="FontItalic">Online Seminar</span><span
                            id="seminarupdate">オンラインセミナー</span></p></div>
            @if($seminars_jp->count() > 0)
                <ul class="TopPageSeminarSlideBox">
                    @foreach($seminars_jp as $item_seminar)
                        <li>
                            <a href="{{ route('seminar.show',$item_seminar->ref_id) }}">
                                <div class="PictureDisplay"><span></span>
                                    <div class="inner"
                                         style="background-image:url('{{ asset($item_seminar->thumbnail) }}');background-size: cover; background-position: center center;">
                                        <div class="BGS"></div>
                                    </div>
                                </div>
                                <div class="RightCaptions Sellers">
                                    @if(!empty($item_seminar->list))
                                        <span class="SpanSeller">{{ $item_seminar->list ?? "ครั้งที่ $item_seminar->id"  }}</span>
                                    @endif
                                    <p class="Title clr">{{ $item_seminar->sub_title }}</p>
                                    {{--@if(!empty($item_seminar->display_time))
                                        <p class="TimeDisplay">{{ $item_seminar->display_time }}</p>
                                    @endif
                                    @if(!empty($item_seminar->pdf1) || !empty($item_seminar->pdf2))
                                        <p class="DocumentDisplay">資料付き</p>
                                    @endif--}}
                                    <a href="{{ route('seminar.show',$item_seminar->ref_id) }}" class="DetailLink">詳細
                                        ></a>
                                </div>
                            </a>
                        </li>
                    @endforeach
                    @php
                        $max = ($seminars_jp->count() <= 3) ? 3 : 6;
                    @endphp
                    @for ($i = $max; $i > $seminars_jp->count(); $i--)
                        <li class="ComingSoon"></li>
                    @endfor

                </ul>
            @else
                <p style="color: red; text-align: center; padding: 50px 0px;">Sorry, Don't have Online seminar.</p>
            @endif
        </div>
        <!-- ENd SeminarPageListSlide สำหรับ Seller -->
    @endif


    <!-- TopPageLink -->
    @if($groups->count() > 0)
        <div class="TopPageLink clr">
            <div class="BGTitleLink">
                <div class="TitleLink" id="usefullinks">
                    <h2><span>Useful Links</span>ข้อมูลที่น่าสนใจอื่นๆ</h2>
                </div>
            </div>
            <!-- CategoryBoxTP -->
            @foreach($groups as $group)
                @if($group->useful_links->count())
                    <div class="CategoryBoxTP clr">
                        <h3>{{ $group->title }}</h3>
                        <ul>
                            @foreach($group->useful_links()->published()->get() as $useful_link)

                                <li>
                                    <a href="{{ $useful_link->url }}" target="_blank">
                                        <div class="PictureDisplay">
                                            <div class="inner"
                                                 style="background-image:url('{{ asset($useful_link->thumbnail_photo) }}');background-size: cover; background-position: center center;"></div>
                                        </div>
                                        <div id="iconUseful">
                                            <div id="iconUsefulTXT">
                                                <span style=" background: url('{{ asset($useful_link->icon) }}')no-repeat; background-size: contain; background-position: center center;"></span>
                                                {{ $useful_link->title }}
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            @endforeach
        </div>
    @endif
    <!-- End TopPageLink -->

@endsection