@extends('frontend.app')
@section('content')
    <!-- SeminarPage -->
    <div class="SeminarPage">

        @include('frontend.includes.breadcrumbs-master')

        <h2>{{ $seminar->title }}</h2>

        <!-- SeminarPageBox -->
        <div class="SeminarPageBox clr">
            @if(!empty($seminar->video))
                <div class="SeminarPageBoxLeft">
                    <div class="YoutubeSeminar">
                        {!! $seminar->video !!}
                    </div>
                </div>
            @endif
            <div class="SeminarPageBoxRight">
                <div class="PaddingBox">
                    <h1>{{ $seminar->sub_title }}</h1>
                    <p class="DisplayDate">{{ $seminar->display_date }}</p>
                    <div class="DisplayTime">{{ $seminar->display_time }}</div>
                </div>
            </div>
        </div>
        <!-- En SeminarPageBox -->
    </div>
    <!-- SeminarPage -->

    <!-- ContentBox -->
    <div class="ContentBox clr">
        <!-- SellerInfoDetail -->
        <div class="NewsBoxDetail clr">
            <div class="SubjectSeminar">{{ $seminar->subject_name }}</div>
            <!-- NewsBoxDetailBox -->
            <div class="NewsBoxDetailBox">
                {!! nl2br($seminar->detail) !!}
            </div>
            <!-- End NewsBoxDetailBox -->

            @if(!empty($seminar->pdf1) || !empty($seminar->pdf2))
                <!-- CompanyProfileList -->
                <div class="CompanyProfileList clr">
                    <h2>เอกสารประกอบ</h2>
                    @if(!empty($seminar->pdf1))
                        <p class="DownloadPDF">
                            <a href="{{ asset($seminar->pdf1) }}" target="_blank">
                                <span>Download PDF</span></a><br> {{ $seminar->pdf_title1 }}
                            <label>{{ formatSizeUnits($seminar->pdf_size1) }}</label></p>
                    @endif
                    @if(!empty($seminar->pdf2))
                        <p class="DownloadPDF">
                            <a href="{{ asset($seminar->pdf2) }}"
                               target="_blank">
                                <span>Download PDF</span></a><br> {{ $seminar->pdf_title2 }}
                            <label>{{ formatSizeUnits($seminar->pdf_size2) }}</label></p>
                    @endif
                    @if(!empty($seminar->pdf3))
                        <p class="DownloadPDF">
                            <a href="{{ asset($seminar->pdf3) }}"
                               target="_blank">
                                <span>Download PDF</span></a><br> {{ $seminar->pdf_title3 }}
                            <label>{{ formatSizeUnits($seminar->pdf_size3) }}</label></p>
                    @endif
                </div>
                <!-- End CompanyProfileList -->
            @endif

        </div>
        <!-- End NewsBoxDetail -->
    </div>
    <!-- End ContentBox -->

    @if($target == 'seller')

    {{--<div class="BTNLinkAll"><a href="{{ route('seminar-list') }}">ดูสัมมนาออนไลน์ทั้งหมด <img src="{{ asset('assets/images/icon/icon-arrow-right-white2.svg') }}"></a></div>--}}

        <!-- SeminarPageList สำหรับ Seller -->
        <div class="SeminarPageListSlide clr">
            <p class="TitleTail TitleTailSeller">オンラインセミナー</p>
            <ul class="RelatedSeminarSlideBox">
                @foreach($seminars_other as $item)
                    <li><a href="{{ route('seminar.show',$item->ref_id) }}">
                            <div class="PictureDisplay"><span></span>
                                <div class="inner"
                                     style="background-image:url('{{ asset($item->thumbnail) }}');background-size: cover; background-position: center center;">
                                    <div class="BGS"></div>
                                </div>
                            </div>
                            <div class="RightCaptions">
                                @if(!empty($item->list))
                                    <span>{{ $item->list ?? "ครั้งที่ $item_seminar->id" }}</span>
                                @endif
                                <p class="Title clr">{{ $item->sub_title }}</p>
                                {{--@if(!empty($item->display_time))
                                    <p class="TimeDisplay">{{ $item->display_time }}</p>
                                @endif
                                @if(!empty($item->pdf1) || !empty($item->pdf2))
                                    <p class="DocumentDisplay">資料付き</p>
                                @endif--}}
                                <a href="{{ route('seminar.show',$item->ref_id) }}" class="DetailLink">詳細
                                    ></a>
                            </div>
                        </a>
                    </li>
                @endforeach
                {{--@if($seminars_other->count() <= 4)
                    @for ($i = 4; $i > $seminars_other->count(); $i--)
                        <li class="ComingSoon"></li>
                    @endfor
                @else
                    @for ($i = 8; $i > $seminars_other->count(); $i--)
                        <li class="ComingSoon"></li>
                    @endfor
                @endif--}}

            </ul>
        </div>
        <!-- ENd SeminarPageList สำหรับ Seller -->
    @else

        <div class="BTNLinkAll"><a href="{{ route('seminar-list') }}">ดูสัมมนาออนไลน์ทั้งหมด <img src="{{ asset('assets/images/icon/icon-arrow-right-white2.svg') }}"></a></div>

        @if($seminars_other->count() > 0)
            <!-- SeminarPageListSlide -->
            <div class="SeminarPageListSlide clr">
                <div class="BorderAll">
                    <p class="CaptionTail">Find out more!</p>
                    <p class="TitleTail">ดูสัมมนาออนไลน์ครั้งถัดไป</p>
                </div>
                <!--<img src="{{ asset('frontend/assets/images/th-top-page.svg') }}">-->
                <ul class="RelatedSeminarSlideBox">
                    @foreach($seminars_other as $item)
                        <li><a href="{{ route('seminar.show',$item->ref_id) }}">
                                <div class="PictureDisplay"><span></span>
                                    <div class="inner"
                                         style="background-image:url('{{ asset($item->thumbnail) }}');background-size: cover; background-position: center center;">
                                        <div class="BGS"></div>
                                    </div>
                                </div>
                                <div class="RightCaptions">
                                    @if(!empty($item->list))
                                        <span>{{ $item->list ?? "ครั้งที่ $item_seminar->id" }}</span>
                                    @endif
                                    <p class="Title clr">{{ $item->sub_title }}</p>
                                    {{--@if(!empty($item->display_time))
                                        <p class="TimeDisplay">{{ $item->display_time }}</p>
                                    @endif
                                    @if(!empty($item->pdf1) || !empty($item->pdf2))
                                        <p class="DocumentDisplay">มีเอกสารประกอบ</p>
                                    @endif--}}
                                    <a href="{{ route('seminar.show',$item->ref_id) }}" class="DetailLink">ดูรายละเอียด
                                        ></a>
                                </div>
                            </a>
                        </li>
                    @endforeach
                    {{--@if($seminars_other->count() <= 4)
                        @for ($i = 4; $i > $seminars_other->count(); $i--)
                            <li class="ComingSoon"></li>
                        @endfor
                    @else
                        @for ($i = 8; $i > $seminars_other->count(); $i--)
                            <li class="ComingSoon"></li>
                        @endfor
                    @endif--}}
                </ul>
            </div>
            <!-- ENd SeminarPageListSlide -->
        @endif
    @endif

@endsection