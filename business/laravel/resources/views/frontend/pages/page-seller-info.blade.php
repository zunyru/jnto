@extends('frontend.app')
@push('css')
    <style>
        .error {
            color: red;
        }
    </style>
@endpush
@section('content')
    @include('frontend.includes.breadcrumbs',$company)
    <!-- ContentBoxFull -->
    <div class="ContentBoxFull clr">
        <!-- SellerInfoPage -->
        <div class="SellerInfoPage clr">
            <h2 class="CompanyName">{{ $company->name_en }}</h2>
            <!-- SlidePhoto -->
            {{--<div id="gal-results" class="containerPhotoSlide">
                <div id="gallery-view">
                    <div id="gal-slide">
                        @if(!empty($company->service_information->main_photo))
                            <div class="gal-slide-img"
                                 style="background: url('{{ asset($company->service_information->main_photo) }}') no-repeat; background-size: cover; background-repeat: no-repeat; background-position: center center;">
                            </div>
                        @endif
                        @if(!empty($company->service_information->other_photo))
                            <div class="gal-slide-img"
                                 style="background: url('{{ asset($company->service_information->other_photo) }}') no-repeat; background-size: cover; background-repeat: no-repeat; background-position: center center;">
                            </div>
                        @endif
                        @if(!empty($company->service_information->other_photo2))
                            <div class="gal-slide-img"
                                 style="background: url('{{ asset($company->service_information->other_photo2) }}') no-repeat; background-size: cover; background-repeat: no-repeat; background-position: center center;">
                            </div>
                        @endif
                        @if(!empty($company->service_information->other_photo3))
                            <div class="gal-slide-img"
                                 style="background: url('{{ asset($company->service_information->other_photo3) }}') no-repeat; background-size: cover; background-repeat: no-repeat; background-position: center center;">
                            </div>
                        @endif
                    </div>
                </div>
                <div id="gallery-thumbs">
                    <div id="gal-thumb">
                        @if(!empty($company->service_information->main_photo))
                            <div class="gal-slide-thumb"
                                 style=" background: url('{{ asset($company->service_information->main_photo) }}')no-repeat; background-size: cover; background-repeat: no-repeat; background-position: center center;">
                            </div>
                        @endif
                        @if(!empty($company->service_information->other_photo))
                            <div class="gal-slide-thumb"
                                 style=" background: url('{{ asset($company->service_information->other_photo) }}')no-repeat; background-size: cover; background-repeat: no-repeat; background-position: center center;">
                            </div>
                        @endif
                        @if(!empty($company->service_information->other_photo2))
                            <div class="gal-slide-thumb"
                                 style=" background: url('{{ asset($company->service_information->other_photo2) }}')no-repeat; background-size: cover; background-repeat: no-repeat; background-position: center center;">
                            </div>
                        @endif
                        @if(!empty($company->service_information->other_photo3))
                            <div class="gal-slide-thumb"
                                 style=" background: url('{{ asset($company->service_information->other_photo3) }}')no-repeat; background-size: cover; background-repeat: no-repeat; background-position: center center;">
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- End SlidePhoto -->
            --}}
        </div>
        <!-- End SellerInfoPage -->
    </div>
    <!-- End ContentBoxFull -->

    <!-- SupportMenuMobile -->
    @if(!empty($company->support_menu->support_menu1)
              || !empty($company->support_menu->support_menu2)
              || !empty($company->support_menu->support_menu3)
              || !empty($company->support_menu->support_menu4)
              || !empty($company->support_menu->support_menu5))
        <div class="SupportMenuMobile clr">
            <div class="SupportMenuMobileBox BTNSupportMenuMobile">
                <a href="#GotoSupportMenu" class="BTNSM BTNSM-4">
                    <span>มีโปรแกรมสนับสนุน</span>
                </a>
            </div>
        </div>
    @endif
    <!-- End SupportMenuMobile -->

    <!-- ContentBox -->
    <div class="ContentBox clr">
        <!-- ContentBoxLeft -->
        <div class="ContentBoxLeft clr">

            <!-- SlidePhoto V2 -->
            <div id="gal-results" class="containerPhotoSlide clr">
                <div id="gallery-view">
                    <div id="gal-slide">
                        @if(!empty($company->service_information->main_photo))
                            <div class="gal-slide-img"
                                 style="background: url('{{ asset($company->service_information->main_photo) }}') no-repeat; background-size: contain; background-repeat: no-repeat; background-position: center center;">
                                @if(!empty($company->service_information->download_main_photo))
                                    <a href="{{ asset($company->service_information->main_photo) }}" target="_blank"
                                       class="PicBigLeft">
                                        <img src="{{ asset('assets/images/icon/icon-download-pic-big.svg') }}">
                                    </a>
                                @endif
                            </div>
                        @endif
                        @if(!empty($company->service_information->other_photo))
                            <div class="gal-slide-img"
                                 style="background: url('{{ asset($company->service_information->other_photo) }}') no-repeat; background-size: contain; background-repeat: no-repeat; background-position: center center;">
                                @if(!empty($company->service_information->download_other_photo))
                                    <a href="{{ asset($company->service_information->other_photo) }}" target="_blank"
                                       class="PicBigLeft">
                                        <img src="{{ asset('assets/images/icon/icon-download-pic-big.svg') }}">
                                    </a>
                                @endif
                            </div>
                        @endif
                        @if(!empty($company->service_information->other_photo2))
                            <div class="gal-slide-img"
                                 style="background: url('{{ asset($company->service_information->other_photo2) }}') no-repeat; background-size: contain; background-repeat: no-repeat; background-position: center center;">
                                @if(!empty($company->service_information->download_other_photo2))
                                    <a href="{{ asset($company->service_information->other_photo2) }}" target="_blank"
                                       class="PicBigLeft">
                                        <img src="{{ asset('assets/images/icon/icon-download-pic-big.svg') }}">
                                    </a>
                                @endif
                            </div>
                        @endif
                        @if(!empty($company->service_information->other_photo3))
                            <div class="gal-slide-img"
                                 style="background: url('{{ asset($company->service_information->other_photo3) }}') no-repeat; background-size: contain; background-repeat: no-repeat; background-position: center center;">
                                @if(!empty($company->service_information->download_other_photo3))
                                    <a href="{{ asset($company->service_information->other_photo3) }}" target="_blank"
                                       class="PicBigLeft">
                                        <img src="{{ asset('assets/images/icon/icon-download-pic-big.svg') }}">
                                    </a>
                                @endif
                            </div>
                        @endif
                    </div>
                </div>
                <div id="gallery-thumbs">
                    <div id="gal-thumb">
                        @if(!empty($company->service_information->main_photo))
                            <div class="gal-slide-thumb"
                                 style=" background: url('{{ asset($company->service_information->main_photo) }}')no-repeat; background-size: cover; background-repeat: no-repeat; background-position: center center;">
                                @if(!empty($company->service_information->download_main_photo))
                                    <a href="{{ asset($company->service_information->main_photo) }}" target="_blank"
                                       class="PicSmallRight">
                                        <img src="{{ asset('assets/images/icon/icon-download-pic-small.svg') }}">
                                    </a>
                                @endif
                            </div>
                        @endif
                        @if(!empty($company->service_information->other_photo))
                            <div class="gal-slide-thumb"
                                 style=" background: url('{{ asset($company->service_information->other_photo) }}')no-repeat; background-size: cover; background-repeat: no-repeat; background-position: center center;">
                                @if(!empty($company->service_information->download_other_photo))
                                    <a href="{{ asset($company->service_information->other_photo) }}" target="_blank"
                                       class="PicSmallRight"><img
                                                src="{{ asset('assets/images/icon/icon-download-pic-small.svg') }}">
                                    </a>
                                @endif
                            </div>
                        @endif
                        @if(!empty($company->service_information->other_photo2))
                            <div class="gal-slide-thumb"
                                 style=" background: url('{{ asset($company->service_information->other_photo2) }}')no-repeat; background-size: cover; background-repeat: no-repeat; background-position: center center;">
                                @if(!empty($company->service_information->download_other_photo2))
                                    <a href="{{ asset($company->service_information->other_photo2) }}" target="_blank"
                                       class="PicSmallRight"><img
                                                src="{{ asset('assets/images/icon/icon-download-pic-small.svg') }}">
                                    </a>
                                @endif
                            </div>
                        @endif
                        @if(!empty($company->service_information->other_photo3))
                            <div class="gal-slide-thumb"
                                 style=" background: url('{{ asset($company->service_information->other_photo3) }}')no-repeat; background-size: cover; background-repeat: no-repeat; background-position: center center;">
                                @if(!empty($company->service_information->download_other_photo3))
                                    <a href="{{ asset($company->service_information->other_photo3) }}" target="_blank"
                                       class="PicSmallRight"><img
                                                src="{{ asset('assets/images/icon/icon-download-pic-small.svg') }}">
                                    </a>
                                @endif
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <!-- End SlidePhoto V2 -->

            @if(!empty($company->service_information->download_condition))
                <!-- ConditionOfPicture -->
                <div class="ConditionOfPicture clr">
                    <p class="ConditionTitle"><img src="{{ asset('assets/images/icon/icon-information.svg') }}">
                        เงื่อนไขในการใช้รูปภาพ</p>
                    <div class="ConditionDetail">
                        {!! $company->service_information->download_condition !!}
                    </div>
                </div>
                <!-- ConditionOfPicture -->
            @endif


            @if($company->topic)
                {{--@if($company->topic->updated_at >= @$start_event->value)--}}
                    @if($company->topic->status == 'done')

                        <!-- Pahse2 TippicBox -->
                        <div class="TippicBox clr" id="Toppic">
                            <p class="TippicBoxPic">
                                @if(!empty($company->topic->photo))
                                    <img src="{{ asset($company->topic->photo) }}">
                                @endif
                            </p>
                            @if($company->topic->category == 'event')
                                <p class="ToppicType">
                                    <img src="{{ asset('assets/images/icon/button-topic-event.svg') }}">
                                    เริ่มวันที่ {{ formatDateThai($company->topic->event_date_start) }}
                                    - {{ formatDateThai($company->topic->event_date_end) }}
                                </p>
                            @else
                                <!-- ถ้าเป็น Toppic News -->
                                <p class="ToppicType"><img
                                            src="{{ asset('assets/images/icon/button-topic-news.svg') }}">
                                </p>
                            @endif
                            <p class="Title">
                                {{ $company->topic->title }}</p>
                            <div class="Descriptions">
                                {!! preg_replace("/<br.*><br.*>/U", "<br>", nl2br($company->topic->detail))!!}
                            </div>
                            <p class="Date">อัพเดทล่าสุดเมื่อ {{ formatDateThai($company->topic->updated_at) }}</p>
                        </div>
                        <!-- Pahse2 TippicBox -->
                    @endif
                {{--@endif--}}
            @endif


            <!-- YoutubeResponsive -->
            @if(!empty($company->service_information->pr_youtube))
                <div class="YoutubeResponsive">
                    @if(LaravelVideoEmbed::parse($company->service_information->pr_youtube, ['YouTube']))
                        {!! LaravelVideoEmbed::parse($company->service_information->pr_youtube, ['YouTube']) !!}
                    @else
                        {!! $company->service_information->pr_youtube !!}
                    @endif
                </div>
            @endif
            <!-- End YoutubeResponsive -->
            @if(!empty($company->service_information->title))
                <h1>{{ $company->service_information->title }}</h1>
            @endif
            <!-- SellerInfoDetail -->
            @if(!empty($company->service_information->detail))
                <div class="SellerInfoDetail clr">
                    {!! nl2br($company->service_information->detail) !!}
                </div>
            @endif
            <!-- End SellerInfoDetail -->
            <!-- SupportMenuList -->
            @if(!empty($company->support_menu->support_menu1)
                || !empty($company->support_menu->support_menu2)
                || !empty($company->support_menu->support_menu3)
                || !empty($company->support_menu->support_menu4)
                || !empty($company->support_menu->support_menu5)
                || !empty($company->support_menu->support_menu6)
                || !empty($company->support_menu->support_menu7)
                || !empty($company->support_menu->support_menu8)
                || !empty($company->support_menu->support_menu_about_pdf))

                <div class="SupportMenuList clr" id="GotoSupportMenu">
                    <h2>โปรแกรมสนับสนุน</h2>
                    @if(!empty($company->support_menu->support_menu1)
               || !empty($company->support_menu->support_menu2)
               || !empty($company->support_menu->support_menu3)
               || !empty($company->support_menu->support_menu4)
               || !empty($company->support_menu->support_menu5)
               || !empty($company->support_menu->support_menu6)
               || !empty($company->support_menu->support_menu7)
               || !empty($company->support_menu->support_menu8)
               )
                        <ul>
                            @if(!empty($company->support_menu->support_menu1))
                                <li>
                                    <p class="Title">{{ $company->support_menu->support_menu1_value->name_th }}</p>
                                    <p class="Captions">{{ $company->support_menu->manu_name1 }}</p>
                                </li>
                            @endif
                            @if(!empty($company->support_menu->support_menu2))
                                <li>
                                    <p class="Title">{{ $company->support_menu->support_menu2_value->name_th }}</p>
                                    <p class="Captions">{{ $company->support_menu->manu_name2 }}</p>
                                </li>
                            @endif
                            @if(!empty($company->support_menu->support_menu3))
                                <li>
                                    <p class="Title">{{ $company->support_menu->support_menu3_value->name_th }}</p>
                                    <p class="Captions">{{ $company->support_menu->manu_name3 }}</p>
                                </li>
                            @endif
                            @if(!empty($company->support_menu->support_menu4))
                                <li>
                                    <p class="Title">{{ $company->support_menu->support_menu4_value->name_th }}</p>
                                    <p class="Captions">{{ $company->support_menu->manu_name4 }}</p>
                                </li>
                            @endif
                            @if(!empty($company->support_menu->support_menu5))
                                <li>
                                    <p class="Title">{{ $company->support_menu->support_menu5_value->name_th }}</p>
                                    <p class="Captions">{{ $company->support_menu->manu_name5 }}</p>
                                </li>
                            @endif
                            @if(!empty($company->support_menu->support_menu6))
                                <li>
                                    <p class="Title">{{ $company->support_menu->support_menu6_value->name_th }}</p>
                                    <p class="Captions">{{ $company->support_menu->manu_name6 }}</p>
                                </li>
                            @endif
                            @if(!empty($company->support_menu->support_menu7))
                                <li>
                                    <p class="Title">{{ $company->support_menu->support_menu7_value->name_th }}</p>
                                    <p class="Captions">{{ $company->support_menu->manu_name7 }}</p>
                                </li>
                            @endif
                            @if(!empty($company->support_menu->support_menu8))
                                <li>
                                    <p class="Title">{{ $company->support_menu->support_menu8_value->name_th }}</p>
                                    <p class="Captions">{{ $company->support_menu->manu_name8 }}</p>
                                </li>
                            @endif
                        </ul>
                    @endif
                    @if(!empty($company->support_menu->support_menu_about_pdf))
                        <p class="DownloadPDF">
                            <a target="_blank" href="{{ asset($company->support_menu->support_menu_about_pdf) }}">
                                <span>Download PDF</span>
                            </a>
                            <br>
                            {{ !empty($company->support_menu->support_about) ?  $company->support_menu->support_about : "" }}
                            <label>{{ formatSizeUnits($company->support_menu->support_menu_about_pdf_size) }}</label>
                        </p>
                    @endif
                </div>
            @endif

            <!-- End SupportMenuList -->
            <!-- CompanyProfileList -->
            @if(!empty($company->service_information->company_profile_pdf)
            || !empty($company->service_information->company_profile_pdf2)
            || !empty($company->service_information->company_profile_pdf3))
                <div class="CompanyProfileList clr">
                    <h2>เอกสารแนะนำบริษัท/แค็ตตาล็อก</h2>
                    @if(!empty($company->service_information->company_profile_pdf))
                        <p class="DownloadPDF">
                            <a target="_blank" href="{{ asset($company->service_information->company_profile_pdf) }}">
                                <span>Download PDF</span>
                            </a>
                            <br>
                            {{ !empty($company->service_information->about) ?  $company->service_information->about : "" }}
                            <label>{{ formatSizeUnits($company->service_information->company_profile_pdf_size) }}</label>
                        </p>
                    @endif
                    @if(!empty($company->service_information->company_profile_pdf2))
                        <p class="DownloadPDF">
                            <a target="_blank" href="{{ asset($company->service_information->company_profile_pdf2) }}">
                                <span>Download PDF</span>
                            </a>
                            <br>
                            {{ !empty($company->service_information->about2) ?  $company->service_information->about2 : "" }}
                            <label>{{ formatSizeUnits($company->service_information->company_profile_pdf2_size) }}</label>
                        </p>
                    @endif
                    @if(!empty($company->service_information->company_profile_pdf3))
                        <p class="DownloadPDF">
                            <a target="_blank" href="{{ asset($company->service_information->company_profile_pdf3) }}">
                                <span>Download PDF</span>
                            </a>
                            <br>
                            {{ !empty($company->service_information->about3) ?  $company->service_information->about3 : "" }}
                            <label>{{ formatSizeUnits($company->service_information->company_profile_pdf3_size) }}</label>
                        </p>
                    @endif
                </div>
            @endif
            <!-- End CompanyProfileList -->
            <!-- OthersLinkList -->
            <div class="OthersLinkList clr">
                @if(!empty($company->service_information->photo_gallery_url)
                || !empty($company->service_information->photo_gallery_url2)
                || !empty($company->service_information->photo_gallery_url3))

                    <h2>ลิงค์ที่เกี่ยวข้อง</h2>
                    <ul>
                        @if(!empty($company->service_information->photo_gallery_url))
                            <li>
                                <p class="Captions">{{ $company->service_information->about_url }}</p>
                                <p class="LinkURL">
                                    <a href="{{ $company->service_information->photo_gallery_url }}"
                                       target="_blank">
                                        {{ $company->service_information->photo_gallery_url }}
                                    </a>
                                </p>
                                <a href="{{ $company->service_information->photo_gallery_url }}"
                                   class="LinkOut"
                                   target="_blank">
                                </a>
                            </li>
                        @endif
                        @if(!empty($company->service_information->photo_gallery_url2))
                            <li>
                                <p class="Captions">{{ $company->service_information->about_url2 }}</p>
                                <p class="LinkURL">
                                    <a href="{{ $company->service_information->photo_gallery_url2 }}"
                                       target="_blank">
                                        {{ $company->service_information->photo_gallery_url2 }}
                                    </a>
                                </p>
                                <a href="{{ $company->service_information->photo_gallery_url2 }}"
                                   target="_blank"
                                   class="LinkOut">
                                </a>
                            </li>
                        @endif
                        @if(!empty($company->service_information->photo_gallery_url3))
                            <li>
                                <p class="Captions">{{ $company->service_information->about_url3 }}</p>
                                <p class="LinkURL">
                                    <a href="{{ $company->service_information->photo_gallery_url3 }}"
                                       target="_blank">
                                        {{ $company->service_information->photo_gallery_url3 }}
                                    </a></p>
                                <a href="{{ $company->service_information->photo_gallery_url3 }}"
                                   class="LinkOut"
                                   target="_blank"></a>
                            </li>
                        @endif
                    </ul>
                @endif
                <div class="LastUpdate">แก้ไขล่าสุด {{ formatDateThai($company->updated_at) }}</div>
            </div>
            <!-- End OthersLinkList -->
        </div>
        <!-- ENd ContentBoxLeft -->

        <!-- ContentBoxRight -->
        @include('frontend.includes.content-box-rigth')
        <!-- End ContentBoxRight -->

        <!-- ContactUsPopupDisplay -->
        @include('frontend.includes.contact-company')
        <!-- End ContactUsPopupDisplay -->

        <!-- ContactUsBranchPopupDisplay -->
        @include('frontend.includes.contact-company-branch')
        <!-- End ContactUsPopupDisplay -->

        @include('frontend.includes.related-company-Info')

    </div>
    <!-- End ContentBox -->
@endsection