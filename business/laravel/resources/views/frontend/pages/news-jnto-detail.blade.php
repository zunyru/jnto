@extends('frontend.app')
@section('content')
    @include('frontend.includes.breadcrumbs-news-jnto')

    <!-- ContentBoxFull -->
    <div class="ContentBoxFull clr">
        <!-- SellerInfoPage -->
        <div class="SellerInfoPage clr">
            <h1 class="NewsTitle">{{ $news->title }}</h1>
        @if(!empty($news->photo1) || !empty($news->photo2) || !empty($news->photo3) || !empty($news->photo4))
            <!-- SlidePhoto -->
                <div id="gal-results" class="containerPhotoSlide">
                    <div id="gallery-view">
                        <div id="gal-slide">
                            @if(!empty($news->photo1))
                                <div class="gal-slide-img"
                                     style="background: url('{{ asset($news->photo1) }}') no-repeat; background-size: contain; background-repeat: no-repeat; background-position: center center;">
                                </div>
                            @endif
                            @if(!empty($news->photo2))
                                <div class="gal-slide-img"
                                     style="background: url('{{ asset($news->photo2) }}') no-repeat; background-size: contain; background-repeat: no-repeat; background-position: center center;">
                                </div>
                            @endif
                            @if(!empty($news->photo3))
                                <div class="gal-slide-img"
                                     style="background: url('{{ asset($news->photo3) }}') no-repeat; background-size: contain; background-repeat: no-repeat; background-position: center center;">
                                </div>
                            @endif
                            @if(!empty($news->photo4))
                                <div class="gal-slide-img"
                                     style="background: url('{{ asset($news->photo4) }}') no-repeat; background-size: contain; background-repeat: no-repeat; background-position: center center;">
                                </div>
                            @endif
                        </div>
                    </div>
                    <div id="gallery-thumbs">
                        <div id="gal-thumb">
                            @if(!empty($news->photo1))
                                <div class="gal-slide-thumb"
                                     style=" background: url('{{ asset($news->photo1) }}')no-repeat; background-size: cover; background-repeat: no-repeat; background-position: center center;">
                                </div>
                            @endif
                            @if(!empty($news->photo2))
                                <div class="gal-slide-thumb"
                                     style=" background: url('{{ asset($news->photo2) }}')no-repeat; background-size: cover; background-repeat: no-repeat; background-position: center center;">
                                </div>
                            @endif
                            @if(!empty($news->photo3))
                                <div class="gal-slide-thumb"
                                     style=" background: url('{{ asset($news->photo3) }}')no-repeat; background-size: cover; background-repeat: no-repeat; background-position: center center;">
                                </div>
                            @endif
                            @if(!empty($news->photo4))
                                <div class="gal-slide-thumb"
                                     style=" background: url('{{ asset($news->photo4) }}')no-repeat; background-size: cover; background-repeat: no-repeat; background-position: center center;">
                                </div>
                            @endif
                        </div>
                    </div>
                    <!-- End SlidePhoto -->
                </div>
        @endif
        <!-- End SellerInfoPage -->
        </div>
        <!-- End ContentBoxFull -->

        <!-- ContentBox -->
        <div class="ContentBox clr">
            <!-- SellerInfoDetail -->
            <div class="NewsBoxDetail clr">
                <!-- NewsBoxDetailBox -->
                <div class="NewsBoxDetailBox">
                    {!! nl2br($news->detail) !!}
                </div>
                <!-- End NewsBoxDetailBox -->

            @if(!empty($news->pdf1) || !empty($news->pdf2) || !empty($news->pdf3))
                <!-- CompanyProfileList -->
                    <div class="CompanyProfileList clr">
                        <h2>รายละเอียดเพิ่มเติม</h2>
                        @if(!empty($news->pdf1))
                            <p class="DownloadPDF">
                                <a target="_blank" href="{{ asset($news->pdf1) }}">
                                    <span>Download file</span>
                                </a><br> {{ $news->file_description1 }}
                                <label>{{ formatSizeUnits($news->pdf_size1) }}</label></p>
                        @endif
                        @if(!empty($news->pdf2))
                            <p class="DownloadPDF">
                                <a target="_blank" href="{{ asset($news->pdf2) }}">
                                    <span>Download file</span>
                                </a><br>
                                {{ $news->file_description2 }}
                                <label>{{ formatSizeUnits($news->pdf_size2) }}</label></p>
                        @endif
                        @if(!empty($news->pdf3))
                            <p class="DownloadPDF">
                                <a target="_blank" href="{{ asset($news->pdf3) }}">
                                    <span>Download file</span>
                                </a><br>{{ $news->file_description3 }}
                                <label>{{ formatSizeUnits($news->pdf_size3) }}</label></p>
                        @endif
                    </div>
                    <!-- End CompanyProfileList -->
                @endif

            </div>
            <!-- End NewsBoxDetail -->
        </div>
        <!-- End ContentBox -->

        <!-- NewsBox -->
        @if($news_other->count() > 0)
            <div class="NewsBox clr">
                <p class="RelatedNewsJNTO">ข่าวสารจาก JNTO <span>JNTO News</span></p>
                <ul>
                    @foreach($news_other as $item)
                        <li>
                            <span>{{ formatDateThai($item->created_at) }}</span>
                            @if($item->template == 'full-content')
                                <a 
                                   href="{{ route('news-jnto.show',$item->ref_id) }}">{{ $item->title }}</a>
                            @endif
                            @if($item->template == 'link-only')
                                <a 
                                   href="{{ $item->link }}">{{ $item->title }}</a>
                            @endif
                            @if($item->template == 'PDF-only')
                                <a 
                                   href="{{ asset($item->pdf_only) }}">{{ $item->title }}</a>
                            @endif
                        </li>
                    @endforeach
                </ul>
                <p class="LinktoAll"><a href="{{ route('news-jnto.all') }}">กลับไปที่หน้ารวมข่าวสารจาก
                        JNTO</a></p>
            </div>
            <!-- End NewsBox -->
    @endif

@endsection