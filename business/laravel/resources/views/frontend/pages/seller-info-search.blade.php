@extends('frontend.app')
@push('css')
    <link href="{{ asset('css/placeholder.css') }}" rel="stylesheet">
@endpush
@section('content')

    <!-- SearchForm -->
    <div class="SearchForm">

        @include('frontend.includes.breadcrumbs-master')

        <!-- SearchFormBox -->
        <div class="SearchFormBox clr">

            <p class="Caption">Discover new partners!</p>
            <h2>ค้นหาพาร์ทเนอร์ฝ่ายญี่ปุ่น</h2>

            <!-- FormBox -->
            <div class="FormBox">
                <!-- FormBoxLeft -->
                <div class="FormBoxLeft">
                    <div class="PaddingMap">
                        <!-- MapRegion -->
                        <div class="MapRegion">
                            <p class="MapSearchTxt" style="margin-top: -5px;">ค้นหาจากสถานที่ <span>คลิกภูมิภาคที่ต้องการ</span>
                            </p>
                            <img src="{{ asset('frontend/assets/images/bg-map/bg-map-cover.png') }}">
                            <a href="#HokkaidoBox" class='SectionClick HokkaidoBox'></a>
                            <a href="#TohokuBox" class='SectionClick TohokuBox'></a>
                            <a href="#KantoBox" class='SectionClick KantoBox'></a>
                            <a href="#HokurikuShinetsuBox" class='SectionClick HokurikuShinetsuBox'><span></span></a>
                            <a href="#ChubuBox" class='SectionClick ChubuBox'><span></span></a>
                            <a href="#KansaiBox" class='SectionClick KansaiBox'><span></span></a>
                            <a href="#ChugokuBox" class='SectionClick ChugokuBox'></a>
                            <a href="#ShikokuBox" class='SectionClick ShikokuBox'></a>
                            <a href="#KyushuBox" class='SectionClick KyushuBox'></a>
                            <a href="#OkinawaBox" class='SectionClick OkinawaBox'></a>

                            <!-- HokkaidoBox -->
                            <div id="HokkaidoBox" class="SectionShow HokkaidoDisplay">
                                <div class="MapProvince">
                                    <p class="MapSearchTxt">ค้นหาจากสถานที่</p>
                                    <img src="{{ asset('frontend/assets/images/bg-map/bg-map-cover.png') }}">
                                    <p class="BacktoRegion">
                                        <a href="javascript:void(0)" class="back SectionClick">< กลับไป</a>
                                    </p>
                                    <div class="HokkaidoCheckbox">
                                        <input type="checkbox"
                                               name="Hokkaido"
                                               class="input_search"
                                               id="Hokkaido"
                                               value="Hokkaido">
                                        <label for="Hokkaido"></label>
                                    </div>
                                </div>
                            </div>
                            <!-- End HokkaidoBox -->

                            <!-- TohokuBox -->
                            <div id="TohokuBox" class="SectionShow TohokuDisplay">
                                <div class="MapProvince">
                                    <p class="MapSearchTxt">ค้นหาจากสถานที่</p>
                                    <img src="{{ asset('frontend/assets/images/bg-map/bg-map-cover.png') }}">
                                    <p class="BacktoRegion">
                                        <a href="javascript:void(0)" class="back SectionClick">< กลับไป</a></p>
                                    <div class="AomoriCheckbox">
                                        <input type="checkbox"
                                               name="Aomori"
                                               id="Aomori"
                                               class="input_search"
                                               value="Aomori">
                                        <label for="Aomori"></label>
                                    </div>
                                    <div class="AkitaCheckbox">
                                        <input type="checkbox"
                                               name="Akita"
                                               id="Akita"
                                               class="input_search"
                                               value="Akita">
                                        <label for="Akita"></label>
                                    </div>
                                    <div class="IwateCheckbox">
                                        <input type="checkbox"
                                               name="Iwate"
                                               id="Iwate"
                                               class="input_search"
                                               value="Iwate">
                                        <label for="Iwate"></label>
                                    </div>
                                    <div class="YamagataCheckbox">
                                        <input type="checkbox"
                                               name="Yamagata"
                                               class="input_search"
                                               id="Yamagata"
                                               value="Yamagata">
                                        <label for="Yamagata"></label>
                                    </div>
                                    <div class="MiyagiCheckbox">
                                        <input type="checkbox"
                                               name="Miyagi"
                                               class="input_search"
                                               id="Miyagi"
                                               value="Miyagi">
                                        <label for="Miyagi"></label>
                                    </div>
                                    <div class="FukushimaCheckbox">
                                        <input type="checkbox"
                                               name="Fukushima"
                                               class="input_search"
                                               id="Fukushima"
                                               value="Fukushima">
                                        <label for="Fukushima"></label>
                                    </div>
                                </div>
                            </div>
                            <!-- End TohokuBox -->

                            <!-- KantoBox -->
                            <div id="KantoBox" class="SectionShow KantoDisplay">
                                <div class="MapProvince">
                                    <p class="MapSearchTxt">ค้นหาจากสถานที่</p>
                                    <img src="{{ asset('frontend/assets/images/bg-map/bg-map-cover.png') }}">
                                    <p class="BacktoRegion">
                                        <a href="javascript:void(0)" class="back SectionClick">< กลับไป</a></p>
                                    <div class="GunmaCheckbox">
                                        <input type="checkbox"
                                               name="Gunma"
                                               id="Gunma"
                                               class="input_search"
                                               value="Gunma">
                                        <label for="Gunma"></label>
                                    </div>
                                    <div class="TochigiCheckbox">
                                        <input type="checkbox"
                                               name="Tochigi"
                                               class="input_search"
                                               id="Tochigi"
                                               value="Tochigi">
                                        <label for="Tochigi"></label>
                                    </div>
                                    <div class="IbarakiCheckbox">
                                        <input type="checkbox"
                                               name="Ibaraki"
                                               id="Ibaraki"
                                               class="input_search"
                                               value="Ibaraki">
                                        <label for="Ibaraki"></label>
                                    </div>
                                    <div class="SaitamaCheckbox">
                                        <input type="checkbox"
                                               name="Saitama"
                                               class="input_search"
                                               id="Saitama"
                                               value="Saitama">
                                        <label for="Saitama"></label>
                                    </div>
                                    <div class="YamanashiCheckbox">
                                        <input type="checkbox"
                                               name="Yamanashi"
                                               id="Yamanashi"
                                               class="input_search"
                                               value="Yamanashi">
                                        <label for="Yamanashi"></label>
                                    </div>
                                    <div class="TokyoCheckbox">
                                        <input type="checkbox"
                                               name="Tokyo"
                                               class="input_search"
                                               id="Tokyo"
                                               value="Tokyo">
                                        <label for="Tokyo"></label>
                                    </div>
                                    <div class="ChibaCheckbox">
                                        <input type="checkbox"
                                               name="Chiba"
                                               class="input_search"
                                               id="Chiba"
                                               value="Chiba">
                                        <label for="Chiba"></label>
                                    </div>
                                    <div class="KanagawaCheckbox">
                                        <input type="checkbox"
                                               name="Kanagawa"
                                               id="Kanagawa"
                                               class="input_search"
                                               value="Kanagawa">
                                        <label for="Kanagawa"></label>
                                    </div>
                                </div>
                            </div>
                            <!-- End KantoBox -->

                            <!-- HokurikuShinetsuBox -->
                            <div id="HokurikuShinetsuBox" class="SectionShow HokurikuShinetsuDisplay">
                                <div class="MapProvince">
                                    <p class="MapSearchTxt">ค้นหาจากสถานที่</p>
                                    <img src="{{ asset('frontend/assets/images/bg-map/bg-map-cover.png') }}">
                                    <p class="BacktoRegion"><a href="javascript:void(0)" class="back SectionClick"><
                                            กลับไป</a></p>
                                    <div class="IshikawaCheckbox">
                                        <input type="checkbox"
                                               name="Ishikawa"
                                               class="input_search"
                                               id="Ishikawa"
                                               value="Ishikawa">
                                        <label for="Ishikawa"></label>
                                    </div>
                                    <div class="ToyamaCheckbox">
                                        <input type="checkbox"
                                               name="Toyama"
                                               class="input_search"
                                               id="Toyama"
                                               value="Toyama">
                                        <label for="Toyama"></label>
                                    </div>
                                    <div class="NiigataCheckbox">
                                        <input type="checkbox"
                                               name="Niigata"
                                               class="input_search"
                                               id="Niigata"
                                               value="Niigata">
                                        <label for="Niigata"></label>
                                    </div>
                                    <div class="NaganoCheckbox">
                                        <input type="checkbox"
                                               name="Nagano"
                                               class="input_search"
                                               id="Nagano"
                                               value="Nagano">
                                        <label for="Nagano"></label>
                                    </div>
                                </div>
                            </div>
                            <!-- End HokurikuShinetsuBox -->

                            <!-- ChubuBox -->
                            <div id="ChubuBox" class="SectionShow ChubuDisplay">
                                <div class="MapProvince">
                                    <p class="MapSearchTxt">ค้นหาจากสถานที่</p>
                                    <img src="{{ asset('frontend/assets/images/bg-map/bg-map-cover.png') }}">
                                    <p class="BacktoRegion"><a href="javascript:void(0)" class="back SectionClick"><
                                            กลับไป</a></p>
                                    <div class="FukuiCheckbox">
                                        <input type="checkbox"
                                               name="Fukui"
                                               class="input_search"
                                               id="Fukui"
                                               value="Fukui">
                                        <label for="Fukui"></label>
                                    </div>
                                    <div class="GifuCheckbox">
                                        <input type="checkbox"
                                               name="Gifu"
                                               class="input_search"
                                               id="Gifu"
                                               value="Gifu">
                                        <label for="Gifu"></label>
                                    </div>
                                    <div class="ShizuokaCheckbox">
                                        <input type="checkbox"
                                               name="Shizuoka"
                                               class="input_search"
                                               id="Shizuoka"
                                               value="Shizuoka">
                                        <label for="Shizuoka"></label>
                                    </div>
                                    <div class="AichiCheckbox">
                                        <input type="checkbox"
                                               name="Aichi"
                                               class="input_search"
                                               id="Aichi"
                                               value="Aichi">
                                        <label for="Aichi"></label>
                                    </div>
                                    <div class="MieCheckbox">
                                        <input type="checkbox"
                                               name="Mie"
                                               class="input_search"
                                               id="Mie"
                                               value="Mie">
                                        <label for="Mie"></label>
                                    </div>
                                </div>
                            </div>
                            <!-- End ChubuBox -->

                            <!-- KansaiBox -->
                            <div id="KansaiBox" class="SectionShow KansaiDisplay">
                                <div class="MapProvince">
                                    <p class="MapSearchTxt">ค้นหาจากสถานที่</p>
                                    <img src="{{ asset('frontend/assets/images/bg-map/bg-map-cover.png') }}">
                                    <p class="BacktoRegion"><a href="javascript:void(0)" class="back SectionClick"><
                                            กลับไป</a></p>
                                    <div class="HyogoCheckbox">
                                        <input type="checkbox"
                                               name="Hyogo"
                                               id="Hyogo"
                                               class="input_search"
                                               value="Hyogo">
                                        <label for="Hyogo"></label>
                                    </div>
                                    <div class="KyotoCheckbox">
                                        <input type="checkbox"
                                               name="Kyoto"
                                               id="Kyoto"
                                               class="input_search"
                                               value="Kyoto">
                                        <label for="Kyoto"></label>
                                    </div>
                                    <div class="ShigaCheckbox">
                                        <input type="checkbox"
                                               name="Shiga"
                                               class="input_search"
                                               id="Shiga"
                                               value="Shiga">
                                        <label for="Shiga"></label>
                                    </div>
                                    <div class="NaraCheckbox">
                                        <input type="checkbox"
                                               name="Nara"
                                               class="input_search"
                                               id="Nara"
                                               value="Nara">
                                        <label for="Nara"></label>
                                    </div>
                                    <div class="OsakaCheckbox">
                                        <input type="checkbox"
                                               name="Osaka"
                                               class="input_search"
                                               id="Osaka"
                                               value="Osaka">
                                        <label for="Osaka"></label>
                                    </div>
                                    <div class="WakayamaCheckbox">
                                        <input type="checkbox"
                                               name="Wakayama"
                                               class="input_search"
                                               id="Wakayama"
                                               value="Wakayama">
                                        <label for="Wakayama"></label>
                                    </div>
                                </div>
                            </div>
                            <!-- End KansaiBox -->

                            <!-- ChugokuBox -->
                            <div id="ChugokuBox" class="SectionShow ChugokuDisplay">
                                <div class="MapProvince">
                                    <p class="MapSearchTxt">ค้นหาจากสถานที่</p>
                                    <img src="{{ asset('frontend/assets/images/bg-map/bg-map-cover.png') }}">
                                    <p class="BacktoRegion"><a href="javascript:void(0)" class="back SectionClick"><
                                            กลับไป</a></p>
                                    <div class="TottoriCheckbox">
                                        <input type="checkbox"
                                               name="Tottori"
                                               class="input_search"
                                               id="Tottori"
                                               value="Tottori">
                                        <label for="Tottori"></label>
                                    </div>
                                    <div class="ShimaneCheckbox">
                                        <input type="checkbox"
                                               name="Shimane"
                                               class="input_search"
                                               id="Shimane"
                                               value="Shimane">
                                        <label for="Shimane"></label>
                                    </div>
                                    <div class="YamaguchiCheckbox">
                                        <input type="checkbox"
                                               name="Yamaguchi"
                                               class="input_search"
                                               id="Yamaguchi"
                                               value="Yamaguchi">
                                        <label for="Yamaguchi"></label>
                                    </div>
                                    <div class="OkayamaCheckbox">
                                        <input type="checkbox"
                                               name="Okayama"
                                               class="input_search"
                                               id="Okayama"
                                               value="Okayama">
                                        <label for="Okayama"></label>
                                    </div>
                                    <div class="HiroshimaCheckbox">
                                        <input type="checkbox"
                                               name="Hiroshima"
                                               class="input_search"
                                               id="Hiroshima"
                                               value="Hiroshima">
                                        <label for="Hiroshima"></label>
                                    </div>
                                </div>
                            </div>
                            <!-- End ChugokuBox -->

                            <!-- ShikokuBox -->
                            <div id="ShikokuBox" class="SectionShow ShikokuDisplay">
                                <div class="MapProvince">
                                    <p class="MapSearchTxt">ค้นหาจากสถานที่</p>
                                    <img src="{{ asset('frontend/assets/images/bg-map/bg-map-cover.png') }}">
                                    <p class="BacktoRegion"><a href="javascript:void(0)" class="back SectionClick"><
                                            กลับไป</a></p>
                                    <div class="KagawaCheckbox">
                                        <input type="checkbox"
                                               name="Kagawa"
                                               class="input_search"
                                               id="Kagawa"
                                               value="Kagawa">
                                        <label for="Kagawa"></label>
                                    </div>
                                    <div class="TokushimaCheckbox">
                                        <input type="checkbox"
                                               name="Tokushima"
                                               class="input_search"
                                               id="Tokushima"
                                               value="Tokushima">
                                        <label for="Tokushima"></label>
                                    </div>
                                    <div class="EhimeCheckbox">
                                        <input type="checkbox"
                                               name="Ehime"
                                               class="input_search"
                                               id="Ehime"
                                               value="Ehime">
                                        <label for="Ehime"></label>
                                    </div>
                                    <div class="KochiCheckbox">
                                        <input type="checkbox"
                                               name="Kochi"
                                               class="input_search"
                                               id="Kochi"
                                               value="Kochi">
                                        <label for="Kochi"></label>
                                    </div>
                                </div>
                            </div>
                            <!-- End ShikokuBox -->

                            <!-- KyushuBox -->
                            <div id="KyushuBox" class="SectionShow KyushuDisplay">
                                <div class="MapProvince">
                                    <p class="MapSearchTxt">ค้นหาจากสถานที่</p>
                                    <img src="{{ asset('frontend/assets/images/bg-map/bg-map-cover.png') }}">
                                    <p class="BacktoRegion"><a href="javascript:void(0)" class="back SectionClick"><
                                            กลับไป</a></p>
                                    <div class="OitaCheckbox">
                                        <input type="checkbox"
                                               name="Oita"
                                               class="input_search"
                                               id="Oita"
                                               value="Oita">
                                        <label for="Oita"></label>
                                    </div>
                                    <div class="FukuokaCheckbox">
                                        <input type="checkbox"
                                               name="Fukuoka"
                                               class="input_search"
                                               id="Fukuoka"
                                               value="Fukuoka">
                                        <label for="Fukuoka"></label>
                                    </div>
                                    <div class="SagaCheckbox">
                                        <input type="checkbox"
                                               name="Saga"
                                               class="input_search"
                                               id="Saga"
                                               value="Saga">
                                        <label for="Saga"></label>
                                    </div>
                                    <div class="NagasakiCheckbox">
                                        <input type="checkbox"
                                               name="Nagasaki"
                                               class="input_search"
                                               id="Nagasaki"
                                               value="Nagasaki">
                                        <label for="Nagasaki"></label>
                                    </div>
                                    <div class="MiyazakiCheckbox">
                                        <input type="checkbox"
                                               name="Miyazaki"
                                               class="input_search"
                                               id="Miyazaki"
                                               value="Miyazaki">
                                        <label for="Miyazaki"></label>
                                    </div>
                                    <div class="KumamotoCheckbox">
                                        <input type="checkbox"
                                               name="Kumamoto"
                                               class="input_search"
                                               id="Kumamoto"
                                               value="Kumamoto">
                                        <label for="Kumamoto"></label>
                                    </div>
                                    <div class="KagoshimaCheckbox">
                                        <input type="checkbox"
                                               name="Kagoshima"
                                               class="input_search"
                                               id="Kagoshima"
                                               value="Kagoshima">
                                        <label for="Kagoshima"></label>
                                    </div>
                                </div>
                            </div>
                            <!-- End KyushuBox -->

                            <!-- OkinawaBox -->
                            <div id="OkinawaBox" class="SectionShow OkinawaDisplay">
                                <div class="MapProvince">
                                    <p class="MapSearchTxt">ค้นหาจากสถานที่</p>
                                    <img src="{{ asset('frontend/assets/images/bg-map/bg-map-cover.png') }}">
                                    <p class="BacktoRegion"><a href="javascript:void(0)" class="back SectionClick"><
                                            กลับไป</a></p>
                                    <div class="OkinawaCheckbox">
                                        <input type="checkbox"
                                               name="Okinawa"
                                               class="input_search"
                                               id="Okinawa"
                                               value="Okinawa">
                                        <label for="Okinawa"></label>
                                    </div>
                                </div>
                            </div>
                            <!-- End OkinawaBox -->
                        </div>
                    </div>
                    <!-- End MapRegion -->
                </div>
                <!-- End FormBoxLeft -->
                <!-- FormBoxRight -->
                <div class="FormBoxRight">
                    <div class="BusinessTypeList clr">
                        <p class="BType">ประเภทธุรกิจ</p>
                        <ul>
                            @foreach($business_types as $business_type)
                                <li><label>
                                        <input
                                                name="business_type[]"
                                                class="business_type"
                                                value="{{ $business_type->id }}"
                                                type="checkbox"> {{ $business_type->name_th }}</label>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- End FormBoxRight -->
                    <!-- SupportMenuListSearch -->
                    <div class="SupportMenuListSearch">
                        <select
                                name="support_menus"
                                class="support_menu"
                        >
                            <option value="">คลิกเลือกโปรแกรมสนับสนุน</option>
                            {{--                            <option value="none">ไม่มี</option>--}}
                            @foreach($support_menus as $support_menu)
                                <option value="{{ $support_menu->id }}">{{ $support_menu->name_th }}</option>
                            @endforeach
                        </select>
                    </div>
                    <!-- End SupportMenuListSearch -->
                    <!-- KeywordS -->
                    <div class="KeywordS clr"><label>
                            <input class="search_txt" name="search_txt" type="text" placeholder="ค้นหาด้วยชื่อบริษัท">
                        </label></div>
                    <!-- End KeywordS -->
                </div>
                <!-- End FormBoxRight -->
            </div>
            <!-- End FormBox -->
            <!-- BTNSearchPage -->
            <div class="BTNSearchPage clr">
                <button id="search" class="BTNN BTNN-4"><span>ค้นหา <img
                                src="{{ asset('frontend/assets/images/icon/icon-search-white.svg') }}"></span>
                </button>
            </div>
            <!-- End BTNSearchPage -->
        </div>
        <!-- En SearchFormBox -->

    </div>


    <!-- ContentBox -->
    <div class="RelatedCompanyInfo">
        <div class="ContentBox clr">
            <!-- CompanyList -->
            <div class="box-loader">
                <div class="loader">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div class="result">

            </div>
        </div>
    </div>
    </div>

@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            const provinces = {};
            @if(request()->section)
            let section = `{{ request()->section }}`;
            let search_arr = @json(request()->search);
            $(section).fadeToggle(200);
            $.each(search_arr, function (key, value) {
                $('#' + value).prop("checked", true);
                provinces[key] = $('#' + value).val();
            });
            @endif
            @if(request()->business_type)
            let business_type_arr = @json(request()->business_type);
            $.each(business_type_arr, function (key, value) {
                $.each($('.business_type'), function (index, data) {
                    if ($(this).val() == value) {
                        $(this).prop("checked", true);
                    }
                });
            });
            @endif
            @if(request()->support_menus)
            let support_menus = `{{ request()->support_menus }}`;
            $(".support_menu").val(support_menus).change();
            @endif
            @if(request()->search_txt)
            let search_txt = `{{ request()->search_txt }}`;
            $(".search_txt").val(search_txt);
            @endif

            loadData(provinces);

            $('.back').click(function () {
                $('.input_search').prop("checked", false)
            });

            function get_provinces(provinces) {

                $.each($('.input_search'), function (key, value) {
                    if ($(this).is(':checked')) {
                        provinces[key] = $(this).val();
                    }
                });
                return provinces;
            }

            $('#search').click(function () {
                const provinces = {};
                const data_provinces = get_provinces(provinces);
                loadData(data_provinces);
            });

            function loadData(provinces, page) {
                const support_menu = $('.support_menu').val();
                const business_type = {};
                const search_txt = $('.search_txt').val();
                $.each($('.business_type'), function (key, value) {
                    if ($(this).is(':checked')) {
                        business_type[key] = $(this).val();
                    }
                });

                $.ajax({
                    url: `{{ route('seller-info.get_search') }}`,
                    method: "POST",
                    data: {
                        _token: `{{ csrf_token() }}`,
                        page: page ?? 1,
                        length: 60,
                        province: provinces,
                        business_types: business_type,
                        support_menus: support_menu,
                        is_public: true,
                        search_keyword: search_txt,
                        order: true
                    },
                    beforeSend: function () {
                        $('.box-loader').show();
                        $('.result').hide();
                    },
                    success: function (response) {
                        $('.result').show();
                        $('.box-loader').hide();
                        $(".result").html(response);
                    }
                });
            }


            $(document).on("click", ".NavigatorList li a", function (e) {
                e.preventDefault();
                const provinces = {};
                const data_provinces = get_provinces(provinces);
                var page = $(this).attr("data-page");
                loadData(provinces, page);
            });

        });
    </script>
@endpush
