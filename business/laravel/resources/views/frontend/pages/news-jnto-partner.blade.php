@extends('frontend.app')
@section('content')
    <div class="NewsBoxBGBlur clr">
    @include('frontend.includes.breadcrumbs-news-jnto-partner',[false])

    <!-- NewsBoxHeadType -->
        <div class="NewsBoxHeadType clr">
            <ul>
                <li>
                    <a href="{{ route('news-jnto.all') }}"><span>JNTO News</span>ข่าวสารจาก JNTO</a>
                </li>
                <li class="Active">
                    <a href="{{ route('news-jnto-partner.all') }}"><span>JNTO Partners’ News</span>ข่าวสารจากองค์กรท่องเที่ยว</a>
                </li>
            </ul>
        </div>
        <!-- End NewsBoxHeadType -->
    </div>

    <!-- NewsBox -->
    <div class="NewsBox clr">
        @if($news->count() > 0)
            <ul>
                @foreach($news as $item)
                    <li>
                        <span>{{ formatDateThai($item->created_at) }}</span>
                        @if($item->template == 'full-content')
                            <a 
                               href="{{ route('news-jnto-partner.show',$item->ref_id) }}">{{ $item->title }}</a>
                        @endif
                        @if($item->template == 'link-only')
                            <a 
                               href="{{ $item->link }}">{{ $item->title }}</a>
                        @endif
                        @if($item->template == 'PDF-only')
                            <a 
                               href="{{ $item->pdf_only }}">{{ $item->title }}</a>
                        @endif
                    </li>
                @endforeach
            </ul>
        @else
            <div class="NewsTopPageLeftBody">
			<span id="JNTO_News" class="JNTO_News" style="display: inline;">
				<p style="color: red; text-align: center; padding: 50px 0px;">ยังไม่มีข่าวสารจาก JNTO</p>
            </span>
            </div>
        @endif
    </div>
    <!-- End NewsBox -->

    <!-- NavigatorList -->
    <div class="NavigatorList">
        {{ $news->links('vendor.pagination.custom') }}
    </div>
    <!-- End NavigatorList -->
@endsection