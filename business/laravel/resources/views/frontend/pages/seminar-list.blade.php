@extends('frontend.app')
@section('content')

{{--
    @if(Auth::guard('seller')->check())
    <div class="HeaderSeminarPageJP">
        <div class="Breadcrumbs clr">
            <a href="{{ route('home') }}">หน้าหลัก</a> &gt;สัมมนาออนไลน์
        </div>
        <div class="SeminarBoxTXT">
            <p><span>Online Seminar</span>オンラインセミナー</p>
        </div>
    </div>
    @endif

    @if(Auth::guard('seller')->check())
    @foreach($seminars_jp_group as $year => $seminars)
        <!-- SeminarPageListSlideAll -->
        <div class="SeminarPageListSlideForBuyer clr" style="background-color: #fff;">
            <p class="TitleYear2">สัมมนาออนไลน์ปี {{ $year }}</p>
            <ul class="TopPageSeminarSlideBoxForBuyer">
                @foreach($seminars as $seminar)
                    <li>
                        <a href="{{ route('seminar.show',$seminar->ref_id) }}">
                            <div class="PictureDisplay"><span></span>
                                <div class="inner"
                                     style="background-image:url('{{ asset($seminar->thumbnail) }}');background-size: cover; background-position: center center;">
                                    <div class="BGS"></div>
                                </div>
                            </div>
                            <div class="RightCaptions">
                                <span>{{ $seminar->list ?? "ครั้งที่ $seminar->id"  }}</span>
                                <p class="Title clr">{{ $seminar->sub_title }}</p>

                                <a href="{{ route('seminar.show',$seminar->ref_id) }}" class="DetailLink">ดูรายละเอียด
                                    ></a>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
        <!-- ENd SeminarPageListSlideAll -->
    @endforeach
    @endif
--}}


    <div class="HeaderSeminarPage" style="margin-top:0px;">
        <div class="Breadcrumbs clr">
            <a href="{{ route('home') }}">หน้าหลัก</a> &gt;สัมมนาออนไลน์
        </div>
        <div class="SeminarBoxTXT">
            <p><span>Online Seminar</span>สัมมนาออนไลน์ ปีงบประมาณล่าสุด! {{ $seminar_th_first->year }}</p>
        </div>
    </div>


    <div class="SeminarPageListSlideForBuyerBGWhite clr">
        <div class="LatestSeminarBig Mar60px">
            <div class="RightWidthBox">
                <div class="PictureDisplay">
                    <a href="{{ route('seminar.show',$seminar_th_first->ref_id) }}"><span></span>
                        <div class="inner"
                             style="background-image:url('{{ asset($seminar_th_first->thumbnail) }}');background-size: cover; background-position: center center;">
                            <div class="BGS"></div>
                    </a>
                </div>
            </div>
            <div class="RightCaptions">
                <div class="New"><img src="{{ asset('assets/images/icon/icon-new.svg') }}"></div>
                <span>{{ $seminar_th_first->list ?? "ครั้งที่ $seminar_th_first->id"  }}</span>
                <p class="Title clr">{{ $seminar_th_first->sub_title }}</p>
                <a href="{{ route('seminar.show',$seminar_th_first->ref_id) }}" class="DetailLink">ดูรายละเอียด ></a>
            </div>
        </div>
    </div>

    @foreach($seminars_th_group as $year => $seminars)
        <!-- SeminarPageListSlideAll -->
        <div class="SeminarPageListSlideForBuyer clr" style="background-color: #fff;">
            <p class="TitleYear2">สัมมนาออนไลน์ ปีงบประมาณ {{ $year }}</p>
            <ul class="TopPageSeminarSlideBoxForBuyer">
                @foreach($seminars as $seminar)
                    <li>
                        <a href="{{ route('seminar.show',$seminar->ref_id) }}">
                            <div class="PictureDisplay"><span></span>
                                <div class="inner"
                                     style="background-image:url('{{ asset($seminar->thumbnail) }}');background-size: cover; background-position: center center;">
                                    <div class="BGS"></div>
                                </div>
                            </div>
                            <div class="RightCaptions">
                                <span>{{ $seminar->list ?? "ครั้งที่ $seminar->id"  }}</span>
                                <p class="Title clr">{{ $seminar->sub_title }}</p>

                                <a href="{{ route('seminar.show',$seminar->ref_id) }}" class="DetailLink">ดูรายละเอียด
                                    ></a>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
        <!-- ENd SeminarPageListSlideAll -->
    @endforeach

@endsection