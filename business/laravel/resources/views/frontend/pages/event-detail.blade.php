@extends('frontend.app')
@section('content')

    <!--Breadcrumbs-->
    <div class="Breadcrumbs clr">
        <a href="{{ route('home') }}">หน้าหลัก</a> > <span>{{ $topic->title }}</span>
    </div>
    <!--Breadcrumbs-->

    <!-- ContentBoxFull -->
    <div class="ContentBoxFull clr">
        <!-- SellerInfoPage -->
        <div class="SellerInfoPage clr">
            <h1 class="NewsTitle">{{ $topic->title }}</h1>
            @if(!empty($topic->photo1) || !empty($topic->photo2) || !empty($topic->photo3) || !empty($topic->photo4))
                <!-- SlidePhoto -->
                <div id="gal-results" class="containerPhotoSlide">
                    <div id="gallery-view">
                        <div id="gal-slide">
                            @if(!empty($topic->photo1))
                                <div class="gal-slide-img"
                                     style="background: url('{{ asset($topic->photo1) }}') no-repeat; background-size: contain; background-repeat: no-repeat; background-position: center center;">
                                </div>
                            @endif
                            @if(!empty($topic->photo2))
                                <div class="gal-slide-img"
                                     style="background: url('{{ asset($topic->photo2) }}') no-repeat; background-size: contain; background-repeat: no-repeat; background-position: center center;">
                                </div>
                            @endif
                            @if(!empty($topic->photo3))
                                <div class="gal-slide-img"
                                     style="background: url('{{ asset($topic->photo3) }}') no-repeat; background-size: contain; background-repeat: no-repeat; background-position: center center;">
                                </div>
                            @endif
                            @if(!empty($topic->photo4))
                                <div class="gal-slide-img"
                                     style="background: url('{{ asset($topic->photo4) }}') no-repeat; background-size: contain; background-repeat: no-repeat; background-position: center center;">
                                </div>
                            @endif
                        </div>
                    </div>
                    <div id="gallery-thumbs">
                        <div id="gal-thumb">
                            @if(!empty($topic->photo1))
                                <div class="gal-slide-thumb"
                                     style=" background: url('{{ asset($topic->photo1) }}')no-repeat; background-size: cover; background-repeat: no-repeat; background-position: center center;">
                                </div>
                            @endif
                            @if(!empty($topic->photo2))
                                <div class="gal-slide-thumb"
                                     style=" background: url('{{ asset($topic->photo2) }}')no-repeat; background-size: cover; background-repeat: no-repeat; background-position: center center;">
                                </div>
                            @endif
                            @if(!empty($topic->photo3))
                                <div class="gal-slide-thumb"
                                     style=" background: url('{{ asset($topic->photo3) }}')no-repeat; background-size: cover; background-repeat: no-repeat; background-position: center center;">
                                </div>
                            @endif
                            @if(!empty($topic->photo4))
                                <div class="gal-slide-thumb"
                                     style=" background: url('{{ asset($topic->photo4) }}')no-repeat; background-size: cover; background-repeat: no-repeat; background-position: center center;">
                                </div>
                            @endif
                        </div>
                    </div>
                    <!-- End SlidePhoto -->
                    @endif
                </div>
                <!-- End SellerInfoPage -->
        </div>
        <!-- End ContentBoxFull -->


        <!-- ContentBox -->
        <div class="ContentBox clr">
            <!-- SellerInfoDetail -->
            <div class="NewsBoxDetail clr">
                <!-- NewsBoxDetailBox -->
                <div class="NewsBoxDetailBox">

                    <p class="EventDetail"><img src="{{ asset('assets/images/icon/button-topic-event.svg') }}">
                        เริ่มวันที่ {{ formatDateThai($topic->event_date_start) }} -
                        {{ formatDateThai($topic->event_date_end) }}</p>

                    <p>{!! $topic->detail !!}</p>
                </div>
                <!-- End NewsBoxDetailBox -->

                @if(!empty($topic->pdf1) || !empty($topic->pdf2) || !empty($topic->pdf3))
                    <!-- CompanyProfileList -->
                    <div class="CompanyProfileList clr">
                        <h2>รายละเอียดเพิ่มเติม</h2>
                        @if(!empty($topic->pdf1))
                            <p class="DownloadPDF">
                                <a target="_blank"
                                   href="{{ asset($topic->pdf1) }}">
                                    <span>Download file</span>
                                </a><br>
                                <label>{{ $topic->file_description1 }} {{ formatSizeUnits($topic->pdf_size1) }}</label>
                            </p>
                        @endif
                        @if(!empty($topic->pdf2))
                            <p class="DownloadPDF">
                                <a target="_blank"
                                   href="{{ asset($topic->pdf2) }}">
                                    <span>Download file</span>
                                </a><br>
                                <label>{{ $topic->file_description2 }} {{ formatSizeUnits($topic->pdf_size2) }}</label>
                            </p>
                        @endif
                        @if(!empty($topic->pdf3))
                            <p class="DownloadPDF">
                                <a target="_blank"
                                   href="{{ asset($topic->pdf3) }}">
                                    <span>Download file</span>
                                </a><br>
                                <label>{{ $topic->file_description3 }} {{ formatSizeUnits($topic->pdf_size3) }}</label>
                            </p>
                        @endif
                    </div>
                    <!-- End CompanyProfileList -->
                @endif
            </div>
            <!-- End NewsBoxDetail -->
        </div>
        <!-- End ContentBox -->

        @if($topic_other->count() > 0)
            <!-- NewsBox -->
            <div class="NewsBox clr">
                <p class="RelatedNewsJNTO">อีเวนต์จาก JNTO <span>JNTO Event</span></p>
                <ul>
                    @foreach($topic_other as $item)
                        <li>
                            <span>{{ formatDateThai($item->updated_at) }}</span>
                            @if($item->template == 'page')
                                <a href="{{ route('event-detail',$item->ref_id) }}">
                                {{ $item->title }}<div class="StartEndDate">เริ่มวันที่ {{ formatDateThai($item->event_date_start) }} - {{ formatDateThai($item->event_date_end) }}</div>
                                </a>
                            @else
                                <a href="{{ $item->link }}">
                                {{ $item->title }}<div class="StartEndDate">เริ่มวันที่ {{ formatDateThai($item->event_date_start) }} - {{ formatDateThai($item->event_date_end) }}</div>
                                </a>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
            <!-- End NewsBox -->
    @endif

@endsection