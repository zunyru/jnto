<aside class="w-full md:w-64 bg-gray-800 md:min-h-screen" x-data="{ isOpen: false }">
    <div class="fixed md:w-64 w-full">
        <div class="flex items-center justify-between bg-white p-4 h-16">

            <div class="flex md:hidden">
                <button type="button" @click="isOpen = !isOpen"
                        class="text-gray-300 hover:text-gray-500 focus:outline-none focus:text-gray-500">
                    <svg class="fill-current w-8" fill="none" stroke-linecap="round" stroke-linejoin="round"
                         stroke-width="2" viewBox="0 0 24 24" stroke="currentColor">
                        <path d="M4 6h16M4 12h16M4 18h16"></path>
                    </svg>
                </button>
            </div>
        </div>
        <div class="px-2 pb-20 py-6 md:block fixed h-full t-0 b-0 overflow-y-scroll {{ Request::segment(2) =='dashboard' ? '' : 'hover:' }}bg-gray-800"
             :class="isOpen? 'block': 'hidden'">
            <ul>
                <li class="px-2 py-3 hover:bg-gray-900 rounded">
                    <a href="{{ route('dashboard') }}" class="flex items-center">
                        <svg class="w-6 text-gray-500" fill="none" stroke-linecap="round"
                             stroke-linejoin="round"
                             stroke-width="2"
                             viewBox="0 0 24 24" stroke="currentColor">
                            <path
                                    d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"></path>
                        </svg>
                        <span class="mx-2 text-gray-300">Home</span>
                    </a>
                </li>
                <li class="px-2 py-3 {{ Request::segment(2) =='form' ? '' : 'hover:' }}bg-gray-900 rounded mt-2">
                    <a href="{{ route('business.index') }}" class="flex items-center">
                        <svg class="w-6 text-gray-500" xmlns="http://www.w3.org/2000/svg" fill="none"
                             viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                  d="M19 21V5a2 2 0 00-2-2H7a2 2 0 00-2 2v16m14 0h2m-2 0h-5m-9 0H3m2 0h5M9 7h1m-1 4h1m4-4h1m-1 4h1m-5 10v-5a1 1 0 011-1h2a1 1 0 011 1v5m-4 0h4"/>
                        </svg>
                        <span class="mx-2 text-gray-300">Discover new partners!</span>
                    </a>
                </li>
                <li class="px-2 py-3 {{ Request::segment(2) =='news-jnto' ? '' : 'hover:' }}bg-gray-900 rounded mt-2">
                    <a href="{{ route('news-jnto.index') }}" class="flex items-center">
                        <svg class="w-6 text-gray-500" xmlns="http://www.w3.org/2000/svg" fill="none"
                             viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                  d="M19 20H5a2 2 0 01-2-2V6a2 2 0 012-2h10a2 2 0 012 2v1m2 13a2 2 0 01-2-2V7m2 13a2 2 0 002-2V9a2 2 0 00-2-2h-2m-4-3H9M7 16h6M7 8h6v4H7V8z"/>
                        </svg>
                        <span class="mx-2 text-gray-300">JNTO News</span>
                    </a>
                </li>
                <li class="px-2 py-3 {{ Request::segment(2) =='news-jnto-partner' ? '' : 'hover:' }}bg-gray-900 rounded mt-2">
                    <a href="{{ route('news-jnto-partner.index') }}" class="flex items-center">
                        <svg class="w-6 text-gray-500" fill="none" stroke-linecap="round"
                             stroke-linejoin="round"
                             stroke-width="2" viewBox="0 0 24 24" stroke="currentColor">
                            <path
                                    d="M20 13V6a2 2 0 00-2-2H6a2 2 0 00-2 2v7m16 0v5a2 2 0 01-2 2H6a2 2 0 01-2-2v-5m16 0h-2.586a1 1 0 00-.707.293l-2.414 2.414a1 1 0 01-.707.293h-3.172a1 1 0 01-.707-.293l-2.414-2.414A1 1 0 006.586 13H4"></path>
                        </svg>
                        <span class="mx-2 text-gray-300">JNTO Partners’ News</span>
                    </a>
                </li>
                <li class="px-2 py-3 {{ Request::segment(2) =='online-seminar' ? '' : 'hover:' }}bg-gray-900 rounded mt-2">
                    <a href="{{ route('seminar.index') }}" class="flex items-center">
                        <svg class="w-6 text-gray-500" fill="none" stroke-linecap="round"
                             stroke-linejoin="round"
                             stroke-width="2" viewBox="0 0 24 24" stroke="currentColor">
                            <path
                                    d="M16 8v8m-4-5v5m-4-2v2m-2 4h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z"></path>
                        </svg>
                        <span class="mx-2 text-gray-300">Online Seminar</span>
                    </a>
                </li>

                <li class="px-2 py-3 {{ Request::segment(2) =='event' ? '' : 'hover:' }}bg-gray-900 rounded mt-2">
                    <a href="{{ route('event.index') }}" class="flex items-center">
                        <svg class="w-6 text-gray-500" xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
                        </svg>
                        <span class="mx-2 text-gray-300">JNTO Event</span>
                    </a>
                </li>

                <li class="px-2 py-3 {{ Request::segment(2) =='useful-links' ? '' : 'hover:' }}bg-gray-900 rounded mt-2">
                    <a href="{{ route('useful-link.index') }}" class="flex items-center">
                        <svg class="w-6 text-gray-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 4v12l-4-2-4 2V4M6 20h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z" />
                        </svg>
                        <span class="mx-2 text-gray-300">Useful Links</span>
                    </a>
                </li>
                <li class="px-2 py-3 {{ Request::segment(2) =='category' ? '' : 'hover:' }}bg-gray-900 rounded mt-2">
                    <a href="{{ route('category.index') }}" class="flex items-center">
                        <svg class="w-6 text-gray-500"  xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 14v6m-3-3h6M6 10h2a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v2a2 2 0 002 2zm10 0h2a2 2 0 002-2V6a2 2 0 00-2-2h-2a2 2 0 00-2 2v2a2 2 0 002 2zM6 20h2a2 2 0 002-2v-2a2 2 0 00-2-2H6a2 2 0 00-2 2v2a2 2 0 002 2z" />
                        </svg>
                        <span class="mx-2 text-gray-300">Categories</span>
                    </a>
                </li>

                <li class="px-2 py-3 {{ Request::segment(2) =='covid-19-informations' ? '' : 'hover:' }}bg-gray-900 rounded mt-2">
                    <a href="{{ route('covid.index') }}" class="flex items-center">
                        <svg class="w-6 text-gray-500" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14 10l-2 1m0 0l-2-1m2 1v2.5M20 7l-2 1m2-1l-2-1m2 1v2.5M14 4l-2-1-2 1M4 7l2-1M4 7l2 1M4 7v2.5M12 21l-2-1m2 1l2-1m-2 1v-2.5M6 18l-2-1v-2.5M18 18l2-1v-2.5" />
                        </svg>
                        <span class="mx-2 text-gray-300">Covid-19 informations</span>
                    </a>
                </li>
                <li class="px-2 py-3 {{ Request::segment(2) =='banner' ? '' : 'hover:' }}bg-gray-900 rounded">
                    <a href="{{ route('banner.index') }}" class="flex items-center">
                        <svg class="w-6 text-gray-500 h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16l4.586-4.586a2 2 0 012.828 0L16 16m-2-2l1.586-1.586a2 2 0 012.828 0L20 14m-6-6h.01M6 20h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z" />
                        </svg>
                        <span class="mx-2 text-gray-300">Top Banner</span>
                    </a>
                </li>
                <li class="px-2 py-3 {{ Request::segment(2) =='setting-page-thank' ? '' : 'hover:' }}bg-gray-900 rounded">
                    <a href="{{ route('setting-thank-page.index') }}" class="flex items-center">
                        <svg class="w-6 text-gray-500"
                             xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20 13V6a2 2 0 00-2-2H6a2 2 0 00-2 2v7m16 0v5a2 2 0 01-2 2H6a2 2 0 01-2-2v-5m16 0h-2.586a1 1 0 00-.707.293l-2.414 2.414a1 1 0 01-.707.293h-3.172a1 1 0 01-.707-.293l-2.414-2.414A1 1 0 006.586 13H4" />
                        </svg>
                        <span class="mx-2 text-gray-300">Thanks page</span>
                    </a>
                </li>
                <li class="px-2 py-3 {{ Request::segment(2) =='setting-page-contact' ? '' : 'hover:' }}bg-gray-900 rounded">
                    <a href="{{ route('setting-contact.index') }}" class="flex items-center">
                        <svg class="w-6 text-gray-500"
                             xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20 13V6a2 2 0 00-2-2H6a2 2 0 00-2 2v7m16 0v5a2 2 0 01-2 2H6a2 2 0 01-2-2v-5m16 0h-2.586a1 1 0 00-.707.293l-2.414 2.414a1 1 0 01-.707.293h-3.172a1 1 0 01-.707-.293l-2.414-2.414A1 1 0 006.586 13H4" />
                        </svg>
                        <span class="mx-2 text-gray-300">Contact</span>
                    </a>
                </li>
                @if(!in_array(Auth::user()->role->name, ['Editor','Viewer']))
                    <li class="px-2 py-3 {{ Request::segment(2) =='user' ? '' : 'hover:' }}bg-gray-900 rounded mt-2">
                        <a href="{{ route('user.index') }}" class="flex items-center">
                            <svg class="w-6 text-gray-500" xmlns="http://www.w3.org/2000/svg" fill="none"
                                 viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                      d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z"/>
                            </svg>
                            <span class="mx-2 text-gray-300">Users</span>
                        </a>
                    </li>
                @endif
                <li class="px-2 py-3 {{ Request::segment(2) =='activity-log' ? '' : 'hover:' }}bg-gray-900 rounded">
                    <a href="{{ route('log.index') }}" class="flex items-center">
                        <svg class="w-6 text-gray-500"
                             xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20 13V6a2 2 0 00-2-2H6a2 2 0 00-2 2v7m16 0v5a2 2 0 01-2 2H6a2 2 0 01-2-2v-5m16 0h-2.586a1 1 0 00-.707.293l-2.414 2.414a1 1 0 01-.707.293h-3.172a1 1 0 01-.707-.293l-2.414-2.414A1 1 0 006.586 13H4" />
                        </svg>
                        <span class="mx-2 text-gray-300">Activity Logs</span>
                    </a>
                </li>
            </ul>
            <div class="border-t border-gray-700 -mx-2 mt-2 md:hidden"></div>
            <ul class="mt-6 md:hidden">
                <li class="px-2 py-3 hover:bg-gray-900 rounded mt-2">
                    <a href="#" class="mx-2 text-gray-300">Account Settings</a>
                </li>
                <li class="px-2 py-3 hover:bg-gray-900 rounded mt-2">
                    <button class="mx-2 text-gray-300" @click="logout">Logout</button>
                </li>
            </ul>
        </div>
    </div>
</aside>