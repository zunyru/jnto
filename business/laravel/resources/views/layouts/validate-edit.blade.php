@prepend('scripts')
    <script type="text/javascript" src="{{ asset('js/validate-edit.js') }}?v=3"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    @include('admin.include.block')

    <script>
        $('.delete-img').click(function () {
            var data_value = $(this).attr('data-filters');
            var data_db = $(this).attr('data-db');
            var data_id = $(this).attr('data-id');
            var data_where = $(this).attr('data-where');
            Swal.fire({
                title: 'Are you sure to delete ?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: "POST",
                        headers: {
                            'X-CSRF-TOKEN': `{{ csrf_token() }}`
                        },
                        data: {
                            id: data_id,
                            field: data_value,
                            db : data_db,
                            where:data_where,
                        },
                        url: `{{ route('file.delete') }}`,
                    }).done(function (msg) {
                        toastr.success(msg.message);
                        setTimeout(function(){
                            location.reload();
                        }, 1000);

                    }).fail(function (msg) {
                        toastr.error(msg.message)
                    });
                }
            });
        });
    </script>
@endprepend