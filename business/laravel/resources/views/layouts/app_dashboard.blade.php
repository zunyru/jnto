<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ setting('site.title') ?? 'JNTO' }}</title>

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <style>
        .block {
            display: block;
        }

        form label.error {
            display: none;
        }

        .top--20 {
            position: relative;
            top: -20px;
        }

        .error {
            margin-top: unset;
            margin-bottom: unset;
        }
    </style>
    @stack('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style-form.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style-responsive.css') }}">
    <link rel="icon" href="{{ asset('favicon-32x32.png') }}" type="image" sizes="32x32">
    <link href="https://fonts.googleapis.com/css2?family=Caveat:wght@400;500;600;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <!--Regular Datatables CSS-->
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
    <!--Responsive Extension Datatables CSS-->
    <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet">

    <script type="module" src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js"></script>
    <script nomodule src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine-ie11.min.js" defer></script>


</head>
<body class="bg-white min-h-screen font-base">
<div id="app">

    <div class="flex flex-col md:flex-row">

        @include('includes.sidebar')

        <div class="w-full md:flex-1">
            <nav class="hidden w-full md:flex justify-between items-center bg-white p-4 shadow-md h-16 fixed z-50 left-0">
                <div>
                    <a class="flex flex-row" target="_blank" href="{{ route('home') }}" >
                        <img class="w-16 m-auto" src="{{ !empty(Voyager::image(setting('admin.icon_image'))) ? Voyager::image(setting('admin.icon_image')) : asset('assets/images/logo.png')  }}" style="width: 6rem;">
                        <span class="pl-4" style="color: #123;font-weight: 400;padding-top:0px;font-size: 1.5rem;">JNTO B2B Website</span>
                    </a>
                </div>
                <div>
                    <div class="flex flex-row">
                        <p class="pr-4" style="color: #ab816b;">
                            {{ Auth::user()->email }}
                        </p>
                    <button class="mx-2 text-gray-700 focus:outline-none flex"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();" style="font-family: 'Caveat', cursive; background-color: #663300; box-shadow: 2px 2px 10px rgba(0,0,0,0.5); font-size: 15px; font-weight: 500; padding: 0px 10px 2px 10px; color: #fff; border-radius: 100px; border: 1px solid #fff;">
                        {{--<svg class="h-6 pr-1" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1" />
                        </svg>--}}

                        Logout
                    </button>
                    </div>
                </div>
            </nav>
            <main class="mt-16">
                <!-- Replace with your content -->
                <div class="px-2 py-6 md:pt-0">
                    <div class="p-4">
                        @yield('content')
                    </div>
                </div>
                <!-- /End replace -->
            </main>
        </div>
        <form id="logout-form" action="{{ route('logout.admin') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </div>
</div>

@include('layouts.script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
@stack('scripts')

@prepend('scripts')
    <script>
        @if(Session::has('message'))

        var alertType = {!! json_encode(Session::get('alert-type', 'info')) !!};
        var alertMessage = {!! json_encode(Session::get('message')) !!};
        var alerter = toastr[alertType];

        if (alerter) {
            alerter(alertMessage);
        } else {
            toastr.error("toastr alert-type " + alertType + " is unknown");
        }
        @endif
    </script>
@endprepend
</body>
</html>
