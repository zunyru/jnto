<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Resources for Travel Agents</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="icon" href="favicon-32x32.png" type="image" sizes="32x32">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Montserrat:wght@300;400;500;600;700&family=Sarabun:wght@100;500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css">
        .BoxAll{ font-family: 'Kanit', sans-serif; padding: 30px; text-align: left; max-width: 750px; margin: 20px auto; border:1px solid #eee; border-radius: 20px;}
        .BoxAll p{ margin-top: 0px; }
        @media(max-width:767px)
        {
            .BoxAll{ margin-left: 20px; margin-right: 20px;}
        }
    </style>
</head>
<body>

<div class="BoxAll">
    <p style="color: #a67c52; font-size: 25px; font-weight: 500;">ดำเนินการปิดปรับปรุงเว็บไซต์ชั่วคราว</p>
    <p style="color: #213054; margin-top: 30px;">เว็บไซต์ JNTO B2B Website อยู่ระหว่างดำเนินการปิดปรับปรุงเว็บไซต์ชั่วคราว<p>
    <p style="color: #213054; margin-top: 30px;font-size: 20px;">จะเปิดบริการให้ทุกท่านเข้าชมอีกครั้งใน<span>วันอังคารที่ 20 กันยายน พ.ศ. 2565 นี้ เวลาประเทศญี่ปุ่น 18.00 น. (เวลาประเทศไทย 16.00 น.) เป็นต้นไป</span><br>
    <span style="color: #000; font-size: 13px; font-weight: 300;">*กำหนดการอาจมีการเปลี่ยนแปลง</span></p>
    <p>ขออภัยในความไม่สะดวกมา ณ ที่นี้</p>
    <p style="color: #666; font-size: 14px; font-weight: 300; margin-top: 30px;">ติดต่อสอบถาม :<br>
    บริษัทตัวแทนดำเนินงานเว็บไซต์ JNTO B2B Website  บริษัท เมดิเอเตอร์ จำกัด<br>
    E-mail : vj_business_th@mediator.co.th<br>
    เบอร์ติดต่อ : +66-2-392-3288 (ต่อ 103) เบอร์โทรในประเทศไทย<br>
    เวลาทำการ : วันจันทร์-ศุกร์ เวลา 10:00～18:00 ( เวลาในประเทศไทย หยุดวันเสาร์-อาทิตย์ และวันนักขัตฤกษ์ )
    </p>

    <hr style="margin-top: 30px; margin-bottom: 30px;">

    <p style="color: #a67c52; font-size: 25px; font-weight: 500;">ただいまメンテナンス中です。</p>
    <p style="color: #000; margin-top: 30px;">現在、「タイ旅行会社向け情報発信サイト」のメンテナンス作業を行っております。</p>
    <p style="color: #213054; margin-top: 30px;">
        <span style="font-size: 19px;">2022年9月20日（火）日本時間18時（タイ時間16時）ごろよりアクセスしていただきます。</span><br>※日程を変更する場合がございます。</p>
    <p style="color: #000; font-size: 14px; font-weight: 300; margin-top: 30px;">ご不便、ご迷惑をお掛け致しますが、何卒ご理解いただけますようお願い申し上げます。</p>
    <p style="color: #666; font-size: 14px; font-weight: 300; margin-top: 30px;">【お問い合わせ先】<br>
    JNTOバンコク事務所 「タイ旅行会社向け情報発信サイト」運営事務局（Mediator.Co.,Ltd.）<br>
    E-mail : vj_business_th@mediator.co.th<br>
    電話番号：＋66-2-392-3288（内線番号103）※タイ国への通話となります。<br>
    営業時間：月～金曜日 12:00～20:00　（日本時間 ・土日祝日休み）
    </p>
</div>

</body>
</html>