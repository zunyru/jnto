<script type="text/javascript" src="{{ asset('assets/js/jquery-2.1.1.min.js') }} "></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/slick.min.js') }}"></script>
<script type="text/javascript"
        src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js"></script>
<script type="text/javascript"
        src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/additional-methods.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script type="text/javascript">

    @if(Session::has('message'))

    var alertType = {!! json_encode(Session::get('alert-type', 'info')) !!};
    var alertMessage = {!! json_encode(Session::get('message')) !!};
    var alerter = toastr[alertType];

    if (alerter) {
        alerter(alertMessage);
    } else {
        toastr.error("toastr alert-type " + alertType + " is unknown");
    }
    @endif
	    @if($errors->any())
		    @foreach ($errors->all() as $error)
                toastr.error("Error :" + `{{ $error }}`);
		    @endforeach
        @endif

    $('#BIT').hide();
    if ($('#bit1:checked').val() == 1) {
        $('#BIT').show();
    } else {
        $('#BIT').hide();
    }

    /*Chck all Province*/
    $('#province_all').click(function () {
        if ($(this).is(":checked")) {
            $('.province').prop("checked", true);
        } else {
            $('.province').prop("checked", false);
        }
    });

    $('.province').click(function () {
        var ch = true;
        $('#province_all').prop("checked", true);
        $('.province').each(function (index, value) {
            if ($(this).is(":not(:checked)")) {
                $('#province_all').prop("checked", false);
                ch = false
            }
        });
        $('#province_all').prop("checked", ch);
    });

    function CheckType(id) {
        var BIT = document.getElementById('BIT');
        if (id == 1) {
            BIT.style.display = 'block';
        }
        if (id == 2) {
            BIT.style.display = 'none';
        }
    }

    ($("[name=\"support_menu1\"]").val() != '')
        ? $('#Support1Name').prop('disabled', false)
        : $('#Support1Name').prop('disabled', true);
    ($("[name=\"support_menu2\"]").val() != '')
        ? $('#Support2Name').prop('disabled', false)
        : $('#Support2Name').prop('disabled', true);
    ($("[name=\"support_menu3\"]").val() != '')
        ? $('#Support3Name').prop('disabled', false)
        : $('#Support3Name').prop('disabled', true);
    ($("[name=\"support_menu4\"]").val() != '')
        ? $('#Support4Name').prop('disabled', false)
        : $('#Support4Name').prop('disabled', true);
    ($("[name=\"support_menu5\"]").val() != '')
        ? $('#Support5Name').prop('disabled', false)
        : $('#Support5Name').prop('disabled', true);
    ($("[name=\"support_menu6\"]").val() != '')
        ? $('#Support6Name').prop('disabled', false)
        : $('#Support6Name').prop('disabled', true);
    ($("[name=\"support_menu7\"]").val() != '')
        ? $('#Support7Name').prop('disabled', false)
        : $('#Support7Name').prop('disabled', true);
    ($("[name=\"support_menu8\"]").val() != '')
    ? $('#Support8Name').prop('disabled', false)
    : $('#Support8Name').prop('disabled', true);


    function CheckSupport1(id) {
        var Support1Name = document.getElementById('Support1Name');
        if (id == 'None' || id == 0) {
            Support1Name.disabled = true;
        } else {
            Support1Name.disabled = false;
        }
    }

    function CheckSupport2(id) {
        var Support2Name = document.getElementById('Support2Name');
        if (id == 'None' || id == 0) {
            Support2Name.disabled = true;
        } else {
            Support2Name.disabled = false;
        }
    }

    function CheckSupport3(id) {
        var Support3Name = document.getElementById('Support3Name');
        if (id == 'None' || id == 0) {
            Support3Name.disabled = true;
        } else {
            Support3Name.disabled = false;
        }
    }

    function CheckSupport4(id) {
        var Support4Name = document.getElementById('Support4Name');
        if (id == 'None' || id == 0) {
            Support4Name.disabled = true;
        } else {
            Support4Name.disabled = false;
        }
    }

    function CheckSupport5(id) {
        var Support5Name = document.getElementById('Support5Name');
        if (id == 'None' || id == 0) {
            Support5Name.disabled = true;
        } else {
            Support5Name.disabled = false;
        }
    }

    function CheckSupport6(id) {
        var Support6Name = document.getElementById('Support6Name');
        if (id == 'None' || id == 0) {
            Support6Name.disabled = true;
        } else {
            Support6Name.disabled = false;
        }
    }

    function CheckSupport7(id) {
        var Support7Name = document.getElementById('Support7Name');
        if (id == 'None' || id == 0) {
            Support7Name.disabled = true;
        } else {
            Support7Name.disabled = false;
        }
    }

    function CheckSupport8(id) {
        var Support8Name = document.getElementById('Support8Name');
        if (id == 'None' || id == 0) {
            Support8Name.disabled = true;
        } else {
            Support8Name.disabled = false;
        }
    }

    function PrivacyCheck(id){
        var PublicDisplay = document.getElementById('PublicDisplay');
        var PrivateDisplay = document.getElementById('PrivateDisplay');
        if(id==1){ PublicDisplay.style.display='block'; PrivateDisplay.style.display='none'; }
        if(id==2){ PublicDisplay.style.display='none'; PrivateDisplay.style.display='block'; }
    }

</script>