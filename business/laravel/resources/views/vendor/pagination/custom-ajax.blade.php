@if ($paginator->hasPages())
    <ul>
        @if ($paginator->onFirstPage())
            <li><</li>
        @else
            <li>
                <a href="javascript:void(0);" data-page="{{ $paginator->onFirstPage() }}"><</a>
            </li>
        @endif
        @foreach ($elements as $element)
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li>
                            <a href="javascript:void(0);" class="ActivePageNavi">
                            {{ $page }}
                            </a>
                        </li>
                    @else
                        <li class="">
                            <a href="javascript:void(0);" data-page="{{$page}}">{{ $page }}</a>
                        </li>
                    @endif
                @endforeach
            @endif
        @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li>
                    <a href="javascript:void(0);" data-page="{{$paginator->lastPage()}}" rel="next" >></a>
                </li>
            @else
                <li>
                    >
                </li>
            @endif
    </ul>
@endif