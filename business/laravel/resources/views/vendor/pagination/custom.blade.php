<div class="NavigatorList">
@if ($paginator->hasPages())
    <ul>
        @if ($paginator->onFirstPage())
            <li><</li>
        @else
            <li>
                <a href="{{ $paginator->previousPageUrl() }}"><</a>
            </li>
        @endif
        @foreach ($elements as $element)
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li>
                            <a href="javascript:void(0);" class="ActivePageNavi">
                            {{ $page }}
                            </a>
                        </li>
                    @else
                        <li class="">
                            <a href="{{ $url }}">{{ $page }}</a>
                        </li>
                    @endif
                @endforeach
            @endif
        @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li>
                    <a href="{{ $paginator->nextPageUrl() }}" rel="next" >></a>
                </li>
            @else
                <li>
                    >
                </li>
            @endif
    </ul>
@endif
</div>