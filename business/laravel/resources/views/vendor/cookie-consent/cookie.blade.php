@if(Auth::guard('web')->check())
    <!-- CookiePopup -->
    <div class="CookiePopup">
        <div class="CookiePopupBox">
            <div class="PopupCondition">
                <h3>เงื่อนไขในการใช้เว็บไซต์</h3>
                <div class="MobileScroll">
                    <p class="LineOne">
                        เว็บไซต์นี้จัดทำขึ้นเพื่อผู้ประกอบการท่องเที่ยวญี่ปุ่นในประเทศไทยโดยเฉพาะ <span class="Boldtxt">โปรดเก็บรักษาชื่อผู้ใช้งาน (User ID) และรหัสผ่าน (Password) ของเว็บไซต์เพื่อใช้ภายในบริษัทของท่านเท่านั้น ไม่นำไปเผยแพร่แก่หน่วยงานอื่นรวมทั้งผู้บริโภค</span>
                    </p>
                    <p class="LineTwo">
                        <span class="Boldtxt">ข่าวสารจาก JNTO จะส่งให้เฉพาะท่านที่ให้การยอมรับนโยบายการคุ้มครองข้อมูลส่วนบุคคล (<a href="{{ setting('admin.privacy-policy-url-th') }}" target="_blank">{{ setting('admin.privacy-policy-url-th') }}</a>) ของ JNTO สำนักงานกรุงเทพฯ และให้การยินยอมรับข่าวสารทางอีเมล สำหรับท่านที่ต้องการได้รับข่าวสารทางอีเมล กรุณาติดต่อมาที่ {{ setting('admin.email') }} </span><br>
                        <!--<span class="Smalltxt">*JNTO ได้มอบหมายให้ บริษัท เมดิเอเตอร์ จำกัด เป็นบริษัทตัวแทนดำเนินงาน</span>-->
                    </p>
                    <p class="LineThree">
                        <span class="Boldtxt">ผู้จัดทำเว็บไซต์ไม่สามารถรับรองลิขสิทธิ์ของเนื้อหา*ทั้งหมดภายในหน้า “ค้นหาพาร์ทเนอร์ฝ่ายญี่ปุ่น”</span>
                        ดังนั้น เพื่อหลีกเลี่ยงความเสี่ยงต่อการละเมิดลิขสิทธิ์ที่อาจเกิดกับท่านและบริษัท <span
                                class="Boldtxt">ห้ามท่านนำเนื้อหาไปดัดแปลง ส่งต่อ โดยไม่ได้รับอนุญาตจากหน่วยงานที่ให้ข้อมูล แม้ในวัตถุประสงค์ในการจัดทำ จำหน่ายสินค้าท่องเที่ยวก็ตาม</span><br>
                        <span class="Smalltxt">*เนื้อหา รวมถึง รูปภาพ วีดีโอ ตัวอักษร กราฟฟิค ดนตรี เสียง และอื่นๆ</span><br>
                        <span class="Smalltxt">*ยกเว้นกรณีที่มีการเขียนระบุคำอนุญาตอย่างชัดเจนด้วยตัวอักษรจากหน่วยงานที่ให้ข้อมูล</span>
                    </p>
                    <!--<p class="LineThree">
                        เว็บไซต์นี้ใช้คุกกี้ในการให้บริการเพื่อให้ผู้ใช้ได้รับประสบการณ์การใช้งานเว็บไซต์ที่ดี หากคุณดำเนินการต่อ ทางเราจะถือว่าคุณยอมรับนโยบายคุกกี้ของเรา (เรียนรู้เพิ่มเติม <a href="page-privacy-policy.php" target="_blank">ที่นี่</a>)
                    </p>-->
                </div>
                <p class="BTNAccept">
                    <a class="js-cookie-consent-agree cookie-consent__agree" href="javascript:void(0);"
                       onclick="AcceptCookie();" id="AcceptCookie">
                        <img src="{{ asset('frontend/assets/images/icon/icon-accept.svg') }}"> ยอมรับ
                    </a>
                </p>
            </div>
        </div>
    </div>
@endif
<!-- End CookiePopup -->