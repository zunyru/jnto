@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="justify-center">

            <div class="FormBox clr">
                <h2>登録情報の確認 | Confirmation page</h2>
                <div class="FormList">
                    <form action="{{ route('business.store.confirm',$ref_id) }}" method="POST"
                          enctype="multipart/form-data">
                        @csrf

                        {{--Company Information--}}
                        @include('sessions.confirm.company-information-confirm')

                        {{--Company Information--}}
                        @include('sessions.confirm.service-information-confirm')

                        {{--Support menu--}}
                        @include('sessions.confirm.support-menu-confirm')

                        {{--Category tag--}}
                        @include('sessions.confirm.category-tag-confirm')

                        {{--policy tag--}}
                        @include('sessions.confirm.policy')

                        <div class="BTNList">

                            <a href="{{ route('business.edit',$ref_id) }}" type="button" class="EditClass">
                                <button type="button">Back to Edit <i
                                            class="fa fa-pencil"></i></button>
                            </a>
                            <button type="submit" class="ConfirmClass">CONFIRM <i class="fa fa-check-circle"></i>
                            </button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection