<table>
	<thead>
	<tr>
		<th>ID</th>
		<th>URL</th>
		<th>company_name_JP</th>
		<th>company_name_EN</th>
		<th>partner</th>
		<th>website</th>
		<th>industries</th>
		<th>province</th>
		<th>support menu1</th>
		<th>menu name1</th>
		<th>support menu2</th>
		<th>menu name2</th>
		<th>support menu3</th>
		<th>menu name3</th>
		<th>support menu4</th>
		<th>menu name4</th>
		<th>support menu5</th>
		<th>menu name5</th>
		<th>support menu6</th>
		<th>menu name6</th>
		<th>support menu7</th>
		<th>menu name7</th>
		<th>support menu8</th>
		<th>menu name8</th>
		<th>Login-Email</th>
		<th>Contact person name</th>
		<th>Contact person email</th>
		<th>Status (Draft-Published)</th>
		<th>Status Inactive-active</th>
	</tr>
	</thead>
	<tbody>
	@foreach($sellers as $seller)
		@php
			$provinces_implode = '';
			$province_arr = [];

			if (!empty($seller->company_region)) {
				foreach ($seller->company_region as $key => $item) {
					$province_arr[$key] = $item->name_ja;
				}
				$provinces_implode = collect($province_arr)->implode(',');
			}
			if(!empty($seller->province_all)){
				$provinces_implode = '日本中';
			}

			$business_pivot = [];
			$business_pivot_arr = [];
			$business_pivot_implode = '';
			if ($seller->business_type_companies()) {
				$business_type_companies = $seller->business_type_companies()->get();
				foreach ($business_type_companies as $key => $item) {
					$business_pivot_arr[] = $item->name_en;
				}
				$business_pivot_implode = collect($business_pivot_arr)->implode(',');
			}
		
		@endphp
		<tr>
			<td>{{ $seller->ref_id }}</td>
			<td>{{ route('seller-info-detail',$seller->ref_id) }}</td>
			<td>{{ $seller->name_ja }}</td>
			<td>{{ $seller->name_en }}</td>
			<td>{{ !empty($seller->is_partner) ? 'Yes' : 'No' }}</td>
			<td>{{ $seller->official_website_url }}</td>
			<td>{{ $business_pivot_implode }}</td>
			<td>{{ !empty($provinces_implode) ? $provinces_implode : '' }}</td>
			<td>{{ $seller->support_menu->support_menu1_value->name_en ?? '' }}</td>
			<td>{{ $seller->support_menu->manu_name1 ?? '' }}</td>
			<td>{{ $seller->support_menu->support_menu2_value->name_en ?? '' }}</td>
			<td>{{ $seller->support_menu->manu_name2 ?? '' }}</td>
			<td>{{ $seller->support_menu->support_menu3_value->name_en ?? '' }}</td>
			<td>{{ $seller->support_menu->manu_name3 ?? '' }}</td>
			<td>{{ $seller->support_menu->support_menu4_value->name_en ?? '' }}</td>
			<td>{{ $seller->support_menu->manu_name4 ?? '' }}</td>
			<td>{{ $seller->support_menu->support_menu5_value->name_en ?? '' }}</td>
			<td>{{ $seller->support_menu->manu_name5 ?? '' }}</td>
			<td>{{ $seller->support_menu->support_menu6_value->name_en ?? '' }}</td>
			<td>{{ $seller->support_menu->manu_name6 ?? '' }}</td>
			<td>{{ $seller->support_menu->support_menu7_value->name_en ?? '' }}</td>
			<td>{{ $seller->support_menu->manu_name7 ?? '' }}</td>
			<td>{{ $seller->support_menu->support_menu8_value->name_en ?? '' }}</td>
			<td>{{ $seller->support_menu->manu_name8 ?? '' }}</td>
			<td>{{ $seller->user_login ?? '' }}</td>
			<td>{{ $seller->pic_name ?? '' }}</td>
			<td>{{ $seller->pic_email ?? '' }}</td>
			<td>{{ ($seller->status) == 'published' ? 'Published' : 'Draft' }}</td>
			<td>{{ ($seller->active_status) == 1 ? 'Active' : 'Inactive' }}</td>
		
		</tr>
	@endforeach
	</tbody>
</table>