@push('css')
    <style>
        .DescriptionsBox {
            margin-top: 20px;
            font-size: 15px;
        }
        .RowsLR {
            width: auto;
            height: auto;
            margin: auto;
            overflow: auto;
            clear: both;
        }
        .RowsLR p:nth-child(1) {
            width: calc(100% - 560px);
            width: 100%;
            max-width: 110px;
            float: left;
        }
        .RowsLR p:nth-child(2) {
            width: calc(100% - 115px);
            float: right;
        }
        .RightContent .OthersPage .DescriptionsBox {
            margin-top: 20px;
            font-size: 15px;
        }
        .RightContent .OthersPage .DescriptionsBox p {
            margin: 0;
        }
    </style>
@endpush
<hr>
<fieldset class="mt-10">
    <legend>日本側セラーへの公開設定 | Privacy Setting</legend>
    <div class="OthersPageBox">
        <label>
            <input type="radio"
                   name="privacy"
                   {{ $company->privacy == 'public' ? 'checked' : '' }}
            value="public"
            id="1"
            onclick="PrivacyCheck(1);"> Public | 公開
        </label>
        <label>
            <input type="radio"
                   {{ $company->privacy == 'private' ? 'checked' : '' }}
            name="privacy"
            value="private"
            id="2"
            onclick="PrivacyCheck(2);"> Private | 非公開
        </label>

        <!-- Public Box -->
        <div id="PublicDisplay" class="clr"
             style="display: {{ $company->privacy == 'public' ? 'block' : 'none' }}">
            <div class="DescriptionsBox FontJP">
                <p>「公開」に設定した場合でも、他の日本側セラーに対しては個人情報・連絡先等は表示されず、タイ旅行会社のみ閲覧できます。</p>
                <p>下記の項目は、タイ旅行会社のみ閲覧できます。</p>
                <br>
                <div class="RowsLR">
                    <p>1-2</p>
                    <p>JNTO賛助団体・会員</p>
                </div>
                <div class="RowsLR">
                    <p>1-14〜1-19</p>
                    <p>ご担当者名、部署名、メールアドレス 、TEL 、対応言語</p>
                </div>
                <div class="RowsLR">
                    <p>1-20〜1-30</p>
                    <p>タイ支店・レップ情報</p>
                </div>
            </div>
            <div class="DescriptionsBox clr FontTH">
                <p>ต่อให้ตั้งค่าเป็น “Public” แล้ว แต่องค์กรญี่ปุ่นอื่นๆ ก็จะไม่สามารถดูข้อมูลส่วนบุคคลหรือช่องทางการติดต่อของท่าน โดยข้อมูลดังกล่าวจะถูกเผยแพร่ให้กับบริษัททัวร์ในประเทศไทยเท่านั้น</p>
                <p>หัวข้อต่อไปนี้ จะถูกเผยแพร่ให้กับบริษัททัวร์ในประเทศไทยเท่านั้น</p>
                <br>
                <div class="RowsLR">
                    <p>1-2</p>
                    <p>Is your company/organization a JNTO Partner?</p>
                </div>
                <div class="RowsLR">
                    <p>1-14〜1-19 </p>
                    <p>Contact person name, Department, Company email address, TEL, Language</p>
                </div>
                <div class="RowsLR">
                    <p>1-20〜1-30</p>
                    <p>Branch/representative office information</p>
                </div>
            </div>
        </div>
        <!-- End Public Box -->

        <!-- Public Box -->
        <div id="PrivateDisplay" class="clr"
             style="display: {{ $company->privacy == 'private' ? 'block' : 'none' }}">
            <div class="DescriptionsBox FontJP">
                <p>「非公開」に設定した場合、他の日本側セラーに対しては登録したページは表示されません。</p>
                <p>また貴団体ご自身も、他の日本側セラーのページを閲覧できません。</p>
                <p class="SmallFont">※タイ旅行会社のみContact Information以外の全ての情報を閲覧できます。</p>
            </div>
            <div class="DescriptionsBox clr FontTH">
                <p>เมื่อตั้งค่าเป็น “Private” แล้ว องค์กรญี่ปุ่นอื่นๆ จะไม่สามารถเข้าชมหน้าเว็บไซต์ของท่าน</p>
                <p>และท่านก็จะไม่สามารถเข้าชมหน้าเว็บไซต์ขององค์กรญี่ปุ่นอื่นๆ ได้เช่นเดียวกัน</p>
                <p class="SmallFont">*เฉพาะบริษัททัวร์ในประเทศไทยเท่านั้นที่เข้าชมหน้าเว็บไซต์ของท่านได้ (ยกเว้นข้อ Contact Information)</p>
            </div>
        </div>
        <!-- End Public Box -->
    </div>
</fieldset>