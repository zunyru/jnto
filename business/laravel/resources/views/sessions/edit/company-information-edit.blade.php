<fieldset style="background-color: #FFFFE6;">
    <legend>{!! __('form.secsion_title_1') !!}</legend>
    <ul>
        @if(Route::currentRouteName() != 'seller.mypage')
            <li>
                <label>
                    <p class="FloatLeft">
                        Email Login
                    </p>
                    <input type="text"
                           name="user_login"
                           value="{{ $company->user_login }}"
                           placeholder="Email Login">
                </label>
            </li>
        @endif
        <li class="mb-2">
            {!! __('form.secsion_title_1_1_label') !!}
            {!! __('form.secsion_title_1_1_sub') !!}
            <label>
                @if(!empty($company->logo))
                    <img src="{{ asset($company->logo) }}"
                         id="logo_preview"
                         width="150">
                @else
                    <img src=""
                         id="logo_preview"
                         width="150">
                @endif

                <label for="logo"
                       class="FileUploadCSS">
                    Choose a file
                    <input type="file"
                           name="logo"
                           accept="image/*"
                           id="logo"/>
                </label>
                <br>
                <label id="logo-error" class="error" for="logo" style="display: none;">
                </label>
            </label>
        </li>
        <li class="LabelMarTop">
            {!! __('form.secsion_title_1_2_label')   !!}
            {!! __('form.secsion_title_1_2_sub') !!}
            <label class="FontNormalStyle">
                <input type="radio" name="is_partner" value="1"
                       {{ $company->is_partner == 1 ? 'checked': '' }} id="1"> Yes
            </label>
            <label
                    class="FontNormalStyle">
                <input type="radio" name="is_partner" value="0"
                       {{ $company->is_partner == 0 ? 'checked': '' }} id="2"> No
            </label>
        </li>
        <label class="error" for="is_partner">This field is required.</label>

        <li><label>
                {!! __('form.secsion_title_1_3_label')   !!}
                {!! __('form.secsion_title_1_3_sub') !!}
                <input type="text"
                       name="name_en"
                       value="{{ $company->name_en }}"
                       placeholder="Company Name Co., Ltd."></label>
        </li>
        <li><label>
                {!! __('form.secsion_title_1_4_label')   !!}
                {!! __('form.secsion_title_1_4_sub') !!}
                <input type="text"
                       name="name_ja"
                       value="{{ $company->name_ja }}"
                       placeholder="株式会社○○○"></label>
        </li>

        <li class="LabelProvince">
            {!! __('form.secsion_title_1_5_label')   !!}
            {!! __('form.secsion_title_1_5_sub') !!}

            @foreach($business_types as $key => $business_type)
                <label>
                    <input type="checkbox"
                           name="business_type_id[]"
                           {{ in_array($business_type->id,$business_pivot) ? 'checked' : ''}}
                           value="{{$business_type->id}}"
                           id="{{ "business_type_id{$key}" }}"> {{$business_type->name_en}}
                </label>
            @endforeach
        </li>
        <label class="error top--20" for="business_type_id[]">This field is required.</label>

        <li><label>
                {!! __('form.secsion_title_1_6_label')   !!}
                {!! __('form.secsion_title_1_6_sub') !!}
                <textarea name="address">{!! $company->address !!}</textarea></label>
        </li>
        <li><label>
                {!! __('form.secsion_title_1_7_label')   !!}
                {!! __('form.secsion_title_1_7_sub') !!}
                <input
                        name="google_map_url"
                        value="{{ $company->google_map_url }}"
                        type="text"></label></li>
        <li><label>
                {!! __('form.secsion_title_1_8_label')   !!}
                {!! __('form.secsion_title_1_8_sub') !!}
                <input
                        name="official_website_url"
                        value="{{ $company->official_website_url ?? null }}"
                        type="text"></label></li>
        <li>
            <label>
                {!! __('form.secsion_title_1_9_label')   !!}
                {!! __('form.secsion_title_1_9_sub') !!}
                <input
                        name="facebook_url"
                        value="{{ $company->facebook_url ?? null }}"
                        type="text"></label>
        </li>
        <li>
            <label>
                {!! __('form.secsion_title_1_10_label')   !!}
                {!! __('form.secsion_title_1_10_sub') !!}
                <input
                        name="instagram_url"
                        value="{{ $company->instagram_url ?? null }}"
                        type="text"></label>
        </li>
        <li>
            <label>
                {!! __('form.secsion_title_1_11_label')   !!}
                {!! __('form.secsion_title_1_11_sub') !!}
                <input
                        name="youtube_channel_url"
                        value="{{ $company->youtube_channel_url ?? null }}"
                        type="text"></label>
        </li>
        <li>
            <label>
                {!! __('form.secsion_title_1_12_label')   !!}
                {!! __('form.secsion_title_1_12_sub') !!}
                <input
                        name="twitter"
                        value="{{ $company->twitter ?? null }}"
                        type="text"></label>
        </li>
        <li>
            <label>
                {!! __('form.secsion_title_1_13_label')   !!}
                {!! __('form.secsion_title_1_13_sub') !!}
                <input
                        name="line"
                        value="{{ $company->line ?? null }}"
                        type="text"></label>
        </li>

        <li><label>
                {!! __('form.secsion_title_1_14_label')   !!}
                {!! __('form.secsion_title_1_14_sub') !!}
                <input
                        name="contact_person_name"
                        value="{{ $company->contact_person_name ?? null }}"
                        type="text"></label>
        </li>

        <li>
            <label>
                {!! __('form.secsion_title_1_15_label')   !!}
                {!! __('form.secsion_title_1_15_sub') !!}
                <input
                        name="department"
                        value="{{ $company->department ?? null }}"
                        type="text"></label>
        </li>

        <li>
            <label>
                {!! __('form.secsion_title_1_16_label')   !!}
                {!! __('form.secsion_title_1_16_sub') !!}
                <input
                        name="email"
                        value="{{ $company->email ?? null }}"
                        type="email"></label>
        </li>
        {{--
        <li>
            <label>
                {!! __('form.secsion_title_1_16_1_label') !!}
                {!! __('form.secsion_title_1_16_1_sub') !!}
                <input
                        name="email_contact"
                        required
                        value="{{ $company->email_contact }}"
                        type="email"></label>
        </li>
        --}}

        <li>
            <label>
                {!! __('form.secsion_title_1_17_label')   !!}
                {!! __('form.secsion_title_1_17_sub') !!}
                <input
                        name="tel"
                        value="{{ $company->tel ?? null }}"
                        type="text" placeholder="81-xx-xxx-xxxx"></label>
        </li>
        <li class="LabelMarTop">
            {!! __('form.secsion_title_1_18_label')   !!}
            {!! __('form.secsion_title_1_18_sub') !!}

            @foreach($languages as $key => $language)
                <label>
                    <input type="checkbox"
                           name="language_id[]"
                           {{ in_array($language->id,$language_pivot ?? []) ? 'checked' : ''}}
                           value="{{$language->id}}"
                           id="{{ "language_id{$key}" }}"> {{ $language->name_en }}
                </label>
            @endforeach
        </li>
        <label class="error top--20" for="language_id[]"></label>

        <li>
            {!! __('form.secsion_title_1_19_1_label')   !!}
            {!! __('form.secsion_title_1_19_1_sub') !!}
            <p>
                <label><input type="radio"
                              name="registered"
                              {{ (!empty($company->registered) && $company->registered == '1' ? 'checked' : '') }}
                              required
                              value="1"> 旅行業登録業者です│Registered travel agency</label><br>
                <label><input type="radio"
                              name="registered"
                              {{ (!empty($company->registered) && $company->registered == '2' ? 'checked' : '') }}
                              required
                              value="2"> 旅行サービス手配業です│Registered land operator</label><br>
                <label><input type="radio"
                              name="registered"
                              {{ (!empty($company->registered) && $company->registered == '3' ? 'checked' : '') }}
                              required
                              value="3"> 旅行業者ではありません│Not Travel Agency</label>
            </p>
            <label id="registered-error" class="error" for="registered" style="display: inline;"></label>
            <br>
            <div class="Registration3"
                 style="{{ (!empty($company->registered) && $company->registered == '3') ? 'display: none' : 'display: block' }}">
                <p>
                    <label>登録番号│Japan travel agency registration number<br>
                        <span class="FontNormal2">（ Japan Tourism Agency Commissioner of Japan Tourism Agency Registered Travel Agency No. 123 ）</span><br>
                        <textarea
                                required
                                name="licens_no">{!! !empty($company->licens_no) ? $company->licens_no : '' !!}</textarea></label>
                </p>
            </div>
        </li>


        <li class="LabelMarTop">
            {!! __('form.secsion_title_1_19_label')   !!}
            {!! __('form.secsion_title_1_19_sub') !!}
            <label>
                <input type="radio"
                       name="is_branch_thai" id="bit1"
                       value="1"
                       {{ $company->is_branch_thai == 1 ? 'checked': '' }}
                       onclick="CheckType(1);"> Yes</label>
            <label>
                <input type="radio"
                       name="is_branch_thai"
                       id="bit2"
                       value="2"
                       {{ $company->is_branch_thai == 2 ? 'checked': '' }}
                       onclick="CheckType(2);"> No </label>
        </li>
        <label class="error top--20" for="is_branch_thai"></label>

        <div class="BIT" id="BIT">
            <p>＜タイ支店・レップ情報│Information of Branch／representative office in Thailand＞</p>
            <li><label>
                    {!! __('form.secsion_title_1_20_label')   !!}
                    {!! __('form.secsion_title_1_20_sub') !!}
                    <input
                            name="branch_company_name_en"
                            value="{{ $company->branch_company_name_en ?? null }}"
                            type="text">
                </label></li>
            <li><label>
                    {!! __('form.secsion_title_1_21_label')   !!}
                    {!! __('form.secsion_title_1_21_sub') !!}
                    <input
                            name="branch_company_name_th"
                            value="{{ $company->branch_company_name_th ?? null }}"
                            type="text"></label></li>
            <li><label>
                    {!! __('form.secsion_title_1_22_label')   !!}
                    {!! __('form.secsion_title_1_22_sub') !!}
                    <textarea
                            name="branch_address"
                            placeholder="Address (English)">{!! $company->branch_address ?? null !!}</textarea></label>
            </li>
            <li><label>
                    {!! __('form.secsion_title_1_23_label')   !!}
                    {!! __('form.secsion_title_1_23_sub') !!}
                    <input name="branch_google_map_url"
                           value="{{ $company->branch_google_map_url ?? null }}"
                           type="text"></label></li>
            <li><label>
                    {!! __('form.secsion_title_1_24_label')   !!}
                    {!! __('form.secsion_title_1_24_sub') !!}
                    <input
                            name="branch_thai_website_url"
                            value="{{ $company->branch_thai_website_url }}"
                            type="text"></label>
            </li>

            <li><label>
                    {!! __('form.secsion_title_1_25_label')   !!}
                    {!! __('form.secsion_title_1_25_sub') !!}
                    <input
                            name="branch_contact_person_name"
                            value="{{ $company->branch_contact_person_name ?? null }}"
                            type="text"></label>
            </li>

            <li>
                <label>
                    {!! __('form.secsion_title_1_26_label')   !!}
                    {!! __('form.secsion_title_1_26_sub') !!}
                    <input
                            name="branch_department"
                            value="{{ $company->branch_department }}"
                            type="text">
                </label>
            </li>


            <li><label>
                    {!! __('form.secsion_title_1_27_label')   !!}
                    {!! __('form.secsion_title_1_27_sub') !!}
                    <input
                            name="branch_email"
                            value="{{ $company->branch_email ?? null }}"
                            type="email"></label></li>
            <li><label>
                    {!! __('form.secsion_title_1_28_label')   !!}
                    {!! __('form.secsion_title_1_28_sub') !!}
                    <input type="text"
                           name="branch_tel"
                           value="{{ $company->branch_tel ?? null }}"
                           placeholder="66-xx-xxx-xxxx"></label>
            </li>
            <li class="LabelMarTop">
                {!! __('form.secsion_title_1_29_label')   !!}
                {!! __('form.secsion_title_1_29_sub') !!}
                @foreach($languages_for_branches as $key => $language_branche)
                    <label>
                        <input type="checkbox"
                               name="language_for_branch_id[]"
                               {{ in_array($language_branche->id,$language_branch_pivot ?? []) ? 'checked' : ''}}
                               value="{{$language_branche->id}}"
                               id="{{ "language_brancheid{$key}" }}"> {{ $language_branche->name_en }}
                    </label>
                @endforeach
            </li>
            <label class="error top--20" for="language_for_branch_id[]">This field is required.</label>
        </div>
    </ul>
</fieldset>
@push('scripts')
    <script>
        $("input[name='registered']").change(function () {
            if ($(this).val() == '3') {
                $('.Registration3').hide();
            } else {
                $('.Registration3').show();
            }
        });
    </script>
@endpush