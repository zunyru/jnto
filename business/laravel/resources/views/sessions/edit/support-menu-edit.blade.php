<fieldset style="background-color: #F0FAFF;">
    <legend style="color: #6F8BFF;">3 Support menu</legend>
    <ul>
        <li><p>3-1 サポートメニュー | Support menu </p>
            <div class="MaterialURL">
                <ul>
                    <li><label>
                            <select
                                    name="support_menu1"
                                    onchange="CheckSupport1(this.value);">
                                <option
                                        name="support_menu1"
                                        value="">Select
                                </option>
                                @foreach($support_menus as $support_menu1)
                                    <option
                                            @if(!empty($suports))
                                            {{ $suports->support_menu1 == $support_menu1->id ? 'selected' : ''}}
                                            @endif
                                            value="{{ $support_menu1->id }}">
                                        {{ $support_menu1->name_en }}
                                    </option>
                                @endforeach
                            </select>
                        </label></li>
                    <li><label>
                            <input
                                    name="manu_name1"
                                    value="{{ !empty($suports) ? $suports->manu_name1 : '' }}"
                                    type="text" id="Support1Name" disabled
                                    placeholder="サポートメニュー名 | Support menu name"></label></li>
                    <li><label>
                            <select
                                    name="support_menu2"
                                    onchange="CheckSupport2(this.value);">
                                <option
                                        value="">Select
                                </option>
                                @foreach($support_menus as $support_menu2)
                                    <option
                                            @if(!empty($suports))
                                            {{ $suports->support_menu2 == $support_menu2->id ? 'selected' : ''}}
                                            @endif
                                            value="{{ $support_menu2->id }}">
                                        {{ $support_menu2->name_en }}
                                    </option>
                                @endforeach
                            </select>
                        </label></li>
                    <li><label>
                            <input
                                    name="manu_name2"
                                    value="{{ !empty($suports) ? $suports->manu_name2 : '' }}"
                                    type="text" id="Support2Name" disabled
                                    placeholder="サポートメニュー名 | Support menu name"></label></li>
                    <li><label>
                            <select
                                    name="support_menu3"
                                    onchange="CheckSupport3(this.value);">
                                <option value="">Select
                                </option>
                                @foreach($support_menus as $support_menu3)
                                    <option
                                            @if(!empty($suports))
                                            {{ $suports->support_menu3 == $support_menu3->id ? 'selected' : ''}}
                                            @endif
                                            value="{{ $support_menu3->id }}">
                                        {{ $support_menu3->name_en }}
                                    </option>
                                @endforeach
                            </select>
                        </label></li>
                    <li><label>
                            <input
                                    name="manu_name3"
                                    value="{{ !empty($suports) ? $suports->manu_name3 : '' }}"
                                    type="text" id="Support3Name" disabled
                                    placeholder="サポートメニュー名 | Support menu name"></label></li>
                    <li><label>
                            <select
                                    name="support_menu4"
                                    onchange="CheckSupport4(this.value);">
                                <option

                                        value="">Select
                                </option>
                                @foreach($support_menus as $support_menu4)
                                    <option
                                            @if(!empty($suports))
                                            {{ $suports->support_menu4 == $support_menu4->id ? 'selected' : ''}}
                                            @endif
                                            value="{{ $support_menu4->id }}">
                                        {{ $support_menu4->name_en }}
                                    </option>
                                @endforeach
                            </select>
                        </label></li>
                    <li><label>
                            <input
                                    name="manu_name4"
                                    value="{{ !empty($suports) ? $suports->manu_name4 : '' }}"
                                    type="text" id="Support4Name" disabled
                                    placeholder="サポートメニュー名 | Support menu name"></label></li>
                    <li>
                        <label>
                            <select
                                    name="support_menu5"
                                    onchange="CheckSupport5(this.value);">
                                <option

                                        value="">Select
                                </option>
                                @foreach($support_menus as $support_menu5)
                                    <option
                                            @if(!empty($suports))
                                            {{ $suports->support_menu5 == $support_menu5->id ? 'selected' : ''}}
                                            @endif
                                            value="{{ $support_menu5->id }}">
                                        {{ $support_menu5->name_en }}
                                    </option>
                                @endforeach
                            </select>
                        </label>
                    </li>
                    <li>
                        <label>
                            <input
                                    name="manu_name5"
                                    value="{{  !empty($suports) ? $suports->manu_name5 : ''}}"
                                    type="text" id="Support5Name" disabled
                                    placeholder="サポートメニュー名 | Support menu name"></label>
                    </li>

                    <li>
                        <label>
                            <select
                                    name="support_menu6"
                                    onchange="CheckSupport6(this.value);">
                                <option

                                        value="">Select
                                </option>
                                @foreach($support_menus as $support_menu6)
                                    <option
                                            @if(!empty($suports))
                                            {{ $suports->support_menu6 == $support_menu6->id ? 'selected' : ''}}
                                            @endif
                                            value="{{ $support_menu6->id }}">
                                        {{ $support_menu6->name_en }}
                                    </option>
                                @endforeach
                            </select>
                        </label>
                    </li>
                    <li>
                        <label>
                            <input
                                    name="manu_name6"
                                    value="{{ !empty($suports) ? $suports->manu_name6 : '' }}"
                                    type="text" id="Support6Name" disabled
                                    placeholder="サポートメニュー名 | Support menu name"></label>
                    </li>

                    <li>
                        <label>
                            <select
                                    name="support_menu7"
                                    onchange="CheckSupport7(this.value);">
                                <option

                                        value="">Select
                                </option>
                                @foreach($support_menus as $support_menu7)
                                    <option
                                            @if(!empty($suports))
                                            {{ $suports->support_menu7 == $support_menu7->id ? 'selected' : ''}}
                                            @endif
                                            value="{{ $support_menu7->id }}">
                                        {{ $support_menu7->name_en }}
                                    </option>
                                @endforeach
                            </select>
                        </label>
                    </li>
                    <li>
                        <label>
                            <input
                                    name="manu_name7"
                                    value="{{ !empty($suports) ? $suports->manu_name7 : '' }}"
                                    type="text" id="Support7Name" disabled
                                    placeholder="サポートメニュー名 | Support menu name"></label>
                    </li>

                    <li>
                        <label>
                            <select
                                    name="support_menu8"
                                    onchange="CheckSupport8(this.value);">
                                <option

                                        value="">Select
                                </option>
                                @foreach($support_menus as $support_menu8)
                                    <option
                                            @if(!empty($suports))
                                            {{ $suports->support_menu8 == $support_menu8->id ? 'selected' : ''}}
                                            @endif
                                            value="{{ $support_menu8->id }}">
                                        {{ $support_menu8->name_en }}
                                    </option>
                                @endforeach
                            </select>
                        </label>
                    </li>
                    <li>
                        <label>
                            <input
                                    name="manu_name8"
                                    value="{{ !empty($suports) ? $suports->manu_name8 : '' }}"
                                    type="text" id="Support8Name" disabled
                                    placeholder="サポートメニュー名 | Support menu name"></label>
                    </li>

                </ul>
            </div>
        </li>


        <label id="support_menu1-error" class="error top--20" for="support_menu1">This field is required.</label>
        <li><p>3-2 添付資料 | <span class="FontNormal">Document</span><br><span class="FontNormal2">PDF、5MB以下 | ไฟล์ PDF ขนาดต่ำกว่า 5MB</span>
            </p>
            <!--<div class="FilePDF">-->
            <label style="display: block; margin-bottom: 10px;">
                <label for="support_menu_about_pdf"
                       class="FileUploadCSS">
                    Choose a file
                    <input type="file"
                           name="support_menu_about_pdf"
                           id="support_menu_about_pdf"/>
                </label>
                <br>
                <label id="support_menu_about_pdf-error" class="error" for="support_menu_about_pdf"
                       style="display: none;">
                </label>
                @if(!empty($suports->support_menu_about_pdf))
                    <a href="{{ asset($suports->support_menu_about_pdf) }}" target="_blank">
                        <img class="mt-3" src="{{ asset('assets/images/pdf.png') }}"
                             style="width: 10%; max-width: auto;margin-top:10px;">
                    </a>
                    <br>
                    <p class="DelPicture2">
                        <a href="javascript:void(0);"
                           data-filters="support_menu_about_pdf"
                           data-db="company_support_menus"
                           data-where="company_id"
                           data-id="{{ $company->ref_id }}"
                           class="delete-img bg-red-600 text-white text-center p-2 rounded-md">
                            x Delete
                        </a></p>
                @endif
            </label>
            <label>説明文 | <span class="FontNormal">About</span>
            <br><span class="FontNormal2">英語またはタイ語100文字以内、URLも記載可能　 ภาษาอังกฤษหรือภาษาไทย สูงสุด 100 ตัวอักษร และสามารถใส่  URL Website ได้</span>
                <input
                        name="support_about"
                        value="{{ !empty($suports) ? $suports->support_about : '' }}"
                        type="text" class="TextPDFName"
                        placeholder="">
            </label>
            <!--<div>-->
        </li>
    </ul>
</fieldset>