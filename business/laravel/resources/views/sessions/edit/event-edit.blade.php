<fieldset style="background-color: #CCF4EC; margin-top:40px;" class="pb-4">
    <legend style="color: #123;">Topic Status</legend>
    <div class="flex flex-row">
        <h6 class="font-bold pr-4">Status</h6>
        <label class="FontNormalStyle mr-2">
            <input type="radio"
                   {{ @$event->status == 'done' ? 'checked' : '' }}
                   name="event_status" value="done"> Done
        </label>
        <label class="FontNormalStyle">
            <input type="radio"
                   {{ @$event->status == 'editing' ? 'checked' : '' }}
                   name="event_status" value="editing"> Editing
        </label>
    </div>

    <div class="mb-4 mt-6" >
        <h6 class="font-bold pr-4">Category</h6>
        <label class="FontNormalStyle mr-2">
            <input type="radio"
                   {{ @$event->category == 'event' ? 'checked' : '' }}
                   onclick="TopicCheck(1);"
                   name="category" value="event"> イベント | Event
        </label>
        <div>
            <span class="FontNormal2">
            開催期間が確定しているイベントで、開催期間が1ヶ月以内のイベント。また登録日から6ヶ月以内に実施されるイベントは「イベント」としてご登録ください。
            </span>
            <br>
            <span class="FontNormal2">
                Please post as event in case of the event has a set duration and will occur within one month of the event's start date and the event will occur within six months of the post date.
            </span>
        </div><br>
        <label class="FontNormalStyle">
            <input type="radio"
                   {{ @$event->category == 'information' ? 'checked' : '' }}
                   onclick="TopicCheck(2);"
                   name="category" value="information"> お知らせ | Latest information
        </label>
        <div>
            <span class="FontNormal2">
            日本側セラーからのお知らせや開催期間が決まってないイベントなど。また開催期間が1ヶ月以上のイベント、登録日から6ヶ月以降に実施されるイベントは「お知らせ」としてご登録ください。
            </span>
            <br>
            <span class="FontNormal2">
                Please post as “Latest Information” in case that There is no set the time for the event and the Event period is more than 1 month.  Additionally it will be held after 6 months.
            </span>
        </div>
    </div>
    <label id="category-error" class="error" for="category" style="display: inline;"></label>

    <div class="flex flex-row mb-4 mt-6" id="TopicCheck"  style="display: {{ @$event->category == 'event' ? 'flex' : 'none' }}">
        <label>
            <p>Start Date</p>
            <input type="text"
                   class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                   name="event_date_start"
                   autocomplete="off"
                   required
                   id="from"
                   value="{{ !empty(@$event) ? is_null($event->event_date_start) ? '' : $event->event_date_start->format('Y-m-d') : '' }}"
                   placeholder="2021-08-00">
        </label>

        <label class="ml-4">
            <p>End Date</p>
            <input type="text"
                   class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                   name="event_date_end"
                   autocomplete="off"
                   required
                   id="to"
                   value="{{ !empty(@$event) ? is_null($event->event_date_end) ? '' : $event->event_date_end->format('Y-m-d') : '' }}"
                   placeholder="2021-08-00">
        </label>
    </div>

    <div class="mb-4 mt-6">
        <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
            Photos
        </label>
        <span class="text-sm text-gray-500">
            画像サイズの目安：W1035×H465ピクセル／解像度200dpi／1MB以下／PNGまたはJPG<br>ขนาดภาพที่แนะนำ W1035×H465 pixels / ความละเอียด 200 dpi / ความจุต่ำกว่า 1MB / PNG หรือ JPG
        </span>
        <div class="flex text-center rounded-xl border-gray-300 border p-3">
            <div class="flex-1 p-2">
                <div class="mb-4">
                    <div class="flex flex-col">
                        @if(!empty(@$event->photo))
                            <img src="{{ asset($event->photo) }}"
                                 id="photo1_preview"
                                 style="width: 100%; height: auto;">
                        @else
                            <img src=""
                                 id="photo_preview"
                                 style="width: 100%; height: auto;">
                        @endif
                        <br>
                        <label for="photo"></label>
                        <label for="photo"
                               class="FileUploadCSS">
                            Choose a file
                            <input type="file"
                                   name="photo"
                                   accept="image/*"
                                   id="photo"/>
                        </label>
                        <br>
                        <label id="photo1-error" class="error" for="photo" style="display: none;">
                        </label>
                        @if(!empty($event->photo))
                            <br>
                            <a href="javascript:void(0);"
                               data-filters="photo"
                               data-db="topics"
                               data-where="id"
                               data-id="{{ $event->ref_id }}"
                               class="w-1/2 delete-img bg-red-600 text-white text-center p-2 rounded-md">
                                <svg class="stroke-2 text-white inline-block h-4 w-4"
                                     xmlns="http://www.w3.org/2000/svg" fill="none"
                                     viewBox="0 0 24 24"
                                     stroke="currentColor">
                                    <path strokeLinecap="round" strokeLinejoin="round"
                                          strokeWidth={2}
                                          d="M6 18L18 6M6 6l12 12"/>
                                </svg>
                                Delete
                            </a>
                        @endif
                    </div>

                </div>
            </div>
        </div>

    </div>

    <div class="mb-4 ">
        <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
            Title
        </label>
        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
               id="title"
               type="text"
               autocomplete="off"
               name="event_title"
               value="{{ !empty($event->title) ? $event->title : '' }}"
               placeholder="Event Title">
    </div>

    <div class="mb-4 mt-10">
        <label class="block text-gray-700 text-sm font-bold mb-2" for="event_detail">
            Detail
        </label>
        <textarea id="detail-event"
                  name="event_detail">{!! !empty($event->detail) ? nl2br(preg_replace('/<p[^>]*>/', '', $event->detail)) : '' !!}</textarea>
    </div>
</fieldset>
@push('scripts')
    <link rel="stylesheet" href="{{ asset('assets/jquery-ui/jquery-ui.min.css') }}">
    <script src="{{ asset('assets/jquery-ui/jquery-ui.min.js') }}"></script>
    <script>
        $("#photo").change(function () {
            readURL(this);
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var preview = $(input).attr('id') + '_preview';
                    $('#' + preview).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            } else {
                $('#previewHolder').attr('src', '');
            }
        }

        tinymce.init({
            selector: 'textarea#detail-event',
            menubar: false,
            width: '100%',
            height: 500,
            forced_root_block: false,
            force_br_newlines: true,
            force_p_newlines: false,
            toolbar: 'bold customBr link underline',
            plugins: 'nonbreaking link',
            convert_urls: false,
            setup: function (editor) {
                editor.ui.registry.addButton('customBr', {
                    text: 'Enter',
                    onAction: function () {
                        editor.insertContent('<br/> ');
                    },
                });
            }
        });

        $('.delete-img').click(function () {
            var data_value = $(this).attr('data-filters');
            var data_db = $(this).attr('data-db');
            var data_id = $(this).attr('data-id');
            var data_where = $(this).attr('data-where');
            Swal.fire({
                title: 'Are you sure to delete ?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            id: data_id,
                            field: data_value,
                            where: data_where,
                            db: data_db
                        },
                        url: `{{ route('file.delete') }}`,
                    }).done(function (msg) {
                        toastr.success(msg.message);
                        setTimeout(function () {
                            location.reload();
                        }, 1000);

                    }).fail(function (msg) {
                        toastr.error(msg.message)
                    });
                }
            });
        });

        $(function () {

            $("#from")
                .datepicker({
                    dateFormat: "yy-mm-dd",
                    changeMonth: true,
                    changeYear: true,
                    monthNamesShort: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                })
                .on("change", function () {
                    $("#to").datepicker("option", "minDate", getDate(this));
                });

            $("#to").datepicker({
                dateFormat: "yy-mm-dd",
                changeMonth: true,
                changeYear: true,
                monthNamesShort: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            }).on("change", function () {
                $("#from").datepicker("option", "maxDate", getDate(this));
            });

            function getDate(element) {
                var date;

                var dateFormat = "yy-mm-dd";
                try {
                    date = $.datepicker.parseDate(dateFormat, element.value);
                } catch (error) {
                    date = null;
                }
                console.log(date);
                return date;
            }
        });

        function TopicCheck(id){
            var TopicCheck = document.getElementById('TopicCheck');
            if(id==1){ TopicCheck.style.display='flex'; }
            if(id==2){ TopicCheck.style.display='none'; }
        }
    </script>
@endpush