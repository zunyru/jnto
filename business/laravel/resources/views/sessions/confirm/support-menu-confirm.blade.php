<fieldset style="background-color: #F0FAFF;">
    <legend style="color: #6F8BFF;">3 Support menu</legend>
    <ul>
        <li><p>3-1 サポートメニュー | Support menu</p>
            <div class="MaterialURL">
                <ul>
                    <li style="width: 100%; clear: both; margin: 0px; padding: 10px 0px; border-bottom: 1px solid #eee;">
                        @foreach($support_menus as $support_menu1)
                            <b>{{ $suports->support_menu1 == $support_menu1->id ? $support_menu1->name_en." : " : ''}}</b>
                        @endforeach
                        @if(empty($suports->support_menu1))
                            <span>-</span>
                        @else
                            <span>{{ $suports->manu_name1 }}</span>
                        @endif
                    </li>
                    @if(!empty($suports->support_menu2))
                        <li style="width: 100%; clear: both; margin: 0px; padding: 10px 0px; border-bottom: 1px solid #eee;">
                            @foreach($support_menus as $support_menu2)
                                <b>
                                    {{ $suports->support_menu2 == $support_menu2->id ? $support_menu2->name_en." : " : ''}}
                                </b>
                            @endforeach
                            @if(empty($suports->support_menu2))
                                <span>-</span>
                            @else
                                <span>{{ $suports->manu_name2 }}</span>
                            @endif
                        </li>
                    @endif

                    @if(!empty($suports->support_menu3))
                        <li style="width: 100%; clear: both; margin: 0px; padding: 10px 0px; border-bottom: 1px solid #eee;">
                            @foreach($support_menus as $support_menu3)
                                <b>
                                    {{ $suports->support_menu3 == $support_menu3->id ? $support_menu3->name_en." : " : ''}}
                                </b>
                            @endforeach
                            @if(empty($suports->support_menu3))
                                <span>-</span>
                            @else
                                <span>{{ $suports->manu_name3 }}</span>
                            @endif
                        </li>
                    @endif

                    @if(!empty($suports->support_menu4))
                        <li style="width: 100%; clear: both; margin: 0px; padding: 10px 0px; border-bottom: 1px solid #eee;">
                            @foreach($support_menus as $support_menu4)
                                <b>
                                    {{ $suports->support_menu4 == $support_menu4->id ? $support_menu4->name_en." : " : ''}}
                                </b>
                            @endforeach
                            @if(empty($suports->support_menu4))
                                <span>-</span>
                            @else
                                <span>{{ $suports->manu_name4 }}</span>
                            @endif
                        </li>
                    @endif

                    @if(!empty($suports->support_menu5))
                        <li style="width: 100%; clear: both; margin: 0px; padding: 10px 0px; border-bottom: 1px solid #eee;">
                            @foreach($support_menus as $support_menu5)
                                <b>
                                    {{ $suports->support_menu5 == $support_menu5->id ? $support_menu5->name_en." : " : ''}}
                                </b>
                            @endforeach
                            @if(empty($suports->support_menu5))
                                <span>-</span>
                            @else
                                <span>{{ $suports->manu_name5 }}</span>
                            @endif
                        </li>
                    @endif
                </ul>
            </div>
        </li>
        <li><p>3-2 添付資料 | <span class="FontNormal">Document</span><br><span class="FontNormal2">PDF、5MB以下 | ไฟล์ PDF ขนาดต่ำกว่า 5MB</span>
            </p>
            <!--<div class="FilePDF">-->
            @if(!empty($suports->support_menu_about_pdf))
                <label style="display: block; margin-bottom: 10px;">
                    <div style="width: 80px; height: 100px; float: left; margin-right: 10px;">
                        <a target="_blank" href="{{ asset($suports->support_menu_about_pdf) }}">
                            <img src="{{ asset('assets/images/pdf.png') }}" style="width: 100%;">
                        </a>
                    </div>
                </label>
                <br>
                <label>説明文 | About : 
                <br><span class="FontNormal2">英語またはタイ語100文字以内、URLも記載可能　 ภาษาอังกฤษหรือภาษาไทย สูงสุด 100 ตัวอักษร และสามารถใส่  URL Website ได้</span>
                <br><span  class="FontNormal">File PDF {{ $suports->support_about ?? '-' }}</span></label>
            @else
                <p>-</p>
        @endif
        <!--<div>-->
        </li>
    </ul>
</fieldset>