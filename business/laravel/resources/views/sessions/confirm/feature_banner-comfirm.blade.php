<fieldset style="border:none">
    <div class="flex FormList w-full items-center">
        <div class="pr-10 w-1/2">
            <label>
                <input
                        disabled
                        {{ $company->is_show_banner == 1 ? 'checked': '' }}
                        name="is_show_banner"
                        id="is_show_banner"
                        value="1"
                        type="checkbox">
                Show banner (Only News1 | News2)
            </label>
        </div>
        <div class="flex w-1/2 items-center">
            <div class="w-1/6">
                <label class="">
                    | URL
                </label>
            </div>
            <div class="w-5/6">
                @if(!empty($company->url_banner))
                    <a target="_blank"href="{{ $company->url_banner }}" class="pl-4 text-blue-700 underline ">
                        {{ $company->url_banner }}
                    </a>
                @endif
            </div>

        </div>
    </div>
    <div class="flex FormList w-full items-center">
        <div class="flex FormList w-full items-center">
            <div class="pr-10 w-1/2">
                <label>
                    <input
                            disabled
                            {{ $company->is_feature_banner == 1 ? 'checked': '' }}
                            name="is_feature_banner"
                            id="is_feature_banner"
                            value="1"
                            type="checkbox">
                    Feature banner (Top page)
                </label>
            </div>
        </div>
    </div>
</fieldset>