<fieldset style="background-color: #FFFFE6;">
    <legend>{!! __('form.secsion_title_1') !!}</legend>
    <ul>
        <li>
            <label>
                <p class="FloatLeft">
                    Email Login
                </p>
                <input type="text"
                       name="user_login"
                       value="{{ $company->user_login }}"
                       placeholder="Email Login">
            </label>
        </li>
        <li class="mb-2">
            {!! __('form.secsion_title_1_1_label') !!}
            {!! __('form.secsion_title_1_1_sub') !!}
            <label>
                <img src="{{ asset($company->logo) }}" width="150">
            </label>
        </li>
        <li class="LabelMarTop">
            {!! __('form.secsion_title_1_2_label') !!}
            {!! __('form.secsion_title_1_2_sub') !!}
            <label class="FontNormalStyle">
                <input type="radio" name="is_partner" value="1" disabled
                       {{ $company->is_partner == 1 ? 'checked': '' }} id="1"> Yes
            </label>
            <label
                    class="FontNormalStyle">
                <input type="radio" name="is_partner" value="0" disabled
                       {{ $company->is_partner == 0 ? 'checked': '' }} id="2"> No
            </label>
        </li>
        <li><label>
                {!! __('form.secsion_title_1_3_label') !!}
                {!! __('form.secsion_title_1_3_sub') !!}
                <p class="pl-4 text-gray-700">
                    {{ $company->name_en ?? '-' }}
                </p>
            </label>
        </li>
        <li>
            <label>
                {!! __('form.secsion_title_1_4_label') !!}
                {!! __('form.secsion_title_1_4_sub') !!}
                <p class="pl-4 text-gray-700">
                    {{ $company->name_ja ?? '-' }}
                </p>
            </label>
        </li>
        <li>
            <p>
            {!! __('form.secsion_title_1_5_label') !!}
            {!! __('form.secsion_title_1_5_sub') !!}
            </p>
            <label class="">
                @foreach($business_types as $key => $business_type)
                    @if(in_array($business_type->id,$business_pivot))
                       <span class=" pl-4 text-gray-700 " style="font-weight: 100;">
                           {{ "• {$business_type->name_en}" }}
                       </span><br>
                    @endif
                @endforeach
            </label>
        </li>

        <li><label>
                {!! __('form.secsion_title_1_6_label') !!}
                {!! __('form.secsion_title_1_6_sub') !!}
                <p class="pl-4 text-gray-700">
                    {!! $company->address ?? '-' !!}
                </p>
            </label>
        </li>
        <li><label>
                {!! __('form.secsion_title_1_7_label') !!}
                {!! __('form.secsion_title_1_7_sub') !!}
                @if(!empty($company->google_map_url))
                    <a target="_blank" href="{{ $company->google_map_url }}" class="pl-4 text-blue-700 underline ">
                        {{ $company->google_map_url ?? '-' }}
                    </a>
                @else
                    <p class="pl-4 text-gray-700">-</p>
                @endif
            </label>
        </li>
        <li><label>
                {!! __('form.secsion_title_1_8_label') !!}
                {!! __('form.secsion_title_1_8_sub') !!}
                @if(!empty($company->official_website_url))
                    <a target="_blank" href="{{ $company->official_website_url }}"
                       class="pl-4 text-blue-700 underline ">

                        {{ $company->official_website_url ?? '-' }}</a>
                @else
                    <p class="pl-4 text-gray-700">-</p>
                @endif
            </label>
        </li>

        <li>
            <label>
                {!! __('form.secsion_title_1_9_label') !!}
                {!! __('form.secsion_title_1_9_sub') !!}
                @if(!empty($company->facebook_url))
                    <a target="_blank" href="{{ $company->facebook_url }}" class="pl-4 text-blue-700 underline ">
                        {{ $company->facebook_url ?? '-' }}
                    </a>
                @else
                    <p class="pl-4 text-gray-700">-</p>
                @endif
            </label>
        </li>
        <li>
            <label>
                {!! __('form.secsion_title_1_10_label') !!}
                {!! __('form.secsion_title_1_10_sub') !!}
                @if(!empty($company->instagram_url))
                    <a target="_blank" href="{{ $company->instagram_url }}" class="pl-4 text-blue-700 underline ">
                        {{ $company->instagram_url ?? '-' }}
                    </a>
                @else
                    <p class="pl-4 text-gray-700">-</p>
                @endif
            </label>
        </li>
        <li>
            <label>
                {!! __('form.secsion_title_1_11_label') !!}
                {!! __('form.secsion_title_1_11_sub') !!}
                @if(!empty($company->youtube_channel_url))
                    <a target="_blank" href="{{ $company->youtube_channel_url }}" class="pl-4 text-blue-700 underline ">
                        {{ $company->youtube_channel_url ?? '-' }}
                    </a>
                @else
                    <p class="pl-4 text-gray-700">-</p>
                @endif
            </label>
        </li>
        <li>
            <label>
                {!! __('form.secsion_title_1_12_label') !!}
                {!! __('form.secsion_title_1_12_sub') !!}
                @if(!empty($company->twitter))
                    <a target="_blank" href="{{ $company->twitter }}" class="pl-4 text-blue-700 underline ">
                        {{ $company->twitter ?? '-' }}
                    </a>
                @else
                    <p class="pl-4 text-gray-700">-</p>
                @endif
            </label>
        </li>
        <li>
            <label>
                {!! __('form.secsion_title_1_13_label') !!}
                {!! __('form.secsion_title_1_13_sub') !!}
                @if(!empty($company->line))
                    <a target="_blank" href="{{ $company->line }}" class="pl-4 text-blue-700 underline ">
                        {{ $company->line ?? '-' }}
                    </a>
                @else
                    <p class="pl-4 text-gray-700">-</p>
                @endif
            </label>
        </li>

        <li>
            <label>
                {!! __('form.secsion_title_1_14_label') !!}
                {!! __('form.secsion_title_1_14_sub') !!}
                <p class="pl-4 text-gray-700">
                    {{ $company->contact_person_name ?? '-'}}
                </p>
            </label>
        </li>
        <li>
            <label>
                {!! __('form.secsion_title_1_15_label') !!}
                {!! __('form.secsion_title_1_15_sub') !!}
                <p class="pl-4 text-gray-700">
                    {{ $company->department ?? '-' }}
                </p>
            </label>
        </li>

        <li>
            <label>
                {!! __('form.secsion_title_1_16_label') !!}
                {!! __('form.secsion_title_1_16_sub') !!}
                <p class="pl-4 text-gray-700">
                    {{ $company->email ?? '-' }}
                </p>
            </label>
        </li>


        <li><label>
                {!! __('form.secsion_title_1_17_label') !!}
                {!! __('form.secsion_title_1_17_sub') !!}
                <p class="pl-4 text-gray-700">
                    {{ $company->tel ?? '-' }}
                </p>
            </label>
        </li>
        <li class="LabelMarTop">
            {!! __('form.secsion_title_1_18_label') !!}
            {!! __('form.secsion_title_1_18_sub') !!}

            @foreach($languages as $key => $language)
                <label>
                    <input type="checkbox"
                           name="language_id[]"
                           disabled
                           {{ in_array($language->id,$language_pivot ?? []) ? 'checked' : ''}}
                           value="{{$language->id}}"
                           id="{{ "language_id{$key}" }}"> {{ $language->name_en }}
                </label>
        @endforeach


        <li>
			<p>1-19.旅行業登録番号│Japan travel agency registration number <span class="NeedRed">*</span></p>
			<span class="FontNormal2">英語の記載例：</span>
			<p><span class="FontNormal2">・観光庁長官登録旅行業第XXX号│Japan Tourism Agency Commissioner of Japan Tourism Agency Registered Travel Agency No. 123</span></p>
			<p><span class="FontNormal2">・東京都知事登録旅行業第XXXX号│The travel agent registered by Tokyo governor No.123</span></p>
			<p>
			<label><input type="radio"> 旅行業登録業者です│Registered travel agency</label><br>
			<label><input type="radio"> 旅行サービス手配業です│Registered land operator</label><br>
			<label><input type="radio"> 旅行業者ではありません│Not Travel Agency</label>
			</p><br>
			<p><label>登録番号│Japan travel agency registration number<br>
			<span class="FontNormal2">（ Japan Tourism Agency Commissioner of Japan Tourism Agency Registered Travel Agency No. 123 ）</span><br>
			<textarea></textarea></label></p>
		</li>


        <li class="LabelMarTop">
            {!! __('form.secsion_title_1_19_label') !!}
            {!! __('form.secsion_title_1_19_sub') !!}
            <label>
                <input type="radio"
                       name="is_branch_thai" id="bit1"
                       disabled
                       value="1"
                       {{ $company->is_branch_thai == 1 ? 'checked': '' }}
                       onclick="CheckType(1);"> Yes</label>
            <label>
                <input type="radio"
                       name="is_branch_thai"
                       disabled
                       id="bit2"
                       value="2"
                       {{ $company->is_branch_thai == 2 ? 'checked': '' }}
                       onclick="CheckType(2);"> No </label></li>
        <div class="BIT" id="BIT">
            <p>＜タイ支店・レップ情報│Information of Branch／representative office in Thailand＞</p>
            <li><label>
                    {!! __('form.secsion_title_1_20_label') !!}
                    {!! __('form.secsion_title_1_20_sub') !!}
                    <p class="pl-4 text-gray-700">
                        {{ $company->branch_company_name_en ?? '-' }}
                    </p>
                </label>
            </li>
            <li><label>
                    {!! __('form.secsion_title_1_21_label') !!}
                    {!! __('form.secsion_title_1_21_sub') !!}
                    <p class="pl-4 text-gray-700">
                        {{ $company->branch_company_name_th ?? '-' }}
                    </p>
                </label>
            </li>
            <li><label>
                    {!! __('form.secsion_title_1_22_label') !!}
                    {!! __('form.secsion_title_1_22_sub') !!}
                    <p class="pl-4 text-gray-700">
                        {!! $company->branch_address ?? '-' !!}
                    </p>
                </label>
            </li>
            <li><label>
                    {!! __('form.secsion_title_1_23_label') !!}
                    {!! __('form.secsion_title_1_23_sub') !!}
                    @if(!empty($company->branch_google_map_url))
                        <a target="_blank" href="{{ $company->branch_google_map_url }}"
                           class="pl-4 text-blue-700 underline ">
                            {{ $company->branch_google_map_url ?? '-' }}
                        </a>
                    @else
                        <p class="pl-4 text-gray-700">-</p>
                    @endif
                </label>
            </li>
            <li><label>
                    {!! __('form.secsion_title_1_24_label') !!}
                    {!! __('form.secsion_title_1_24_sub') !!}
                    @if(!empty($company->branch_thai_website_url))
                        <a target="_blank" href="{{ $company->branch_thai_website_url }}"
                           class="pl-4 text-blue-700 underline ">
                            {{ $company->branch_thai_website_url ?? '-' }}
                        </a>
                    @else
                        <p class="pl-4 text-gray-700">-</p>
                    @endif
                </label>
            </li>
            <li>
                <label>
                    {!! __('form.secsion_title_1_25_label') !!}
                    {!! __('form.secsion_title_1_25_sub') !!}
                    <p class="pl-4 text-gray-700">
                        {{ $company->branch_contact_person_name ?? '-' }}
                    </p>
                </label>
            </li>
            <li>
                <label>
                    {!! __('form.secsion_title_1_26_label') !!}
                    {!! __('form.secsion_title_1_26_sub') !!}
                    <p class="pl-4 text-gray-700">
                        {{ $company->branch_department ?? '-' }}
                    </p>
                </label>
            </li>
            <li><label>
                    {!! __('form.secsion_title_1_27_label') !!}
                    {!! __('form.secsion_title_1_27_sub') !!}
                    <p class="pl-4 text-gray-700">
                        {{ $company->branch_email ?? '-' }}
                    </p>
                </label>
            </li>
            <li><label>
                    {!! __('form.secsion_title_1_28_label') !!}
                    {!! __('form.secsion_title_1_28_sub') !!}

                    <p class="pl-4 text-gray-700">
                        {{ $company->branch_tel ?? '-' }}
                    </p>
                </label>
            </li>
            <li class="LabelMarTop">
                {!! __('form.secsion_title_1_29_label') !!}
                {!! __('form.secsion_title_1_29_sub') !!}
                @foreach($languages_for_branches as $key => $language_branche)
                    <label>
                        <input type="checkbox"
                               name="language_for_branch_id[]"
                               disabled
                               {{ in_array($language_branche->id,$language_branch_pivot ?? []) ? 'checked' : ''}}
                               value="{{$language_branche->id}}"
                               id="{{ "language_id{$key}" }}"> {{ $language_branche->name_en }}
                    </label>
            @endforeach

        </div>
    </ul>
</fieldset>