<fieldset style="background-color: #F5FFF0;">
    <legend style="color: #52AF00;">{!! __('service.secsion_title_2') !!}</legend>
    <ul>
        <li><label>
                {!! __('service.secsion_title_2_1_label') !!}
                {!! __('service.secsion_title_2_1_sub') !!}
                <p class="pl-4 text-gray-700">
                    {{ $services->title ?? '-' }}
                </p>
            </label>
        </li>
        <li><label>
                {!! __('service.secsion_title_2_2_label') !!}
                {!! __('service.secsion_title_2_2_sub') !!}
                <p class="pl-4 text-gray-700">
                    {!! nl2br($services->detail) ?? '-' !!}
                </p>
            </label>
        </li>
        <li>
            {!! __('service.secsion_title_2_3_label') !!}
            {!! __('service.secsion_title_2_3_sub') !!}
            @if(!empty($services->main_photo))
                <span>
            <img src="{{ asset($services->main_photo) }}" style="width: 100%; height: auto;">
            </span>
            @endif
        </li>
        <li>
            {!! __('service.secsion_title_2_4_label') !!}
            {!! __('service.secsion_title_2_4_sub') !!}
            <div class="FileList">
                <ul>
                    <li>
                        @if(!empty($services->other_photo))
                            <img src="{{ asset($services->other_photo) }}" style="width: 100%; height: auto;">
                        @else
                            <p>-</p>
                        @endif
                    </li>
                    <li>
                        @if(!empty($services->other_photo2))
                            <img src="{{ asset($services->other_photo2) }}" style="width: 100%; height: auto;">
                        @else
                            <p>-</p>
                        @endif
                    </li>
                    <li>
                        @if(!empty($services->other_photo3))
                            <img src="{{ asset($services->other_photo3) }}" style="width: 100%; height: auto;">
                        @else
                            <p>-</p>
                        @endif
                    </li>
                </ul>
            </div>
        </li>
        <li>
            <label>
                {!! __('service.secsion_title_2_5_label') !!}
                {!! __('service.secsion_title_2_5_sub') !!}
                @if(!empty($services->pr_youtube))
                    {!! $services->pr_youtube !!}
                @else
                    <p class="pl-4 text-gray-700">-</p>
                @endif
            </label>
        </li>
        <li>
            {!! __('service.secsion_title_2_6_label') !!}
            {!! __('service.secsion_title_2_6_sub') !!}
            @if(!empty($services->company_profile_pdf))
                <label style="display: block; margin-bottom: 10px;clear:both;">
                    <div style="width: 80px; height: 100px; float: left; margin-right: 10px;">
                        <a href="{{ asset($services->company_profile_pdf) }}" target="_blank">
                            <img
                                    class="mt-3"
                                    src="{{ asset('assets/images/pdf.png') }}"
                                    style="width: 90%; ">
                        </a>
                    </div>
                </label>
                <br>
                <label>説明文 | About : <br><span class="FontNormal">File PDF {{ $services->about ?? '-' }}</span></label>
            @else
                <p class=" pl-4 text-gray-700">-</p>
            @endif

            @if(!empty($services->company_profile_pdf2))
                <label style="display: block; margin-bottom: 10px;clear:both;">
                    <div style="width: 80px; height: 100px; float: left; margin-right: 10px;">
                        <a href="{{ asset($services->company_profile_pdf2) }}" target="_blank">
                            <img
                                    class="mt-3"
                                    src="{{ asset('assets/images/pdf.png') }}"
                                    style="width: 90%; ">
                        </a>
                    </div>
                </label>
                <br>
                <label>説明文 | About : <br><span class="FontNormal">File PDF {{ $services->about2 ?? '-' }}</span></label>
            @endif
            @if(!empty($services->company_profile_pdf3))
                <label style="display: block; margin-bottom: 10px;clear:both;">
                    <div style="width: 80px; height: 100px; float: left; margin-right: 10px;">
                        <a href="{{ asset($services->company_profile_pdf3) }}" target="_blank">
                            <img
                                    class="mt-3"
                                    src="{{ asset('assets/images/pdf.png') }}"
                                    style="width: 90%;">
                        </a>
                    </div>
                </label>
                <br>
                <label>説明文 | About : <br><span class="FontNormal">File PDF {{ $services->about3 ?? '-' }}</span></label>
            @endif
        </li>
        <!--<p class="FontHead">画像 | Photo</p>
        <li><p><span class="FontNormal2" style="line-height: 18px!important;">横長の画像（3:2の比率）をアップロードしていただくときれいに表示されます。（画像はサイズ：1200×400ピクセル以上／拡張子：PNGまたはJPG／容量2MB以下）<br>เพื่อความสวยงามบนเว็บไซต์ เราแนะนำให้ท่านอัพโหลดรูปภาพแนวนอน (สัดส่วนภาพ3:2) ที่มีขนาดใหญ่กว่า 1200x400 pixels / ต่ำกว่า 2MB / นามสกุล PNG หรือ JPG</span></p></span></li>-->
        <li>
            {!! __('service.secsion_title_2_7_label') !!}
            {!! __('service.secsion_title_2_7_sub') !!}
            <div class="MaterialURL_">
                <ul>
                    @if(!empty($services->photo_gallery_url))
                        <li style="width: 100%; clear: both; padding-bottom: 10px; border-bottom: 1px solid #eee;">
                            <label>
                                <span>URL : </span>
                                <a target="_blank" href="{{ $services->photo_gallery_url }}"
                                   class="pl-4 text-blue-700 underline ">
                                    {{ $services->photo_gallery_url ?? '-' }}
                                </a>
                            </label>
                            <br>
                            <span>説明文 | About : {{ $services->about_url ?? '-' }}</span>
                        </li>
                        @if(!empty($services->photo_gallery_url2))
                            <li style="width: 100%; clear: both; padding-bottom: 10px; border-bottom: 1px solid #eee;">
                                <label>
                                    <span>URL : </span>
                                    <a target="_blank" href="{{ $services->photo_gallery_url2 }}"
                                       class="pl-4 text-blue-700 underline ">
                                        {{ $services->photo_gallery_url2 ?? '-' }}
                                    </a>
                                </label>
                                <br>
                                <span>説明文 | About : {{ $services->about_url2 ?? '-' }}</span>
                            </li>
                        @endif
                        @if(!empty($services->photo_gallery_url3))
                            <li style="width: 100%; clear: both; padding-bottom: 10px; border-bottom: 1px solid #eee;">
                                <label>
                                    <span>URL : </span>
                                    <a target="_blank" href="{{ $services->photo_gallery_url3 }}"
                                       class="pl-4 text-blue-700 underline ">
                                        {{ $services->photo_gallery_url3 ?? '-' }}
                                    </a>
                                </label>
                                <br>
                                <span>説明文 | About : {{ $services->about_url3 ?? '-' }}</span>
                            </li>
                        @endif
                    @else
                        <p class="pl-4 text-gray-700">-</p>
                    @endif
                </ul>
            </div>
        </li>
        <li><span class="FontNormal3"
                  style="line-height: 21px!important; display: block; margin-bottom: 10px;">{!! __('service.secsion_title_2_8_title_label') !!}</span><label>
                <input
                        type="checkbox"
                        name="is_accept"
                        value="1"
                        disabled
                        {{ $services->is_accept  == 1 ? 'checked' : '' }}
                        id="is_accept">
                {!! __('service.secsion_title_2_8_label') !!}
                {!! __('service.secsion_title_2_8_sub') !!}
            </label></li>
    </ul>
</fieldset>