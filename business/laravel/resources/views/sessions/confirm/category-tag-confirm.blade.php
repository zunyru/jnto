<fieldset style="background-color: #F5F5FF;">
    <legend style="color: #8967C8;">4 Category tag</legend>
    <ul>
        <li class="LabelProvince"><p>4-1 都道府県名・地域 | <span
                        class="FontNormal">Province/Region</span> <span class="NeedRed">*</span>
            </p>
            <label>
                @if(!empty($company->province_all))
                    <span class="pl-4 text-gray-700">{{  "• All over Japan " }}</span>
                @else
                    @foreach($provinces as $key => $value)
                        @foreach($value as $key => $item)
                            @if(in_array($item['province']['id'],$company_region_pivot))
                                <span class="pl-4 text-gray-700">{{ "• {$item['province']['name_en']}" }}</span><br>
                            @endif
                        @endforeach
                    @endforeach
                @endif
            </label>
        </li>
        <li class="LabelProvince"><p>4-2 季節 | <span class="FontNormal">Season</span></p>
            <label>
                @foreach($seasons as $key =>  $season)
                    @if(in_array($season->id,$company_season_pivot))
                        <span class="pl-4 text-gray-700">{{ "• {$season->name_en}" }}</span><br>
                    @endif
                @endforeach
                @if(empty($company_season_pivot))
                    <span class="pl-4 text-gray-700">-</span>
                @endif
            </label>
        </li>
        <li class="LabelProvince"><p>4-3 付加価値ポイント | <span
                        class="FontNormal">Special features</span></p>
            <label>
                @foreach($special_features as $key =>  $special_feature)
                    @if(in_array($special_feature->id,$company_special_feature_pivot))
                        <span class="pl-4 text-gray-700">{{ "• {$special_feature->name_en}" }}</span><br>
                    @endif
                @endforeach
                @if(empty($company_special_feature_pivot))
                    <span class="pl-4 text-gray-700">-</span>
                @endif
            </label>
        </li>
        <li class="LabelProvince"><p>4-4 旅行タイプ | <span class="FontNormal">Type of travel</span>
            </p>
            <label>
                @foreach($type_of_traves as $key =>  $type_of_trave)

                    @if(in_array($type_of_trave->id,$company_type_of_travel_pivot))
                        <span class="pl-4 text-gray-700"> {{ "• {$type_of_trave->name_en}" }}</span><br>
                    @endif
                @endforeach
                @if(empty($company_type_of_travel_pivot))
                    <span class="pl-4 text-gray-700">-</span>
                @endif
            </label>
        </li>
    </ul>
</fieldset>