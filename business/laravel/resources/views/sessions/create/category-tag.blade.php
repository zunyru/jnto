<fieldset style="background-color: #F5F5FF;">
    <legend style="color: #8967C8;">4 Category tag</legend>
    <ul>
        <li class="LabelProvince"><p>4-1 都道府県名・地域 | <span
                        class="FontNormal">Province/Region</span> <span class="NeedRed">*</span>
            </p>
            <div class="ProvinceRegionList">
                <ul>
                    <li>
                        <div class="LeftRegion">All</div>
                        <div class="RightRegion">
                            <label>
                                <input
                                        name="province_all"
                                        id="province_all"
                                        {{ old('province_all') == 1 ? 'checked' : '' }}
                                        value="1"
                                        type="checkbox"> All over Japan
                            </label>
                        </div>
                    </li>
                    @foreach($provinces as $key => $value)
                        <li>
                            <div class="LeftRegion">{{ $key }}</div>
                            <div class="RightRegion">
                                @foreach($value as $key => $item)
                                    <label>
                                        <input
                                                name="region[]"
                                                class="province"
                                                {{ in_array($item['province']['id'],old('region') ?? []) ? 'checked' : ''}}
                                                value="{{ $item['province']['id'] }}"
                                                type="checkbox"> {{ $item['province']['name_en'] }}
                                    </label>
                                @endforeach
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </li>
        <label class="error top--20" for="region[]"></label>
        {{--<li class="LabelProvince"><p>4-2 季節 | <span class="FontNormal">Season</span></p>
            @foreach($seasons as $key =>  $season)
                <label>
                    <input
                            name="season[]"
                            id="{{ "season{$key}" }}"
                            {{ in_array($season->id,old('season') ?? []) ? 'checked' : ''}}
                            value="{{ $season->id }}"
                            type="checkbox"> {{ $season->name_en }}</label>
            @endforeach
        </li>--}}
        <li class="LabelProvince"><p>4-2 付加価値ポイント | <span
                        class="FontNormal">Special features</span></p>
            @foreach($special_features as $key =>  $special_feature)
                <label>
                    <input
                            name="special_feature[]"
                            id="{{ "special_feature{$key}" }}"
                            {{ in_array($special_feature->id,old('special_feature') ?? []) ? 'checked' : ''}}
                            value="{{ $special_feature->id }}"
                            type="checkbox"> {{ $special_feature->name_en }}</label>
            @endforeach
        </li>
        <li class="LabelProvince"><p>4-3 旅行タイプ | <span class="FontNormal">Type of travel</span>
            </p>
            @foreach($type_of_traves as $key =>  $type_of_trave)
                <label>
                    <input
                            name="type_of_trave[]"
                            id="{{ "type_of_trave{$key}" }}"
                            {{ in_array($type_of_trave->id,old('type_of_trave') ?? []) ? 'checked' : ''}}
                            value="{{ $type_of_trave->id }}"
                            type="checkbox"> {{ $type_of_trave->name_en }}</label>
            @endforeach
        </li>
        {{--<li class="LabelProvince"><p>4-5 特別な体験の提供 | <span
                        class="FontNormal">Special experience</span></p>
            @foreach($special_experiences as $key =>  $special_experience)
                <label>
                    <input
                            name="special_experience[]"
                            id="{{ "special_experience{$key}" }}"
                            {{ in_array($special_experience->id,old('special_experience') ?? []) ? 'checked' : ''}}
                            value="{{ $special_experience->id }}"
                            type="checkbox"> {{ $special_experience->name_en }}</label>
            @endforeach
        </li>--}}
    </ul>
</fieldset>