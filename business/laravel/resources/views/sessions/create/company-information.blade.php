<fieldset style="background-color: #FFFFE6;">
    <legend>{!! __('form.secsion_title_1') !!}</legend>
    <ul>
        <li>
            <label>
                <p class="FloatLeft">
                    Email Login
                </p>
                <input type="text"
                       name="user_login"
                       value="{{ old('user_login') }}"
                       placeholder="Email Login">
            </label>
        </li>
        <li class="mb-2">
            {!! __('form.secsion_title_1_1_label') !!}
            {!! __('form.secsion_title_1_1_sub') !!}
            <label>
                <img src=""
                     id="logo_preview"
                     width="150">
                <label for="logo"
                       class="FileUploadCSS">
                    Choose a file
                    <input type="file"
                           name="logo"
                           accept="image/*"
                           id="logo"/>
                </label>
                <br>
                <label id="logo-error" class="error" for="logo" style="display: none;">
                </label>
            </label>
        </li>
        <li class="LabelMarTop">
            {!! __('form.secsion_title_1_2_label')   !!}
            {!! __('form.secsion_title_1_2_sub') !!}
            <label class="FontNormalStyle">
                <input type="radio" name="is_partner" value="1"
                       {{ old('is_partner') == 1 ? 'checked': '' }} id="1"> Yes
            </label>
            <label
                    class="FontNormalStyle">
                <input type="radio" name="is_partner" value="0"
                       {{ old('is_partner') === "0" ? 'checked': '' }} id="2"> No
            </label>
        </li>
        <label class="error top--20" for="is_partner">This field is required.</label>

        <li><label>
                <p class="FloatLeft">
                    {!! __('form.secsion_title_1_3_label')   !!}
                    {!! __('form.secsion_title_1_3_sub') !!}
                </p>
                <input type="text"
                       name="name_en"
                       value="{{ old('name_en') }}"
                       placeholder="Company Name Co., Ltd."></label>
        </li>
        <li><label><p>
                    {!! __('form.secsion_title_1_4_label')   !!}
                    {!! __('form.secsion_title_1_4_sub') !!}
                </p>
                <input type="text"
                       name="name_ja"
                       value="{{ old('name_ja') }}"
                       placeholder="株式会社○○○"></label>
        </li>

        <li class="LabelProvince"><p>
                {!! __('form.secsion_title_1_5_label')   !!}
                {!! __('form.secsion_title_1_5_sub') !!}
            </p>
            @foreach($business_types as $key => $business_type)
                <label>
                    <input type="checkbox" name="business_type_id[]"
                           {{ in_array($business_type->id,old('business_type_id') ?? []) ? 'checked' : ''}}
                           value="{{$business_type->id}}"
                           id="{{ "business_type_id{$key}" }}"> {{$business_type->name_en}}
                </label>
            @endforeach
        </li>
        <label class="error top--20" for="business_type_id[]">This field is required.</label>

        <li><label>
                {!! __('form.secsion_title_1_6_label')   !!}
                {!! __('form.secsion_title_1_6_sub') !!}
                <textarea
                        name="address">{!! old('address') !!}</textarea></label></li>
        <li><label>
                {!! __('form.secsion_title_1_7_label')   !!}
                {!! __('form.secsion_title_1_7_sub') !!}
                <input
                        name="google_map_url"
                        value="{{ old('google_map_url') }}"
                        type="text"></label></li>
        <li><label>
                {!! __('form.secsion_title_1_8_label')   !!}
                {!! __('form.secsion_title_1_8_sub') !!}
                <input
                        name="official_website_url"
                        value="{{ old('official_website_url') }}"
                        type="text"></label></li>
        {{--<li>
            <label><p>1-9 その他のウェブサイト | <span class="FontNormal">Other website URL </span>
                </p> <input
                        name="website_url"
                        value="{{ old('website_url') }}"
                        type="text">
            </label>--}}
        </li>
        <li>
            <label>
                {!! __('form.secsion_title_1_9_label')   !!}
                {!! __('form.secsion_title_1_9_sub') !!}
                <input
                        name="facebook_url"
                        value="{{ old('facebook_url') }}"
                        type="text"></label>
        </li>
        <li>
            <label>
                {!! __('form.secsion_title_1_10_label')   !!}
                {!! __('form.secsion_title_1_10_sub') !!}
                <input
                        name="instagram_url"
                        value="{{ old('instagram_url') }}"
                        type="text"></label>
        </li>
        <li>
            <label>
                {!! __('form.secsion_title_1_11_label')   !!}
                {!! __('form.secsion_title_1_11_sub') !!}
                <input
                        name="youtube_channel_url"
                        value="{{ old('youtube_channel_url') }}"
                        type="text"></label>
        </li>
        <li>
            <label>
                {!! __('form.secsion_title_1_12_label')   !!}
                {!! __('form.secsion_title_1_12_sub') !!}
                <input
                        name="twitter"
                        value="{{ old('twitter') }}"
                        type="text"></label>
        </li>
        <li>
            <label>
                {!! __('form.secsion_title_1_13_label')   !!}
                {!! __('form.secsion_title_1_13_sub') !!}
                <input
                        name="line"
                        value="{{ old('line') }}"
                        type="text"></label>
        </li>

        <li>
            <label>
                {!! __('form.secsion_title_1_14_label') !!}
                {!! __('form.secsion_title_1_14_sub') !!}
                <input
                        name="contact_person_name"
                        value="{{ old('contact_person_name') }}"
                        type="text"></label>
        </li>
        <li>
            <label>
                {!! __('form.secsion_title_1_15_label') !!}
                {!! __('form.secsion_title_1_15_sub') !!}
                <input
                        name="department"
                        required
                        value="{{ old('department') }}"
                        type="text"></label>
        </li>

        <li>
            <label>
                {!! __('form.secsion_title_1_16_label') !!}
                {!! __('form.secsion_title_1_16_sub') !!}

                <input
                        name="email"
                        value="{{ old('email') }}"
                        type="email"></label>
        </li>
        {{--
        <li>
            <label>
                {!! __('form.secsion_title_1_16_1_label') !!}
                {!! __('form.secsion_title_1_16_1_sub') !!}
                <input
                        name="email_contact"
                        required
                        value="{{ old('email_contact') }}"
                        type="email"></label>
        </li>
        --}}
        <li>
            <label>
                {!! __('form.secsion_title_1_17_label') !!}
                {!! __('form.secsion_title_1_17_sub') !!}
                <input
                        name="tel"
                        value="{{ old('tel') }}"
                        type="text" placeholder="81-xx-xxx-xxxx"></label>
        </li>
        <li class="LabelMarTop">
            {!! __('form.secsion_title_1_18_label') !!}
            {!! __('form.secsion_title_1_18_sub') !!}

            @foreach($languages as $key => $language)
                <label>
                    <input type="checkbox"
                           name="language_id[]"
                           {{ in_array($language->id,old('language_id') ?? []) ? 'checked' : ''}}
                           value="{{$language->id}}"
                           id="{{ "language_id{$key}" }}"> {{ $language->name_en }}
                </label>
            @endforeach
        </li>
        <label class="error top--20" for="language_id[]">This field is required.</label>


        <li>
            {!! __('form.secsion_title_1_19_1_label')   !!}
            {!! __('form.secsion_title_1_19_1_sub') !!}
            <p>
                <label><input type="radio"
                              name="registered"
                              required
                              value="1"> 旅行業登録業者です│Registered travel agency</label><br>
                <label><input type="radio"
                              name="registered"
                              required
                              value="2"> 旅行サービス手配業です│Registered land operator</label><br>
                <label><input type="radio"
                              name="registered"
                              required
                              value="3"> 旅行業者ではありません│Not Travel Agency</label>
            </p>
            <label id="registered-error" class="error" for="registered" style="display: inline;"></label>
            <br>
            <div class="Registration3"
                 style="display: none;">
                <p>
                    <label>登録番号│Japan travel agency registration number<br>
                        <span class="FontNormal2">（ Japan Tourism Agency Commissioner of Japan Tourism Agency Registered Travel Agency No. 123 ）</span><br>
                        <textarea
                                name="licens_no"></textarea></label>
                </p>
            </div>
        </li>


        <li class="LabelMarTop">
            {!! __('form.secsion_title_1_19_label') !!}
            {!! __('form.secsion_title_1_19_sub') !!}
            <label>
                <input type="radio"
                       name="is_branch_thai"
                       id="bit1"
                       value="1"
                       {{ old('is_branch_thai') == 1 ? 'checked': '' }}
                       onclick="CheckType(1);"> Yes</label>
            <label>
                <input type="radio"
                       name="is_branch_thai"
                       id="bit2"
                       value="2"
                       {{ old('is_branch_thai') == 2 ? 'checked': '' }}
                       onclick="CheckType(2);"> No </label>
        </li>
        <label class="error top--20" for="is_branch_thai">This field is required.</label>

        <div class="BIT" id="BIT">
            <p>＜タイ支店・レップ情報│Information of Branch／representative office in Thailand＞</p>
            <li>
                <label>
                    {!! __('form.secsion_title_1_20_label') !!}
                    {!! __('form.secsion_title_1_20_sub') !!}
                    <input
                            name="branch_company_name_en"
                            value="{{ old('branch_company_name_en') }}"
                            type="text">
                </label>
            </li>
            <li>
                <label>
                    {!! __('form.secsion_title_1_21_label') !!}
                    {!! __('form.secsion_title_1_21_sub') !!}
                    <input
                            name="branch_company_name_th"
                            value="{{ old('branch_company_name_th') }}"
                            type="text"></label></li>
            <li><label>
                    {!! __('form.secsion_title_1_22_label') !!}
                    {!! __('form.secsion_title_1_22_sub') !!}
                    <textarea
                            name="branch_address">{!! old('branch_address') !!}</textarea></label>
            </li>
            <li><label>
                    {!! __('form.secsion_title_1_23_label') !!}
                    {!! __('form.secsion_title_1_23_sub') !!}
                    <input name="branch_google_map_url"
                           value="{{ old('branch_google_map_url') }}"
                           type="text"></label></li>
            <li><label>
                    {!! __('form.secsion_title_1_24_label') !!}
                    {!! __('form.secsion_title_1_24_sub') !!}
                    <input
                            name="branch_thai_website_url"
                            value="{{ old('branch_thai_website_url') }}"
                            type="text"></label>
            </li>

            <li>
                <label>
                    {!! __('form.secsion_title_1_25_label') !!}
                    {!! __('form.secsion_title_1_25_sub') !!}
                    <input
                            name="branch_contact_person_name"
                            value="{{ old('branch_contact_person_name') }}"
                            type="text"></label>
            </li>

            <li>
                <label>
                    {!! __('form.secsion_title_1_26_label') !!}
                    {!! __('form.secsion_title_1_26_sub') !!}
                    <input
                            name="branch_department"
                            value="{{ old('branch_department') }}"
                            type="text">
                </label>
            </li>

            <li><label>
                    {!! __('form.secsion_title_1_27_label') !!}
                    {!! __('form.secsion_title_1_27_sub') !!}
                    <input
                            name="branch_email"
                            value="{{ old('branch_email') }}"
                            type="email"></label></li>
            <li><label>
                    {!! __('form.secsion_title_1_28_label') !!}
                    {!! __('form.secsion_title_1_28_sub') !!}
                    <input type="text"
                           name="branch_tel"
                           value="{{ old('branch_tel') }}"
                           placeholder="66-xx-xxx-xxxx">
                </label>
            </li>
            <li class="LabelMarTop">
                {!! __('form.secsion_title_1_29_label') !!}
                {!! __('form.secsion_title_1_29_sub') !!}

                @foreach($languages_for_branches as $key => $language_branches)
                    <label>
                        <input type="checkbox"
                               name="language_for_branch_id[]"
                               {{ in_array($language_branches->id,old('language_for_branch_id') ?? []) ? 'checked' : ''}}
                               value="{{$language_branches->id}}"
                               id="{{ "language_branches_id{$key}" }}"> {{ $language_branches->name_en }}
                    </label>
                @endforeach

            </li>
            <label class="error top--20" for="language_for_branch_id[]">This field is required.</label>
        </div>
    </ul>
</fieldset>
@push('scripts')
    <script>
        $("input[name='registered']").change(function () {
            if ($(this).val() == '3') {
                $('.Registration3').hide();
            } else {
                $('.Registration3').show();
            }
        });
    </script>
@endpush