<fieldset style="background-color: #F5FFF0;">
	<legend style="color: #52AF00;">{!! __('service.secsion_title_2') !!}</legend>
	<ul>
		<li><label>
				{!! __('service.secsion_title_2_1_label') !!}
				{!! __('service.secsion_title_2_1_sub') !!}
				<input type="text"
				       name="title"
				       maxlength="100"
				       value="{{ old('title') }}"
				       placeholder="英語またはタイ語100字以内　ภาษาอังกฤษหรือภาษาไทย สูงสุด 100 ตัวอักษร"></label>
			<p><span class="title">0</span>/100</p>
		</li>
		<li><label>
				{!! __('service.secsion_title_2_2_label') !!}
				{!! __('service.secsion_title_2_2_sub') !!}
				<textarea
						name="detail"
						id="detail"
						maxlength="4000"
						placeholder="英語またはタイ語4,000字以内、URLも掲載可能 | ภาษาอังกฤษหรือภาษาไทย สูงสุด 4,000 ตัวอักษร และสามารถใส่ URL Website ได้"></textarea></label>
			<p id="character_count"></p>
			<p id="character_count_min" style="color: red;display: none;">
				500文字以下では入力できないようにしたい รายละเอียดของ Seller ใส่อย่างน้อย 500 ตัวอักษร
			</p>
		</li>
		<li>
			{!! __('service.secsion_title_2_3_label') !!}
			{!! __('service.secsion_title_2_3_sub') !!}
			<div class="my-3">
				<label for="main_photo"
					   class="FileUploadCSS">
					Choose a file
					<input type="file"
						   name="main_photo"

						   @if(empty($services->main_photo))
							   required
						   @endif

						   accept="image/*"
						   id="main_photo"/>
				</label>
				<div class="flex mt-4" style="margin-top: 10px;color:#1736f5; font-weight: bold;">画像 1</div>
				<div class="flex mt-4" style="margin-top: 10px">
					<input type="checkbox"
						   {{ !empty($services->download_main_photo) ? 'checked' : '' }}
						   id="download_main_photo"
						   value="1"
						   name="download_main_photo">
					<label for="download_main_photo">
						タイ旅行会社の画像の利用を許可する｜Allow Thai Travel Company to use this photo
					</label>
				</div>
				<br>
				<label id="main_photo-error" class="error" for="main_photo" style="display: none;">
				</label>
			</div>
			<span>
                @if(!empty($services))
					<img src="{{ asset($services->main_photo) }}" id="main_photo_preview"
						 style="width: 100%; height: auto; margin-top: 10px;">
				@else
					<img src="" id="main_photo_preview" style="width: 100%; height: auto; margin-top: 10px;">
				@endif

            </span>
		</li>
		<li>
			{!! __('service.secsion_title_2_4_label') !!}
			{!! __('service.secsion_title_2_4_sub') !!}
			<div class="flex flex-col">
				<div>
					<p class="my-3">
						<label for="other_photo"
							   class="FileUploadCSS">
							Choose a file
							<input type="file"
								   name="other_photo"
								   accept="image/*"
								   id="other_photo"/>
						</label>
					<div class="flex mt-4" style="margin-top: 10px;color:#1736f5; font-weight: bold;">画像 2</div>
					<div class="flex mt-4" style="margin-top: 10px">
						<input type="checkbox"
							   {{ !empty($services->download_other_photo) ? 'checked' : '' }}
							   value="1"
							   id="download_other_photo"
							   name="download_other_photo">
						<label for="download_other_photo">
							タイ旅行会社の画像の利用を許可する｜Allow Thai Travel Company to use this photo
						</label>
					</div>
					<br>
					<label id="other_photo-error" class="error" for="other_photo" style="display: none;">
					</label>
					</p>
					@if(!empty($services->other_photo))
						<img src="{{ asset($services->other_photo) }}"
							 id="other_photo_preview"
							 style="width: 100%; height: auto; margin-top: 10px;">
						<br>
						<p class="DelPicture">
							<a href="javascript:void(0);"
							   data-filters="other_photo"
							   data-db="services"
							   data-where="company_id"
							   data-id="{{ $company->ref_id }}"
							   class="delete-img bg-red-600 text-white text-center p-2 rounded-md">
								x Delete
							</a>
						</p>
					@else
						<img src=""
							 id="other_photo_preview"
							 style="width: 100%; height: auto; margin-top: 10px;">
					@endif
				</div>
				<br>
				<hr>
				<br>
				<div>
					<p class="my-3">
						<label for="other_photo2"
							   class="FileUploadCSS">
							Choose a file
							<input type="file"
								   name="other_photo2"
								   accept="image/*"
								   id="other_photo2"/>
						</label>
					<div class="flex mt-4" style="margin-top: 10px;color:#1736f5; font-weight: bold;">画像 3</div>
					<div class="flex mt-4" style="margin-top: 10px">
						<input type="checkbox"
							   {{ !empty($services->download_other_photo2) ? 'checked' : '' }}
							   value="1"
							   id="download_other_photo2"
							   name="download_other_photo2">
						<label for="download_other_photo2">
							タイ旅行会社の画像の利用を許可する｜Allow Thai Travel Company to use this photo
						</label>
					</div>
					<br>
					<label id="other_photo2-error" class="error" for="other_photo2" style="display: none;">
					</label>
					</p>
					@if(!empty($services->other_photo2))
						<img src="{{ asset($services->other_photo2) }}"
							 id="other_photo2_preview"
							 style="width: 100%; height: auto; margin-top: 10px;">
						<br>
						<p class="DelPicture">
							<a href="javascript:void(0);"
							   data-filters="other_photo2"
							   data-db="services"
							   data-where="company_id"
							   data-id="{{ $company->ref_id }}"
							   class="delete-img bg-red-600 text-white text-center p-2 rounded-md">
								x Delete
							</a>
						</p>
					@else
						<img src=""
							 id="other_photo2_preview"
							 style="width: 100%; height: auto; margin-top: 10px;">
					@endif
				</div>
				<br>
				<hr>
				<br>
				<div>
					<p class="my-3">
						<label for="other_photo3"
							   class="FileUploadCSS">
							Choose a file
							<input type="file"
								   name="other_photo3"
								   accept="image/*"
								   id="other_photo3"/>
						</label>
					<div class="flex mt-4" style="margin-top: 10px;color:#1736f5; font-weight: bold;">画像 4</div>
					<div class="flex mt-4" style="margin-top: 10px">
						<input type="checkbox"
							   {{ !empty($services->download_other_photo3) ? 'checked' : '' }}
							   value="1"
							   id="download_other_photo3"
							   name="download_other_photo3">
						<label for="download_other_photo3">
							タイ旅行会社の画像の利用を許可する｜Allow Thai Travel Company to use this photo
						</label>
					</div>
					<br>
					<label id="other_photo3-error" class="error" for="other_photo3" style="display: none;">
					</label>
					</p>
					@if(!empty($services->other_photo3))
						<img src="{{ asset($services->other_photo3) }}"
							 id="other_photo3_preview"
							 style="width: 100%; height: auto; margin-top: 10px;">
						<br>
						<p class="DelPicture">
							<a href="javascript:void(0);"
							   data-filters="other_photo3"
							   data-db="services"
							   data-where="company_id"
							   data-id="{{ $company->ref_id }}"
							   class="delete-img bg-red-600 text-white text-center p-2 rounded-md">
								x Delete
							</a>
						</p>
					@else
						<img src=""
							 id="other_photo3_preview"
							 style="width: 100%; height: auto; margin-top: 10px;">
					@endif
				</div>
				<div class="mt-4" style="margin-top: 20px">
					<label for="download_condition">
						画像の利用の条件 |  Remark about photo usage
					</label>
					<textarea
							name="download_condition"
							class="">{!! $services->download_condition ?? '' !!}</textarea>
				</div>
			</div>
		</li>
		<li>
			<label>
				{!! __('service.secsion_title_2_5_label') !!}
				{!! __('service.secsion_title_2_5_sub') !!}
				<textarea
						name="pr_youtube"
						type="text" placeholder="">{{ old('pr_youtube') }}</textarea>
			</label>
		</li>
		<li>
			{!! __('service.secsion_title_2_6_label') !!}
			{!! __('service.secsion_title_2_6_sub') !!}
			
			<label style="display: block; margin-bottom: 10px;">
				<label for="company_profile_pdf"
				       class="FileUploadCSS">
					Choose a file
					<input type="file"
					       name="company_profile_pdf"
					       id="company_profile_pdf"/>
				</label>
				<br>
				<label id="company_profile_pdf-error" class="error" for="company_profile_pdf" style="display: none;">
				</label>
			</label><label>説明文 | About
				<input
						name="about"
						value="{{ old('about') }}"
						type="text"
						class="TextPDFName"
						placeholder="英語またはタイ語 100 文字以内　ภาษาอังกฤษหรือภาษาไทย สูงสุด 100 ตัวอักษร"></label>
			<br>
			<label style="display: block; margin-bottom: 10px;">
				<label for="company_profile_pdf2"
				       class="FileUploadCSS">
					Choose a file
					<input type="file"
					       name="company_profile_pdf2"
					       id="company_profile_pdf2"/>
				</label>
				<br>
				<label id="company_profile_pdf2-error" class="error" for="company_profile_pdf2" style="display: none;">
				</label>
			</label><label>説明文 | About
				<input
						name="about2"
						value="{{ old('about2') }}"
						type="text"
						class="TextPDFName"
						placeholder="英語またはタイ語 100 文字以内　ภาษาอังกฤษหรือภาษาไทย สูงสุด 100 ตัวอักษร">
			</label>
			<br>
			<label style="display: block; margin-bottom: 10px;">
				<label for="company_profile_pdf3"
				       class="FileUploadCSS">
					Choose a file
					<input type="file"
					       name="company_profile_pdf3"
					       id="company_profile_pdf3"/>
				</label>
				<br>
				<label id="company_profile_pdf3-error" class="error" for="company_profile_pdf3" style="display: none;">
				</label>
			</label><label>説明文 | About
				<input
						name="about3"
						value="{{ old('about3') }}"
						type="text"
						class="TextPDFName"
						placeholder="英語またはタイ語 100 文字以内　ภาษาอังกฤษหรือภาษาไทย สูงสุด 100 ตัวอักษร">
			</label>
		</li>
		<!--<p class="FontHead">画像 | Photo</p>
		<li><p><span class="FontNormal2" style="line-height: 18px!important;">横長の画像（3:2の比率）をアップロードしていただくときれいに表示されます。（画像はサイズ：1200×400ピクセル以上／拡張子：PNGまたはJPG／容量2MB以下）<br>เพื่อความสวยงามบนเว็บไซต์ เราแนะนำให้ท่านอัพโหลดรูปภาพแนวนอน (สัดส่วนภาพ3:2) ที่มีขนาดใหญ่กว่า 1200x400 pixels / ต่ำกว่า 2MB / นามสกุล PNG หรือ JPG</span></p></span></li>-->
		<li>
			{!! __('service.secsion_title_2_7_label') !!}
			{!! __('service.secsion_title_2_7_sub') !!}
			<div class="MaterialURL">
				<ul>
					<li><label><span>URL</span>
							<input
									name="photo_gallery_url"
									value="{{ old('photo_gallery_url') }}"
									type="text"></label></li>
					<li>
						<label><span>説明文 | About </span>
							<input
									name="about_url"
									value="{{ old('about_url') }}"
									type="text"
									placeholder="100 文字以内　สูงสุด 100 ตัวอักษร"></label>
					</li>
					<li><label><span>URL</span>
							<input name="photo_gallery_url2"
							       value="{{ old('photo_gallery_url2') }}"
							       type="text"></label></li>
					<li><label><span>説明文 | About </span>
							<input
									name="about_url2"
									value="{{ old('about_url2') }}"
									type="text"
									placeholder="100 文字以内　สูงสุด 100 ตัวอักษร"></label>
					</li>
					<li><label><span>URL</span>
							<input
									name="photo_gallery_url3"
									value="{{ old('photo_gallery_url3') }}"
									type="text"></label></li>
					<li><label><span>説明文 | About </span>
							<input
									name="about_url3"
									value="{{ old('about_url3') }}"
									type="text"
									placeholder="100 文字以内　สูงสุด 100 ตัวอักษร"></label>
					</li>
				</ul>
			</div>
		</li>
		<label id="photo_gallery_url-error" class="error" for="photo_gallery_url">Please enter a valid URL.</label>
		<br>
		<label id="photo_gallery_url2-error" class="error" for="photo_gallery_url2">Please enter a valid URL.</label>
		<br>
		<label id="photo_gallery_url3-error" class="error" for="photo_gallery_url3">Please enter a valid URL.</label>
		{{--<li><span class="FontNormal3"
		          style="line-height: 21px!important; display: block; margin-bottom: 10px;">{!! __('service.secsion_title_2_8_title_label') !!}</span><label>
				<input
						type="checkbox"
						name="is_accept"
						value="1"
						{{ old('is_accept')  == 1? 'checked' : '' }}
						id="is_accept">
				{!! __('service.secsion_title_2_8_label') !!}
				{!! __('service.secsion_title_2_8_sub') !!}
			</label>
		</li>
		<label class="error top--20" for="is_accept">This field is required.</label>
		--}}
	</ul>
</fieldset>
@push('scripts')
	@once
		<script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
	@endonce
	<script>
		@error('detail')
        toastr.error(`{{ $errors->first() }}`);
		@enderror
        let title = $('input[name="title"]');
        $('span.title').html(title.val().length);
        title.on('keyup' , function () {
            $('span.title').html($(this).val().length);
        });

        tinymce.init({
            selector: 'textarea#detail' ,
            menubar: false ,
            width: '100%' ,
            height: 500 ,
            forced_root_block: false ,
            force_br_newlines: true ,
            force_p_newlines: false ,
            toolbar: 'bold customBr link underline' ,
            plugins: 'nonbreaking link' ,
			convert_urls: false,
            setup: function (editor) {
                editor.ui.registry.addButton('customBr' , {
                    text: 'Enter' ,
                    onAction: function () {
                        editor.insertContent('<br/> ');
                    } ,
                });
                editor.on('keyup' , function (e) {
                    var count = CountCharacters();
                    ValidateCharacterLength(count);
                    document.getElementById("character_count").innerHTML = count.toLocaleString() + "/4,000";
                });
                editor.on('init' , function (e) {
                    //var body = editor.getContent();
                    var body = tinymce.get("detail").getContent({format: "text"});
                    $('#character_count').html(body.length.toLocaleString() + "/4,000");
                });
            }
        });


        function CountCharacters() {
            var body = tinymce.get("detail").getBody();
            var content = tinymce.trim(body.innerText || body.textContent);
            return content.length;
        };

        function ValidateCharacterLength() {
            var max = 4000;
			var min = 500;
            var count = CountCharacters();
            if (count > max) {
                $('button[type="submit"]').prop('disabled' , true);
                toastr.error("Maximum " + max + " characters allowed.")
                return false;
            }
			/*if (count < min) {
				$('button[type="submit"]').prop('disabled', true);
				//toastr.error("Minimum " + min + " characters allowed.")
				$("#character_count_min").show();
				return false;
			}*/
            $('button[type="submit"]').prop('disabled' , false);
			$("#character_count_min").hide();
            return;
        }

        $("#main_photo").change(function() {
            readURL(this);
        });
        $("#other_photo").change(function() {
            readURL(this);
        });
        $("#other_photo2").change(function() {
            readURL(this);
        });
        $("#other_photo3").change(function() {
            readURL(this);
        });

        $("#logo").change(function() {
            readURL(this);
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    var preview = $(input).attr('id')+'_preview';
                    $('#'+preview).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            } else {
                $('#previewHolder').attr('src', '');
            }
        }
	</script>
@endpush