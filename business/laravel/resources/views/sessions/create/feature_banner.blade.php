<fieldset style="border:none">
    <div class="flex FormList w-full items-center">
        <div class="pr-10 w-1/2">
            <label>
                <input
                        {{ old('is_show_banner') == 1 ? 'checked': '' }}
                        name="is_show_banner"
                        id="is_show_banner"
                        value="1"
                        type="checkbox">
                Show "JNTO Partners' News" banner
            </label>
        </div>
        <div class="flex w-1/2 items-center">
            <div class="w-1/6">
                <label class="">
                    | URL
                </label>
            </div>
            <div class="w-5/6">
                <input
                        type="text"
                        name="url_banner"
                        id="url_banner"
                        placeholder="https://www.xxxx.com"
                        value="{{ old('url_banner') }}">
            </div>

        </div>
    </div>
{{--    <div class="flex FormList w-full items-center">--}}
{{--        <div class="flex FormList w-full items-center">--}}
{{--            <div class="pr-10 w-1/2">--}}
{{--                <label>--}}
{{--                    <input--}}
{{--                            {{ old('is_feature_banner') == 1 ? 'checked': '' }}--}}
{{--                            name="is_feature_banner"--}}
{{--                            id="is_feature_banner"--}}
{{--                            value="1"--}}
{{--                            type="checkbox">--}}
{{--                    Feature banner (Top page)--}}
{{--                </label>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
</fieldset>