@extends('layouts.app_dashboard')
@section('content')
    <div class="container">
        <div class="justify-center">
            <div class="pt-4">
                <div class="w-full shadow bg-white rounded p-4 mb-10">
                    <h1>HOME</h1>
                    <div class="w-full">

                        <form action="{{ route('settings.update') }}" method="POST"
                              class=""
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            @if(auth()->user()->role->name != 'Viewer')
                                <div class="md:float-right mb-4 pr-4 absolute" style="right: 38px;top: 100px;">
                                    <div class="flex flex-wrap mt-10 md:mt-0 md:flex-row items-center">
                                        <div class="">
                                            <button class="bg-teal-500 p-3 rounded-lg hover:bg-indigo-600 text-white w-50"
                                                    type="submit">
                                                Save
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    Text editor
                                </label>

                                <textarea id="detail"
                                          name="value">{!! !empty($setting_value->value) ? nl2br($setting_value->value) : '' !!}</textarea>
                            </div>

                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    File upload 1
                                </label>
                                <label for="file1"></label>
                                <label for="file1"
                                       class="FileUploadCSS">
                                    Choose a file
                                    <input type="file"
                                           name="file1"
                                           id="file1"/>
                                </label>
                                <br>
                                <label id="file1-error" class="error" for="file1" style="display: none;">
                                </label>
                            </div>

                            @if(!empty($setting_file1->value))
                                <a href="{{ asset($setting_file1->value) }}"
                                   target="_blank">
                                    <img class="mt-3" src="{{ asset($icon1) }}"
                                         style="width: 120px; max-width: 10%;">
                                </a>
                                <p>{{ nameFile($setting_file1->value) }}</p>
                                <br>
                                <a href="javascript:void(0);"
                                   data-filters="value"
                                   data-db="settings"
                                   data-where="id"
                                   data-id="{{ $setting_file1->id }}"
                                   class="w-1/2 delete-img bg-red-600 text-white text-center p-2 rounded-md">
                                    <svg class="stroke-2 text-white inline-block h-4 w-4"
                                         xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                         stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2}
                                              d="M6 18L18 6M6 6l12 12"/>
                                    </svg>
                                    Delete
                                </a>
                            @endif
                            <hr>
                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    File upload 2
                                </label>
                                <label for="file2"></label>
                                <label for="file2"
                                       class="FileUploadCSS">
                                    Choose a file
                                    <input type="file"
                                           name="file2"
                                           id="file2"/>
                                </label>
                                <br>
                                <label id="file2-error" class="error" for="file2" style="display: none;">
                                </label>
                            </div>

                            @if(!empty($setting_file2->value))
                                <a href="{{ asset($setting_file2->value) }}"
                                   target="_blank">
                                    <img class="mt-3" src="{{ asset($icon2) }}"
                                         style="width: 120px; max-width: 10%;">
                                </a>
                                <p>{{ nameFile($setting_file2->value) }}</p>
                                <br>
                                <a href="javascript:void(0);"
                                   data-filters="value"
                                   data-db="settings"
                                   data-where="id"
                                   data-id="{{ $setting_file2->id }}"
                                   class="w-1/2 delete-img bg-red-600 text-white text-center p-2 rounded-md">
                                    <svg class="stroke-2 text-white inline-block h-4 w-4"
                                         xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                         stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2}
                                              d="M6 18L18 6M6 6l12 12"/>
                                    </svg>
                                    Delete
                                </a>
                            @endif
                            <hr>
                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    File upload 3
                                </label>
                                <label for="file3"></label>
                                <label for="file3"
                                       class="FileUploadCSS">
                                    Choose a file
                                    <input type="file"
                                           name="file3"
                                           id="file3"/>
                                </label>
                                <br>
                                <label id="file3-error" class="error" for="file3" style="display: none;">
                                </label>
                            </div>

                            @if(!empty($setting_file3->value))
                                <a href="{{ asset($setting_file3->value) }}"
                                   target="_blank">
                                    <img class="mt-3" src="{{ asset($icon3) }}"
                                         style="width: 120px; max-width: 10%;">
                                </a>
                                <p>{{ nameFile($setting_file3->value) }}</p>
                                <br>
                                <a href="javascript:void(0);"
                                   data-filters="value"
                                   data-db="settings"
                                   data-where="id"
                                   data-id="{{ $setting_file3->id }}"
                                   class="w-1/2 delete-img bg-red-600 text-white text-center p-2 rounded-md">
                                    <svg class="stroke-2 text-white inline-block h-4 w-4"
                                         xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                         stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2}
                                              d="M6 18L18 6M6 6l12 12"/>
                                    </svg>
                                    Delete
                                </a>
                            @endif
                            <hr>
                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    File upload 4
                                </label>
                                <label for="file4"></label>
                                <label for="file4"
                                       class="FileUploadCSS">
                                    Choose a file
                                    <input type="file"
                                           name="file4"
                                           id="file4"/>
                                </label>
                                <br>
                                <label id="file4-error" class="error" for="file4" style="display: none;">
                                </label>
                            </div>

                            @if(!empty($setting_file4->value))
                                <a href="{{ asset($setting_file4->value) }}"
                                   target="_blank">
                                    <img class="mt-3" src="{{ asset($icon4) }}"
                                         style="width: 120px; max-width: 10%;">
                                </a>
                                <p>{{ nameFile($setting_file4->value) }}</p>
                                <br>
                                <a href="javascript:void(0);"
                                   data-filters="value"
                                   data-db="settings"
                                   data-where="id"
                                   data-id="{{ $setting_file4->id }}"
                                   class="w-1/2 delete-img bg-red-600 text-white text-center p-2 rounded-md">
                                    <svg class="stroke-2 text-white inline-block h-4 w-4"
                                         xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                         stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2}
                                              d="M6 18L18 6M6 6l12 12"/>
                                    </svg>
                                    Delete
                                </a>
                            @endif
                            <hr>
                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    File upload 5
                                </label>
                                <label for="file5"></label>
                                <label for="file5"
                                       class="FileUploadCSS">
                                    Choose a file
                                    <input type="file"
                                           name="file5"
                                           id="file5"/>
                                </label>
                                <br>
                                <label id="file5-error" class="error" for="file5" style="display: none;">
                                </label>
                            </div>
                            @if(!empty($setting_file5->value))
                                <a href="{{ asset($setting_file5->value) }}"
                                   target="_blank">
                                    <img class="mt-3" src="{{ asset($icon5) }}"
                                         style="width: 120px; max-width: 10%;">
                                </a>
                                <p>{{ nameFile($setting_file5->value) }}</p>
                                <br>
                                <a href="javascript:void(0);"
                                   data-filters="value"
                                   data-db="settings"
                                   data-where="id"
                                   data-id="{{ $setting_file5->id }}"
                                   class="w-1/2 delete-img bg-red-600 text-white text-center p-2 rounded-md">
                                    <svg class="stroke-2 text-white inline-block h-4 w-4"
                                         xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                         stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2}
                                              d="M6 18L18 6M6 6l12 12"/>
                                    </svg>
                                    Delete
                                </a>
                            @endif
                            <hr>
                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    File upload 6
                                </label>
                                <label for="file6"></label>
                                <label for="file6"
                                       class="FileUploadCSS">
                                    Choose a file
                                    <input type="file"
                                           name="file6"
                                           id="file6"/>
                                </label>
                                <br>
                                <label id="file6-error" class="error" for="file6" style="display: none;">
                                </label>
                            </div>
                            @if(!empty($setting_file6->value))
                                <a href="{{ asset($setting_file6->value) }}"
                                   target="_blank">
                                    <img class="mt-3" src="{{ asset($icon6) }}"
                                         style="width: 120px; max-width: 10%;">
                                </a>
                                <p>{{ nameFile($setting_file6->value) }}</p>
                                <br>
                                <a href="javascript:void(0);"
                                   data-filters="value"
                                   data-db="settings"
                                   data-where="id"
                                   data-id="{{ $setting_file6->id }}"
                                   class="w-1/2 delete-img bg-red-600 text-white text-center p-2 rounded-md">
                                    <svg class="stroke-2 text-white inline-block h-4 w-4"
                                         xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                         stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2}
                                              d="M6 18L18 6M6 6l12 12"/>
                                    </svg>
                                    Delete
                                </a>
                            @endif
                            <hr>
                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    File upload 7
                                </label>
                                <label for="file7"></label>
                                <label for="file7"
                                       class="FileUploadCSS">
                                    Choose a file
                                    <input type="file"
                                           name="file7"
                                           id="file7"/>
                                </label>
                                <br>
                                <label id="file7-error" class="error" for="file7" style="display: none;">
                                </label>
                            </div>
                            @if(!empty($setting_file7->value))
                                <a href="{{ asset($setting_file7->value) }}"
                                   target="_blank">
                                    <img class="mt-3" src="{{ asset($icon7) }}"
                                         style="width: 120px; max-width: 10%;">
                                </a>
                                <p>{{ nameFile($setting_file7->value) }}</p>
                                <br>
                                <a href="javascript:void(0);"
                                   data-filters="value"
                                   data-db="settings"
                                   data-where="id"
                                   data-id="{{ $setting_file7->id }}"
                                   class="w-1/2 delete-img bg-red-600 text-white text-center p-2 rounded-md">
                                    <svg class="stroke-2 text-white inline-block h-4 w-4"
                                         xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                         stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2}
                                              d="M6 18L18 6M6 6l12 12"/>
                                    </svg>
                                    Delete
                                </a>
                            @endif
                            <hr>
                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    File upload 8
                                </label>
                                <label for="file8"></label>
                                <label for="file8"
                                       class="FileUploadCSS">
                                    Choose a file
                                    <input type="file"
                                           name="file8"
                                           id="file8"/>
                                </label>
                                <br>
                                <label id="file8-error" class="error" for="file8" style="display: none;">
                                </label>
                            </div>
                            @if(!empty($setting_file8->value))
                                <a href="{{ asset($setting_file8->value) }}"
                                   target="_blank">
                                    <img class="mt-3" src="{{ asset($icon8) }}"
                                         style="width: 120px; max-width: 10%;">
                                </a>
                                <p>{{ nameFile($setting_file8->value) }}</p>
                                <br>
                                <a href="javascript:void(0);"
                                   data-filters="value"
                                   data-db="settings"
                                   data-where="id"
                                   data-id="{{ $setting_file8->id }}"
                                   class="w-1/2 delete-img bg-red-600 text-white text-center p-2 rounded-md">
                                    <svg class="stroke-2 text-white inline-block h-4 w-4"
                                         xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                         stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2}
                                              d="M6 18L18 6M6 6l12 12"/>
                                    </svg>
                                    Delete
                                </a>
                            @endif
                            <hr>
                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    File upload 9
                                </label>
                                <label for="file9"></label>
                                <label for="file9"
                                       class="FileUploadCSS">
                                    Choose a file
                                    <input type="file"
                                           name="file9"
                                           id="file9"/>
                                </label>
                                <br>
                                <label id="file9-error" class="error" for="file9" style="display: none;">
                                </label>
                            </div>
                            @if(!empty($setting_file9->value))
                                <a href="{{ asset($setting_file9->value) }}"
                                   target="_blank">
                                    <img class="mt-3" src="{{ asset($icon9) }}"
                                         style="width: 120px; max-width: 10%;">
                                </a>
                                <p>{{ nameFile($setting_file9->value) }}</p>
                                <br>
                                <a href="javascript:void(0);"
                                   data-filters="value"
                                   data-db="settings"
                                   data-where="id"
                                   data-id="{{ $setting_file9->id }}"
                                   class="w-1/2 delete-img bg-red-600 text-white text-center p-2 rounded-md">
                                    <svg class="stroke-2 text-white inline-block h-4 w-4"
                                         xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                         stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2}
                                              d="M6 18L18 6M6 6l12 12"/>
                                    </svg>
                                    Delete
                                </a>
                            @endif
                            <hr>
                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    File upload 10
                                </label>
                                <label for="file10"></label>
                                <label for="file10"
                                       class="FileUploadCSS">
                                    Choose a file
                                    <input type="file"
                                           name="file10"
                                           id="file10"/>
                                </label>
                                <br>
                                <label id="file10-error" class="error" for="file10" style="display: none;">
                                </label>
                            </div>
                            @if(!empty($setting_file10->value))
                                <a href="{{ asset($setting_file10->value) }}"
                                   target="_blank">
                                    <img class="mt-3" src="{{ asset($icon10) }}"
                                         style="width: 120px; max-width: 10%;">
                                </a>
                                <p>{{ nameFile($setting_file10->value) }}</p>
                                <br>
                                <a href="javascript:void(0);"
                                   data-filters="value"
                                   data-db="settings"
                                   data-where="id"
                                   data-id="{{ $setting_file10->id }}"
                                   class="w-1/2 delete-img bg-red-600 text-white text-center p-2 rounded-md">
                                    <svg class="stroke-2 text-white inline-block h-4 w-4"
                                         xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                         stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2}
                                              d="M6 18L18 6M6 6l12 12"/>
                                    </svg>
                                    Delete
                                </a>
                            @endif
                            <hr>
                            <div class="mb-4 mt-10">
                                <label>
                                    <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                        Start Event
                                    </label>
                                    <input type="text"
                                           class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                           name="start_event"
                                           id="start_event"
                                           value="{{ $setting_start_event->value ?? "" }}"
                                           placeholder="2022-09-01">
                                </label>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <link rel="stylesheet" href="{{ asset('assets/jquery-ui/jquery-ui.min.css') }}">
    <script src="{{ asset('assets/jquery-ui/jquery-ui.min.js') }}"></script>

    @include('admin.include.block')

    <script>
        tinymce.init({
            readonly: readonly ?? false,
            selector: 'textarea#detail',
            menubar: false,
            width: '100%',
            height: 500,
            forced_root_block: false,
            force_br_newlines: true,
            force_p_newlines: false,
            //toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | showcomments addcomment',
            toolbar: 'bold italic  | link | insertfile |',
            //plugins: 'print preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists checklist wordcount tinymcespellchecker a11ychecker imagetools textpattern noneditable help formatpainter permanentpen pageembed charmap tinycomments mentions quickbars linkchecker emoticons advtable',
            plugins: 'nonbreaking link',
            convert_urls: false,
            file_picker_types: 'file',
            setup: function (editor) {
                editor.ui.registry.addButton('customBr', {
                    text: 'Enter',
                    onAction: function () {
                        editor.insertContent('<br/> ');
                    },
                });
            }
        });

        $("#start_event").datepicker({
            dateFormat: "yy-mm-dd"
        });

        $('.delete-img').click(function () {
            var data_value = $(this).attr('data-filters');
            var data_db = $(this).attr('data-db');
            var data_id = $(this).attr('data-id');
            var data_where = $(this).attr('data-where');
            Swal.fire({
                title: 'Are you sure to delete ?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            id: data_id,
                            field: data_value,
                            where: data_where,
                            db: data_db
                        },
                        url: `{{ route('file.delete') }}`,
                    }).done(function (msg) {
                        toastr.success(msg.message);
                        setTimeout(function () {
                            location.reload();
                        }, 1000);

                    }).fail(function (msg) {
                        toastr.error(msg.message)
                    });
                }
            });
        });

    </script>

@endpush
