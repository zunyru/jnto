@extends('layouts.app_dashboard')
@push('css')

@endpush
@section('content')
    <div class="container">
        <div class="justify-center">
            <h1 class="text-2xl p-4">{{ Route::getCurrentRoute()->getActionMethod() == 'edit' ? 'View' : 'Create new' }}
            </h1>
            <div class="w-full">
                <form
                        action="{{ Route::getCurrentRoute()->getActionMethod() == 'edit'
                                    ? route('banner.update',$banner->id)
                                    : route('banner.store') }}"
                        method="post"
                        enctype="multipart/form-data"
                        id="seminarForm"
                        class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
                    @csrf

                    @if(Route::getCurrentRoute()->getActionMethod() == 'edit')
                        @method('put')
                    @else
                        @method('post')
                    @endif
                    <div class="md:float-right mb-4 pr-4 absolute" style="right: 38px;top: 100px;">
                        <div class="flex flex-wrap mt-10 md:mt-0 md:flex-row items-center">
                            <div class="flex px-4 mb-2 md:mb-0">
                                <div style="margin-top: -24px;">
                                    <label>status</label>
                                    <select name="status"
                                            class="block appearance-none w-full border-2 bg-white border-blue-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500 valid"
                                            id="grid-state" aria-invalid="false">
                                        <option {{ @$banner->status == 'draft' ? 'selected' : 'selected' }} value="draft">
                                            Draft
                                        </option>
                                        <option {{ @$banner->status == 'published' ? 'selected' : '' }} value="published">
                                            Published
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="">
                                <button class="bg-teal-500 p-3 rounded-lg hover:bg-indigo-600 text-white w-50"
                                        type="submit">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="mb-4">
                        <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                            Url
                        </label>
                        <input class="shadow appearance-none border rounded w-full py-3 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                               type="text"
                               name="url"
                               value="{{ !empty($banner->url) ? $banner->url : '' }}"
                               placeholder="Url">
                    </div>

                    <div class="mb-4">
                        <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                            Thumbnail
                        </label>
                        <p class="text-sm text-gray-500">ขนาดภาพที่แนะนำ W822×H505 pixels / ความละเอียด 150 dpi / ความจุต่ำกว่า 1MB / PNG หรือ JPG</p>
                        <br>
                        <div class="flex flex-col text-left">
                            @if(!empty($banner->thumbnail))
                                <img src="{{ asset($banner->thumbnail) }}"
                                     id="thumbnail_preview"
                                     style="width: 100%; height: auto;">
                            @else
                                <img src=""
                                     id="thumbnail_preview"
                                     style="width: 100%; height: auto;">
                            @endif
                            <br>
                            <label for="thumbnail"></label>
                            <label for="thumbnail"
                                   class="FileUploadCSS">
                                Choose a file
                                <input type="file"
                                       name="thumbnail"
                                       accept="image/*"
                                       id="thumbnail"/>
                            </label>
                            <br>
                            <label id="thumbnail-error" class="error" for="thumbnail" style="display: none;">
                            </label>
                            @if(!empty($banner->thumbnail))
                                <br>
                                <a href="javascript:void(0);"
                                   data-filters="thumbnail"
                                   data-db="banners"
                                   data-where="id"
                                   data-id="{{ $banner->id }}"
                                   class="w-1/2 delete-img bg-red-600 text-white text-center p-2 rounded-md">
                                    <svg class="stroke-2 text-white inline-block h-4 w-4"
                                         xmlns="http://www.w3.org/2000/svg" fill="none"
                                         viewBox="0 0 24 24"
                                         stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round"
                                              strokeWidth={2}
                                              d="M6 18L18 6M6 6l12 12"/>
                                    </svg>
                                    Delete
                                </a>
                            @endif
                        </div>
                    </div>

                    <div class="mb-4">
                        <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                            Target
                        </label>
                        <select name="target"
                                class="block appearance-none w-full border-2 bg-white border-blue-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500 valid"
                                aria-invalid="false">
                            <option {{ @$banner->target == 'seller' ? 'selected' : 'selected' }} value="seller">
                                Japan seller
                            </option>
                            <option {{ @$banner->target == 'buyer' ? 'selected' : '' }} value="buyer">
                                Thai buyer
                            </option>
                        </select>
                    </div>


                </form>

            </div>
        </div>
        @endsection
        @push('scripts')
            <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
            <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
            <script src="{{ asset('js/validate-banner.js') }}"></script>

            @include('admin.include.block')

            <script>
                $('.delete-img').click(function () {
                    var data_value = $(this).attr('data-filters');
                    var data_db = $(this).attr('data-db');
                    var data_id = $(this).attr('data-id');
                    var data_where = $(this).attr('data-where');
                    Swal.fire({
                        title: 'Are you sure to delete ?' ,
                        text: "You won't be able to revert this!" ,
                        type: 'warning' ,
                        showCancelButton: true ,
                        confirmButtonColor: '#3085d6' ,
                        cancelButtonColor: '#d33' ,
                        confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                        if (result.value) {
                            $.ajax({
                                method: "POST" ,
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                } ,
                                data: {
                                    id: data_id ,
                                    field: data_value ,
                                    where: data_where ,
                                    db: data_db
                                } ,
                                url: `{{ route('file.delete') }}` ,
                            }).done(function (msg) {
                                toastr.success(msg.message);
                                setTimeout(function () {
                                    location.reload();
                                } , 1000);

                            }).fail(function (msg) {
                                toastr.error(msg.message)
                            });
                        }
                    });
                });
            </script>
    @endpush