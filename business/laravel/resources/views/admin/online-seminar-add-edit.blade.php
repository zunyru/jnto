@extends('layouts.app_dashboard')
@push('css')

@endpush
@section('content')
    <div class="container">
        <div class="justify-center">
            <h1 class="text-2xl p-4">{{ Route::getCurrentRoute()->getActionMethod() == 'edit' ? 'Edit' : 'Create new' }}
            </h1>
            @if(Route::getCurrentRoute()->getActionMethod() == 'edit')
                <span class="">Permalink :
                    <a class="text-uppercase text-blue-500"
                       target="_blank"
                       href="{{ route('seminar.show',$seminar->ref_id) }}"
                    >
                         {{ route('seminar.show',$seminar->ref_id) }}
                    </a>
                </span>
            @endif

            <div class="w-full">
                <form
                        action="{{ Route::getCurrentRoute()->getActionMethod() == 'edit'
                                    ? route('seminar.update',$seminar->ref_id)
                                    : route('seminar.store') }}"
                        method="post"
                        enctype="multipart/form-data"
                        id="seminarForm"
                        class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
                    @csrf

                    @if(Route::getCurrentRoute()->getActionMethod() == 'edit')
                        @method('put')
                    @else
                        @method('post')
                    @endif
                    <div class="md:float-right mb-4 pr-4 absolute" style="right: 38px;top: 100px;">
                        <div class="flex flex-wrap mt-10 md:mt-0 md:flex-row items-center">
                            <div class="flex px-4 mb-2 md:mb-0">
                                <div style="margin-top: -24px;">
                                    <label>Target</label>
                                    <select name="target"
                                            class="block appearance-none w-full border-2 bg-white border-blue-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                        <option {{ @$seminar->target == 'all' ? 'selected' : 'selected' }} value="all">
                                            All
                                        </option>
                                        <option {{ @$seminar->target == 'buyer' ? 'selected' : '' }} value="buyer">
                                            Thai buyer
                                        </option>
                                        <option {{ @$seminar->target == 'seller' ? 'selected' : '' }} value="seller">
                                            Japan seller (JP)
                                        </option>
                                        <option {{ @$seminar->target == 'seller_th' ? 'selected' : '' }} value="seller_th">
                                            Japan seller (TH)
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="flex px-4 mb-2 md:mb-0">
                                <div style="margin-top: -24px;">
                                    <label>status</label>
                                    <select name="status"
                                            class="block appearance-none w-full border-2 bg-white border-blue-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                            id="grid-state">
                                        <option {{ @$seminar->status == 'draft' ? 'selected' : 'selected' }} value="draft">
                                            Draft
                                        </option>
                                        <option {{ @$seminar->status == 'published' ? 'selected' : '' }} value="published">
                                            Published
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="">
                                <button class="bg-teal-500 p-3 rounded-lg hover:bg-indigo-600 text-white w-50"
                                        type="submit">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="w-full">

                        <div class="mb-4 mt-5">
                            <label class="block text-gray-700 text-sm font-bold mb-2" for="sub_title">
                                Thumbnail Badge
                            </label>
                            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                   id="list"
                                   type="text"
                                   name="list"
                                   value="{{ !empty($seminar->list) ? $seminar->list : '' }}"
                                   placeholder="Thumbnail Badge">
                        </div>

                        <div class="mb-4">
                            <label class="block text-gray-700 text-sm font-bold mb-2" for="sub_title">
                                Title
                            </label>
                            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                   id="sub_title"
                                   type="text"
                                   name="sub_title"
                                   value="{{ !empty($seminar->sub_title) ? $seminar->sub_title : '' }}"
                                   placeholder="Title">
                        </div>

                        <div class="flex text-center rounded-xl border-gray-300 border p-3">
                            <div class="flex-1 p-2">
                                <div class="mb-4">
                                    <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                        Thumbnail
                                    </label>
                                    <span class="text-sm text-gray-500">ขนาด 560×315 pixels ขึ้นไป/ความจุต่ำกว่า 1MB/PNG หรือ JPG</span>
                                    <div class="flex flex-col items-center text-center">
                                        @if(!empty($seminar->thumbnail))
                                            <img src="{{ asset($seminar->thumbnail) }}"
                                                 style="width: 100%; height: auto;">
                                        @endif
                                        <br>
                                        <label for="thumbnail"></label>
                                        <label for="thumbnail"
                                               class="FileUploadCSS">
                                            Choose a file
                                            <input type="file"
                                                   name="thumbnail"
                                                   accept="image/*"
                                                   id="thumbnail"/>
                                        </label>
                                        <br>
                                        <label id="photo1-error" class="error" for="photo1" style="display: none;">
                                        </label>
                                        @if(!empty($seminar->thumbnail))
                                            <br>
                                            <a href="javascript:void(0);"
                                               data-filters="thumbnail"
                                               data-db="seminars"
                                               data-where="id"
                                               data-id="{{ $seminar->ref_id }}"
                                               class="w-1/2 delete-img bg-red-600 text-white text-center p-2 rounded-md">
                                                <svg class="stroke-2 text-white inline-block h-4 w-4"
                                                     xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                                     stroke="currentColor">
                                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2}
                                                          d="M6 18L18 6M6 6l12 12"/>
                                                </svg>
                                                Delete
                                            </a>
                                        @endif
                                    </div>

                                </div>
                            </div>
                            <div class="flex-1 p-2">
                                <div class="mb-4">
                                    <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                        Embed youtube seminar
                                    </label>
                                    <br>
                                    <div class="flex flex-col items-center text-center">
                                    <textarea
                                            rows="8"
                                            name="video"
                                            class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">{!! !empty($seminar->video) ? $seminar->video : '' !!}</textarea>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="my-4">
                            <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                Page Header
                            </label>
                            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                   id="title"
                                   type="text"
                                   name="title"
                                   value="{{ !empty($seminar->title) ? $seminar->title : '' }}"
                                   placeholder="Title">
                        </div>

                        <div class="mb-4 mt-5">
                            <label class="block text-gray-700 text-sm font-bold mb-2" for="sub_title">
                                Display date
                            </label>
                            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                   id="sub_title"
                                   type="text"
                                   name="display_date"
                                   value="{{ !empty($seminar->display_date) ? $seminar->display_date : '' }}"
                                   placeholder="Display date">
                        </div>

                        <div class="mb-4">
                            <label class="block text-gray-700 text-sm font-bold mb-2" for="sub_title">
                                Display time
                            </label>
                            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                   id="sub_title"
                                   type="text"
                                   name="display_time"
                                   value="{{ !empty($seminar->display_time) ? $seminar->display_time : '' }}"
                                   placeholder="Display time">
                        </div>

                        <div class="mb-4 mt-5">
                            <label class="block text-gray-700 text-sm font-bold mb-2" for="sub_title">
                                Subject name
                            </label>
                            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                   id="subject_name"
                                   type="text"
                                   name="subject_name"
                                   value="{{ !empty($seminar->subject_name) ? $seminar->subject_name : '' }}"
                                   placeholder="Subject name">
                        </div>

                        <div class="mb-4 mt-10">
                            <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                Detail
                            </label>
                            <textarea id="detail"
                                      name="detail">{!! !empty($seminar->detail) ? nl2br(e($seminar->detail)) : '' !!}</textarea>
                        </div>

                        <div class="mb-4 mt-10 rounded-xl border-gray-300 border p-3">
                            <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                PDF
                            </label>
                            <p class="text-sm text-gray-500">PDF、5MB以下 | ไฟล์ PDF ขนาดต่ำกว่า 5MB</p>
                            <label for="pdf1"></label>
                            <label for="pdf1"
                                   class="FileUploadCSS">
                                Choose a file
                                <input type="file"
                                       name="pdf1"
                                       id="pdf1"/>
                            </label>
                            <br>
                            <label id="pdf1-error" class="error" for="pdf1" style="display: none;">
                            </label>
                            @if(!empty($seminar->pdf1))
                                <a href="{{ asset($seminar->pdf1) }}"
                                   target="_blank">
                                    <img class="mt-3" src="{{ asset('assets/images/pdf.png') }}"
                                         style="width: auto; max-width: 10%;">
                                </a>
                                <p>{{ nameFile($seminar->pdf1) }}</p>
                                <br>
                                <a href="javascript:void(0);"
                                   data-filters="pdf1"
                                   data-db="seminars"
                                   data-where="id"
                                   data-id="{{ $seminar->ref_id }}"
                                   class="w-1/2 delete-img bg-red-600 text-white text-center p-2 rounded-md">
                                    <svg class="stroke-2 text-white inline-block h-4 w-4"
                                         xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                         stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2}
                                              d="M6 18L18 6M6 6l12 12"/>
                                    </svg>
                                    Delete
                                </a>
                            @endif
                            <label class="block text-gray-600 text-sm font-bold mt-4" for="username">
                                File description
                            </label>

                            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                   type="text"
                                   maxlength="100"
                                   name="pdf_title1"
                                   value="{{ !empty($seminar->pdf_title1) ? $seminar->pdf_title1 : '' }}"
                                   placeholder="100文字以内|สูงสุด 100 ตัวอักษร">
                        </div>

                        <div class="mb-4 mt-10 rounded-xl border-gray-300 border p-3">
                            <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                PDF
                            </label>
                            <p class="text-sm text-gray-500">PDF、5MB以下 | ไฟล์ PDF ขนาดต่ำกว่า 5MB</p>
                            <label for="pdf2"></label>
                            <label for="pdf2"
                                   class="FileUploadCSS">
                                Choose a file
                                <input type="file"
                                       name="pdf2"
                                       id="pdf2"/>
                            </label>
                            <br>
                            <label id="pdf2-error" class="error" for="pdf2" style="display: none;">
                            </label>
                            @if(!empty($seminar->pdf2))
                                <a href="{{ asset($seminar->pdf2) }}"
                                   target="_blank">
                                    <img class="mt-3" src="{{ asset('assets/images/pdf.png') }}"
                                         style="width: auto; max-width: 10%;">
                                </a>
                                <p>{{ nameFile($seminar->pdf2) }}</p>
                                <br>
                                <a href="javascript:void(0);"
                                   data-filters="pdf2"
                                   data-db="seminars"
                                   data-where="id"
                                   data-id="{{ $seminar->ref_id }}"
                                   class="w-1/2 delete-img bg-red-600 text-white text-center p-2 rounded-md">
                                    <svg class="stroke-2 text-white inline-block h-4 w-4"
                                         xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                         stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2}
                                              d="M6 18L18 6M6 6l12 12"/>
                                    </svg>
                                    Delete
                                </a>
                            @endif
                            <label class="block text-gray-600 text-sm font-bold mt-4" for="username">
                                File description
                            </label>
                            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                   type="text"
                                   maxlength="100"
                                   name="pdf_title2"
                                   value="{{ !empty($seminar->pdf_title2) ? $seminar->pdf_title2 : '' }}"
                                   placeholder="100文字以内|สูงสุด 100 ตัวอักษร">
                        </div>

                        <div class="mb-4 mt-10 rounded-xl border-gray-300 border p-3">
                            <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                PDF
                            </label>
                            <p class="text-sm text-gray-500">PDF、5MB以下 | ไฟล์ PDF ขนาดต่ำกว่า 5MB</p>
                            <label for="pdf3"></label>
                            <label for="pdf3"
                                   class="FileUploadCSS">
                                Choose a file
                                <input type="file"
                                       name="pdf3"
                                       id="pdf3"/>
                            </label>
                            <br>
                            <label id="pdf3-error" class="error" for="pdf3" style="display: none;">
                            </label>
                            @if(!empty($seminar->pdf3))
                                <a href="{{ asset($seminar->pdf3) }}"
                                   target="_blank">
                                    <img class="mt-3" src="{{ asset('assets/images/pdf.png') }}"
                                         style="width: auto; max-width: 10%;">
                                </a>
                                <p>{{ nameFile($seminar->pdf3) }}</p>
                                <br>
                                <a href="javascript:void(0);"
                                   data-filters="pdf3"
                                   data-db="seminars"
                                   data-where="id"
                                   data-id="{{ $seminar->ref_id }}"
                                   class="w-1/2 delete-img bg-red-600 text-white text-center p-2 rounded-md">
                                    <svg class="stroke-2 text-white inline-block h-4 w-4"
                                         xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                         stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2}
                                              d="M6 18L18 6M6 6l12 12"/>
                                    </svg>
                                    Delete
                                </a>
                            @endif
                            <label class="block text-gray-600 text-sm font-bold mt-4" for="username">
                                File description
                            </label>
                            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                   type="text"
                                   maxlength="100"
                                   name="pdf_title3"
                                   value="{{ !empty($seminar->pdf_title3) ? $seminar->pdf_title3 : '' }}"
                                   placeholder="100文字以内|สูงสุด 100 ตัวอักษร">
                        </div>

                        <div class="mb-4 mt-10 rounded-xl border-gray-300 border p-3">
                            @php
                                $fix_year = '2020';
                                $arr_year = [];
                                $year  = Carbon\Carbon::now()->year;
                                for ($i = $fix_year; $year >= $fix_year; $year--){
                                    $arr_year[] = $year;
                                }
                            @endphp
                            <select name="year"
                                    class="block appearance-none w-full border-2 bg-white border-blue-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500">
                                @foreach($arr_year as $year_txt)
                                    <option {{ @$seminar->year == $year_txt ? 'selected' : '' }} value="{{ $year_txt }}">
                                        {{ $year_txt }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="mb-4 mt-10 rounded-xl border-gray-300 border p-3">
                            <label class="block text-gray-600 text-sm font-bold mt-4" for="seminar_date">
                                Seminar date
                            </label>
                            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                   type="date"
                                   name="updated_at"
                                   value="{{ !empty($seminar->updated_at) ? $seminar->updated_at->format('Y-m-d') : '' }}">
                        </div>

                    </div>
                </form>

            </div>
        </div>
        @endsection
        @push('scripts')
            <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
            <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
            <script src="{{ asset('js/validate-seminar.js') }}"></script>

            @include('admin.include.block')

            <script>
                tinymce.init({
                    readonly: readonly ?? false,
                    selector: 'textarea#detail',
                    width: '100%',
                    height: 500,
                    forced_root_block: false,
                    force_br_newlines: true,
                    force_p_newlines: false,
                    toolbar: 'bold customBr link underline',
                    plugins: 'link',
                    convert_urls: false,
                    menubar: false,
                    setup: function (editor) {
                        editor.ui.registry.addButton('customBr', {
                            text: 'Enter',
                            onAction: function () {
                                editor.insertContent('<br/> ');
                            },
                        });
                    }
                });

                $('.delete-img').click(function () {
                    var data_value = $(this).attr('data-filters');
                    var data_db = $(this).attr('data-db');
                    var data_id = $(this).attr('data-id');
                    var data_where = $(this).attr('data-where');
                    Swal.fire({
                        title: 'Are you sure to delete ?',
                        text: "You won't be able to revert this!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                        if (result.value) {
                            $.ajax({
                                method: "POST",
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                data: {
                                    id: data_id,
                                    field: data_value,
                                    where: data_where,
                                    db: data_db
                                },
                                url: `{{ route('file.delete') }}`,
                            }).done(function (msg) {
                                toastr.success(msg.message);
                                setTimeout(function () {
                                    location.reload();
                                }, 1000);

                            }).fail(function (msg) {
                                toastr.error(msg.message)
                            });
                        }
                    });
                });
            </script>
    @endpush