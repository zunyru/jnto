<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ setting('site.title') ?? config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    {{--<script src="{{ asset('js/app.js') }}" defer></script>--}}

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        .block {
            display: block;
        }

        form label.error {
            display: none;
        }

        .top--20 {
            position: relative;
            top: -20px;
        }

        .error {
            margin-top: unset;
            margin-bottom: unset;
        }

        .bg-gold-500 {
            --bg-opacity: 1;
            background-color: #a6806b;
        }

        .bg-gold-500:hover {
            --bg-opacity: 1;
            background-color: #6d5040;
        }

        .text-gold-500 {
            color: #a6806b;
        }

        .border-gold-300:focus {
            --border-opacity: 1;
            border-color: #a6806b;
        }
    </style>

    {{-- --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style-form.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style-responsive.css') }}">
    <link rel="icon" href="{{ asset('favicon-32x32.png') }}" type="image" sizes="32x32">
    <link href="https://fonts.googleapis.com/css2?family=Prompt:ital,wght@0,300;0,500;0,600;0,900;1,900&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


</head>
<body class="">

<div id="app">
    @if(Auth::check())
        <div class="float-right">
            <button class="mx-2 text-gray-700 focus:outline-none p-3"
                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <span class="text-uppercase">Logout</span>
            </button>
        </div>
    @endif
    <main class="py-4">
        @yield('content')
    </main>
</div>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>

@include('layouts.script')
@stack('scripts')
</body>
</html>
