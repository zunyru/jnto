@extends('layouts.app_dashboard')
@push('css')

@endpush
@section('content')
    <div class="container">
        <div class="justify-center">
            <h1 class="text-2xl p-4">{{ Route::getCurrentRoute()->getActionMethod() == 'edit' ? 'View' : 'Create new' }}
            </h1>
            <div class="w-full">
                <form
                        action="{{ Route::getCurrentRoute()->getActionMethod() == 'edit'
                                    ? route('covid.update',$covid->id)
                                    : route('covid.store') }}"
                        method="post"
                        enctype="multipart/form-data"
                        id="seminarForm"
                        class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
                    @csrf

                    @if(Route::getCurrentRoute()->getActionMethod() == 'edit')
                        @method('put')
                    @else
                        @method('post')
                    @endif
                    <div class="md:float-right mb-4 pr-4 absolute" style="right: 38px;top: 100px;">
                        <div class="flex flex-wrap mt-10 md:mt-0 md:flex-row items-center">
                            <div class="flex px-4 mb-2 md:mb-0">
                                <div style="margin-top: -24px;">
                                    <label>status</label>
                                    <select name="status" class="block appearance-none w-full border-2 bg-white border-blue-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500 valid" id="grid-state" aria-invalid="false">
                                        <option {{ @$covid->status == 'draft' ? 'selected' : 'selected' }} value="draft">
                                            Draft
                                        </option>
                                        <option {{ @$covid->status == 'published' ? 'selected' : '' }} value="published">
                                            Published
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="">
                                <button class="bg-teal-500 p-3 rounded-lg hover:bg-indigo-600 text-white w-50"
                                        type="submit">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="mb-4">
                        <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                            Title
                        </label>
                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                               id="username"
                               type="text"
                               name="title"
                               value="{{ !empty($covid->title) ? $covid->title : '' }}"
                               placeholder="Title">
                    </div>

                    <div class="mb-4">
                        <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                            URL
                        </label>
                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                               id="url"
                               type="text"
                               name="url"
                               value="{{ !empty($covid->url) ? $covid->url : '' }}"
                               placeholder="URL">
                    </div>

                </form>

            </div>
        </div>
        @endsection
        @push('scripts')
            <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
            <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
            <script src="{{ asset('js/validate-covid.js') }}"></script>

            @include('admin.include.block')

            <script>
                $('.delete-img').click(function () {
                    var data_value = $(this).attr('data-filters');
                    var data_db = $(this).attr('data-db');
                    var data_id = $(this).attr('data-id');
                    var data_where = $(this).attr('data-where');
                    Swal.fire({
                        title: 'Are you sure to delete ?',
                        text: "You won't be able to revert this!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                        if (result.value) {
                            $.ajax({
                                method: "POST",
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                data: {
                                    id: data_id,
                                    field: data_value,
                                    where: data_where,
                                    db: data_db
                                },
                                url: `{{ route('file.delete') }}`,
                            }).done(function (msg) {
                                toastr.success(msg.message);
                                setTimeout(function () {
                                    location.reload();
                                }, 1000);

                            }).fail(function (msg) {
                                toastr.error(msg.message)
                            });
                        }
                    });
                });
            </script>
    @endpush