@extends('layouts.app_dashboard')
@section('content')
    <div class="container">
        <div class="justify-center">
            <div class="pt-4">
                <div class="w-full shadow bg-white rounded p-4 mb-10">
                    <h1>HOME</h1>
                    <div class="w-full">
                        <div class="mb-4 mt-10">

                            {!! !empty($setting_value->value) ? nl2br($setting_value->value) : '' !!}
                        </div>
                        <hr>
                        @if(!empty($setting_file1->value))
                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    File upload 1
                                </label>
                            </div>

                            @if(!empty($setting_file1->value))
                                <a href="{{ asset($setting_file1->value) }}"
                                   target="_blank">
                                    <img class="mt-3" src="{{ asset($icon1) }}"
                                         style="width: 120px; max-width: 10%;">
                                </a>
                                <p>{{ nameFile($setting_file1->value) }}</p>
                                <br>
                            @endif
                            <hr>
                        @endif

                        @if(!empty($setting_file2->value))
                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    File upload 2
                                </label>
                            </div>

                            @if(!empty($setting_file2->value))
                                <a href="{{ asset($setting_file2->value) }}"
                                   target="_blank">
                                    <img class="mt-3" src="{{ asset($icon2) }}"
                                         style="width: 120px; max-width: 10%;">
                                </a>
                                <p>{{ nameFile($setting_file2->value) }}</p>
                                <br>

                            @endif
                            <hr>
                        @endif
                        @if(!empty($setting_file3->value))
                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    File upload 3
                                </label>
                            </div>

                            @if(!empty($setting_file3->value))
                                <a href="{{ asset($setting_file3->value) }}"
                                   target="_blank">
                                    <img class="mt-3" src="{{ asset($icon3) }}"
                                         style="width: 120px; max-width: 10%;">
                                </a>
                                <p>{{ nameFile($setting_file3->value) }}</p>
                                <br>
                            @endif
                            <hr>
                        @endif
                        @if(!empty($setting_file4->value))
                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    File upload 4
                                </label>
                            </div>

                            @if(!empty($setting_file4->value))
                                <a href="{{ asset($setting_file4->value) }}"
                                   target="_blank">
                                    <img class="mt-3" src="{{ asset($icon4) }}"
                                         style="width: 120px; max-width: 10%;">
                                </a>
                                <p>{{ nameFile($setting_file4->value) }}</p>
                                <br>
                            @endif
                            <hr>
                        @endif
                        @if(!empty($setting_file5->value))
                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    File upload 5
                                </label>
                            </div>
                            @if(!empty($setting_file5->value))
                                <a href="{{ asset($setting_file5->value) }}"
                                   target="_blank">
                                    <img class="mt-3" src="{{ asset($icon5) }}"
                                         style="width: 120px; max-width: 10%;">
                                </a>
                                <p>{{ nameFile($setting_file5->value) }}</p>
                                <br>
                            @endif
                            <hr>
                        @endif
                        @if(!empty($setting_file6->value))
                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    File upload 6
                                </label>

                            </div>
                            @if(!empty($setting_file6->value))
                                <a href="{{ asset($setting_file6->value) }}"
                                   target="_blank">
                                    <img class="mt-3" src="{{ asset($icon6) }}"
                                         style="width: 120px; max-width: 10%;">
                                </a>
                                <p>{{ nameFile($setting_file6->value) }}</p>
                                <br>
                            @endif
                            <hr>
                        @endif
                        @if(!empty($setting_file7->value))
                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    File upload 7
                                </label>
                            </div>
                            @if(!empty($setting_file7->value))
                                <a href="{{ asset($setting_file7->value) }}"
                                   target="_blank">
                                    <img class="mt-3" src="{{ asset($icon7) }}"
                                         style="width: 120px; max-width: 10%;">
                                </a>
                                <p>{{ nameFile($setting_file7->value) }}</p>
                                <br>

                            @endif
                            <hr>
                        @endif
                        @if(!empty($setting_file8->value))
                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    File upload 8
                                </label>
                            </div>
                            @if(!empty($setting_file8->value))
                                <a href="{{ asset($setting_file8->value) }}"
                                   target="_blank">
                                    <img class="mt-3" src="{{ asset($icon8) }}"
                                         style="width: 120px; max-width: 10%;">
                                </a>
                                <p>{{ nameFile($setting_file8->value) }}</p>
                                <br>

                            @endif
                            <hr>
                        @endif
                        @if(!empty($setting_file9->value))
                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    File upload 9
                                </label>
                            </div>
                            @if(!empty($setting_file9->value))
                                <a href="{{ asset($setting_file9->value) }}"
                                   target="_blank">
                                    <img class="mt-3" src="{{ asset($icon9) }}"
                                         style="width: 120px; max-width: 10%;">
                                </a>
                                <p>{{ nameFile($setting_file9->value) }}</p>
                                <br>

                            @endif
                            <hr>
                        @endif
                        @if(!empty($setting_file10->value))
                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    File upload 10
                                </label>
                            </div>
                            @if(!empty($setting_file10->value))
                                <a href="{{ asset($setting_file10->value) }}"
                                   target="_blank">
                                    <img class="mt-3" src="{{ asset($icon10) }}"
                                         style="width: 120px; max-width: 10%;">
                                </a>
                                <p>{{ nameFile($setting_file10->value) }}</p>
                                <br>
                            @endif
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>


@endpush
