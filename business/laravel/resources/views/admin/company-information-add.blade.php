@extends('layouts.app_dashboard')

@section('content')
    <div class="container">
        <div class="justify-center">

            <div class="MyPageTemplate">
                <div class="RightContent">
                    <div class="FormBox clr">

                        @if ($errors->any())
                            <div class="mb-3" role="alert">
                                <div class="bg-red-500 text-white font-bold rounded-t px-4 py-2">
                                    Error
                                </div>
                                <div class="border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
                                    @foreach ($errors->all() as $error)
                                        <p>{{ $error }}</p>
                                    @endforeach
                                </div>
                            </div>
                        @endif

                        <form action="{{ route('business.store') }}"
                              method="POST"
                              enctype="multipart/form-data"
                              id="myform">
                            @csrf

                            <input type="hidden" name="admin_mode" value="1">

                            <div class="FormList">

                                {{--Company Information--}}
                                @include('sessions.create.company-information')

                                {{--Company Information--}}
                                @include('sessions.create.service-information')

                                {{--Support menu--}}
                                @include('sessions.create.support-menu')

                                {{--Category tag--}}
                                @include('sessions.create.category-tag')

                                {{--policy tag--}}
                                @include('sessions.create.policy')

                                {{--Feature Banner--}}
                                @include('sessions.create.feature_banner')

                                @include('sessions.edit.event-edit')

                                @include('sessions.create.privacy-setting')

                                <div class="BTNList">
                                    <button type="submit">SUBMIT <i class="fa fa-send-o"></i></button>
                                </div>

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('layouts.validate')
