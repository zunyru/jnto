@extends('layouts.app_dashboard')
@prepend('css')
    <style>
        /*Form fields*/
        .dataTables_wrapper select,
        .dataTables_wrapper .dataTables_filter input {
            color: #4a5568; /*text-gray-700*/
            padding-left: 1rem; /*pl-4*/
            padding-right: 1rem; /*pl-4*/
            padding-top: .5rem; /*pl-2*/
            padding-bottom: .5rem; /*pl-2*/
            line-height: 1.25; /*leading-tight*/
            border-width: 2px; /*border-2*/
            border-radius: .25rem;
            border-color: #edf2f7; /*border-gray-200*/
            background-color: #edf2f7; /*bg-gray-200*/
        }

        /*Row Hover*/
        table.dataTable.hover tbody tr:hover, table.dataTable.display tbody tr:hover {
            background-color: #ebf4ff; /*bg-indigo-100*/
        }

        /*Pagination Buttons*/
        .dataTables_wrapper .dataTables_paginate .paginate_button {
            font-weight: 700; /*font-bold*/
            border-radius: .25rem; /*rounded*/
            border: 1px solid transparent; /*border border-transparent*/
        }

        /*Pagination Buttons - Current selected */
        .dataTables_wrapper .dataTables_paginate .paginate_button.current {
            color: #fff !important; /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06); /*shadow*/
            font-weight: 700; /*font-bold*/
            border-radius: .25rem; /*rounded*/
            background: #667eea !important; /*bg-indigo-500*/
            border: 1px solid transparent; /*border border-transparent*/
        }

        /*Pagination Buttons - Hover */
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            color: #fff !important; /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06); /*shadow*/
            font-weight: 700; /*font-bold*/
            border-radius: .25rem; /*rounded*/
            background: #667eea !important; /*bg-indigo-500*/
            border: 1px solid transparent; /*border border-transparent*/
        }

        /*Add padding to bottom border */
        table.dataTable.no-footer {
            border-bottom: 1px solid #e2e8f0; /*border-b-1 border-gray-300*/
            margin-top: 0.75em;
            margin-bottom: 0.75em;
        }

        /*Change colour of responsive icon*/
        table.dataTable.dtr-inline.collapsed > tbody > tr > td:first-child:before, table.dataTable.dtr-inline.collapsed > tbody > tr > th:first-child:before {
            background-color: #667eea !important; /*bg-indigo-500*/
        }

        table.dataTable tbody td {
            padding: 15px 10px !important;
        }

        .container .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
            color: #fff !important;
        }

    </style>
@endprepend
@section('content')
    <div class="container">
        <div class="justify-center">
            <h1>JNTO News</h1>
            <div class="pt-4">
                <div class="w-full shadow bg-white rounded p-4 mb-10">
                    <h1 class="text-2xl text-left p-2">Keyword Search</h1>
                    <div class="md:flex md:flex-row w-full p-2 pb-4">
                        <div class="md:flex-auto w-full md:w-2/3 md:pr-2">
                            <input
                                    id="input_search"
                                    type="text"
                                    class="form-input mt-1 block w-full"
                                    placeholder="Title">
                        </div>
                        <div class="md:flex-auto w-full md:w-1/3 md:pr-2">
                            <button
                                    id="search"
                                    type="button"
                                    class="bg-blue-500 active:bg-blue-700 py-3 px-4 rounded text-white shadow">Search
                            </button>
                        </div>
                    </div>
                </div>

                <div class="container w-full mx-auto">
                    <div id="recipients">
                        @if (auth()->user()->role->name != 'Viewer')
                            <a href="{{route('news-jnto.create')}}"
                               type="button"
                               class="float-right bg-green-500 active:bg-green-700 py-3 px-4 rounded text-white shadow">Create</a>
                        @endif

                        <div class="w-full shadow bg-white rounded p-4">
                            <div class="border-gray-200 w-full rounded bg-white overflow-x-auto">
                                <table class="leading-normal " id="list_table"
                                       style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
                                    <thead
                                            class="text-gray-600 text-xs font-semibold border-gray tracking-wider text-left px-5 py-3 bg-gray-100 hover:cursor-pointer uppercase border-b-2 border-gray-200">
                                    <tr class="border-b border-gray">
                                        <th scope="col"
                                            data-priority="2"
                                            class="text-gray-dark border-gray border-b-2 border-t-2 border-gray-200 py-3 px-3 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                            ID
                                        </th>
                                        <th scope="col"
                                            data-priority="3"
                                            class="text-gray-dark border-gray border-b-2 border-t-2 border-gray-200 py-3 px-3 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                            Title
                                        </th>
                                        <th scope="col"
                                            data-priority="3"
                                            class="text-gray-dark border-gray border-b-2 border-t-2 border-gray-200 py-3 px-3 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                            Template
                                        </th>
                                        <th scope="col"
                                            data-priority="8"
                                            class="text-gray-dark border-gray border-b-2 border-t-2 border-gray-200 py-3 px-3 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                            Status
                                        </th>
                                        <th scope="col"
                                            data-priority="9"
                                            class="text-gray-dark border-gray border-b-2 border-t-2 border-gray-200 py-3 px-3 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                            {{ auth()->user()->role->name == 'Viewer' ? 'View' : 'Edit' }}
                                        </th>
                                        @if (auth()->user()->role->name != 'Viewer')
                                            <th scope="col"
                                                data-priority="10"
                                                class="text-gray-dark border-gray border-b-2 border-t-2 border-gray-200 py-3 px-3 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                                Delete
                                            </th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection
@prepend('scripts')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.6/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/colreorder/1.5.2/js/dataTables.colReorder.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <script>

        fill_datatable();

        function delete_item(t) {
            Swal.fire({
                title: 'Are you sure to delete?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            id: $(t).attr('data-id')
                        },
                        url: `{{ route('news-jnto.delete') }}`,
                    }).done(function (msg) {
                        toastr.success(msg.message);
                        $('#list_table').DataTable().destroy();
                        fill_datatable();
                    }).fail(function (msg) {
                        toastr.error(msg.message)
                    });
                }
            });
        }

        function fill_datatable(filters = []) {
            var base_url = `{{route('news-jnto.data.index')}}`;

            var dataList = $('#list_table');
            dataList.DataTable({
                searching: false,
                serverSide: true,
                processing: true,
                responsive: true,
                ajax: {
                    url: base_url,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    data: {
                        filters: filters
                    }
                },
                order: [[0, "desc"]],
                pageLength: 50,
                lengthMenu: [50, 100, 150, 200, 300],
                columns: [
                    {
                        data: "id",
                        width: "10%",
                        class: "py-4 px-6 border border-gray-200 text-gray-900 text-sm text-center",
                        orderable: true
                    },
                    {
                        data: "title",
                        width: "40%",
                        class: "py-4 px-6 border border-gray-200 text-gray-900 text-sm",
                        orderable: true
                    },
                    {
                        data: "template",
                        width: "11%",
                        class: "py-4 px-6 border border-gray-200 text-gray-900 text-sm text-center",
                        orderable: true
                    },
                    {
                        data: "status",
                        width: "11%",
                        class: "py-4 px-6 border border-gray-200 text-gray-900 text-sm text-center",
                        orderable: true
                    },
                    {
                        data: "edit_btn",
                        width: "10%",
                        class: "py-4 px-6 border border-gray-200 text-gray-900 text-sm text-center",
                        orderable: false
                    },
                        @if (auth()->user()->role->name != 'Viewer')
                    {
                        data: "delete_btn",
                        width: "15%",
                        class: "py-4 px-6 border border-gray-200 text-gray-900 text-sm text-center",
                        orderable: false
                    }
                    @endif
                ]
            }).columns.adjust()
                .responsive.recalc();
        }

        $(document).ready(function () {
            var params = {};
            $('#search').click(function () {
                var input_search = $('#input_search').val();
                if (input_search != '') {
                    params['search'] = input_search;
                    $('#list_table').DataTable().destroy();
                    fill_datatable(params);
                } else {
                    params['search'] = '';
                    $('#list_table').DataTable().destroy();
                    fill_datatable(params)
                }
            });
        });

        function update_staus(t) {
            var id = $(t).attr('data-id');

            $.ajax({
                method: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    id: id,
                    status: $(t).val()
                },
                url: `{{ route('news-jnto.update.status') }}`,
            }).done(function (msg) {
                toastr.success(msg.message)
            })
        }

    </script>
@endprepend