@extends('layouts.app_dashboard')

@section('content')
    <div class="container">
        <div class="justify-center">

            <div class="MyPageTemplate">
                <div class="RightContent">
                    <div class="FormBox clr">

                        @if ($errors->any())
                            <div class="mb-3" role="alert">
                                <div class="bg-red-500 text-white font-bold rounded-t px-4 py-2">
                                    Error
                                </div>
                                <div class="border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
                                    @foreach ($errors->all() as $error)
                                        <p>{{ $error }}</p>
                                    @endforeach
                                </div>
                            </div>
                        @endif

                        <form action="{{ route('business.storeQuick') }}"
                              method="POST"
                              enctype="multipart/form-data"
                              id="myform">
                            @csrf

                            <input type="hidden" name="admin_mode" value="1">

                            <div class="FormList">
                                <fieldset style="background-color: #FFFFE6;">
                                    <legend>Quick Create</legend>
                                    <ul>
                                        <li>
                                            <label>
                                                <p class="FloatLeft">
                                                    Email Login
                                                </p>
                                                <input type="text"
                                                       name="user_login"
                                                       value="{{ old('user_login') }}"
                                                       placeholder="Email Login">
                                            </label>
                                        </li>
                                    </ul>

                                    <div class="BTNList">
                                        <button type="submit">SUBMIT <i class="fa fa-send-o"></i></button>
                                    </div>
                                </fieldset>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('layouts.validate')