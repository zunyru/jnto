@extends('layouts.app_dashboard')
@push('css')

@endpush
@section('content')
    <div class="container">
        <div class="justify-center">
            <h1 class="text-2xl p-4">{{ Route::getCurrentRoute()->getActionMethod() == 'edit' ? 'Edit' : 'Create new' }}
            </h1>
            <div class="w-full">
                <form
                        action="{{ Route::getCurrentRoute()->getActionMethod() == 'edit'
                                    ? route('user.update',$user->id)
                                    : route('user.store') }}"
                        method="post"
                        enctype="multipart/form-data"
                        id="seminarForm"
                        class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
                    @csrf

                    @if(Route::getCurrentRoute()->getActionMethod() == 'edit')
                        @method('put')
                    @else
                        @method('post')
                    @endif
                    <div class="md:float-right mb-4 pr-4 absolute" style="right: 38px;top: 100px;">
                        <div class="flex flex-wrap mt-10 md:mt-0 md:flex-row items-center">
                            <div class="">
                                <button class="bg-teal-500 p-3 rounded-lg hover:bg-indigo-600 text-white w-50"
                                        type="submit">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="mb-4">
                        <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                            Username
                        </label>
                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                               id="username"
                               type="text"
                               name="username"
                               value="{{ !empty($user->username) ? $user->username : '' }}"
                               placeholder="Username">
                    </div>
                    <div class="mb-4">
                        <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                            Password
                        </label>
                        <div class="relative">
                            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                   id="password"
                                   type="password"
                                   name="password"
                                   placeholder="Password"
                            >
                            <div class="absolute top-1 right-1">
                                <a href="javascript:void(0)" id="password-open" onclick="triggerpassword()">
                                    <svg
                                            class="w-6"
                                            xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                            stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                              d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"/>
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                              d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"/>
                                    </svg>
                                </a>
                                <a href="javascript:void(0)" id="password-close" onclick="triggerpassword()">
                                    <svg
                                            class="w-6"
                                            xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                            stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                              d="M13.875 18.825A10.05 10.05 0 0112 19c-4.478 0-8.268-2.943-9.543-7a9.97 9.97 0 011.563-3.029m5.858.908a3 3 0 114.243 4.243M9.878 9.878l4.242 4.242M9.88 9.88l-3.29-3.29m7.532 7.532l3.29 3.29M3 3l3.59 3.59m0 0A9.953 9.953 0 0112 5c4.478 0 8.268 2.943 9.543 7a10.025 10.025 0 01-4.132 5.411m0 0L21 21"/>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="mb-4">
                        <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                            Email
                        </label>
                        <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                               id="email"
                               type="email"
                               name="email"
                               value="{{ !empty($user->email) ? $user->email : '' }}"
                               placeholder="Email">
                    </div>
                    <div class="mb-4">
                        <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                            Role
                        </label>
                        <select name="role_id"
                                class="block appearance-none w-full border-2 bg-white border-blue-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                id="grid-state"
                                required>
                            <option value="">--Select role--</option>
                            @foreach($roles as $role)
                                <option {{ @$user->role_id == $role->id ? 'selected' : '' }} value="{{ $role->id }}">
                                    {{ $role->display_name }}
                                </option>
                            @endforeach

                        </select>
                    </div>

                </form>

                <div class="p-4">
                    <h1 class="text-2xl mb-4">
                        Roles and Capabilities
                    </h1>

                    <p class="p-4block text-gray-700 text-sm font-bold mb-2">
                        Access web
                    </p>
                    <p class="text-sm text-gray-500">
                        ウェブサイトの閲覧が可能。
                    </p>
                    <p class="text-sm text-gray-500">
                        เข้าชมเว็บไซต์
                    </p>

                    <p class="p-4block text-gray-700 text-sm font-bold mb-2 mt-4">
                        Access form
                    </p>
                    <p class="text-sm text-gray-500">
                        セラー情報登録フォームのアクセスが可能。
                    </p>
                    <p class="text-sm text-gray-500">
                        กรอกแบบฟอร์มเพื่อส่งโพสต์ไปที่ Discover new partners!
                    </p>

                    <p class="p-4block text-gray-700 text-sm font-bold mb-2 mt-4">
                        Editor
                    </p>
                    <p class="text-sm text-gray-500">
                        投稿の新規作成・編集・公開・削除が可能。
                    </p>
                    <p class="text-sm text-gray-500">
                        จัดการโพสต์ต่างๆ เช่น เพิ่ม แก้ไข เปลี่ยน Status หรือลบเนื้อหา
                    </p>

                    <p class="p-4block text-gray-700 text-sm font-bold mb-2 mt-4">
                        Super admin
                    </p>
                    <p class="text-sm text-gray-500">
                        Editor と同じ役割の他、ユーザーの追加・編集・削除が可能。
                    </p>
                    <p class="text-sm text-gray-500">
                        นอกจากฟังค์ชั่นของ Editor แล้ว ยังสามารถจัดการ Users ได้ เช่น เพิ่ม แก้ไข หรือลบผู้ใช้
                    </p>
                </div>

            </div>
        </div>
        @endsection
        @push('scripts')
            <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
            <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

            @include('admin.include.block')

            <script>
                $(document).ready(function () {
                    var open = document.getElementById("password-open");
                    open.style.display = "none";
                });

                function triggerpassword() {
                    var password = document.getElementById("password");
                    var open = document.getElementById("password-open");
                    var close = document.getElementById("password-close");
                    if (password.type === "password") {
                        open.style.display = "block";
                        close.style.display = "none";
                        password.type = "text";
                    } else {
                        open.style.display = "none";
                        close.style.display = "block";
                        password.type = "password";
                    }
                }
            </script>
    @endpush