@extends('layouts.app_dashboard')
@section('content')
    <div class="container">
        <div class="justify-center">

            <div class="MyPageTemplate">
                <div class="RightContent">
                    <div class="FormBox clr">

                        @if ($errors->any())
                            <div class="mb-3" role="alert">
                                <div class="bg-red-500 text-white font-bold rounded-t px-4 py-2">
                                    Error
                                </div>
                                <div class="border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
                                    @foreach ($errors->all() as $error)
                                        <p>{{ $error }}</p>
                                    @endforeach
                                </div>
                            </div>
                        @endif

                        <form action="{{ route('business.store') }}" method="POST" enctype="multipart/form-data"
                              id="myform">
                            @csrf
                            <div class="md:float-right mt-10">
                                <div class="flex flex-wrap mt-10 md:mt-0 md:flex-row items-center">
                                    <div class="mb-2 md:mb-0">
                                        <a href="{{ route('preview',$company->ref_id) }}"
                                           target="_blank"
                                           class="bg-indigo-500 p-3 rounded-lg hover:bg-indigo-600 text-white">
                                            Preview</a>
                                    </div>
                                    <div class="flex px-4 mb-2 md:mb-0">
                                        <div style="margin-top: -24px;">
                                            <label>status</label>
                                            <select
                                                    name="status"
                                                    class="block appearance-none w-full border-2 bg-white border-blue-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                                    id="grid-state">
                                                <option {{ $company->status == 'confirm' ? 'selected' : '' }} value="confirm">
                                                    Draft
                                                </option>
                                                <option {{ $company->status == 'published' ? 'selected' : '' }} value="published">
                                                    Published
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="flex px-4 mb-2 md:mb-0">
                                        <div style="margin-top: -24px;">
                                            <label>status active</label>
                                            <select
                                                    name="active_status"
                                                    class="block appearance-none w-full border-2 bg-white border-blue-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                                    id="grid-state-active">
                                                <option {{ $company->active_status == '0' ? 'selected' : '' }} value="0">
                                                    Inactive
                                                </option>
                                                <option {{ $company->active_status == '1' ? 'selected' : '' }} value="1">
                                                    Active
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="">
                                        <button class="p-3 rounded-lg hover:bg-indigo-600 text-white" type="submit">
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="flex items-center mt-4 ">
                                <p class="text-xl">
                                    ID : {{ $company->ref_id }}
                                </p>
                                <p class="pl-4 text-sm ">
                       <span>Permalink: <a target="_blank" class="text-uppercase text-blue-500"
                                           href="{{ route('seller-info-detail',$company->ref_id) }}">
                               {{ route('seller-info-detail',$company->ref_id) }}</a>
                       </span>
                                </p>
                            </div>

                            <input type="hidden" name="id" value="{{$id}}">
                            <input type="hidden" name="mode" value="edit">
                            <input type="hidden" name="admin_mode" value="1">
                            <div class="FormList">


                            @include('sessions.edit.company-information-edit')

                            {{--Company Information--}}
                            @include('sessions.edit.service-information-edit')

                            {{--Support menu--}}
                            @include('sessions.edit.support-menu-edit')

                            {{--Category tag--}}
                            @include('sessions.edit.category-tag-edit')

                            {{--policy tag--}}
                            @include('sessions.edit.policy')

                            {{--Feature Banner--}}
                            @include('sessions.edit.feature_banner-edit')

                            @if(!empty($company->topic))
                                @include('sessions.edit.event-edit')
                            @endif

                            @include('sessions.edit.privacy-setting')

                            {{-- <div class="BTNList">
                                 <button type="submit">SUBMIT <i class="fa fa-send-o"></i></button>
                             </div>--}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('layouts.validate-edit')
