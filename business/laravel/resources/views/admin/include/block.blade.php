<script>
    let readonly = false;
    @if((auth()->guard('web')->check()))
    @if(auth()->guard('web')->user()->role->name == 'Viewer')
    $('.delete-img').each(function () {
        $(this).hide();
    });
    $("form :input").prop("disabled", true);
    $("form select").prop("disabled", true);
      readonly = true;
    @endif
    @else
      readonly = false;
    @endif
</script>