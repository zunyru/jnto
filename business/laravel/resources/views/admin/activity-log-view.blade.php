@extends('layouts.app_dashboard')
@push('css')

@endpush
@section('content')
    <div class="container">
        <div class="justify-center">
            <h1 class="text-2xl my-2 text-2xl pt-4">
                {{ $log->log_name }}
            </h1>
            <h2 class="text-2xl my-2 text-gray-800">
                Description : {{ $log->description }}
            </h2>
            <h2 class="text-2xl my-2 text-gray-800">
                User: ID : {{ !empty($log->subject_id) ? $log->subject_id : $log->causer_id }}
            </h2>
            <h2 class="text-2xl my-2 text-gray-800">
                Time update :
                {{ $log->updated_at->format('d-m-Y H:i:s') }}
            </h2>
            <div class="w-full mt-4">
                <h2 class="text-2xl text-gray-800">
                    Log detail :
                </h2>
                <div class="mb-4 pl-4">

                    @foreach($log->properties as $key => $properties)
                        <div class="text-xl mt-4">
                            @if($key == 'old')
                                Before update
                            @elseif($key == 'attributes')
                                Last update
                            @else
                                {{ Str::title($key) }}
                            @endif
                        </div>
                        <div class="bg-gray-100 p-8">
                            @if(!empty($properties['password']))
                                {{ '{ "password : ****************" }'}}
                            @else
                                @json($properties)
                            @endif

                        </div>
                    @endforeach

                </div>
            </div>
        </div>
        @endsection
        @push('scripts')
            <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
            <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    @endpush