@extends('layouts.app_dashboard')
@section('content')
    <div class="container">
        <div class="justify-center">
            <div class="pt-4">
                <div class="w-full shadow bg-white rounded p-4 mb-10">
                    <h1>Contact</h1>
                    <div class="w-full">
                        <form action="{{ route('setting-contact.update') }}" method="POST"
                              class=""
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="md:float-right mb-4 pr-4 absolute" style="right: 38px;top: 100px;">
                                <div class="flex flex-wrap mt-10 md:mt-0 md:flex-row items-center">
                                    <div class="">
                                        <button class="bg-teal-500 p-3 rounded-lg hover:bg-indigo-600 text-white w-50"
                                                type="submit">
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="value_jp">
                                    Contact JP
                                </label>
                                <textarea id="detail_jp"
                                          name="value_jp">{!! !empty($setting_value_jp->value) ? nl2br($setting_value_jp->value) : '' !!}</textarea>
                            </div>

                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="detail_th">
                                    Contact TH
                                </label>
                                <textarea id="detail_th"
                                          name="value_th">{!! !empty($setting_value_th->value) ? nl2br($setting_value_th->value) : '' !!}</textarea>
                            </div>

                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="detail_th">
                                    Email
                                </label>
                                <input type="text"
                                       class="form-input mt-1 block w-full"
                                       name="email"
                                       value="{{ !empty($setting_email->value) ? $setting_email->value : '' }}">
                            </div>

                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="signature">
                                    Signature (Email template)
                                </label>
                                <textarea id="signature"
                                          name="signature">{!! !empty($setting_signature->value) ? nl2br($setting_signature->value) : '' !!}</textarea>
                            </div>

                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="detail_th">
                                    Privacy Policy URL [TH]
                                </label>
                                <input type="text"
                                       class="form-input mt-1 block w-full"
                                       name="privacy_th"
                                       value="{{ !empty($setting_privacy_th->value) ? $setting_privacy_th->value : '' }}">
                            </div>

                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="detail_th">
                                 Privacy Policy URL [JP]
                                </label>
                                <input type="text"
                                       class="form-input mt-1 block w-full"
                                       name="privacy_jp"
                                       value="{{ !empty($setting_privacy_jp->value) ? $setting_privacy_jp->value : '' }}">
                            </div>

                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="detail_th">
                                    Cookie Policy URL
                                </label>
                                <input type="text"
                                       class="form-input mt-1 block w-full"
                                       name="policy"
                                       value="{{ !empty($setting_cookie->value) ? $setting_cookie->value : '' }}">
                            </div>

                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="detail_th">
                                    Copyrights & Disclaimer
                                </label>
                                <textarea id="copyrights"
                                          name="copyrights">{!! !empty($setting_copyrights->value) ? nl2br($setting_copyrights->value) : '' !!}</textarea>
                            </div>


                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="detail_th">
                                    Footer Text
                                </label>
                                <textarea id="footer_text"
                                          name="footer_text">{!! !empty($setting_footer_text->value) ? nl2br($setting_footer_text->value) : '' !!}</textarea>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    @include('admin.include.block')

    <script>
        tinymce.init({
            readonly: readonly ?? false,
            selector: 'textarea#detail_jp',
            menubar: false,
            width: '100%',
            height: 500,
            forced_root_block: false,
            force_br_newlines: true,
            force_p_newlines: false,
            //toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | showcomments addcomment',
            toolbar: 'bold italic  | link | insertfile |',
            //plugins: 'print preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists checklist wordcount tinymcespellchecker a11ychecker imagetools textpattern noneditable help formatpainter permanentpen pageembed charmap tinycomments mentions quickbars linkchecker emoticons advtable',
            plugins: 'nonbreaking link',
            convert_urls: false,
            file_picker_types: 'file',
            setup: function (editor) {
                editor.ui.registry.addButton('customBr', {
                    text: 'Enter',
                    onAction: function () {
                        editor.insertContent('<br/> ');
                    },
                });
            }
        });

        tinymce.init({
            readonly: readonly ?? false,
            selector: 'textarea#detail_th',
            menubar: false,
            width: '100%',
            height: 500,
            forced_root_block: false,
            force_br_newlines: true,
            force_p_newlines: false,
            //toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | showcomments addcomment',
            toolbar: 'bold italic  | link | insertfile |',
            //plugins: 'print preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists checklist wordcount tinymcespellchecker a11ychecker imagetools textpattern noneditable help formatpainter permanentpen pageembed charmap tinycomments mentions quickbars linkchecker emoticons advtable',
            plugins: 'nonbreaking link',
            convert_urls: false,
            file_picker_types: 'file',
            setup: function (editor) {
                editor.ui.registry.addButton('customBr', {
                    text: 'Enter',
                    onAction: function () {
                        editor.insertContent('<br/> ');
                    },
                });
            }
        });

        tinymce.init({
            readonly: readonly ?? false,
            selector: 'textarea#signature',
            menubar: false,
            width: '100%',
            height: 500,
            forced_root_block: false,
            force_br_newlines: true,
            force_p_newlines: false,
            //toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | showcomments addcomment',
            toolbar: 'bold italic  | link | insertfile |',
            //plugins: 'print preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists checklist wordcount tinymcespellchecker a11ychecker imagetools textpattern noneditable help formatpainter permanentpen pageembed charmap tinycomments mentions quickbars linkchecker emoticons advtable',
            plugins: 'nonbreaking link',
            convert_urls: false,
            file_picker_types: 'file',
            setup: function (editor) {
                editor.ui.registry.addButton('customBr', {
                    text: 'Enter',
                    onAction: function () {
                        editor.insertContent('<br/> ');
                    },
                });
            }
        });

        tinymce.init({
            readonly: readonly ?? false,
            selector: 'textarea#copyrights',
            menubar: false,
            width: '100%',
            height: 500,
            forced_root_block: false,
            force_br_newlines: true,
            force_p_newlines: false,
            //toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | showcomments addcomment',
            toolbar: 'bold italic  | link | insertfile |',
            //plugins: 'print preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists checklist wordcount tinymcespellchecker a11ychecker imagetools textpattern noneditable help formatpainter permanentpen pageembed charmap tinycomments mentions quickbars linkchecker emoticons advtable',
            plugins: 'nonbreaking link',
            convert_urls: false,
            file_picker_types: 'file',
            setup: function (editor) {
                editor.ui.registry.addButton('customBr', {
                    text: 'Enter',
                    onAction: function () {
                        editor.insertContent('<br/> ');
                    },
                });
            }
        });

        tinymce.init({
            readonly: readonly ?? false,
            selector: 'textarea#footer_text',
            menubar: false,
            width: '100%',
            height: 500,
            forced_root_block: false,
            force_br_newlines: true,
            force_p_newlines: false,
            //toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | showcomments addcomment',
            toolbar: 'bold italic  | link | insertfile |',
            //plugins: 'print preview powerpaste casechange importcss tinydrive searchreplace autolink autosave save directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists checklist wordcount tinymcespellchecker a11ychecker imagetools textpattern noneditable help formatpainter permanentpen pageembed charmap tinycomments mentions quickbars linkchecker emoticons advtable',
            plugins: 'nonbreaking link',
            convert_urls: false,
            file_picker_types: 'file',
            setup: function (editor) {
                editor.ui.registry.addButton('customBr', {
                    text: 'Enter',
                    onAction: function () {
                        editor.insertContent('<br/> ');
                    },
                });
            }
        });
    </script>

@endpush
