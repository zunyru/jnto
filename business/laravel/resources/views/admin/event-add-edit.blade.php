@extends('layouts.app_dashboard')
@push('css')
    <style>
        .FileUploadCSS {
            width: 130px;
        }
    </style>
@endpush
@section('content')
    <div class="container">
        <div class="justify-center">
            <h1 class="text-2xl p-4">{{ Route::getCurrentRoute()->getActionMethod() == 'edit' ? 'Edit Event' : 'New Create Event ' }}
            </h1>
            @if(Route::getCurrentRoute()->getActionMethod() == 'edit')
                @if(@$event->template == 'page')
                    <span class="">Permalink :
                        @if(@$event->status == 'draft')
                            <a class="text-uppercase text-blue-500"
                               target="_blank"
                               href="{{ route('event.show.preview',$event->ref_id) }}"
                            >
                                 {{ route('event-detail',$event->ref_id) }}
                            </a>
                        @else
                            <a class="text-uppercase text-blue-500"
                               target="_blank"
                               href="{{ route('event-detail',$event->ref_id) }}"
                            >
                                 {{ route('event-detail',$event->ref_id) }}
                            </a>
                        @endif
                    </span>
                @endif
            @endif

            <div class="w-full">
                <form
                        action="{{ Route::getCurrentRoute()->getActionMethod() == 'edit'
                                    ? route('event.update',$event->ref_id)
                                    : route('event.store') }}"
                        method="post"
                        enctype="multipart/form-data"
                        id="newsForm"
                        class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
                    @csrf
                    @if(Route::getCurrentRoute()->getActionMethod() == 'edit')
                        @method('put')
                    @else
                        @method('post')
                    @endif
                    <div class="md:float-right mb-4 pr-4 absolute" style="right: 38px;top: 100px;">
                        <div class="flex flex-wrap mt-10 md:mt-0 md:flex-row items-center">
                            @if(Route::getCurrentRoute()->getActionMethod() == 'edit')
                                <div class="mb-2 md:mb-0">
                                    @if(@$event->template == 'page')
                                        <a href="{{ route('event.show.preview',$event->ref_id) }}" target="_blank"
                                           class="bg-indigo-500 p-3 rounded-lg hover:bg-indigo-600 text-white">
                                            Preview</a>
                                    @else
                                        <a href="{{ $event->link }}" target="_blank"
                                           class="bg-indigo-500 p-3 rounded-lg hover:bg-indigo-600 text-white">
                                            Preview</a>
                                    @endif
                                </div>
                            @endif
                            <div class="flex px-4 mb-2 md:mb-0">
                                <div style="margin-top: -24px;">
                                    <label>status</label>
                                    <select name="status"
                                            class="block appearance-none w-full border-2 bg-white border-blue-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                            id="grid-state">
                                        <option {{ @$event->status == 'draft' ? 'selected' : 'selected' }} value="draft">
                                            Draft
                                        </option>
                                        <option {{ @$event->status == 'published' ? 'selected' : '' }} value="published">
                                            Published
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="">
                                <button class="bg-teal-500 p-3 rounded-lg hover:bg-indigo-600 text-white w-50"
                                        type="submit">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="w-full">

                        <div class="mb-4">
                            <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                Template
                            </label>
                            <label class="inline-flex items-center">
                                <input type="radio" class="form-radio"
                                       {{ @$event->template == 'page' ? 'checked' : 'checked' }}
                                       name="template"
                                       value="page">
                                <span class="ml-2">Page</span>
                            </label>
                            <label class="inline-flex items-center ml-6">
                                <input type="radio" class="form-radio"
                                       name="template"
                                       {{ @$event->template == 'link-only' ? 'checked' : '' }}
                                       value="link-only">
                                <span class="ml-2">Link only</span>
                            </label>
                        </div>

                        <div class="mb-4">
                            <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                Title
                            </label>
                            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                   id="title"
                                   type="text"
                                   name="title"
                                   value="{{ !empty($event->title) ? $event->title : '' }}"
                                   placeholder="Title">
                        </div>

                        <div class="flex flex-row mb-4">
                            <label>
                                <p>Start Date</p>
                                <input type="text"
                                       class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                       name="event_date_start"
                                       id="from"
                                       value="{{ !empty(@$event) ? is_null($event->event_date_start) ? '' : $event->event_date_start->format('Y-m-d') : '' }}"
                                       placeholder="2021-08-00">
                            </label>

                            <label class="ml-4">
                                <p>End Date</p>
                                <input type="text"
                                       class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                       name="event_date_end"
                                       id="to"
                                       value="{{ !empty(@$event) ? is_null($event->event_date_end) ? '' : $event->event_date_end->format('Y-m-d') : '' }}"
                                       placeholder="2021-08-00">
                            </label>
                        </div>
                        <div class="page">
                            <div>
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    Photos
                                </label>
                                <span class="text-sm text-gray-500">画像サイズの目安：W1035×H465ピクセル／解像度200dpi／1MB以下／PNGまたはJPG<br>ขนาดภาพที่แนะนำ W1035×H465 pixels / ความละเอียด 200 dpi / ความจุต่ำกว่า 1MB / PNG หรือ JPG
                                </span>
                            </div>
                            <div class="flex text-center rounded-xl border-gray-300 border p-3">
                                <div class="flex-1 p-2">
                                    <div class="mb-4">
                                        <div class="flex flex-col items-center text-center">
                                            @if(!empty($event->photo1))
                                                <img src="{{ asset($event->photo1) }}"
                                                     id="photo1_preview"
                                                     style="width: 100%; height: auto;">
                                            @else
                                                <img src=""
                                                     id="photo1_preview"
                                                     style="width: 100%; height: auto;">
                                            @endif
                                            <br>
                                            <label for="photo1"></label>
                                            <label for="photo1"
                                                   class="FileUploadCSS">
                                                Choose a file
                                                <input type="file"
                                                       name="photo1"
                                                       accept="image/*"
                                                       id="photo1"/>
                                            </label>
                                            <br>
                                            <label id="photo1-error" class="error" for="photo1" style="display: none;">
                                            </label>
                                            @if(!empty($event->photo1))
                                                <br>
                                                <a href="javascript:void(0);"
                                                   data-filters="photo1"
                                                   data-db="news_jntos"
                                                   data-where="id"
                                                   data-id="{{ $event->ref_id }}"
                                                   class="w-1/2 delete-img bg-red-600 text-white text-center p-2 rounded-md">
                                                    <svg class="stroke-2 text-white inline-block h-4 w-4"
                                                         xmlns="http://www.w3.org/2000/svg" fill="none"
                                                         viewBox="0 0 24 24"
                                                         stroke="currentColor">
                                                        <path strokeLinecap="round" strokeLinejoin="round"
                                                              strokeWidth={2}
                                                              d="M6 18L18 6M6 6l12 12"/>
                                                    </svg>
                                                    Delete
                                                </a>
                                            @endif
                                        </div>

                                    </div>
                                </div>
                                <div class="flex-1 p-2">
                                    <div class="mb-4">

                                        <div class="flex flex-col items-center text-center">
                                            @if(!empty($event->photo2))
                                                <img src="{{ asset($event->photo2) }}"
                                                     id="photo2_preview"
                                                     style="width: 100%; height: auto;">
                                            @else
                                                <img src=""
                                                     id="photo2_preview"
                                                     style="width: 100%; height: auto;">
                                            @endif
                                            <br>
                                            <label for="photo2"></label>
                                            <label for="photo2"
                                                   class="FileUploadCSS">
                                                Choose a file
                                                <input type="file"
                                                       name="photo2"
                                                       accept="image/*"
                                                       id="photo2"/>
                                            </label>
                                            <br>
                                            <label id="photo2-error" class="error" for="photo2" style="display: none;">
                                            </label>
                                            @if(!empty($event->photo2))
                                                <br>
                                                <a href="javascript:void(0);"
                                                   data-filters="photo2"
                                                   data-db="news_jntos"
                                                   data-where="id"
                                                   data-id="{{ $event->ref_id }}"
                                                   class="w-1/2 delete-img bg-red-600 text-white text-center p-2 rounded-md">
                                                    <svg class="stroke-2 text-white inline-block h-4 w-4"
                                                         xmlns="http://www.w3.org/2000/svg" fill="none"
                                                         viewBox="0 0 24 24"
                                                         stroke="currentColor">
                                                        <path strokeLinecap="round" strokeLinejoin="round"
                                                              strokeWidth={2}
                                                              d="M6 18L18 6M6 6l12 12"/>
                                                    </svg>
                                                    Delete
                                                </a>
                                            @endif
                                        </div>

                                    </div>
                                </div>
                                <div class="flex-1 p-2">
                                    <div class="mb-4">

                                        <div class="flex flex-col items-center text-center">
                                            @if(!empty($event->photo3))
                                                <img src="{{ asset($event->photo3) }}"
                                                     id="photo3_preview"
                                                     style="width: 100%; height: auto;">
                                            @else
                                                <img src=""
                                                     id="photo3_preview"
                                                     style="width: 100%; height: auto;">
                                            @endif
                                            <br>
                                            <label for="photo3"></label>
                                            <label for="photo3"
                                                   class="FileUploadCSS">
                                                Choose a file
                                                <input type="file"
                                                       name="photo3"
                                                       accept="image/*"
                                                       id="photo3"/>
                                            </label>
                                            <br>
                                            <label id="photo3-error" class="error" for="photo3" style="display: none;">
                                            </label>
                                            @if(!empty($event->photo3))
                                                <br>
                                                <a href="javascript:void(0);"
                                                   data-filters="photo3"
                                                   data-db="news_jntos"
                                                   data-where="id"
                                                   data-id="{{ $event->ref_id }}"
                                                   class="w-1/2 delete-img bg-red-600 text-white text-center p-2 rounded-md">
                                                    <svg class="stroke-2 text-white inline-block h-4 w-4"
                                                         xmlns="http://www.w3.org/2000/svg" fill="none"
                                                         viewBox="0 0 24 24"
                                                         stroke="currentColor">
                                                        <path strokeLinecap="round" strokeLinejoin="round"
                                                              strokeWidth={2}
                                                              d="M6 18L18 6M6 6l12 12"/>
                                                    </svg>
                                                    Delete
                                                </a>
                                            @endif
                                        </div>

                                    </div>
                                </div>
                                <div class="flex-1 p-2">
                                    <div class="mb-4">
                                        <div class="flex flex-col items-center text-center">
                                            @if(!empty($event->photo4))
                                                <img src="{{ asset($event->photo4) }}"
                                                     id="photo4_preview"
                                                     style="width: 100%; height: auto;">
                                            @else
                                                <img src=""
                                                     id="photo4_preview"
                                                     style="width: 100%; height: auto;">
                                            @endif
                                            <br>
                                            <label for="photo4"></label>
                                            <label for="photo4"
                                                   class="FileUploadCSS">
                                                Choose a file
                                                <input type="file"
                                                       name="photo4"
                                                       accept="image/*"
                                                       id="photo4"/>
                                            </label>
                                            <br>
                                            <label id="photo4-error" class="error" for="photo4" style="display: none;">
                                            </label>
                                            @if(!empty($event->photo4))
                                                <br>
                                                <a href="javascript:void(0);"
                                                   data-filters="photo4"
                                                   data-db="news_jntos"
                                                   data-where="id"
                                                   data-id="{{ $event->ref_id }}"
                                                   class="w-1/2 delete-img bg-red-600 text-white text-center p-2 rounded-md">
                                                    <svg class="stroke-2 text-white inline-block h-4 w-4"
                                                         xmlns="http://www.w3.org/2000/svg" fill="none"
                                                         viewBox="0 0 24 24"
                                                         stroke="currentColor">
                                                        <path strokeLinecap="round" strokeLinejoin="round"
                                                              strokeWidth={2}
                                                              d="M6 18L18 6M6 6l12 12"/>
                                                    </svg>
                                                    Delete
                                                </a>
                                            @endif
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="mb-4 mt-10">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    Detail
                                </label>
                                <textarea id="detail"
                                          name="detail">{!! !empty($event->detail) ? nl2br(preg_replace('/<p[^>]*>/', '', $event->detail)) : '' !!}</textarea>
                            </div>

                            <div class="mb-4 mt-10 rounded-xl border-gray-300 border p-3">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    Upload file (pdf, excel , word)
                                </label>
                                <p class="text-sm text-gray-500">PDF, Excel, Word、3MB以下 | ไฟล์ PDF, Excel, Word
                                    ขนาดต่ำกว่า 3MB</p>
                                <label for="photo4"></label>
                                {{--<label for="pdf1"
                                       class="FileUploadCSS">
                                    Choose a file--}}
                                <input type="file"
                                       name="pdf1"
                                       id="pdf1"/>
                                {{--</label>--}}
                                <br>
                                <label id="pdf1-error" class="error" for="pdf1" style="display: none;">
                                </label>
                                @if(!empty($event->pdf1))
                                    <a href="{{ asset($event->pdf1) }}"
                                       target="_blank">
                                        <img class="mt-3" src="{{ getIconFile($event->pdf1) }}"
                                             style="width: auto; max-width: 10%;">
                                    </a>
                                    <p>{{ nameFile($event->pdf1) }}</p>
                                    <br>
                                    <a href="javascript:void(0);"
                                       data-filters="pdf1"
                                       data-db="news_jntos"
                                       data-where="id"
                                       data-id="{{ $event->ref_id }}"
                                       class="w-1/2 delete-img bg-red-600 text-white text-center p-2 rounded-md">
                                        <svg class="stroke-2 text-white inline-block h-4 w-4"
                                             xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                             stroke="currentColor">
                                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2}
                                                  d="M6 18L18 6M6 6l12 12"/>
                                        </svg>
                                        Delete
                                    </a>
                                @endif
                                <label class="block text-gray-600 text-sm font-bold mt-4" for="username">
                                    File description
                                </label>
                                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                       type="text"
                                       maxlength="100"
                                       name="file_description1"
                                       value="{{ !empty($event->file_description1) ? $event->file_description1 : '' }}"
                                       placeholder="100文字以内|สูงสุด 100 ตัวอักษร">

                            </div>
                            <div class="mb-4 mt-4 rounded-xl border-gray-300 border p-3">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    Upload file (pdf, excel , word)
                                </label>
                                <p class="text-sm text-gray-500">PDF, Excel, Word、3MB以下 | ไฟล์ PDF, Excel, Word
                                    ขนาดต่ำกว่า 3MB</p>
                                <label for="photo4"></label>
                                {{--<label for="pdf2"
                                       class="FileUploadCSS">
                                    Choose a file--}}
                                <input type="file"
                                       name="pdf2"
                                       id="pdf2"/>
                                {{--</label>--}}
                                <br>
                                <label id="pdf2-error" class="error" for="pdf2" style="display: none;">
                                </label>
                                @if(!empty($event->pdf2))
                                    <a href="{{ asset($event->pdf2) }}"
                                       target="_blank">
                                        <img class="mt-3" src="{{ getIconFile($event->pdf2) }}"
                                             style="width: auto; max-width: 10%;">
                                    </a>
                                    <p>{{ nameFile($event->pdf2) }}</p>
                                    <br>
                                    <a href="javascript:void(0);"
                                       data-filters="pdf2"
                                       data-db="news_jntos"
                                       data-where="id"
                                       data-id="{{ $event->ref_id }}"
                                       class="w-1/2 delete-img bg-red-600 text-white text-center p-2 rounded-md">
                                        <svg class="stroke-2 text-white inline-block h-4 w-4"
                                             xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                             stroke="currentColor">
                                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2}
                                                  d="M6 18L18 6M6 6l12 12"/>
                                        </svg>
                                        Delete
                                    </a>
                                @endif
                                <label class="block text-gray-600 text-sm font-bold mt-4" for="username">
                                    File description
                                </label>
                                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                       type="text"
                                       maxlength="100"
                                       name="file_description2"
                                       value="{{ !empty($event->file_description2) ? $event->file_description2 : '' }}"
                                       placeholder="100文字以内|สูงสุด 100 ตัวอักษร">

                            </div>
                            <div class="mb-4 mt-4 rounded-xl border-gray-300 border p-3">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    Upload file (pdf, excel , word)
                                </label>
                                <p class="text-sm text-gray-500">PDF, Excel, Word、3MB以下 | ไฟล์ PDF, Excel, Word
                                    ขนาดต่ำกว่า 3MB</p>
                                {{--<label for="pdf3"
                                       class="FileUploadCSS">
                                    Choose a file--}}
                                <input type="file"
                                       name="pdf3"
                                       id="pdf3"/>
                                {{--</label>--}}
                                <br>
                                <label id="pdf3-error" class="error" for="pdf3" style="display: none;">
                                </label>
                                @if(!empty($event->pdf3))
                                    <a href="{{ asset($event->pdf3) }}"
                                       target="_blank">
                                        <img class="mt-3" src="{{ getIconFile($event->pdf3) }}"
                                             style="width: auto; max-width: 10%;">
                                    </a>
                                    <p>{{ nameFile($event->pdf3) }}</p>
                                    <br>
                                    <a href="javascript:void(0);"
                                       data-filters="pdf3"
                                       data-db="news_jntos"
                                       data-where="id"
                                       data-id="{{ $event->ref_id }}"
                                       class="w-1/2 delete-img bg-red-600 text-white text-center p-2 rounded-md">
                                        <svg class="stroke-2 text-white inline-block h-4 w-4"
                                             xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                             stroke="currentColor">
                                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2}
                                                  d="M6 18L18 6M6 6l12 12"/>
                                        </svg>
                                        Delete
                                    </a>
                                @endif
                                <label class="block text-gray-600 text-sm font-bold mt-4" for="username">
                                    File description
                                </label>
                                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                       type="text"
                                       maxlength="100"
                                       name="file_description3"
                                       value="{{ !empty($event->file_description3) ? $event->file_description3 : '' }}"
                                       placeholder="100文字以内|สูงสุด 100 ตัวอักษร">

                            </div>
                        </div>

                        <div class="link-only">
                            <div class="mb-4">
                                <label class="block text-gray-700 text-sm font-bold mb-2" for="username">
                                    Link
                                </label>
                                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                       id="link"
                                       type="text"
                                       name="link"
                                       value="{{ !empty($event->link) ? $event->link : '' }}"
                                       placeholder="link">
                            </div>
                        </div>

                    </div>
                </form>

            </div>
        </div>
        @endsection
        @push('scripts')
            <script src="{{ asset('node_modules/tinymce/tinymce.js') }}"></script>
            <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
            <script src="{{ asset('js/validate-event.js') }}"></script>
            <link rel="stylesheet" href="{{ asset('assets/jquery-ui/jquery-ui.min.css') }}">
            <script src="{{ asset('assets/jquery-ui/jquery-ui.min.js') }}"></script>

            @include('admin.include.block')

            <script>
                $(document).ready(function () {
                    $('.page').hide();
                    if ($("input[type='radio']:checked").val() == 'page') {
                        $('.page').show();
                    }
                    $('.link-only').hide();
                    if ($("input[type='radio']:checked").val() == 'link-only') {
                        $('.link-only').show();
                    }

                    $('input[type=radio][name=template]').change(function () {
                        if (this.value == 'page') {
                            $('.page').show();
                            $('.link-only').hide();
                        } else if (this.value == 'link-only') {
                            $('.link-only').show();
                            $('.page').hide();
                        }
                    });

                });

                tinymce.init({
                    readonly: readonly ?? false,
                    selector: 'textarea#detail',
                    menubar: false,
                    width: '100%',
                    height: 500,
                    forced_root_block: false,
                    force_br_newlines: true,
                    force_p_newlines: false,
                    toolbar: 'bold customBr link underline',
                    plugins: 'nonbreaking link',
                    convert_urls: false,
                    setup: function (editor) {
                        editor.ui.registry.addButton('customBr', {
                            text: 'Enter',
                            onAction: function () {
                                editor.insertContent('<br/> ');
                            },
                        });
                    }
                });

                $('.delete-img').click(function () {
                    var data_value = $(this).attr('data-filters');
                    var data_db = $(this).attr('data-db');
                    var data_id = $(this).attr('data-id');
                    var data_where = $(this).attr('data-where');
                    Swal.fire({
                        title: 'Are you sure to delete ?',
                        text: "You won't be able to revert this!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                        if (result.value) {
                            $.ajax({
                                method: "POST",
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                data: {
                                    id: data_id,
                                    field: data_value,
                                    where: data_where,
                                    db: data_db
                                },
                                url: `{{ route('file.delete') }}`,
                            }).done(function (msg) {
                                toastr.success(msg.message);
                                setTimeout(function () {
                                    location.reload();
                                }, 1000);

                            }).fail(function (msg) {
                                toastr.error(msg.message)
                            });
                        }
                    });
                });

                $("#photo1").change(function () {
                    readURL(this);
                });
                $("#photo2").change(function () {
                    readURL(this);
                });
                $("#photo3").change(function () {
                    readURL(this);
                });
                $("#photo4").change(function () {
                    readURL(this);
                });

                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            var preview = $(input).attr('id') + '_preview';
                            $('#' + preview).attr('src', e.target.result);
                        }
                        reader.readAsDataURL(input.files[0]);
                    } else {
                        $('#previewHolder').attr('src', '');
                    }
                }

                $(function () {

                    $("#from")
                        .datepicker({
                            dateFormat: "yy-mm-dd",
                            changeMonth: true,
                            changeYear: true,
                            monthNamesShort: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                        })
                        .on("change", function () {
                            $("#to").datepicker("option", "minDate", getDate(this));
                        });

                    $("#to").datepicker({
                        dateFormat: "yy-mm-dd",
                        changeMonth: true,
                        changeYear: true,
                        monthNamesShort: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                    }).on("change", function () {
                        $("#from").datepicker("option", "maxDate", getDate(this));
                    });

                    function getDate(element) {
                        var date;

                        var dateFormat = "yy-mm-dd";
                        try {
                            date = $.datepicker.parseDate(dateFormat, element.value);
                        } catch (error) {
                            date = null;
                        }
                        console.log(date);
                        return date;
                    }
                });
            </script>
    @endpush