@extends('layouts.app_dashboard')
@prepend('css')
    <style>
        /*Form fields*/
        .dataTables_wrapper select,
        .dataTables_wrapper .dataTables_filter input {
            color: #4a5568; /*text-gray-700*/
            padding-left: 1rem; /*pl-4*/
            padding-right: 1rem; /*pl-4*/
            padding-top: .5rem; /*pl-2*/
            padding-bottom: .5rem; /*pl-2*/
            line-height: 1.25; /*leading-tight*/
            border-width: 2px; /*border-2*/
            border-radius: .25rem;
            border-color: #edf2f7; /*border-gray-200*/
            background-color: #edf2f7; /*bg-gray-200*/
        }

        /*Row Hover*/
        table.dataTable.hover tbody tr:hover, table.dataTable.display tbody tr:hover {
            background-color: #ebf4ff; /*bg-indigo-100*/
        }

        /*Pagination Buttons*/
        .dataTables_wrapper .dataTables_paginate .paginate_button {
            font-weight: 700; /*font-bold*/
            border-radius: .25rem; /*rounded*/
            border: 1px solid transparent; /*border border-transparent*/
        }

        /*Pagination Buttons - Current selected */
        .dataTables_wrapper .dataTables_paginate .paginate_button.current {
            color: #fff !important; /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06); /*shadow*/
            font-weight: 700; /*font-bold*/
            border-radius: .25rem; /*rounded*/
            background: #667eea !important; /*bg-indigo-500*/
            border: 1px solid transparent; /*border border-transparent*/
        }

        /*Pagination Buttons - Hover */
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            color: #fff !important; /*text-white*/
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06); /*shadow*/
            font-weight: 700; /*font-bold*/
            border-radius: .25rem; /*rounded*/
            background: #667eea !important; /*bg-indigo-500*/
            border: 1px solid transparent; /*border border-transparent*/
        }

        /*Add padding to bottom border */
        table.dataTable.no-footer {
            border-bottom: 1px solid #e2e8f0; /*border-b-1 border-gray-300*/
            margin-top: 0.75em;
            margin-bottom: 0.75em;
        }

        /*Change colour of responsive icon*/
        table.dataTable.dtr-inline.collapsed > tbody > tr > td:first-child:before, table.dataTable.dtr-inline.collapsed > tbody > tr > th:first-child:before {
            background-color: #667eea !important; /*bg-indigo-500*/
        }

        .container .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
            color: #fff !important;
        }

        th#edit-col {
            width: 100px !important;
        }

        th#delete-col {
            width: 120px !important;
        }

    </style>
@endprepend
@section('content')
    <div class="container">
        <div class="justify-center">
            <h1>Discover new partners!</h1>
            <div class="pt-4">
                <div class="w-full shadow bg-white rounded p-4 mb-10">
                    <h1 class="text-2xl text-left p-2">Keyword Search</h1>
                    <div class="md:flex md:flex-row w-full p-2 pb-4">
                        <div class="md:flex-auto w-full md:w-2/3 md:pr-2">
                            <input
                                    id="input_search"
                                    type="text"
                                    class="form-input mt-1 block w-full"
                                    placeholder="ID / Company name (EN) / Login E-mail ">
                        </div>
                        <div class="md:flex-auto w-full md:w-1/3 md:pr-2">
                            <button
                                    id="search"
                                    type="button"
                                    class="bg-blue-500 active:bg-blue-700 py-3 px-4 rounded text-white shadow">Search
                            </button>
                        </div>
                    </div>
                </div>

                <div class="w-full shadow bg-white rounded p-4 mb-10">
                    <h1 class="text-2xl text-left p-2">Filtering by</h1>
                    <div class="md:flex md:flex-row w-full p-2 pb-4">
                        <div class="md:flex-auto w-full md:w-1/4 md:pr-2">
                            <label class="block">
                                <span class="text-gray-900">JNTO partner or not</span>
                                <select
                                        id="is_partner"
                                        name="is_partner"
                                        class="form-select block w-full mt-1">
                                    <option value="">--select--</option>
                                    <option value="1">Partner</option>
                                    <option value="0">Not Partner</option>
                                </select>
                            </label>
                        </div>
                        <div class="md:flex-auto w-full md:w-1/4 md:px-2">
                            <span class="text-gray-900">Business Type </span>
                            <label class="block mt-1">
                                <select
                                        id="business_types"
                                        name="business_types"
                                        {{--multiple="multiple"--}}
                                        class="form-select block w-full mt-1">
                                    <option value="">--select--</option>
                                    @foreach($business_types as $item)
                                        <option value="{{ $item->id }}">{{ $item->name_en }}</option>
                                    @endforeach
                                </select>
                            </label>
                        </div>
                        <div class="md:flex-auto w-full md:w-1/4 md:px-2">
                            <label class="block">
                                <span class="text-gray-900">Branch in Thailand</span>
                                <select
                                        id="is_branch_thai"
                                        name="is_branch_thai"
                                        class="form-select block w-full mt-1">
                                    <option value="">--select--</option>
                                    <option value="1">Yes</option>
                                    <option value="2">No</option>
                                </select>
                            </label>
                        </div>
                        <div class="w-full md:flex-auto  md:w-1/4 md:px-2">
                            <label class="block">
                                <span class="text-gray-900">Support menu</span>
                                <select
                                        id="support_menus"
                                        name="support_menus"
                                        class="form-select block w-full mt-1">
                                    <option value="">--select--</option>
                                    @foreach($support_menus as $item)
                                        <option value="{{ $item->id }}">{{ $item->name_en }}</option>
                                    @endforeach
                                </select>
                            </label>
                        </div>
                    </div>
                    {{----}}
                    <div class="md:flex md:flex-row w-full p-2 pb-6">
                        <div class="md:flex-auto w-full md:w-1/4 md:pr-2">
                            <label class="block">
                                <span class="text-gray-900">Province</span>
                                <select
                                        id="province"
                                        name="province"
                                        class="form-select block w-full mt-1">
                                    <option value="">--select--</option>
                                    <option value="all">All over Japan</option>
                                    @foreach($provinces as $item)
                                        <option value="{{ $item->id }}">{{ $item->name_en }}</option>
                                    @endforeach
                                </select>
                            </label>
                        </div>
                        <div class="md:flex-auto w-full md:w-1/4 md:pr-2">
                            <label class="block">
                                <span class="text-gray-900">Season</span>
                                <select
                                        id="season"
                                        name="season"
                                        class="form-select block w-full mt-1">
                                    <option value="">--select--</option>
                                    @foreach($seasons as $item)
                                        <option value="{{ $item->id }}">{{ $item->name_en }}</option>
                                    @endforeach
                                </select>
                            </label>
                        </div>
                        <div class="md:flex-auto w-full md:w-1/4 md:pr-2">
                            <label class="block">
                                <span class="text-gray-900">Type of Travel</span>
                                <select
                                        id="type_of_travels"
                                        name="type_of_travels"
                                        class="form-select block w-full mt-1">
                                    <option value="">--select--</option>
                                    @foreach($type_of_traves as $item)
                                        <option value="{{ $item->id }}">{{ $item->name_en }}</option>
                                    @endforeach
                                </select>
                            </label>
                        </div>
                        <div class="md:flex-auto w-full md:w-1/4 md:pr-2">
                            <label class="block">
                                <span class="text-gray-900">Status</span>
                                <select
                                        id="status"
                                        name="status"
                                        class="form-select block w-full mt-1">
                                    <option value="">--select--</option>
                                    <option value="confirm">Draft</option>
                                    <option value="published">Published</option>
                                </select>
                            </label>
                        </div>
                    </div>

                    <div class="md:flex md:flex-row w-full p-2 pb-6">
                        <div class="md:flex w-full md:w-60 md:pr-2">
                            <label class="block">
                                <span class="text-gray-900">Status Inactive-active</span>
                                <select
                                        id="active_status"
                                        name="active_status"
                                        class="form-select block w-full mt-1">
                                    <option value="">--select--</option>
                                    <option value="0">Inactive</option>
                                    <option value="1">Active</option>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="container w-full mx-auto">
                    <div id="recipients">
                        @if(auth()->user()->role->name != 'Viewer')
                            <a href="{{route('business.quick-create.admin')}}"
                               type="button"
                               class="ml-2 float-right bg-green-900 active:bg-green-700 py-3 px-4 rounded text-white shadow">Quick
                                create</a>


                            <a href="{{route('business.create.admin')}}"
                               type="button"
                               class="ml-2 float-right bg-green-500 active:bg-green-700 py-3 px-4 rounded text-white shadow">Create</a>
                        @endif
                        <a href="{{route('business.export')}}"
                           type="button"
                           class="float-right bg-blue-500 active:bg-blue-700 py-3 px-4 rounded text-white shadow">Export</a>

                        <div class="w-full shadow bg-white rounded p-4">
                            <div class="border-gray-200 w-full rounded bg-white overflow-x-auto">
                                <table class="leading-normal " id="list_table"
                                       style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
                                    <thead
                                            class="text-gray-600 text-xs font-semibold border-gray tracking-wider text-left px-5 py-3 bg-gray-100 hover:cursor-pointer uppercase border-b-2 border-gray-200">
                                    <tr class="border-b border-gray">
                                        <th scope="col"
                                            data-priority="1"
                                            class="text-gray-dark border-gray border-b-2 border-t-2 border-gray-200 py-3 px-3 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                            ID
                                        </th>
                                        <th scope="col"
                                            data-priority="2"
                                            class="text-gray-dark border-gray border-b-2 border-t-2 border-gray-200 py-3 px-3 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                            Company name (EN)
                                        </th>
                                        <th scope="col"
                                            data-priority="3"
                                            class="text-gray-dark border-gray border-b-2 border-t-2 border-gray-200 py-3 px-3 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                            Login E-mail
                                        </th>
                                        <th scope="col"
                                            data-priority="4"
                                            class="text-gray-dark border-gray border-b-2 border-t-2 border-gray-200 py-3 px-3 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                            Business Type
                                        </th>
                                        <!--                                        <th scope="col"
                                                                                    data-priority="5"
                                                                                    class="text-gray-dark border-gray border-b-2 border-t-2 border-gray-200 py-3 px-3 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                                                                    Title (selling points)
                                                                                </th>
                                                                                <th scope="col"
                                                                                    data-priority="6"
                                                                                    class="text-gray-dark border-gray border-b-2 border-t-2 border-gray-200 py-3 px-3 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                                                                    Support menu
                                                                                </th>
                                                                                <th scope="col"
                                                                                    data-priority="7"
                                                                                    class="text-gray-dark border-gray border-b-2 border-t-2 border-gray-200 py-3 px-3 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                                                                    Province
                                                                                </th>-->
                                        <th scope="col"
                                            data-priority="5"
                                            class="text-gray-dark border-gray border-b-2 border-t-2 border-gray-200 py-3 px-3 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                            Active Status
                                        </th>
                                        <th scope="col"
                                            data-priority="6"
                                            class="text-gray-dark border-gray border-b-2 border-t-2 border-gray-200 py-3 px-3 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                            Status
                                        </th>
                                        <th scope="col"
                                            id="edit-col"
                                            data-priority="7"
                                            class="text-gray-dark border-gray border-b-2 border-t-2 border-gray-200 py-3 px-3 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                            @if(auth()->user()->role->name == 'Viewer')
                                                View
                                            @else
                                                Edit
                                            @endif
                                        </th>
                                        @if(auth()->user()->role->name != 'Viewer')
                                            <th scope="col"
                                                id="delete-col"
                                                data-priority="8"
                                                class="text-gray-dark border-gray border-b-2 border-t-2 border-gray-200 py-3 px-3 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                                Delete
                                            </th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
@endsection
@prepend('scripts')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.6/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/colreorder/1.5.2/js/dataTables.colReorder.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    @include('admin.include.block')

    <script>

        fill_datatable();

        function delete_item(t) {
            Swal.fire({
                title: 'Are you sure to delete?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        method: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            id: $(t).attr('data-id')
                        },
                        url: `{{ route('business.delete') }}`,
                    }).done(function (msg) {
                        toastr.success(msg.message);
                        $('#list_table').DataTable().destroy();
                        fill_datatable();
                    }).fail(function (msg) {
                        toastr.error(msg.message)
                    });
                }
            });
        }

        function update_staus(t) {
            var id = $(t).attr('data-id');

            $.ajax({
                method: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    id: id,
                    status: $(t).val()
                },
                url: `{{ route('business.update.status') }}`,
            }).done(function (msg) {
                toastr.success(msg.message)
            })
        }

        function update_staus_active(t) {
            var id = $(t).attr('data-id');

            $.ajax({
                method: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    id: id,
                    active_status: $(t).val()
                },
                url: `{{ route('business.update.status_active') }}`,
            }).done(function (msg) {
                toastr.success(msg.message)
            })
        }

        function fill_datatable(filters = []) {
            var base_url = `{{route('business.data.index')}}`;

            var dataList = $('#list_table');
            dataList.DataTable({
                searching: false,
                serverSide: true,
                processing: true,
                responsive: true,
                columnDefs: [
                    {responsivePriority: 1, targets: 0},
                    {responsivePriority: 10001, targets: 4},
                    {responsivePriority: 2, targets: -2}
                ],
                ajax: {
                    url: base_url,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    data: {
                        filters: filters
                    }
                },
                order: [[0, "desc"]],
                pageLength: 50,
                lengthMenu: [50, 100, 150, 200, 300],
                columns: [
                    {
                        data: "id",
                        width: "7%",
                        class: "py-4 px-6 border border-gray-200 text-gray-900 text-sm text-center",
                        orderable: true
                    },
                    {
                        data: "company_name",
                        width: "20%",
                        class: "py-4 px-6 border border-gray-200 text-gray-900 text-sm",
                        orderable: true
                    },
                    {
                        data: "user_login",
                        width: "15%",
                        class: "py-4 px-6 border border-gray-200 text-gray-900 text-sm",
                        orderable: true
                    },
                    {
                        data: "business_type",
                        width: "13%",
                        class: "py-4 px-6 border border-gray-200 text-gray-900 text-sm",
                        orderable: false
                    },
                    {
                        data: "active_status",
                        width: "10%",
                        class: "py-4 px-6 border border-gray-200 text-gray-900 text-sm text-center",
                        orderable: true
                    },
                    {
                        data: "status",
                        width: "10%",
                        class: "py-4 px-6 border border-gray-200 text-gray-900 text-sm text-center",
                        orderable: true
                    },
                    {
                        data: "edit_btn",
                        width: "10%",
                        class: "py-4 lg:px-6 px-1 border border-gray-200 text-gray-900 text-sm text-center",
                        orderable: false
                    },
                        @if(auth()->user()->role->name != 'Viewer')
                    {
                        data: "delete_btn",
                        width: "12%",
                        class: "py-4 lg:px-6 px-1 border border-gray-200 text-gray-900 text-sm text-center",
                        orderable: false
                    }
                    @endif
                ]
            }).columns.adjust()
                .responsive.recalc();
        }

        $(document).ready(function () {

            $('.select2').select2({
                width: '270'
            });
            var params = {};

            $('#is_partner').change(function () {
                var is_partner = $('#is_partner').val();
                if (is_partner != '') {
                    params['is_partner'] = is_partner;
                    $('#list_table').DataTable().destroy();
                    fill_datatable(params);
                } else {
                    params['is_partner'] = '';
                    $('#list_table').DataTable().destroy();
                    fill_datatable(params)
                }
            });

            $('#business_types').change(function () {
                var business_types = $('#business_types').val();
                if (business_types != '') {
                    params['business_types'] = business_types;
                    $('#list_table').DataTable().destroy();
                    fill_datatable(params);
                } else {
                    params['business_types'] = '';
                    $('#list_table').DataTable().destroy();
                    fill_datatable(params)
                }
            });

            $('#is_branch_thai').change(function () {
                var is_branch_thai = $('#is_branch_thai').val();
                if (is_branch_thai != '') {
                    params['is_branch_thai'] = is_branch_thai;
                    $('#list_table').DataTable().destroy();
                    fill_datatable(params);
                } else {
                    params['is_branch_thai'] = '';
                    $('#list_table').DataTable().destroy();
                    fill_datatable(params);
                }
            });

            $('#support_menus').change(function () {
                var support_menus = $('#support_menus').val();
                if (support_menus != '') {
                    params['support_menus'] = support_menus;
                    $('#list_table').DataTable().destroy();
                    fill_datatable(params);
                } else {
                    params['support_menus'] = '';
                    $('#list_table').DataTable().destroy();
                    fill_datatable(params);
                }
            });

            $('#type_of_travels').change(function () {
                var type_of_travels = $('#type_of_travels').val();
                if (type_of_travels != '') {
                    params['type_of_travels'] = type_of_travels;
                    $('#list_table').DataTable().destroy();
                    fill_datatable(params);
                } else {
                    params['type_of_travels'] = '';
                    $('#list_table').DataTable().destroy();
                    fill_datatable(params);
                }
            });

            $('#province').change(function () {
                var province = $('#province').val();
                if (province != '') {
                    params['province'] = province;
                    $('#list_table').DataTable().destroy();
                    fill_datatable(params);
                } else {
                    params['province'] = '';
                    $('#list_table').DataTable().destroy();
                    fill_datatable(params);
                }
            });

            $('#season').change(function () {
                var season = $('#season').val();
                if (season != '') {
                    params['season'] = season;
                    $('#list_table').DataTable().destroy();
                    fill_datatable(params);
                } else {
                    params['season'] = '';
                    $('#list_table').DataTable().destroy();
                    fill_datatable(params)
                }
            });

            $('#type_of_travels').change(function () {
                var type_of_travels = $('#type_of_travels').val();
                if (type_of_travels != '') {
                    params['type_of_travels'] = type_of_travels;
                    $('#list_table').DataTable().destroy();
                    fill_datatable(params);
                } else {
                    params['type_of_travels'] = '';
                    $('#list_table').DataTable().destroy();
                    fill_datatable(params);
                }
            });

            $('#status').change(function () {
                var status = $('#status').val();
                if (status != '') {
                    params['status'] = status;
                    $('#list_table').DataTable().destroy();
                    fill_datatable(params);
                } else {
                    params['status'] = '';
                    $('#list_table').DataTable().destroy();
                    fill_datatable(params)
                }
            });

            $('#active_status').change(function () {
                var status_active = $('#active_status').val();
                if (status_active != '') {
                    params['status_active'] = status_active;
                    $('#list_table').DataTable().destroy();
                    fill_datatable(params);
                } else {
                    params['active_status'] = '';
                    $('#list_table').DataTable().destroy();
                    fill_datatable(params)
                }
            });

            $('#search').click(function () {
                var input_search = $('#input_search').val();
                if (input_search != '') {
                    params['search'] = input_search;
                    $('#list_table').DataTable().destroy();
                    fill_datatable(params);
                } else {
                    params['search'] = '';
                    $('#list_table').DataTable().destroy();
                    fill_datatable(params)
                }
            });
        });

    </script>
@endprepend