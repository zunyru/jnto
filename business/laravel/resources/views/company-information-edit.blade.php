@extends('layouts.app')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
@section('content')
    <div class="container">
        <div class="justify-center">

            <div class="FormBox clr">

                @if ($errors->any())
                    <div class="mb-3" role="alert">
                        <div class="bg-red-500 text-white font-bold rounded-t px-4 py-2">
                            Error
                        </div>
                        <div class="border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
                            @foreach ($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    </div>
                @endif


                <div class="BannerTop">
                    <img src="{{ asset('banner/banner.png') ?? 'http://enfete.co/jnto/assets/images/googleform_CS6.png' }}" alt="banner">
                    <p>「パートナーになりたい！連絡して話を聞いてみたい！」と思わせるようなサービスの魅力をタイの旅行会社にアピールしましょう。
                        タイ語での記載やサポートメニューの提供はタイの旅行会社にとって嬉しいポイントです。</p>
                </div>
                <form action="{{ route('business.store') }}" method="POST" enctype="multipart/form-data"
                id="myform">
                    @csrf

                    <input type="hidden" name="id" value="{{$id}}">
                    <input type="hidden" name="mode" value="edit">
                    <div class="FormList">

                        {{--Company Information--}}
                        @include('sessions.edit.company-information-edit')

                        {{--Company Information--}}
                        @include('sessions.edit.service-information-edit')

                        {{--Support menu--}}
                        @include('sessions.edit.support-menu-edit')

                        {{--Category tag--}}
                        @include('sessions.edit.category-tag-edit')

                        {{--policy tag--}}
                        @include('sessions.edit.policy')

                        <div class="BTNList">
                            <button type="submit">SUBMIT <i class="fa fa-send-o"></i></button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@include('layouts.validate-edit')
