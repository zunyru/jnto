@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="justify-center">
            <div class="FormBox clr">

                <div class="ThanksPageDisplay">
                    <p>
                        <img src="{{ asset('assets/images/thankyou.png') }}"
                             style="width: 100px; height: auto;margin: auto; padding-bottom: 30px;">
                    </p>
                    @if(!empty($setting->value))
                        <p>
                            {!! nl2br($setting->value) !!}
                        </p>
                    @else
                        <p>Thank you !!</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
