<?php

return [
    'secsion_title_2' => '2 Service information',


    'secsion_title_2_1_label' => '<p>2-1 サービスのセールスポイント | <span class="FontNormal">Title (selling points)</span>
                    <span class="NeedRed">*</span></p>',
    'secsion_title_2_1_sub' => '',


    'secsion_title_2_2_label' => '<p>2-2 本文 | <span class="FontNormal">Detail</span> <span
                        class="NeedRed">*</span></p>',
    'secsion_title_2_2_sub' => '',


    'secsion_title_2_3_label' => '<p>2-3 WEBサイト表示用 メイン画像  | <span class="FontNormal">Main photo for website </span> <span
                    class="NeedRed">*</span><br>',
    'secsion_title_2_3_sub' => '<span class="FontNormal2">画像サイズの目安：W710xH470ピクセル／解像度200dpi／1MB以下／PNGまたはJPG <br>ขนาดภาพที่แนะนำ W710xH470 pixels / ความละเอียด 200 dpi / ความจุต่ำกว่า 1MB / PNG หรือ JPG</span>
            </p>',

    'secsion_title_2_4_label' => '<p>2-4 WEBサイト表示用 その他画像  | <span class="FontNormal">Other photo for website</span> <br>',
    'secsion_title_2_4_sub' => '<span class="FontNormal2">画像サイズの目安：W710xH470ピクセル／解像度200dpi／1MB以下／PNGまたはJPG <br>ขนาดภาพที่แนะนำ W710xH470 pixels / ความละเอียด 200 dpi / ความจุต่ำกว่า 1MB / PNG หรือ JPG</span>
            </p>',


    'secsion_title_2_5_label' => '<p>2-5 PR動画URL （Youtube）｜PR VDO </p>',
    'secsion_title_2_5_sub' => '',


    'secsion_title_2_6_label' => '<p>2-6 添付資料（会社案内・パンフレットなど）| <span class="FontNormal">Company profile/pamphlet</span><br>',
    'secsion_title_2_6_sub' => '<span
                    class="FontNormal2">PDF、5MB以下 | ไฟล์ PDF ขนาดต่ำกว่า 5MB</span></p>',


    'secsion_title_2_7_label' => '<p>2-7 その他関連サイト | <span class="FontNormal">Other useful links</span></p>',
    'secsion_title_2_7_sub' => '',


    'secsion_title_2_8_label' => '2-8 同意 - Accept <span
                    class="NeedRed">*</span>',
    'secsion_title_2_8_sub' => '',

    'secsion_title_2_8_title_label' => '本フォームより登録、アップロードされた全ての情報（文字情報、画像情報等含む）は、本サービスの円滑な運営及び情報発信（タイ旅行会社向けセミナーやニュースレター含む）に利用することを目的とし、登録者はこれに同意するものとします。また、本サービスでは、登録された全ての情報（文字情報、画像情報等含む）の著作権・肖像権・商標権等の帰属について確認する事が出来ません。よってこれらの権利帰属につきましては登録者ご自身でお調べ頂きますようお願いいたします。ご登録頂きました情報等が著作権・商標登録その他の権利に関わるものでも、本サービスでは一切の責任を負いかねます。'

];
