<?php

return [
    'description' => '「パートナーになりたい！連絡して話を聞いてみたい！」と思わせるようなサービスの魅力をタイの旅行会社にアピールしましょう。 タイ語での記載やサポートメニューの提供はタイの旅行会社にとって嬉しいポイントです。',

    'secsion_title_1' => '1 Company information',

    'secsion_title_1_1_label' => '<p class="FloatLeft">1-1 組織・団体のロゴマーク | <span class="FontNormal">Company logo</span>
                <span class="NeedRed">*</span><br>',
    'secsion_title_1_1_sub' => '<span class="FontNormal2">横幅300ピクセル以上／1MB以下／PNGまたはJPG<br>ความกว้าง 300 pixels ขึ้นไป/ความจุต่ำกว่า 1MB/PNG หรือ JPG</span>
            </p>',

    'secsion_title_1_2_label' => '<p class="FloatLeft">1-2 JNTO賛助団体・会員 | <span class="FontNormal">Is your company/organization a JNTO partner? </span><span class="NeedRed">*</span></p>',
    'secsion_title_1_2_sub' => '',

    'secsion_title_1_3_label' => '1-3 組織・団体名（英語）| <span
                            class="FontNormal">Company name (English) </span><span
                            class="NeedRed">*</span>',
    'secsion_title_1_3_sub' => '',

    'secsion_title_1_4_label' => '1-4 組織・団体名（日本語）| <span
                            class="FontNormal">Company name (Japanese) </span><span
                            class="NeedRed">*</span>',
    'secsion_title_1_4_sub' => '',

    'secsion_title_1_5_label' => '1-5 業種 | <span class="FontNormal">Business type</span>
                <span class="NeedRed">*</span>',
    'secsion_title_1_5_sub' => '',

    'secsion_title_1_6_label' => '<p>1-6 所在地（英語）| <span class="FontNormal">Address (English)</span> <span
                            class="NeedRed">*</span></p>',
    'secsion_title_1_6_sub' => '',

    'secsion_title_1_7_label' => '<p>1-7 所在地（URL）| <span class="FontNormal">Google map URL</span></p>',
    'secsion_title_1_7_sub' => '',

    'secsion_title_1_8_label' => '<p>1-8 公式WEBサイトURL（英語・タイ語）| <span class="FontNormal">English/Thai official website URL</span>
                    <span class="NeedRed">*</span></p>',
    'secsion_title_1_8_sub' => '<span class="FontNormal2">公式Webサイトがない場合はSNSでも登録可能です。</span>',

    'secsion_title_1_9_label' => '<p>1-9 Facebook URL </p>',
    'secsion_title_1_9_sub' => '',

    'secsion_title_1_10_label' => '<p>1-10 Instagram URL </p>',
    'secsion_title_1_10_sub' => '',

    'secsion_title_1_11_label' => '<p>1-11 Youtube Channel URL </p>',
    'secsion_title_1_11_sub' => '',

    'secsion_title_1_12_label' => '<p>1-12 Twitter URL </p>',
    'secsion_title_1_12_sub' => '',

    'secsion_title_1_13_label' => ' <p>1-13 LINE公式アカウント友達追加用URL | <span class="FontNormal">LINE for Business</span></p>',
    'secsion_title_1_13_sub' => '',

    'secsion_title_1_14_label' => '<p>1-14 担当者名（英語）| <span class="FontNormal">Contact person name (English)</span></p>',
    'secsion_title_1_14_sub' => '<span class="FontNormal2">タイ旅行会社からの連絡に対応可能なご担当者名をご入力ください。※こちらの担当者名はサイトに表示されます。</span>',

    'secsion_title_1_15_label' => '<p>1-15 部署名（英語）| <span class="FontNormal">Department (English)</span> <span class="NeedRed">*</span></p>',
    'secsion_title_1_15_sub' => '',

    'secsion_title_1_16_label' => '<p>1-16 タイ旅行会社からの連絡先となるメールアドレス | <span class="FontNormal">Company email address</span> <span
                            class="NeedRed">*</span></p>',
    'secsion_title_1_16_sub' => '<span class="FontNormal2">タイ旅行会社から連絡可能なメールアドレスをご入力ください。※こちらのメールアドレスはサイトに表示されます。</span>',

    'secsion_title_1_16_1_label' => '<p>担当者連絡先メールアドレス | <span class="FontNormal">Contact person email address</span><span class="NeedRed">*</span></p>',
    'secsion_title_1_16_1_sub' => '<span class="FontNormal2">JNTOや事務局からの連絡が可能なメールアドレスをご入力ください。※こちらのメールアドレスはサイトには表示されません。</span>',

    'secsion_title_1_17_label' => '<p>1-17 TEL </p>',
    'secsion_title_1_17_sub' => '<span class="FontNormal2">必ず連絡のつく電話番号を、国番号からご入力ください。※外国語対応が可能な電話番号をご入力ください。 例：03-1234-5678の場合、最初の0を取り 81-3-1234-5678と入力します。</span>',

    'secsion_title_1_18_label' => '<p class="FloatLeft">1-18 対応言語（複数選択）| <span class="FontNormal">Language</span>
                <span class="NeedRed">*</span></p>',
    'secsion_title_1_18_sub' => '',

    'secsion_title_1_19_1_label' => '<p>1-19.旅行業登録番号 │ Japan Tourism Agency Registration Number <span class="NeedRed">*</span></p>',
    'secsion_title_1_19_1_sub' => '<span class="FontNormal2">英語の記載例：</span>
			<p><span class="FontNormal2">・観光庁長官登録旅行業第XXX号│Japan Tourism Agency Commissioner of Japan Tourism Agency Registered Travel Agency No. 123</span></p>
			<p><span class="FontNormal2">・東京都知事登録旅行業第XXXX号│The travel agent registered by Tokyo governor No.123</span></p>',


    'secsion_title_1_19_label' => '<p class="FloatLeft">1-20 タイ支店・レップの有無 | <span
                        class="FontNormal">Branch/representative office in Thailand</span> <span
                        class="NeedRed">*</span></p>',
    'secsion_title_1_19_sub' => '',

    'secsion_title_1_20_label' => '<p>1-21 企業・団体名（英語）| <span
                                class="FontNormal">Company name (English)</span> <span
                                class="NeedRed">*</span></p>',
    'secsion_title_1_20_sub' => '',

    'secsion_title_1_21_label' => '<p>1-22 企業・団体名（タイ語）| <span class="FontNormal">Company name (Thai)</span>
                    </p>',
    'secsion_title_1_21_sub' => '',

    'secsion_title_1_22_label' => '<p>1-23 所在地（英語）| <span class="FontNormal">Address (English)</span> <span
                                class="NeedRed">*</span></p>',
    'secsion_title_1_22_sub' => '',

    'secsion_title_1_23_label' => '<p>1-24 所在地（URL）| <span class="FontNormal">Google map URL</span></p>',
    'secsion_title_1_23_sub' => '',

    'secsion_title_1_24_label' => '<p>1-25 公式WEBサイトURL（タイ語）| <span class="FontNormal">Thai website URL</span> <span
                                class="NeedRed">*</span></p>',
    'secsion_title_1_24_sub' => '<span class="FontNormal2">公式WEBサイトがない場合はSNSでも登録可能です。</span>',

    'secsion_title_1_25_label' => '<p>1-26 担当者名（英語）| <span
                                class="FontNormal">Contact person name (English)</span></p>',
    'secsion_title_1_25_sub' => '',

    'secsion_title_1_26_label' => '<p>1-27 部署名（英語）| <span class="FontNormal">Department (English)</span> <span class="NeedRed">*</span>
                    </p>',
    'secsion_title_1_26_sub' => '',

    'secsion_title_1_27_label' => '<p>1-28 旅行会社からの連絡先となるメールアドレス | <span class="FontNormal">Company email address</span> <span
                                class="NeedRed">*</span>
                    </p>',
    'secsion_title_1_27_sub' => '<span class="FontNormal2">タイ旅行会社から連絡可能なメールアドレスをご入力ください。※こちらのメールアドレスはサイトに表示されます。</span>',

    'secsion_title_1_28_label' => '<p>1-29 TEL </p>',
    'secsion_title_1_28_sub' => '<span class="FontNormal2">必ず連絡のつく電話番号を、国番号からご入力ください。※外国語対応が可能な電話番号をご入力ください。 例：03-1234-5678の場合、最初の0を取り 81-3-1234-5678と入力します。</span>',

    'secsion_title_1_29_label' => '<p class="FloatLeft">1-30 対応言語（複数選択）| <span
                            class="FontNormal">Language</span> <span class="NeedRed">*</span> :
                </p>',
    'secsion_title_1_29_sub' => '',

    'secsion_title_1_30_label' => '',
    'secsion_title_1_30_sub' => '',

];