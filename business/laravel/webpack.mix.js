const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');*/


const tailwindcss = require('tailwindcss')

mix.js('resources/js/validate.js', 'public/js').sourceMaps();
mix.js('resources/js/validate-edit.js', 'public/js').sourceMaps();
mix.js('resources/js/validate-contact-form.js', 'public/js').sourceMaps();
mix.js('resources/js/validate-news-jnto.js', 'public/js').sourceMaps();
mix.js('resources/js/validate-news-jnto-partner.js', 'public/js').sourceMaps();
mix.js('resources/js/validate-seminar.js', 'public/js').sourceMaps();
mix.js('resources/js/validate-useful-link.js', 'public/js').sourceMaps();
mix.js('resources/js/validate-useful-link-edit.js', 'public/js').sourceMaps();
mix.js('resources/js/validate-covid.js', 'public/js').sourceMaps();
mix.js('resources/js/validate-category.js', 'public/js').sourceMaps();
mix.js('resources/js/validate-banner.js', 'public/js').sourceMaps();
mix.js('resources/js/validate-event.js', 'public/js').sourceMaps();

mix.sass('resources/sass/placeholder.scss','public/css').sourceMaps();
mix.sass('resources/sass/app.scss', 'public/css')
    .options({
        processCssUrls: false,
        postCss: [tailwindcss('tailwind.config.js')],
    }).sourceMaps();

mix.copyDirectory('node_modules/tinymce/icons', 'public/node_modules/tinymce/icons');
mix.copyDirectory('node_modules/tinymce/plugins', 'public/node_modules/tinymce/plugins');
mix.copyDirectory('node_modules/tinymce/skins', 'public/node_modules/tinymce/skins');
mix.copyDirectory('node_modules/tinymce/themes', 'public/node_modules/tinymce/themes');
//mix.copy('node_modules/tinymce/jquery.tinymce.js', 'public/node_modules/tinymce/jquery.tinymce.js');
//mix.copy('node_modules/tinymce/jquery.tinymce.min.js', 'public/node_modules/tinymce/jquery.tinymce.min.js');
mix.copy('node_modules/tinymce/tinymce.js', 'public/node_modules/tinymce/tinymce.js');
mix.copy('node_modules/tinymce/tinymce.min.js', 'public/node_modules/tinymce/tinymce.min.js');

mix.version();
