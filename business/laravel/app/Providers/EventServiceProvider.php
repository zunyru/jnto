<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use Illuminate\Http\Request;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Event::listen('Illuminate\Auth\Events\Login', function ($data) {
            if ($data->guard == 'seller') {
                $Auth = 'Seller Auth';
                $data->user->last_login = Carbon::now();
                $data->user->save();

                activity($Auth)
                    ->causedBy($data->user)
                    ->withProperties([
                        'ip' => request()->ip()
                    ])
                    ->log('login');
            }

        });

        Event::listen('Illuminate\Auth\Events\Logout', function ($data) {

            if ($data->guard == 'seller') {
                $Auth = 'Seller Logout';
            } else {
                $Auth = 'Admin Logout';
                if(isset($data->user->role)) {
                    if ($data->user->role->name == 'Access web') {
                        $Auth = 'Buyer Logout';
                    } else {
                        $Auth = 'Admin Logout';
                    }
                }
            }
            activity($Auth)
                ->causedBy($data->user)
                ->withProperties([
                    'ip' => request()->ip()
                ])
                ->log('logout');
        });
    }
}
