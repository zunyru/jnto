<?php

use Carbon\Carbon;

if (!function_exists('formatSizeUnits')) {
    function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = $bytes . ' bytes';
        }

        return $bytes;
    }
}

if (!function_exists('nameFile')) {
    function nameFile($file)
    {
        $path = explode('/', $file);
        return array_pop($path);
    }
}

if (!function_exists('formatDateThai')) {
    function formatDateThai($strDate)
    {
        Carbon::setLocale('th_TH');
        $date = Carbon::rawParse($strDate);
        $strDay = $date->isoFormat('D');
        $strMonth = $date->isoFormat('M');
        $strYear = $date->addYears(543)->isoFormat('YYYY');
        $arrMonth = [
            '',
            'ม.ค.',
            'ก.พ.',
            'มี.ค.',
            'เม.ย.',
            'พ.ค.',
            'มิ.ย.',
            'ก.ค.',
            'ส.ค.',
            'ก.ย.',
            'ต.ค.',
            'พ.ย.',
            'ธ.ค.'
        ];
        return "$strDay $arrMonth[$strMonth] $strYear";
    }
}

if (!function_exists('getIconFile')) {
    function getIconFile($file)
    {
        $files = explode('.', $file);

        if (isset($files[1])) {
            switch ($files[1]) {
                case "pdf":
                    return asset('assets/images/pdf.png');
                    break;
                case "docx":
                case "doc":
                    return asset('assets/images/word.png');
                    break;
                case "xlsx":
                case "xls":
                    return asset('assets/images/excel.png');
                    break;
                default:
                    return "";
            }
        }

        return "";

    }
}
