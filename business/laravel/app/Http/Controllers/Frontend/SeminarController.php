<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repositories\SeminarRepository;
use Illuminate\Http\Request;

class SeminarController extends Controller
{
    protected $seminar_repo;

    public function __construct(SeminarRepository $seminarRepository)
    {
        $this->seminar_repo = $seminarRepository;
    }

    public function all()
    {
        //seminar
        $seminars_jp = $this->seminar_repo->all_published('jp', false, false, true);
        $seminar_th_first = $this->seminar_repo->getFirst('th');
        $seminars_th = $this->seminar_repo->all_published('th', false, false, true);

        $seminars_jp_group = $seminars_jp->groupBy('year');
        $seminars_th_group = $seminars_th->groupBy('year');

        return view('frontend.pages.seminar-list',
            compact(
                'seminars_jp_group',
                'seminar_th_first',
                'seminars_th_group'
            )
        );
    }
}
