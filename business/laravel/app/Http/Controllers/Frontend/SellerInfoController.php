<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repositories\BusinessTypeRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\ProvinceRepository;
use App\Repositories\SeasonRepository;
use App\Repositories\ServiceRepository;
use App\Repositories\SupportMenuRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use TCG\Voyager\Models\Setting;

class SellerInfoController extends Controller
{
    protected $business_type_repo;
    protected $language_repo;
    protected $support_menu_repo;
    protected $province_repo;
    protected $category_repo;
    protected $company_repo;
    protected $service_repo;
    protected $season_repo;

    public function __construct(
        BusinessTypeRepository $business_type_repo,
        LanguageRepository     $language_repo,
        SupportMenuRepository  $support_menu_repo,
        ProvinceRepository     $province_repo,
        CategoryRepository     $category_repo,
        CompanyRepository      $company_repo,
        ServiceRepository      $service_repo,
        SeasonRepository       $season_ropo
    )
    {
        $this->business_type_repo = $business_type_repo;
        $this->language_repo = $language_repo;
        $this->support_menu_repo = $support_menu_repo;
        $this->province_repo = $province_repo;
        $this->category_repo = $category_repo;
        $this->company_repo = $company_repo;
        $this->service_repo = $service_repo;
        $this->season_repo = $season_ropo;
    }

    public function show($ref_id)
    {
        $preview = false;
        if (Route::getCurrentRoute()->getName() === 'preview') {
            $preview = true;
        }

        $company = $this->company_repo->getDetail($ref_id, $preview);
        if (empty($company)) {
            abort(404);
        }
        $provinces_implode = '';
        $province_arr = [];

        if (!empty($company->company_region)) {
            foreach ($company->company_region as $key => $item) {
                $province_arr[$key] = $item->name_th;
            }
            $provinces_implode = collect($province_arr)->implode(' ');
        }
        if (!empty($company->province_all)) {
            $provinces_implode = 'ทั่วญี่ปุ่น';
        }

        $seasons = $this->season_repo->all();
        $season_implode = '';
        $season_arr = [];
        if (!empty($company->company_season)) {
            if ($company->company_season->count() == $seasons->count()) {
                $season_implode = 'ตลอดปี';
            } else {
                foreach ($company->company_season as $key => $item) {
                    $season_arr[$key] = $item->name_th;
                }
                $season_implode = collect($season_arr)->implode(' ');
            }
        }

        $special_features_implode = '';
        $special_features_arr = [];
        if (!empty($company->company_special_feature)) {
            foreach ($company->company_special_feature as $key => $item) {
                $special_features_arr[$key] = $item->name_th;
            }
            $special_features_implode = collect($special_features_arr)->implode('<br>');
        }

        $type_of_travels_arr = [];
        $type_of_travels_implode = '';
        if (!empty($company->company_type_of_travel)) {
            foreach ($company->company_type_of_travel as $key => $item) {
                $type_of_travels_arr[$key] = $item->name_en;
            }
            $type_of_travels_implode = collect($type_of_travels_arr)->implode('<br>');
        }

        $business_types_arr = [];
        $business_types_implode = '';
        if (!empty($company->business_type_companies)) {
            foreach ($company->business_type_companies as $key => $item) {
                $business_types_arr[$key] = $item->name_en;
            }
            $business_types_implode = collect($business_types_arr)->implode(' ');
        }

        $languages_arr = [];
        $languages_implode = '';
        if (!empty($company->company_language)) {
            foreach ($company->company_language as $key => $item) {
                $languages_arr[$key] = $item->name_th;
            }
            $languages_implode = collect($languages_arr)->implode(' ');
        }

        $languages_branch_arr = [];
        $languages_branch_implode = '';
        if (!empty($company->company_language_for_branch)) {
            foreach ($company->company_language_for_branch as $key => $item) {
                $languages_branch_arr[$key] = $item->name_th;
            }
            $languages_branch_implode = collect($languages_branch_arr)->implode(' ');
        }

        $others = $this->company_repo->getOther($ref_id, false, 'region');
        if ($others->count() < 3) {
            $others = $this->company_repo->getOther($ref_id, false, 'business_type');
            if ($others->count() < 3) {
                $others = $this->company_repo->getOther($ref_id, false, 'all');
            }
        }

        $province_other_arr = [];
        $provinces_other_implode = '';
        if (!empty($others)) {
            foreach ($others as $other) {
                if (!empty($other->company_region)) {
                    foreach ($other->company_region as $key => $item) {
                        $province_other_arr[$key] = $item->name_th;
                    }
                }
                $provinces_other_implode = collect($province_other_arr)->implode(' ');
                if (!empty($other->province_all)) {
                    $provinces_other_implode = 'ทั่วญี่ปุ่น';
                }
            }
        }

        if (Auth::guard('seller')->check()) {
            if (Auth::guard('seller')->user()->privacy == 'private') {
                $others = [];
            }
        }

        $start_event = Setting::query()
            ->where('key', 'admin.dashboard.start_event')
            ->first();
        

        return view('frontend.pages.page-seller-info',
            compact(
                'company',
                'provinces_implode',
                'season_implode',
                'special_features_implode',
                'type_of_travels_implode',
                'business_types_implode',
                'languages_implode',
                'languages_branch_implode',
                'others',
                'provinces_other_implode',
                'preview',
                'start_event'
            )
        );
    }

    public function search()
    {
        if (Auth::guard('seller')->check() && Auth::guard('seller')->user()->privacy == 'private') {
            abort(403);
        }

        $business_types = $this->business_type_repo->getAll();
        $support_menus = $this->support_menu_repo->getAll();
        return view(
            'frontend.pages.seller-info-search',
            compact(
                'business_types',
                'support_menus'
            )
        );
    }

    public function get_search(Request $request)
    {
        $_province = $this->province_repo->getAll();

        $arr_province = [];
        foreach ($_province as $item) {
            $arr_province[$item->name_en] = [
                'id' => $item->id,
                'name' => $item->name_en
            ];
        }

        $provinces = $request->province ?? null;
        $filters = [];
        if (!empty($provinces)) {
            foreach ($provinces as $province) {
                foreach ($arr_province as $key => $list) {
                    if ($key == $province) {
                        $filters[] = $list['id'];
                    }
                }
            }
        }

        $request->filters = array_merge([
            'province' => $filters,
            'business_types' => $request->business_types,
            'support_menus' => $request->support_menus,
            'is_public' => $request->is_public,
            'search' => $request->search_keyword
        ]);

        $companies = $this->company_repo->getPaginateByFrontend($request);

        if ($companies->count() > 0) {
            $output = "<div class=\"CompanyList\">";
            $output .= "<ul>";
            foreach ($companies as $data) {
                $rounte = route('seller-info-detail', $data->ref_id);
                $background_image = optional($data->service_information)->main_photo;
                $name_en = $data->name_en;
                $title = optional($data->service_information)->title;
                $links = $companies->links('vendor.pagination.custom-ajax');
                $is_partner = ($data->is_partner && Auth::guard('web')->check()) ? "<span>JNTO Partners</span>" : "";

                $province_arr = [];
                $provinces_implode = '';
                if (!empty($data->company_region)) {
                    foreach ($data->company_region as $key => $item) {
                        $province_arr[] = $item->name_th;
                    }
                    $provinces_implode = collect($province_arr)->implode(' ');
                }
                if (!empty($data->province_all)) {
                    $provinces_implode = 'ทั่วญี่ปุ่น';
                }

                $support_menu = '';
                if (!empty($data->support_menu->support_menu1)
                    || !empty($data->support_menu->support_menu2)
                    || !empty($data->support_menu->support_menu3)
                    || !empty($data->support_menu->support_menu4)
                    || !empty($data->support_menu->support_menu5)) {
                    $support_menu = "<p class=\"SupportMenuDisplay\">มีโปรแกรมสนับสนุน</p>";
                }

                $topic = '';
                if (!empty($data->topic)) {
                    if ($data->topic->category == 'event') {
                        $topic = "<p class=\"ToppicEvent\" > มีอีเวนท์ใหม่</p >";
                    } else {
                        $topic = "<p class=\"ToppicNews\" > ข่าวสาร</p >";
                    }
                }

                $output .= "<li>
                    <a href=\"{$rounte}\" target=\"_blank\">
                        <div class=\"PictureDisplayRe\">
                        {$is_partner}
                            <div class=\"inner\"
                                 style=\"background-image:url('{$background_image}');background-size: cover; background-position: center center;\"></div>
                        </div>
                        <div class=\"RightCaptions\">
                            <p class=\"CompanyName\">{$name_en}</p>
                            <p class=\"Title\">{$title}</p>
                            <p class=\"ProvinceDisplay\">{$provinces_implode}</p>
                            {$support_menu}
                            {$topic}
                        </div>
                    </a>
                </li>";
            }
            $output .= "</ul>";
            $output .= "</div>";


            $output .= "<div class=\"NavigatorList\">
                  {$links}
               </div>";
        } else {
            $output = "<div class=''>
                         <h3 style='color: red'>ไม่พบข้อมูล</h3>
                      </div>";
        }


        echo $output;
    }
}