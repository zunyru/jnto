<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Jobs\ContactFormSendMailJob;
use App\Repositories\CompanyRepository;
use App\Repositories\ContactFormReposiroty;
use Illuminate\Http\Request;

class ContactFormController extends Controller
{
    protected $contact_form_repo;

    public function __construct(ContactFormReposiroty $contactFormReposiroty)
    {
        $this->contact_form_repo = $contactFormReposiroty;
    }

    public function store(Request $request)
    {
        $last_id = $this->contact_form_repo->store($request->all());

        $company = (new CompanyRepository)->find($request->company_id);

        if (!$company) {
            throw new \Exception('company not found');
        }

        if($request->type == 'JAPAN') {
            $params_branch_japan_seller['company_name_jp'] = $company->name_ja;
            $params_branch_japan_seller['company_name_en'] = $company->name_en;
            $params_branch_japan_seller['branch_company_name_th'] = '';
            $params_branch_japan_seller['branch_japan_seller'] = true;
            $params_branch_japan_seller['send_mail_to'] = $company->email;
            $params_branch_japan_seller['requests'] = $request->all();
            $params_branch_japan_seller['last_id'] = $last_id;
            $this->sendMailJapanSeller($params_branch_japan_seller);

            $params_branch_japan_user['branch_japan_user'] = true;
            $params_branch_japan_user['company_name_en'] = $company->name_en;
            $params_branch_japan_user['send_mail_to'] = $request->email;
            $params_branch_japan_user['requests'] = $request->all();
            $params_branch_japan_user['last_id'] = $last_id;
            $this->sendMailJapanUser($params_branch_japan_user);
        }

        if($request->type == 'THAI') {
            $params_branch_thai_seller['company_name_jp'] = null;
            $params_branch_thai_seller['company_name_en'] = $company->name_en;
            $params_branch_thai_seller['branch_company_name_en'] = $company->branch_company_name_en;
            $params_branch_thai_seller['branch_thai_seller'] = true;

            $params_branch_thai_seller['send_mail_to'] = $company->branch_email;
            $params_branch_thai_seller['requests'] = $request->all();
            $params_branch_thai_seller['last_id'] = $last_id;
            $this->sendMailThaiSeller($params_branch_thai_seller);

            $params_branch_japan_user['branch_thai_user'] = true;
            $params_branch_japan_user['company_name_en'] = $company->name_en;
            $params_branch_japan_user['branch_company_name_en'] = $company->branch_company_name_en;
            $params_branch_japan_user['send_mail_to'] = $request->email;
            $params_branch_japan_user['requests'] = $request->all();
            $params_branch_japan_user['last_id'] = $last_id;
            $this->sendMailJapanUser($params_branch_japan_user);
        }

        return ($last_id) ? true : false;
    }

    private function sendMailJapanSeller(array $params)
    {
        ContactFormSendMailJob::dispatch($params);
    }

    private function sendMailJapanUser(array $params)
    {
        ContactFormSendMailJob::dispatch($params);
    }

    private function sendMailThaiSeller(array $params)
    {
        ContactFormSendMailJob::dispatch($params);
    }
}
