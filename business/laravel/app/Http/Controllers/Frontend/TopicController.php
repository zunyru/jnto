<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Repositories\TopicRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class TopicController extends Controller
{
    protected $topic_repo;

    public function __construct(
        TopicRepository $topicRepository
    )
    {
        $this->topic_repo = $topicRepository;
    }

    public function all()
    {
        if (Auth::guard('seller')->check() && Auth::guard('seller')->user()->privacy == 'private') {
            abort(403);
        }

        $menu = 'all';
        $topics = $this->topic_repo->getTopicsAll([
            'paginate' => true,
            'today'    => true
        ]);

        return view('frontend.pages.topic', compact('topics', 'menu'));
    }

    public function getEvent()
    {
        $menu = 'event';
        $topics = $this->topic_repo->getTopics([
            'paginate'    => true,
            'privacy'     => 'public',
            'event'       => 'event',
            'status'      => 'done',
            'event_start' => true,
            'today'       => true,
            'is_event'    => true
        ]);

        return view('frontend.pages.topic', compact('topics', 'menu'));
    }

    public function getNews()
    {
        $menu = 'news';
        $topics = $this->topic_repo->getTopics([
            'paginate'    => true,
            'privacy'     => 'public',
            'event'       => 'information',
            'status'      => 'done',
            'event_start' => true
        ]);

        return view('frontend.pages.topic', compact('topics', 'menu'));
    }

    public function getEventJnto($ref_id)
    {
        $preview = false;
        if (Route::getCurrentRoute()->getName() === 'event.show.preview') {
            $preview = true;
        }

        $topic = $this->topic_repo->getEventJntoDetail($ref_id, $preview);

        $topic_other = $this->topic_repo->getEventJntoOther($ref_id, $preview);

        return view('frontend.pages.event-detail',
            compact('topic', 'topic_other')
        );
    }

    public function getEventList(Request $request)
    {

        $html = "<div class='flex justify-between p-10'>
                    <h1 class='c-event-title'>Upcoming events</h1>
                    <div class='flex'>";
        if (Auth::guard('seller')->check()) {
            if (Auth::guard('seller')->user()->privacy == 'private') {
                $html .= "";
            } else {
                $html .= "<div class='flex align-items-center'>
                            <div class='box-partner'></div>
                            <span class='m-left font-sm'>PARTNER'S EVENTS</span>
                        </div>";
            }
        }else{
            $html .= "<div class='flex align-items-center'>
                            <div class='box-partner'></div>
                            <span class='m-left font-sm'>PARTNER'S EVENTS</span>
                        </div>";
        }
        $html .= "<div class='flex align-items-center m-left'>
                            <div class='box-jnto'></div>
                            <span class='m-left font-sm'>JNTO EVENTS</span>
                        </div>";
        $html .= "</div></div>";

        $topics = $this->topic_repo->getTopicCalendarList($request->date, $request->next);
        $html .= "<div class='scroll space-y-4'>";
        foreach ($topics as $topic) {
            /*
             eq()เท่ากับ
             ne()ไม่เท่ากับ
             gt()มากกว่า
             gte()มากกว่าหรือเท่ากับ
             lt()น้อยกว่า
             lte()น้อยกว่าหรือเท่ากับ
             */
            $a = Carbon::parse(Carbon::today())->lte($topic->event_date_start);
            $b = Carbon::parse($topic->event_date_end)->gte(Carbon::today());
            //if ($a || $b) {
                $event_date = formatDateThai($topic->event_date_start) . ' - ' . formatDateThai($topic->event_date_end);
                if (isset($topic->company)) {
                    $route = route('seller-info-detail', $topic->company->ref_id);

                    if (Auth::guard('seller')->check()) {
                        if (Auth::guard('seller')->user()->privacy == 'private') {
                            if (Auth::guard('seller')->user()->ref_id == $topic->company->ref_id
                                && $topic->status == 'done'
                                && $topic->company->status == 'published'
                            ) {
                                $html .= "<div class='flex justify-between c-event-item'>
                                <div class='line-event-partner'>
                                    <div class='m-14 pt-4'>{$topic->title}</div>
                                    <div class='m-14 pb-4'>{$event_date}</div>
                                </div>
                                <div>
                                    <a class='view-partner' href='{$route}#Toppic'>View</a>
                                </div>
                              </div>";
                            }
                        } else {
                            if ($topic->company->privacy == 'public'
                                && $topic->status == 'done'
                                && $topic->category == 'event'
                                && $topic->company->status == 'published'
                            ) {
                                $html .= "<div class='flex justify-between c-event-item'>
                                <div class='line-event-partner'>
                                    <div class='m-14 pt-4'>{$topic->title}</div>
                                    <div class='m-14 pb-4'>{$event_date}</div>
                                </div>
                                <div>
                                    <a class='view-partner' href='{$route}#Toppic'>View</a>
                                </div>
                              </div>";
                            }
                        }
                    } elseif (Auth::guard('web')->check()) {
                        if ($topic->status == 'done' && $topic->category == 'event') {
                            if (!empty($topic->company)) {
                                if ($topic->company->status == 'published') {
                                    $html .= "<div class='flex justify-between c-event-item'>
                                            <div class='line-event-partner'>
                                                <div class='m-14 pt-4'>{$topic->title}</div>
                                                <div class='m-14 pb-4'>{$event_date}</div>
                                            </div>
                                            <div>
                                                <a class='view-partner' href='{$route}#Toppic'>View</a>
                                            </div>
                                          </div>";
                                }
                            } else {
                                $html .= "<div class='flex justify-between c-event-item'>
                                    <div class='line-event-partner'>
                                        <div class='m-14 pt-4'>{$topic->title}</div>
                                        <div class='m-14 pb-4'>{$event_date}</div>
                                    </div>
                                    <div>
                                        <a class='view-partner' href='{$route}#Toppic'>View</a>
                                    </div>
                                  </div>";
                            }
                        }
                    }
                } else {
                    if ($topic->template == 'page') {
                        $route = route('event-detail', $topic->ref_id);
                    } else {
                        $route = $topic->link;
                    }
                    if ($topic->status == 'published') {
                        $html .= "<div class='flex justify-between c-event-item'>
                            <div class='line-event-jnto'> 
                                 <div class='m-14 pt-4'>{$topic->title}</div>
                                 <div class='m-14 pb-4'>{$event_date}</div>
                            </div>
                            <div>
                                <a class='view-jnto' href='{$route}'>View</a>
                            </div>
                          </div>";
                    }
                }
            //}
        }
        $html .= "</div>";

        return $html;
    }
}
