<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\UploadeFileController;
use App\Repositories\SeminarRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SeminarController extends Controller
{
    protected $seminar_repo;

    public function __construct(SeminarRepository $seminarRepository)
    {
        $this->seminar_repo = $seminarRepository;
    }

    public function index(Request $request)
    {
        return view('admin.online-seminar');
    }

    public function dataIndex(Request $request)
    {
        $seminars = $this->seminar_repo->getPaginate($request);

        foreach ($seminars as $key => $item) {

            $edit_btn = "";
            if (auth()->user()->role->name == 'Viewer') {
                $edit_btn .= "<a href=\"" . route('seminar.edit.admin', $item->ref_id) . "\" data=\"$item->id\" class=\"bg-orange-500 hover:bg-orange-700 focus:border-orange-400 active:border-orange-700 text-white font-bold py-3 lg:px-4 px-1 rounded edited\">View</a>";
            } else {
                $edit_btn .= "<a href=\"" . route('seminar.edit.admin', $item->ref_id) . "\" data=\"$item->id\" class=\"bg-orange-500 hover:bg-orange-700 focus:border-orange-400 active:border-orange-700 text-white font-bold py-3 lg:px-4 px-1 rounded edited\">Edit</a>";
            }

            $delete_btn = "";
            $delete_btn .= "<a href=\"#\" onclick=\"delete_item(this);\" data-id=\"$item->id\" class=\"bg-red-500 hover:bg-red-700 focus:border-red-400 active:border-red-700 text-white font-bold py-3 lg:px-4 px-1 rounded deleted\">Delete</a>";

            $select_srt = "";
            $selected_confirm = ($item->status == 'draf') ? 'selected' : '';
            $selected_published = ($item->status == 'published') ? 'selected' : '';
            if (auth()->user()->role->name == 'Viewer') {
                $status = ($item->status == 'draf') ? 'Draf' : 'Published';
                $select_srt .= $status;
            } else {
                $select_srt .= "<select name=\"status\" data-id=\"{$item->id}\" onchange=\"update_staus(this);\" class=\"select_status block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 lg:px-4 px-1 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500\"><option " . $selected_confirm . " value=\"confirm\">Draft</option><option " . $selected_published . " value=\"published\">Published</option></select>";
            }

            $select_target = "";
            $selected_target_all = ($item->target == 'all') ? 'selected' : '';
            $selected_target_buyer = ($item->target == 'buyer') ? 'selected' : '';
            $selected_target_seller = ($item->target == 'seller') ? 'selected' : '';
            $selected_target_seller_th = ($item->target == 'seller_th') ? 'selected' : '';
            if (auth()->user()->role->name == 'Viewer') {
                if(($item->target == 'all')){
                    $target = "All";
                }elseif(($item->target == 'buyer')){
                    $target = "Thai buyer";
                }elseif(($item->target == 'seller')){
                    $target = "Japan seller (JP)";
                }elseif(($item->target == 'seller_th')){
                    $target = "Japan seller (TH)";
                }
                $select_target .= $target;
            }else {
                $select_target .= "<select name=\"target\" data-id=\"{$item->id}\" onchange=\"update_staus_target(this);\" class=\"select_status block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 lg:px-4 px-1 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500\"><option " . $selected_target_all . " value=\"all\">All</option><option " . $selected_target_buyer . " value=\"buyer\">Thai buyer</option><option " . $selected_target_seller . " value=\"seller\">Japan seller (JP)</option><option " . $selected_target_seller_th . " value=\"seller_th\">Japan seller (TH)</option></select>";
            }

            $column[$key]['no'] = $seminars->firstItem() + $key;
            $column[$key]['id'] = $item->ref_id;
            $column[$key]['title'] = $item->list;
            $column[$key]['sub_title'] = $item->sub_title;
            $column[$key]['status'] = $select_srt;
            $column[$key]['target'] = $select_target;
            $column[$key]['year'] = $item->year;
            $column[$key]['edit_btn'] = $edit_btn;
            if (auth()->user()->role->name != 'Viewer') {
                $column[$key]['delete_btn'] = $delete_btn;
            }
        }

        $data['data'] = $column ?? [];
        $data['recordsTotal'] = $seminars->total();
        $data['recordsFiltered'] = $seminars->count();
        $data['draw'] = $request->draw;

        return response($data);
    }

    public function create()
    {
        return view('admin.online-seminar-add-edit');
    }

    public function store(Request $request)
    {
        try {
            //TODO::image
            $uploade = new UploadeFileController();
            if (!empty($request->thumbnail)) {
                $request->thumbnail = $uploade->uploadImage($request->thumbnail, 'saminar', Str::random(10));
            }
            //TODO::pdf
            if (!empty($request->pdf1)) {
                $pdf = $uploade->uploadPDF($request->pdf1, 'saminar/pdf', Str::random(10));
                $request->pdf1 = $pdf['path'];
                $request->pdf_size1 = $pdf['file_size'];
            }
            if (!empty($request->pdf2)) {
                $pdf = $uploade->uploadPDF($request->pdf2, 'saminar/pdf', Str::random(10));
                $request->pdf2 = $pdf['path'];
                $request->pdf_size2 = $pdf['file_size'];
            }
            if (!empty($request->pdf3)) {
                $pdf = $uploade->uploadPDF($request->pdf3, 'saminar/pdf', Str::random(10));
                $request->pdf3 = $pdf['path'];
                $request->pdf_size3 = $pdf['file_size'];
            }

            $data = $this->seminar_repo->store($request);

            return redirect()->route('seminar.index')
                ->with([
                    'message'    => __('voyager::generic.successfully_updated'),
                    'alert-type' => 'success',
                ]);

        } catch (\Exception $e) {
            return $message = $e->getMessage();
        }
    }

    public function show($ref_id)
    {
        $seminar = $this->seminar_repo->getDetail($ref_id);
        $seminars_other = $this->seminar_repo->getOtherShow($ref_id);

        $target = $seminar->target;

        return view('frontend.pages.seminar-detail',
            compact(
                'seminar',
                'seminars_other',
                'target'
            )
        );
    }

    public function edit(Request $request, $id)
    {
        $code = substr($id, -6);
        $id = intval($code);

        $seminar = $this->seminar_repo->find($id);

        return view('admin.online-seminar-add-edit', compact('seminar'));
    }

    public function update(Request $request, $ref_id)
    {
        try {
            //TODO::image
            $uploade = new UploadeFileController();
            if (!empty($request->thumbnail)) {
                $request->thumbnail = $uploade->uploadImage($request->thumbnail, 'saminar', Str::random(10));
            }
            //TODO::pdf
            if (!empty($request->pdf1)) {
                $pdf = $uploade->uploadPDF($request->pdf1, 'saminar/pdf', $request->pdf1);
                $request->pdf1 = $pdf['path'];
                $request->pdf_size1 = $pdf['file_size'];
            }
            if (!empty($request->pdf2)) {
                $pdf = $uploade->uploadPDF($request->pdf2, 'saminar/pdf', $request->pdf2);
                $request->pdf2 = $pdf['path'];
                $request->pdf_size2 = $pdf['file_size'];
            }
            if (!empty($request->pdf3)) {
                $pdf = $uploade->uploadPDF($request->pdf3, 'saminar/pdf', $request->pdf3);
                $request->pdf3 = $pdf['path'];
                $request->pdf_size3 = $pdf['file_size'];
            }

            $data = $this->seminar_repo->find($ref_id);
            $this->seminar_repo->valuable($data, $request);

            return redirect()->route('seminar.index')
                ->with([
                    'message'    => __('voyager::generic.successfully_updated'),
                    'alert-type' => 'success',
                ]);

        } catch (\Exception $e) {
            return $message = $e->getMessage();
        }
    }

    public function delete(Request $request)
    {
        $code = substr($request->id, -6);
        $id = intval($code);

        $data = $this->seminar_repo->delete($id);

        return response([
            'message'    => __('voyager::generic.successfully_updated'),
            'alert-type' => 'success',
        ]);
    }

    public function updateStatus(Request $request)
    {
        $code = substr($request->id, -6);
        $id = intval($code);

        $data = $this->seminar_repo->status($request, $id);

        return response([
            'message'    => __('voyager::generic.successfully_updated'),
            'alert-type' => 'success',
        ]);
    }

    public function updateStatusTarget(Request $request)
    {
        $code = substr($request->id, -6);
        $id = intval($code);

        $data = $this->seminar_repo->statusTarget($request, $id);

        return response([
            'message'    => __('voyager::generic.successfully_updated'),
            'alert-type' => 'success',
        ]);
    }
}
