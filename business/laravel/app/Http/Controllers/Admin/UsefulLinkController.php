<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\UploadeFileController;
use App\Models\Category;
use App\Repositories\UsefulLinkRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UsefulLinkController extends Controller
{
    protected $usefullink_repo;

    public function __construct(UsefulLinkRepository $usefulLinkRepository)
    {
        $this->usefullink_repo = $usefulLinkRepository;
    }

    public function index(Request $request)
    {
        return view('admin.useful-link');
    }

    public function dataIndex(Request $request)
    {
        $datas = $this->usefullink_repo->getPaginate($request);

        foreach ($datas as $key => $item) {
            $edit_btn = "";
            if (auth()->user()->role->name == 'Viewer') {
                $edit_btn .= "<a href=\"" . route('useful-link.edit.admin', $item->id) . "\" data=\"$item->id\" class=\"bg-orange-500 hover:bg-orange-700 focus:border-orange-400 active:border-orange-700 text-white font-bold py-3 px-4 rounded edited\">View</a>";
            }else {
                $edit_btn .= "<a href=\"" . route('useful-link.edit.admin', $item->id) . "\" data=\"$item->id\" class=\"bg-orange-500 hover:bg-orange-700 focus:border-orange-400 active:border-orange-700 text-white font-bold py-3 px-4 rounded edited\">Edit</a>";
            }

            $delete_btn = "";
            $delete_btn .= "<a href=\"#\" onclick=\"delete_item(this);\" data-id=\"$item->id\" class=\"bg-red-500 hover:bg-red-700 focus:border-red-400 active:border-red-700 text-white font-bold py-3 px-4 rounded deleted\">Delete</a>";

            $select_srt = "";
            $selected_confirm = ($item->status == 'draf') ? 'selected' : '';
            $selected_published = ($item->status == 'published') ? 'selected' : '';
            if (auth()->user()->role->name == 'Viewer') {
                $select_srt .= ($item->status == 'draf') ? 'Draf' : 'Published';
            } else {
                $select_srt .= "<select name=\"status\" data-id=\"{$item->id}\" onchange=\"update_staus(this);\" class=\"select_status block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500\"><option " . $selected_confirm . " value=\"confirm\">Draft</option><option " . $selected_published . " value=\"published\">Published</option></select>";
            }
            $thumbnail_photo = asset($item->thumbnail_photo);
            $column[$key]['no'] = $datas->firstItem() + $key;
            $column[$key]['title'] = $item->title;
            $column[$key]['category_title'] = $item->category->title ?? null;
            $column[$key]['thumbnail_photo'] = "<img class=\"w-24\" src=\"{$thumbnail_photo}\">";
            $column[$key]['status'] = $select_srt;
            $column[$key]['edit_btn'] = $edit_btn;
            if (auth()->user()->role->name != 'Viewer') {
                $column[$key]['delete_btn'] = $delete_btn;
            }
        }

        $data['data'] = $column ?? [];
        $data['recordsTotal'] = $datas->total();
        $data['recordsFiltered'] = $datas->count();
        $data['draw'] = $request->draw;

        return response($data);
    }

    public function create()
    {
        $categories = Category::all();
        return view('admin.useful-link-add-edit', compact('categories'));
    }

    public function store(Request $request)
    {
        try {
            //TODO::image
            $uploade = new UploadeFileController();
            if (!empty($request->thumbnail_photo)) {
                $request->thumbnail_photo = $uploade->uploadImage($request->thumbnail_photo, 'usefullink', Str::random(10));
            }
            if (!empty($request->icon)) {
                $request->icon = $uploade->uploadFile($request->icon, 'usefullink/icon', Str::random(10));
            }

            $data = $this->usefullink_repo->store($request);

            return redirect()->route('useful-link.index');

        } catch (\Exception $e) {
            return $message = $e->getMessage();
        }
    }

    public function edit($id)
    {
        $useful = $this->usefullink_repo->find($id);
        $categories = Category::all();
        return view('admin.useful-link-add-edit', compact('useful', 'categories'));
    }

    public function update(Request $request, $id)
    {
        try {
            //TODO::image
            $uploade = new UploadeFileController();
            if (!empty($request->thumbnail_photo)) {
                $request->thumbnail_photo = $uploade->uploadImage($request->thumbnail_photo, 'usefullink', Str::random(10));
            }
            if (!empty($request->icon)) {
                $request->icon = $uploade->uploadFile($request->icon, 'usefullink/icon', Str::random(10));
            }

            $data = $this->usefullink_repo->find($id);
            $this->usefullink_repo->valuable($data, $request);

            return redirect()->route('useful-link.index');

        } catch (\Exception $e) {
            return $message = $e->getMessage();
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;

        $data = $this->usefullink_repo->delete($id);

        return response([
            'message' => __('voyager::generic.successfully_updated'),
            'alert-type' => 'success',
        ]);
    }

    public function updateStatus(Request $request)
    {
        $id = $request->id;

        $data = $this->usefullink_repo->status($request, $id);

        return response([
            'message' => __('voyager::generic.successfully_updated'),
            'alert-type' => 'success',
        ]);
    }

}
