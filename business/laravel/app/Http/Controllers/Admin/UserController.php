<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use TCG\Voyager\Models\Role;

class UserController extends Controller
{
    protected $user_repo;

    public function __construct(UserRepository $userRepository)
    {
        $this->user_repo = $userRepository;
    }

    public function index(Request $request)
    {
        return view('admin.user');
    }

    public function dataIndex(Request $request)
    {
        $admins = $this->user_repo->getPaginate($request);

        foreach ($admins as $key => $item) {

            $edit_btn = "";
            if (auth()->user()->role->name == 'Viewer') {
                $edit_btn .= "<a href=\"" . route('user.edit.admin', $item->id) . "\" data=\"$item->id\" class=\"bg-orange-500 hover:bg-orange-700 focus:border-orange-400 active:border-orange-700 text-white font-bold py-3 px-4 rounded edited\">View</a>";
            } else {
                $edit_btn .= "<a href=\"" . route('user.edit.admin', $item->id) . "\" data=\"$item->id\" class=\"bg-orange-500 hover:bg-orange-700 focus:border-orange-400 active:border-orange-700 text-white font-bold py-3 px-4 rounded edited\">Edit</a>";
            }

            $delete_btn = "";
            $delete_btn .= "<a href=\"#\" onclick=\"delete_item(this);\" data-id=\"$item->id\" class=\"bg-red-500 hover:bg-red-700 focus:border-red-400 active:border-red-700 text-white font-bold py-3 px-4 rounded deleted\">Delete</a>";

            $select_srt = "";
            $selected_confirm = ($item->status == 'draf') ? 'selected' : '';
            $selected_published = ($item->status == 'published') ? 'selected' : '';
            if (auth()->user()->role->name == 'Viewer') {
                $select_srt .= ($item->status == 'draf') ? 'Draf' : 'Published';
            } else {
                $select_srt .= "<select name=\"status\" data-id=\"{$item->id}\" onchange=\"update_staus(this);\" class=\"select_status block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500\"><option " . $selected_confirm . " value=\"confirm\">Draft</option><option " . $selected_published . " value=\"published\">Published</option></select>";
            }

            $column[$key]['no'] = $admins->firstItem() + $key;
            $column[$key]['username'] = $item->username;
            $column[$key]['role'] = $item->role->display_name ?? null;
            $column[$key]['edit_btn'] = $edit_btn;
            if (auth()->user()->role->name != 'Viewer') {
                $column[$key]['delete_btn'] = $delete_btn;
            }
        }

        $data['data'] = $column ?? [];
        $data['recordsTotal'] = $admins->total();
        $data['recordsFiltered'] = $admins->count();
        $data['draw'] = $request->draw;

        return response($data);
    }

    public function create()
    {
        $roles = Role::whereIn('name', [
            'Editor',
            'Super admin',
            'Access form',
            'Access web',
            'Viewer'
        ])->get();
        return view('admin.user-add-edit', compact('roles'));
    }

    public function store(Request $request)
    {
        $user = new User;
        $user->name = $request->username;
        $user->username = $request->username;
        $user->role_id = $request->role_id;
        $user->email = $request->email;
        $user->avatar = 'users/default.png';
        $user->password = Hash::make($request->password);
        $user->save();

        return redirect()->route('user.index');
    }

    public function edit(Request $request, $id)
    {
        $user = $this->user_repo->find($id);
        $roles = Role::whereIn('name', [
            'Editor',
            'Super admin',
            'Access form',
            'Access web',
            'Viewer'
        ])->get();

        return view('admin.user-add-edit', compact('user', 'roles'));
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->username = $request->username;
        $user->role_id = $request->role_id;
        $user->email = $request->email;
        $user->password = (!empty($request->password)) ? Hash::make($request->password) : $user->password;
        $user->save();
        return redirect()->route('user.index')->with([
            'message'    => __('voyager::generic.successfully_updated'),
            'alert-type' => 'success',
        ]);
    }

    public function delete(Request $request)
    {
        $flight = User::find($request->id);
        $flight->delete();

        return response([
            'message'    => __('voyager::generic.successfully_updated'),
            'alert-type' => 'success',
        ]);
    }

}
