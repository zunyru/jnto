<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\SendgridEmailJobOtp;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password'        => 'required|string',
        ]);

        if ($this->attemptLogin($request)) {
            $user = Auth::guard('web')->user();

            if ($user->role->name != 'Access web') {

                Auth::guard('web')->logout();

                //send OTP
                if (config('app.env') != 'local') {
                    SendgridEmailJobOtp::dispatchSync($user->id, 'SendgridEmailJobOtp', 'otp');
                } else {
                    $otp = [
                        'locale' => $user->settings['locale'] ?? 'en',
                        'otp'    => random_int(100000, 999999)
                    ];

                    $user->settings = collect($otp);
                    $user->save();
                }

                activity('Admin Login')
                    ->causedBy($user)
                    ->withProperties([
                        'ip' => request()->ip()
                    ])
                    ->log('login');

                $token = Str::random(60);
                $user->remember_token = $token;
                $user->save();

                Session::put('data_token', $token);

                return redirect()->route('otp-page', $token);
            }
            $this->guard()->logout();
            return redirect()->back()->withErrors([
                'username' => __('auth.failed'),
                'password' => __('auth.password')
            ]);

        }
        return redirect()->back()->withErrors([
            'username' => __('auth.failed'),
            'password' => __('auth.password')
        ]);
        //return $this->sendFailedLoginResponse($request);
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        if ($response = $this->loggedOut($request)) {
            return $response;
        }

        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : redirect('/');
    }

    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    protected function guard()
    {
        return Auth::guard('web');
    }

    public function username()
    {
        return 'username';
    }

    public function verifyOtp(Request $request)
    {
        $user = (new UserRepository())->find($request->id);

        if ((string)$user->settings['otp'] === $request->otp) {
            Auth::guard('web')->login($user);
            return redirect()->route('dashboard');
        }

        return redirect()->back()->withErrors([
            'otp' => 'Not macth',
        ]);

    }

    public function otpPage($token)
    {
        $user = (new UserRepository())->findByToken($token);

        if ($user) {
            if ($user->remember_token != $token || Carbon::now()->gt($user->updated_at->addMinute(5))) {
                return redirect()->route('login.admin');
            }
            $id = $user->id;
            return view('auth.otp-page', compact('token', 'id'));
        }

        return redirect()->route('login.admin');
    }
}
