<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\UploadeFileController;
use App\Http\Requests\StoreNewsJntoRequest;
use App\Repositories\BusinessTypeRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\NewsJntoPartnerRepository;
use App\Repositories\ProvinceRepository;
use App\Repositories\SeasonRepository;
use App\Repositories\ServiceRepository;
use App\Repositories\SupportMenuRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

class NewsPartnerController extends Controller
{
    protected $news_partner_repo;
    protected $company_repo;
    protected $business_type_repo;
    protected $language_repo;
    protected $support_menu_repo;
    protected $province_repo;
    protected $category_repo;
    protected $service_repo;
    protected $season_repo;

    public function __construct(
        NewsJntoPartnerRepository $newsJnto_partner_repo,
        CompanyRepository         $company_repo,
        BusinessTypeRepository    $business_type_repo,
        LanguageRepository        $language_repo,
        SupportMenuRepository     $support_menu_repo,
        ProvinceRepository        $province_repo,
        CategoryRepository        $category_repo,
        ServiceRepository         $service_repo,
        SeasonRepository          $season_ropo
    )
    {
        $this->news_partner_repo = $newsJnto_partner_repo;
        $this->company_repo = $company_repo;
        $this->business_type_repo = $business_type_repo;
        $this->language_repo = $language_repo;
        $this->support_menu_repo = $support_menu_repo;
        $this->province_repo = $province_repo;
        $this->category_repo = $category_repo;
        $this->service_repo = $service_repo;
        $this->season_repo = $season_ropo;
    }

    public function index(Request $request)
    {
        return view('admin.news-jnto-partner');
    }

    public function dataIndex(Request $request)
    {
        $news = $this->news_partner_repo->getPaginateByBackend($request);

        foreach ($news as $key => $item) {

            $edit_btn = "";
            if (auth()->user()->role->name == 'Viewer') {
                $edit_btn .= "<a href=\"" . route('news-jnto-partner.edit.admin', $item->ref_id) . "\" data=\"$item->id\" class=\"bg-orange-500 hover:bg-orange-700 focus:border-orange-400 active:border-orange-700 text-white font-bold py-3 px-4 rounded edited\">View</a>";
            } else {
                $edit_btn .= "<a href=\"" . route('news-jnto-partner.edit.admin', $item->ref_id) . "\" data=\"$item->id\" class=\"bg-orange-500 hover:bg-orange-700 focus:border-orange-400 active:border-orange-700 text-white font-bold py-3 px-4 rounded edited\">Edit</a>";
            }

            $delete_btn = "";
            $delete_btn .= "<a href=\"#\" onclick=\"delete_item(this);\" data-id=\"$item->id\" class=\"bg-red-500 hover:bg-red-700 focus:border-red-400 active:border-red-700 text-white font-bold py-3 px-4 rounded deleted\">Delete</a>";

            $select_srt = "";
            $selected_confirm = ($item->status == 'draf') ? 'selected' : '';
            $selected_published = ($item->status == 'published') ? 'selected' : '';
            if (auth()->user()->role->name == 'Viewer') {
                $status = $item->status == 'draf' ? 'Draf' : 'Published';
                $select_srt .= $status;
            } else {
                $select_srt .= "<select name=\"status\" data-id=\"{$item->id}\" onchange=\"update_staus(this);\" class=\"select_status block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500\"><option " . $selected_confirm . " value=\"confirm\">Draft</option><option " . $selected_published . " value=\"published\">Published</option></select>";
            }

            if ($item->template == 'full-content') {
                $template = 'Full content';
            } else if ($item->template == 'link-only') {
                $template = 'Link only';
            } else if ($item->template == 'PDF-only') {
                $template = 'File only';
            }

            if (!empty($item->company->name_en)) {
                $status = $item->company->status == 'confirm' ? "Draft" : "Published";
                $private = Str::title($item->company->privacy);
            }

            $column[$key]['no'] = $news->firstItem() + $key;
            $column[$key]['id'] = $item->ref_id;
            $column[$key]['title'] = $item->title;
            $column[$key]['partner'] = !empty($item->company->name_en) ? $item->company->name_en . ' (' . $status . ' / ' . $private . ')' : '-';
            $column[$key]['template'] = $template;
            $column[$key]['status'] = $select_srt;
            $column[$key]['edit_btn'] = $edit_btn;
            if (auth()->user()->role->name != 'Viewer') {
                $column[$key]['delete_btn'] = $delete_btn;
            }
        }

        $data['data'] = $column ?? [];
        $data['recordsTotal'] = $news->total();
        $data['recordsFiltered'] = $news->total();
        $data['draw'] = $request->draw;

        return response($data);
    }

    public function all_jnto(Request $request)
    {
        $news = $this->news_partner_repo->getPaginate(
            array_merge([
                'public' => true
            ])
        );

        return view('frontend.pages.news-jnto-partner',
            compact(
                'news'
            )
        );
    }

    public function create()
    {
        $companies = $this->company_repo->all();

        return view('admin.news-info-add-edit-partner',
            compact('companies')
        );
    }

    public function edit(Request $request, $id)
    {
        $code = substr($id, -6);
        $id = intval($code);

        $companies = $this->company_repo->all();

        $news = $this->news_partner_repo->find($id);
        return view('admin.news-info-add-edit-partner',
            compact(
                'news',
                'companies'
            )
        );
    }

    public function store(StoreNewsJntoRequest $request)
    {
        try {
            //TODO::image
            $uploade = new UploadeFileController();
            if (!empty($request->photo1)) {
                $request->photo1 = $uploade->uploadImage($request->photo1, 'news', Str::random(10));
            }
            if (!empty($request->photo2)) {
                $request->photo2 = $uploade->uploadImage($request->photo2, 'news', Str::random(10));
            }
            if (!empty($request->photo3)) {
                $request->photo3 = $uploade->uploadImage($request->photo3, 'news', Str::random(10));
            }
            if (!empty($request->photo4)) {
                $request->photo4 = $uploade->uploadImage($request->photo4, 'news', Str::random(10));
            }

            //TODO::pdf
            if (!empty($request->pdf1)) {
                $pdf = $uploade->uploadPDF($request->pdf1, 'news/pdf', Str::random(10));
                $request->pdf1 = $pdf['path'];
                $request->pdf_size1 = $pdf['file_size'];
            }
            if (!empty($request->pdf2)) {
                $pdf = $uploade->uploadPDF($request->pdf2, 'news/pdf', Str::random(10));
                $request->pdf2 = $pdf['path'];
                $request->pdf_size2 = $pdf['file_size'];
            }
            if (!empty($request->pdf3)) {
                $pdf = $uploade->uploadPDF($request->pdf3, 'news/pdf', Str::random(10));
                $request->pdf3 = $pdf['path'];
                $request->pdf_size3 = $pdf['file_size'];
            }

            if (!empty($request->pdf_only)) {
                $pdf = $uploade->uploadPDF($request->pdf_only, 'news/pdf', Str::random(10));
                $request->pdf_only = $pdf['path'];
                $request->pdf_size_only = $pdf['file_size'];
            }

            $data = $this->news_partner_repo->store($request);

            return redirect()->route('news-jnto-partner.index');

        } catch (\Exception $e) {
            return $message = $e->getMessage();
        }
    }

    public function update(StoreNewsJntoRequest $request, $ref_id)
    {
        try {
            //TODO::image
            $uploade = new UploadeFileController();
            if (!empty($request->photo1)) {
                $request->photo1 = $uploade->uploadImage($request->photo1, 'news', Str::random(10));
            }
            if (!empty($request->photo2)) {
                $request->photo2 = $uploade->uploadImage($request->photo2, 'news', Str::random(10));
            }
            if (!empty($request->photo3)) {
                $request->photo3 = $uploade->uploadImage($request->photo3, 'news', Str::random(10));
            }
            if (!empty($request->photo4)) {
                $request->photo4 = $uploade->uploadImage($request->photo4, 'news', Str::random(10));
            }

            //TODO::pdf
            if (!empty($request->pdf1)) {
                $pdf = $uploade->uploadPDF($request->pdf1, 'news/pdf', Str::random(10));
                $request->pdf1 = $pdf['path'];
                $request->pdf_size1 = $pdf['file_size'];
            }

            if (!empty($request->pdf2)) {
                $pdf = $uploade->uploadPDF($request->pdf2, 'news/pdf', Str::random(10));
                $request->pdf2 = $pdf['path'];
                $request->pdf_size2 = $pdf['file_size'];
            }
            if (!empty($request->pdf3)) {
                $pdf = $uploade->uploadPDF($request->pdf3, 'news/pdf', Str::random(10));
                $request->pdf3 = $pdf['path'];
                $request->pdf_size3 = $pdf['file_size'];
            }

            if (!empty($request->pdf_only)) {
                $pdf = $uploade->uploadPDF($request->pdf_only, 'news/pdf', Str::random(10));
                $request->pdf_only = $pdf['path'];
                $request->pdf_size_only = $pdf['file_size'];
            }

            $data = $this->news_partner_repo->find($ref_id);
            $this->news_partner_repo->valuable($data, $request);

            return redirect()->back()->with([
                'message'    => __('voyager::generic.successfully_updated'),
                'alert-type' => 'success',
            ]);

        } catch (\Exception $e) {
            return $message = $e->getMessage();
        }
    }

    public function show($ref_id)
    {
        $preview = false;
        if (Route::getCurrentRoute()->getName() === 'news-jnto-partner.show.preview') {
            $preview = true;
        }

        $page = 'news-partner';
        $news = $this->news_partner_repo->getDetail($ref_id, $preview);

        $detail = true;
        $news_other = $this->news_partner_repo->getOtherShow($ref_id, $preview);

        $company = null;
        $provinces_implode = '';
        $season_implode = '';
        $special_features_implode = '';
        $type_of_travels_implode = '';
        $business_types_implode = '';
        $languages_implode = '';
        $languages_branch_implode = '';
        $provinces_other_implode = '';

        if (!empty($news->company->id)) {
            $company_id = $news->company->id;
            $company = $this->company_repo->getDetail($company_id);

            $province_arr = [];
            if (!empty($company->company_region)) {
                foreach ($company->company_region as $key => $item) {
                    $province_arr[$key] = $item->name_th;
                }
                $provinces_implode = collect($province_arr)->implode(' ');
            }

            $seasons = $this->season_repo->all();

            $season_arr = [];
            if (!empty($company->company_season)) {
                if ($company->company_season->count() == $seasons->count()) {
                    $season_implode = 'ตลอดปี';
                } else {
                    foreach ($company->company_season as $key => $item) {
                        $season_arr[$key] = $item->name_th;
                    }
                    $season_implode = collect($season_arr)->implode(' ');
                }
            }

            $special_features_arr = [];
            if (!empty($company->company_special_feature)) {
                foreach ($company->company_special_feature as $key => $item) {
                    $special_features_arr[$key] = $item->name_th;
                }
                $special_features_implode = collect($special_features_arr)->implode('<br>');
            }

            $type_of_travels_arr = [];

            if (!empty($company->company_type_of_travel)) {
                foreach ($company->company_type_of_travel as $key => $item) {
                    $type_of_travels_arr[$key] = $item->name_en;
                }
                $type_of_travels_implode = collect($type_of_travels_arr)->implode(' ');
            }

            $business_types_arr = [];

            if (!empty($company->business_type_companies)) {
                foreach ($company->business_type_companies as $key => $item) {
                    $business_types_arr[$key] = $item->name_en;
                }
                $business_types_implode = collect($business_types_arr)->implode(' ');
            }

            $languages_arr = [];

            if (!empty($company->company_language)) {
                foreach ($company->company_language as $key => $item) {
                    $languages_arr[$key] = $item->name_th;
                }
                $languages_implode = collect($languages_arr)->implode(' ');
            }

            $languages_branch_arr = [];

            if (!empty($company->company_language_for_branch)) {
                foreach ($company->company_language_for_branch as $key => $item) {
                    $languages_branch_arr[$key] = $item->name_th;
                }
                $languages_branch_implode = collect($languages_branch_arr)->implode(' ');
            }

            $province_other_arr = [];

            if (!empty($others)) {
                foreach ($others as $other) {
                    if (!empty($other->company_region)) {
                        foreach ($other->company_region as $key => $item) {
                            $province_other_arr[$key] = $item->province->name_th;
                        }
                    }
                    $provinces_other_implode = collect($province_other_arr)->implode(' ');
                }
            }
        }

        return view('frontend.pages.news-jnto-partner-detail',
            compact(
                'news',
                'news_other',
                'detail',
                'company',
                'provinces_implode',
                'season_implode',
                'special_features_implode',
                'type_of_travels_implode',
                'business_types_implode',
                'languages_implode',
                'languages_branch_implode',
                'provinces_other_implode',
                'page'
            )
        );
    }

    public function delete(Request $request)
    {
        $code = substr($request->id, -6);
        $id = intval($code);

        $data = $this->news_partner_repo->delete($id);

        return response([
            'message'    => __('voyager::generic.successfully_updated'),
            'alert-type' => 'success',
        ]);
    }

    public function updateStatus(Request $request)
    {
        $code = substr($request->id, -6);
        $id = intval($code);

        $data = $this->news_partner_repo->status($request, $id);

        return response([
            'message'    => __('voyager::generic.successfully_updated'),
            'alert-type' => 'success',
        ]);
    }

}
