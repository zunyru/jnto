<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\TopicRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\UploadeFileController;

class EventController extends Controller
{

    protected $topic_repo;

    public function __construct(TopicRepository $topicRepository)
    {
        $this->topic_repo = $topicRepository;
    }

    public function index()
    {
        return view('admin.event');
    }

    public function dataIndex(Request $request)
    {
        $topics = $this->topic_repo->getPaginate($request);

        foreach ($topics as $key => $item) {

            $edit_btn = "";
            if (auth()->user()->role->name == 'Viewer') {
                $edit_btn .= "<a href=\"" . route('event.edit.admin', $item->id) . "\" data=\"$item->id\" class=\"bg-orange-500 hover:bg-orange-700 focus:border-orange-400 active:border-orange-700 text-white font-bold py-3 px-4 rounded edited\">View</a>";
            } else {
                $edit_btn .= "<a href=\"" . route('event.edit.admin', $item->id) . "\" data=\"$item->id\" class=\"bg-orange-500 hover:bg-orange-700 focus:border-orange-400 active:border-orange-700 text-white font-bold py-3 px-4 rounded edited\">Edit</a>";
            }

            $delete_btn = "";
            $delete_btn .= "<a href=\"#\" onclick=\"delete_item(this);\" data-id=\"$item->id\" class=\"bg-red-500 hover:bg-red-700 focus:border-red-400 active:border-red-700 text-white font-bold py-3 px-4 rounded deleted\">Delete</a>";

            $select_srt = "";
            $selected_confirm = ($item->status == 'draf') ? 'selected' : '';
            $selected_published = ($item->status == 'published') ? 'selected' : '';
            if (auth()->user()->role->name == 'Viewer') {
                $select_srt .= ($item->status == 'draf') ? 'Draf' : 'Published';
            } else {
                $select_srt .= "<select name=\"status\" data-id=\"{$item->id}\" onchange=\"update_staus(this);\" class=\"select_status block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500\"><option " . $selected_confirm . " value=\"confirm\">Draft</option><option " . $selected_published . " value=\"published\">Published</option></select>";
            }

            if ($item->template == 'page') {
                $template = 'Page';
            } else if ($item->template == 'link-only') {
                $template = 'Link only';
            }

            $column[$key]['no'] = $topics->firstItem() + $key;
            $column[$key]['id'] = $item->ref_id;
            $column[$key]['title'] = $item->title;
            $column[$key]['template'] = $template;
            $column[$key]['status'] = $select_srt;
            $column[$key]['edit_btn'] = $edit_btn;
            if (auth()->user()->role->name != 'Viewer') {
                $column[$key]['delete_btn'] = $delete_btn;
            }
        }

        $data['data'] = $column ?? [];
        $data['recordsTotal'] = $topics->total();
        $data['recordsFiltered'] = $topics->total();
        $data['draw'] = $request->draw;

        return response($data);
    }

    public function create()
    {
        return view('admin.event-add-edit');
    }

    public function store(Request $request)
    {
        try {
            $request->category = 'event';
            //TODO::image
            $uploade = new UploadeFileController();
            if (!empty($request->photo1)) {
                $request->photo1 = $uploade->uploadImage($request->photo1, 'event', Str::random(10));
            }
            if (!empty($request->photo2)) {
                $request->photo2 = $uploade->uploadImage($request->photo2, 'event', Str::random(10));
            }
            if (!empty($request->photo3)) {
                $request->photo3 = $uploade->uploadImage($request->photo3, 'event', Str::random(10));
            }
            if (!empty($request->photo4)) {
                $request->photo4 = $uploade->uploadImage($request->photo4, 'event', Str::random(10));
            }

            //TODO::pdf
            if (!empty($request->pdf1)) {
                $pdf = $uploade->uploadPDF($request->pdf1, 'event/pdf', Str::random(10));
                $request->pdf1 = $pdf['path'];
                $request->pdf_size1 = $pdf['file_size'];
            }
            if (!empty($request->pdf2)) {
                $pdf = $uploade->uploadPDF($request->pdf2, 'event/pdf', Str::random(10));
                $request->pdf2 = $pdf['path'];
                $request->pdf_size2 = $pdf['file_size'];
            }
            if (!empty($request->pdf3)) {
                $pdf = $uploade->uploadPDF($request->pdf3, 'event/pdf', Str::random(10));
                $request->pdf3 = $pdf['path'];
                $request->pdf_size3 = $pdf['file_size'];
            }

            if (!empty($request->pdf_only)) {
                $pdf = $uploade->uploadPDF($request->pdf_only, 'event/pdf', Str::random(10));
                $request->pdf_only = $pdf['path'];
                $request->pdf_size_only = $pdf['file_size'];
            }

            $data = $this->topic_repo->storeEvent($request);

            return redirect()->route('event.index');

        } catch (\Exception $e) {
            return $message = $e->getMessage();
        }
    }

    public function edit(Request $request, $id)
    {
        $code = substr($id, -6);
        $id = intval($code);

        $event = $this->topic_repo->find($id);

        return view('admin.event-add-edit', compact('event'));
    }

    public function update(Request $request, $ref_id)
    {
        try {
            //TODO::image
            $uploade = new UploadeFileController();
            if (!empty($request->photo1)) {
                $request->photo1 = $uploade->uploadImage($request->photo1, 'event', Str::random(10));
            }
            if (!empty($request->photo2)) {
                $request->photo2 = $uploade->uploadImage($request->photo2, 'event', Str::random(10));
            }
            if (!empty($request->photo3)) {
                $request->photo3 = $uploade->uploadImage($request->photo3, 'event', Str::random(10));
            }
            if (!empty($request->photo4)) {
                $request->photo4 = $uploade->uploadImage($request->photo4, 'event', Str::random(10));
            }

            //TODO::pdf
            if (!empty($request->pdf1)) {
                $pdf = $uploade->uploadPDF($request->pdf1, 'event/pdf', Str::random(10));
                $request->pdf1 = $pdf['path'];
                $request->pdf_size1 = $pdf['file_size'];
            }
            if (!empty($request->pdf2)) {
                $pdf = $uploade->uploadPDF($request->pdf2, 'event/pdf', Str::random(10));
                $request->pdf2 = $pdf['path'];
                $request->pdf_size2 = $pdf['file_size'];
            }
            if (!empty($request->pdf3)) {
                $pdf = $uploade->uploadPDF($request->pdf3, 'event/pdf', Str::random(10));
                $request->pdf3 = $pdf['path'];
                $request->pdf_size3 = $pdf['file_size'];
            }


            $data = $this->topic_repo->find($ref_id);
            $this->topic_repo->valiableEvent($data, $request);

            return redirect()->back()->with([
                'message'    => __('voyager::generic.successfully_updated'),
                'alert-type' => 'success',
            ]);

        } catch (\Exception $e) {
            return $message = $e->getMessage();
        }
    }

    public function delete(Request $request)
    {
        $code = substr($request->id, -6);
        $id = intval($code);

        $data = $this->topic_repo->delete($id);

        return response([
            'message'    => __('voyager::generic.successfully_updated'),
            'alert-type' => 'success',
        ]);
    }

    public function updateStatus(Request $request)
    {
        $code = substr($request->id, -6);
        $id = intval($code);

        $data = $this->topic_repo->status($request, $id);

        return response([
            'message'    => __('voyager::generic.successfully_updated'),
            'alert-type' => 'success',
        ]);
    }

}
