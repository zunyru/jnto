<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\UploadeFileController;
use App\Http\Requests\StoreNewsJntoRequest;
use App\Repositories\BusinessTypeRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\NewsJntoRepository;
use App\Repositories\ProvinceRepository;
use App\Repositories\SeasonRepository;
use App\Repositories\ServiceRepository;
use App\Repositories\SupportMenuRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

class NewsController extends Controller
{
    protected $news_repo;
    protected $company_repo;
    protected $business_type_repo;
    protected $language_repo;
    protected $support_menu_repo;
    protected $province_repo;
    protected $category_repo;
    protected $service_repo;
    protected $season_repo;

    public function __construct(
        NewsJntoRepository     $newsJnto_repo,
        CompanyRepository      $company_repo,
        BusinessTypeRepository $business_type_repo,
        LanguageRepository     $language_repo,
        SupportMenuRepository  $support_menu_repo,
        ProvinceRepository     $province_repo,
        CategoryRepository     $category_repo,
        ServiceRepository      $service_repo,
        SeasonRepository       $season_ropo
    )
    {
        $this->news_repo = $newsJnto_repo;
        $this->company_repo = $company_repo;
        $this->business_type_repo = $business_type_repo;
        $this->language_repo = $language_repo;
        $this->support_menu_repo = $support_menu_repo;
        $this->province_repo = $province_repo;
        $this->category_repo = $category_repo;
        $this->service_repo = $service_repo;
        $this->season_repo = $season_ropo;
    }

    public function index(Request $request)
    {
        return view('admin.news-jnto');
    }

    public function dataIndex(Request $request)
    {
        $news = $this->news_repo->getPaginate($request);

        foreach ($news as $key => $item) {

            $edit_btn = "";
            if (auth()->user()->role->name == 'Viewer') {
                $edit_btn .= "<a href=\"" . route('news-jnto.edit.admin', $item->ref_id) . "\" data=\"$item->id\" class=\"bg-orange-500 hover:bg-orange-700 focus:border-orange-400 active:border-orange-700 text-white font-bold py-3 px-4 rounded edited\">View</a>";
            }else {
                $edit_btn .= "<a href=\"" . route('news-jnto.edit.admin', $item->ref_id) . "\" data=\"$item->id\" class=\"bg-orange-500 hover:bg-orange-700 focus:border-orange-400 active:border-orange-700 text-white font-bold py-3 px-4 rounded edited\">Edit</a>";
            }

            $delete_btn = "";
            $delete_btn .= "<a href=\"#\" onclick=\"delete_item(this);\" data-id=\"$item->id\" class=\"bg-red-500 hover:bg-red-700 focus:border-red-400 active:border-red-700 text-white font-bold py-3 px-4 rounded deleted\">Delete</a>";

            $select_srt = "";
            $selected_confirm = ($item->status == 'draf') ? 'selected' : '';
            $selected_published = ($item->status == 'published') ? 'selected' : '';
            if (auth()->user()->role->name == 'Viewer') {
                $status = $item->status == 'draf' ? 'Draf' : 'Published';
                $select_srt .= $status;
            } else {
                $select_srt .= "<select name=\"status\" data-id=\"{$item->id}\" onchange=\"update_staus(this);\" class=\"select_status block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500\"><option " . $selected_confirm . " value=\"draft\">Draft</option><option " . $selected_published . " value=\"published\">Published</option></select>";
            }

            if ($item->template == 'full-content') {
                $template = 'Full content';
            } else if ($item->template == 'link-only') {
                $template = 'Link only';
            } else if ($item->template == 'PDF-only') {
                $template = 'File only';
            }

            $column[$key]['no'] = $news->firstItem() + $key;
            $column[$key]['id'] = $item->ref_id;
            $column[$key]['title'] = $item->title;
            $column[$key]['template'] = $template;
            $column[$key]['status'] = $select_srt;
            $column[$key]['edit_btn'] = $edit_btn;
            if (auth()->user()->role->name != 'Viewer') {
                $column[$key]['delete_btn'] = $delete_btn;
            }
        }

        $data['data'] = $column ?? [];
        $data['recordsTotal'] = $news->total();
        $data['recordsFiltered'] = $news->total();
        $data['draw'] = $request->draw;

        return response($data);
    }

    public function all_jnto(Request $request)
    {
        $news = $this->news_repo->getPaginate(
            array_merge([
                'public' => true,
            ])
        );

        return view('frontend.pages.news-jnto',
            compact(
                'news'
            )
        );
    }

    public function create()
    {
        return view('admin.news-info-add-edit');
    }

    public function edit(Request $request, $id)
    {
        $code = substr($id, -6);
        $id = intval($code);

        $news = $this->news_repo->find($id);

        return view('admin.news-info-add-edit', compact('news'));
    }

    public function store(StoreNewsJntoRequest $request)
    {
        try {
            //TODO::image
            $uploade = new UploadeFileController();
            if (!empty($request->photo1)) {
                $request->photo1 = $uploade->uploadImage($request->photo1, 'news', Str::random(10));
            }
            if (!empty($request->photo2)) {
                $request->photo2 = $uploade->uploadImage($request->photo2, 'news', Str::random(10));
            }
            if (!empty($request->photo3)) {
                $request->photo3 = $uploade->uploadImage($request->photo3, 'news', Str::random(10));
            }
            if (!empty($request->photo4)) {
                $request->photo4 = $uploade->uploadImage($request->photo4, 'news', Str::random(10));
            }

            //TODO::pdf
            if (!empty($request->pdf1)) {
                $pdf = $uploade->uploadPDF($request->pdf1, 'news/pdf', Str::random(10));
                $request->pdf1 = $pdf['path'];
                $request->pdf_size1 = $pdf['file_size'];
            }
            if (!empty($request->pdf2)) {
                $pdf = $uploade->uploadPDF($request->pdf2, 'news/pdf', Str::random(10));
                $request->pdf2 = $pdf['path'];
                $request->pdf_size2 = $pdf['file_size'];
            }
            if (!empty($request->pdf3)) {
                $pdf = $uploade->uploadPDF($request->pdf3, 'news/pdf', Str::random(10));
                $request->pdf3 = $pdf['path'];
                $request->pdf_size3 = $pdf['file_size'];
            }

            if (!empty($request->pdf_only)) {
                $pdf = $uploade->uploadPDF($request->pdf_only, 'news/pdf', Str::random(10));
                $request->pdf_only = $pdf['path'];
                $request->pdf_size_only = $pdf['file_size'];
            }

            $data = $this->news_repo->store($request);

            return redirect()->route('news-jnto.index');

        } catch (\Exception $e) {
            return $message = $e->getMessage();
        }
    }

    public function update(StoreNewsJntoRequest $request, $ref_id)
    {
        try {
            //TODO::image
            $uploade = new UploadeFileController();
            if (!empty($request->photo1)) {
                $request->photo1 = $uploade->uploadImage($request->photo1, 'news', Str::random(10));
            }
            if (!empty($request->photo2)) {
                $request->photo2 = $uploade->uploadImage($request->photo2, 'news', Str::random(10));
            }
            if (!empty($request->photo3)) {
                $request->photo3 = $uploade->uploadImage($request->photo3, 'news', Str::random(10));
            }
            if (!empty($request->photo4)) {
                $request->photo4 = $uploade->uploadImage($request->photo4, 'news', Str::random(10));
            }

            //TODO::pdf
            if (!empty($request->pdf1)) {
                $pdf = $uploade->uploadPDF($request->pdf1, 'news/pdf', Str::random(10));
                $request->pdf1 = $pdf['path'];
                $request->pdf_size1 = $pdf['file_size'];
            }
            if (!empty($request->pdf2)) {
                $pdf = $uploade->uploadPDF($request->pdf2, 'news/pdf', Str::random(10));
                $request->pdf2 = $pdf['path'];
                $request->pdf_size2 = $pdf['file_size'];
            }
            if (!empty($request->pdf3)) {
                $pdf = $uploade->uploadPDF($request->pdf3, 'news/pdf', Str::random(10));
                $request->pdf3 = $pdf['path'];
                $request->pdf_size3 = $pdf['file_size'];
            }

            if (!empty($request->pdf_only)) {
                $pdf = $uploade->uploadPDF($request->pdf_only, 'news/pdf', Str::random(10));
                $request->pdf_only = $pdf['path'];
                $request->pdf_size_only = $pdf['file_size'];
            }

            $data = $this->news_repo->find($ref_id);
            $this->news_repo->valuable($data, $request);

            return redirect()->back()->with([
                'message'    => __('voyager::generic.successfully_updated'),
                'alert-type' => 'success',
            ]);

        } catch (\Exception $e) {
            return $message = $e->getMessage();
        }
    }

    public function show($ref_id)
    {
        $preview = false;
        if (Route::getCurrentRoute()->getName() === 'news-jnto.show.preview') {
            $preview = true;
        }

        $news = $this->news_repo->getDetail($ref_id, $preview);

        $detail = true;
        $news_other = $this->news_repo->getOtherShow($ref_id, 3, $preview);

        return view('frontend.pages.news-jnto-detail',
            compact(
                'news',
                'news_other',
                'detail'
            )
        );
    }

    public function delete(Request $request)
    {
        $code = substr($request->id, -6);
        $id = intval($code);

        $data = $this->news_repo->delete($id);

        return response([
            'message'    => __('voyager::generic.successfully_updated'),
            'alert-type' => 'success',
        ]);
    }

    public function updateStatus(Request $request)
    {
        $code = substr($request->id, -6);
        $id = intval($code);

        $data = $this->news_repo->status($request, $id);

        return response([
            'message'    => __('voyager::generic.successfully_updated'),
            'alert-type' => 'success',
        ]);
    }
}
