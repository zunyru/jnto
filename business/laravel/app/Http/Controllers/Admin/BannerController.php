<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\UploadeFileController;
use App\Repositories\BannerRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class BannerController extends Controller
{
    protected $banner_repo;

    public function __construct(BannerRepository $bannerRepository)
    {
        $this->banner_repo = $bannerRepository;
    }

    public function index(Request $request)
    {
        return view('admin.banner');
    }

    public function dataIndex(Request $request)
    {
        $datas = $this->banner_repo->getPaginate($request);

        foreach ($datas as $key => $item) {
            $edit_btn = "";
            if (auth()->user()->role->name == 'Viewer') {
                $edit_btn .= "<a href=\"" . route('banner.edit.admin', $item->id) . "\" data=\"$item->id\" class=\"bg-orange-500 hover:bg-orange-700 focus:border-orange-400 active:border-orange-700 text-white font-bold py-3 px-4 rounded edited\">View</a>";
            } else {
                $edit_btn .= "<a href=\"" . route('banner.edit.admin', $item->id) . "\" data=\"$item->id\" class=\"bg-orange-500 hover:bg-orange-700 focus:border-orange-400 active:border-orange-700 text-white font-bold py-3 px-4 rounded edited\">Edit</a>";
            }

            $delete_btn = "";
            $delete_btn .= "<a href=\"#\" onclick=\"delete_item(this);\" data-id=\"$item->id\" class=\"bg-red-500 hover:bg-red-700 focus:border-red-400 active:border-red-700 text-white font-bold py-3 px-4 rounded deleted\">Delete</a>";

            $select_srt = "";
            $selected_confirm = ($item->status == 'draf') ? 'selected' : '';
            $selected_published = ($item->status == 'published') ? 'selected' : '';
            if (auth()->user()->role->name == 'Viewer') {
                $select_srt .= ($item->status == 'draf') ? 'Draf' : 'Published';
            } else {
                $select_srt .= "<select name=\"status\" data-id=\"{$item->id}\" onchange=\"update_staus(this);\" class=\"select_status block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500\"><option " . $selected_confirm . " value=\"confirm\">Draft</option><option " . $selected_published . " value=\"published\">Published</option></select>";
            }
            $select_target = "";
            $selected_target_all = ($item->target == 'seller') ? 'selected' : '';
            $selected_target_buyer = ($item->target == 'buyer') ? 'selected' : '';
            if (auth()->user()->role->name == 'Viewer') {
                $select_target .= ($item->status == 'seller') ? 'Seller' : 'Buyer';
            } else {
                $select_target .= "<select name=\"target\" data-id=\"{$item->id}\" onchange=\"update_staus_target(this);\" class=\"select_status block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500\"><option " . $selected_target_all . " value=\"all\">Japan seller</option><option " . $selected_target_buyer . " value=\"buyer\">Thai buyer</option></select>";
            }

            $thumbnail = '-';
            if ($item->thumbnail) {
                $image = asset($item->thumbnail);
                $thumbnail = "<img src=\"{$image}\" class='w-35'>";
            }

            $column[$key]['no'] = $datas->firstItem() + $key;
            $column[$key]['url'] = $item->url;
            $column[$key]['thumbnail'] = $thumbnail;
            $column[$key]['target'] = $select_target;
            $column[$key]['status'] = $select_srt;
            $column[$key]['edit_btn'] = $edit_btn;
            if (auth()->user()->role->name != 'Viewer') {
                $column[$key]['delete_btn'] = $delete_btn;
            }
        }

        $data['data'] = $column ?? [];
        $data['recordsTotal'] = $datas->total();
        $data['recordsFiltered'] = $datas->count();
        $data['draw'] = $request->draw;

        return response($data);
    }

    public function create()
    {
        return view('admin.banner-add-edit');
    }

    public function store(Request $request)
    {
        try {
            $uploade = new UploadeFileController();
            if (!empty($request->thumbnail)) {
                $request->thumbnail = $uploade->uploadImage($request->thumbnail, 'banner', Str::random(10));
            }

            $data = $this->banner_repo->store($request);

            return redirect()->route('banner.index');

        } catch (\Exception $e) {
            return $message = $e->getMessage();
        }
    }

    public function edit($id)
    {
        $banner = $this->banner_repo->find($id);

        return view('admin.banner-add-edit', compact('banner'));
    }

    public function update(Request $request, $id)
    {
        try {
            $data = $this->banner_repo->find($id);
            $uploade = new UploadeFileController();
            if (!empty($request->thumbnail)) {
                $request->thumbnail = $uploade->uploadImage($request->thumbnail, 'banner', Str::random(10));
            }
            $this->banner_repo->valuable($data, $request);

            return redirect()->route('banner.index');

        } catch (\Exception $e) {
            return $message = $e->getMessage();
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;

        $this->banner_repo->delete($id);

        return response([
            'message'    => __('voyager::generic.successfully_updated'),
            'alert-type' => 'success',
        ]);
    }

    public function updateStatus(Request $request)
    {
        $id = $request->id;

        $data = $this->banner_repo->status($request, $id);

        return response([
            'message'    => __('voyager::generic.successfully_updated'),
            'alert-type' => 'success',
        ]);
    }

    public function updateStatusTarget(Request $request)
    {
        $id = $request->id;

        $data = $this->banner_repo->statusTarget($request, $id);

        return response([
            'message'    => __('voyager::generic.successfully_updated'),
            'alert-type' => 'success',
        ]);
    }
}
