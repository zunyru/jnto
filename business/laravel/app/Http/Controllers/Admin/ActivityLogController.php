<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\ActivityLogRepository;
use Illuminate\Http\Request;

class ActivityLogController extends Controller
{
    protected $log_repo;

    public function __construct(ActivityLogRepository $activityLogRepository)
    {
        $this->log_repo = $activityLogRepository;
    }

    public function index(Request $request)
    {
        return view('admin.activity-log');
    }

    public function dataIndex(Request $request)
    {
        $datas = $this->log_repo->getPaginate($request);

        foreach ($datas as $key => $item) {
            $view_btn = "";
            $view_btn .= "<a href=\"" . route('log.view', $item->id) . "\" data=\"$item->id\" class=\"bg-orange-500 hover:bg-orange-700 focus:border-orange-400 active:border-orange-700 text-white font-bold py-3 px-4 rounded viewed\">View</a>";

            $user = "";
            if (!empty($item->subject_type)) {
                if ($item->subject_type == 'App\Models\Company' || $item->subject_type == 'App\Models\CompanySupportMenu') {
                    $user = "Seller : {$item->subject_id}";
                } else {
                    $user = "{$item->subject_type} : {$item->subject_id}";
                }
            }
            if (!empty($item->causer_type)) {
                if ($item->causer_type == 'App\Models\Company') {
                    $user = "Seller : {$item->causer_id}";
                } else {
                    if (!empty($item->causer_type) && !empty($item->causer_id))
                        $user = "{$item->causer_type} : {$item->causer_id}";
                }
            }
            $column[$key]['no'] = $datas->firstItem() + $key;
            $column[$key]['log_name'] = $item->log_name;
            $column[$key]['user'] = $user;
            $column[$key]['description'] = $item->description;
            $column[$key]['updated_at'] = $item->updated_at->format('d-m-Y H:i:s');
            $column[$key]['view_btn'] = $view_btn;
        }

        $data['data'] = $column ?? [];
        $data['recordsTotal'] = $datas->total();
        $data['recordsFiltered'] = $datas->total();
        $data['draw'] = $request->draw;

        return response($data);
    }

    public function show($id)
    {
        $log = $this->log_repo->find($id);

        return view('admin.activity-log-view', compact('log'));
    }
}
