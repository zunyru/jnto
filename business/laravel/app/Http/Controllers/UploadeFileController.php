<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class UploadeFileController extends Controller
{
    public function uploadImage($image, $path, $name_file)
    {
        $extension = $image->getClientOriginalExtension();
        $name_file = str_replace(']', '-', $name_file);
        $name_file = str_replace('[', '-', $name_file);
        $name_file = str_replace('*', '-', $name_file);
        $name_file = str_replace('"', '', $name_file);
        $name_file = str_replace("'", "-", $name_file);
        $name_file = str_replace(".", "-", $name_file);
        $name_file = str_replace("/", "-", $name_file);
        $name_file = str_replace("#", "-", $name_file);
        $name_file = str_replace('"', '', $name_file);
        $name_file = str_replace('(', '-', $name_file);
        $name_file = str_replace(')', '-', $name_file);
        $this->filesystem = config('voyager.storage.disk');

        try {
            $ramdom = Str::random(5);
            $name_img = $name_file . '_' . $ramdom;
            $file = $image->storeAs($path, $name_img . '.' . $extension, $this->filesystem);

            $thumbnail = Image::make($file);

            $thumbnail = $thumbnail->resize(
                710, 417
            );
            $thumbnail->save($path . '/' . $thumbnail->filename . '-thumbnail' . '.' . $extension);

        } catch (\Exception $e) {
            $message = $e->getMessage();
            throw new \Exception($message);
        }

        return $path = preg_replace('/^public\//', '', $file);
    }

    public function uploadPDF($pdf, $path, $name_file)
    {
        $extension = $pdf->getClientOriginalExtension();
        $this->filesystem = config('voyager.storage.disk');

        $name_file = str_replace("." . $extension, "", $pdf->getClientOriginalName());
        $name_file = str_replace(']', '-', $name_file);
        $name_file = str_replace('[', '-', $name_file);
        $name_file = str_replace('*', '-', $name_file);
        $name_file = str_replace('"', '', $name_file);
        $name_file = str_replace("'", "-", $name_file);
        $name_file = str_replace(".", "-", $name_file);
        $name_file = str_replace("/", "-", $name_file);
        $name_file = str_replace("#", "-", $name_file);
        $name_file = str_replace('"', '', $name_file);
        $name_file = str_replace('(', '-', $name_file);
        $name_file = str_replace(')', '-', $name_file);
        try {
            $ramdom = Str::random(5);
            $file = $pdf->storeAs($path, $name_file . '_' . $ramdom . '.' . $extension, $this->filesystem);
            $file_size = filesize($file);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $path = '';
        }

        $path = preg_replace('/^public\//', '', $file);

        return [
            'file_size' => $file_size,
            'path' => $path
        ];
    }

    public function uploadFile($pdf, $path, $name_file)
    {
        $extension = $pdf->getClientOriginalExtension();
        $this->filesystem = config('voyager.storage.disk');

        $name_file = str_replace("." . $extension, "", $pdf->getClientOriginalName());
        $name_file = str_replace(']', '-', $name_file);
        $name_file = str_replace('[', '-', $name_file);
        $name_file = str_replace('*', '-', $name_file);
        $name_file = str_replace('"', '', $name_file);
        $name_file = str_replace("'", "-", $name_file);
        $name_file = str_replace(".", "-", $name_file);
        $name_file = str_replace("/", "-", $name_file);
        $name_file = str_replace("#", "-", $name_file);
        $name_file = str_replace('"', '', $name_file);
        $name_file = str_replace('(', '-', $name_file);
        $name_file = str_replace(')', '-', $name_file);
        try {
            $ramdom = Str::random(5);
            $file = $pdf->storeAs($path, $name_file . '_' . $ramdom . '.' . $extension, $this->filesystem);
            $file_size = filesize($file);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $path = '';
        }

        return $path = preg_replace('/^public\//', '', $file);
    }

    public function fileDelete(Request $request)
    {
        $code = substr($request->id, -6);
        $id = intval($code);

        DB::table($request->db)
            ->where("{$request->where}", $id)
            ->update([
                "{$request->field}" => null
            ]);

        return response([
            'message' => __('voyager::generic.successfully_updated'),
            'alert-type' => 'success',
        ]);
    }
}
