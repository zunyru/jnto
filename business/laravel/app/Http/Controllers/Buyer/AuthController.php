<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function showLoginFormBuyer()
    {
        session()->forget('url_previous');
        session(['url_previous' => url()->previous()]);

        $uri = str_replace(config('app.url'), '', session('url_previous'));

        if (url()->full() == session('url_previous') || $uri == '/login' || $uri == '/admin/login' || $uri == '/seller/login' || $uri == '') {
            session()->forget('url_previous');
        }

        return view('buyer.login');
    }

    public function login(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);

        if ($this->attemptLogin($request)) {
            $user = Auth::guard('web')->user();
            if ($user->role->name == 'Access web') {

                activity('Buyer Login')
                    ->causedBy($user)
                    ->withProperties([
                        'ip' => request()->ip()
                    ])
                    ->log('login');
                if (session()->has('url_previous')) {
                    if (!empty(strpos(session('url_previous'), '/business/'))) {
                        return redirect(session('url_previous'));
                    }
                }
                return redirect()->route('home');

            }
            $this->guard()->logout();
            return redirect()->back()->withErrors([
                'username' => __('auth.failed'),
                'password' => __('auth.password')
            ]);
        }

        return $this->sendFailedLoginResponse($request);
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();
        session()->forget('url_previous');
        if ($response = $this->loggedOut($request)) {
            return $response;
        }

        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : redirect('/');
    }

    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    protected function guard()
    {
        return Auth::guard('web');
    }

    public function username()
    {
        return 'username';
    }
}
