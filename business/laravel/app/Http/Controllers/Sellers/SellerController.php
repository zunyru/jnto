<?php

namespace App\Http\Controllers\Sellers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Seller\PasswordRequest;
use App\Jobs\Seller\SendMailConfirmChangeEmail;
use App\Jobs\Seller\SendMailResetPassword;
use App\Jobs\Seller\SendMailSetPassword;
use App\Jobs\Seller\SendMailSignupSuccessful;
use App\Repositories\BusinessTypeRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\ProvinceRepository;
use App\Repositories\ServiceRepository;
use App\Repositories\SupportMenuRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Activitylog\Models\Activity;

class SellerController extends Controller
{
    protected $seller_repo;
    protected $business_type_repo;
    protected $language_repo;
    protected $support_menu_repo;
    protected $province_repo;
    protected $category_repo;
    protected $service_repo;

    public function __construct(
        CompanyRepository      $companyRepository,
        BusinessTypeRepository $business_type_repo,
        LanguageRepository     $language_repo,
        SupportMenuRepository  $support_menu_repo,
        ProvinceRepository     $province_repo,
        CategoryRepository     $category_repo,
        ServiceRepository      $service_repo
    )
    {
        $this->seller_repo = $companyRepository;
        $this->business_type_repo = $business_type_repo;
        $this->language_repo = $language_repo;
        $this->support_menu_repo = $support_menu_repo;
        $this->province_repo = $province_repo;
        $this->category_repo = $category_repo;
        $this->service_repo = $service_repo;
    }

    public function checkHasEmail(Request $request)
    {
        return $this->seller_repo->checkHasUser($request->email);
    }

    public function register(Request $request)
    {
        $seller = $this->seller_repo->findUsername($request->email);
        if ($seller) {

            if (!empty($seller->email_signup_successful)) {
                return redirect()->back()->withErrors(['regis' => 'คุณเคยสมัครไปแล้ว']);
            }

            $token = Str::random(60);

            $seller->forceFill([
                'remember_token' => hash('sha256', $token)
            ])->save();

            //TODO::send email
            SendMailSetPassword::dispatchSync($seller->id, "JNTO B2B Website : Set Password", "set-password");

            return view('sellers.register-email-send');
        }
        return redirect()->back()->withInput()
            ->withErrors(['Account not Active']);
    }

    public function setPasswordPage(string $token)
    {
        $seller = $this->seller_repo->findToken($token);

        return view('sellers.register-set-password', compact('token'));
    }

    public function setPassword(PasswordRequest $request)
    {
        $seller = $this->seller_repo->findToken($request->remember_token);

        if (!empty($seller->email_signup_successful)) {
            return redirect()->back()->withErrors(['regis' => 'คุณเคยสมัครไปแล้ว']);
        }

        $request->last_login = Carbon::now();
        $this->seller_repo->valuable($seller, $request);

        activity('Seller Set New Password')
            ->withProperties([
                'ip' => request()->ip()
            ])
            ->causedBy($seller)
            ->log('set new password');

        //TODO::auto login
        Auth::guard('seller')->login($seller);

        //TODO::send mail signup_successful
        SendMailSignupSuccessful::dispatchSync($seller->id, "JNTO B2B Website : Password Successfully Set", "registration-successfull");

        return view('sellers.register-thanks');
    }

    public function myPage()
    {
        $id = Auth::guard('seller')->user()->id;
        $company = $this->seller_repo->find($id);

        $business_pivot = [];
        if ($company->business_type_companies()) {
            $business_type_companies = $company->business_type_companies()->get();
            foreach ($business_type_companies as $item) {
                $business_pivot[] = $item['pivot']['business_type_id'];
            }
        }

        $language_pivot = [];
        if ($company->company_language()) {
            $company_languages = $company->company_language()->get();
            foreach ($company_languages as $item) {
                $language_pivot[] = $item['pivot']['language_id'];
            }
        }

        $language_branch_pivot = [];
        if ($company->company_language_for_branch()) {
            $company_languages = $company->company_language_for_branch()->get();
            foreach ($company_languages as $item) {
                $language_branch_pivot[] = $item['pivot']['language_for_branch_id'];
            }
        }

        $services = $this->service_repo->findByCompanyID($id);

        $suports = $this->support_menu_repo->findByCompanyID($id);

        $company_region_pivot = [];
        $company_region_pivot_name = [];

        if ($company->company_region()) {
            $company_region = $company->company_region()->get();
            foreach ($company_region as $item) {
                $company_region_pivot[] = $item['pivot']['region_id'];
                $company_region_pivot_name[] = $item['name_en'];
            }
        }

        $company_season_pivot = [];
        if ($company->company_season()) {
            $company_season = $company->company_season()->get();
            foreach ($company_season as $item) {
                $company_season_pivot[] = $item['pivot']['season_id'];
            }
        }

        $company_special_feature_pivot = [];
        if ($company->company_special_feature()) {
            $company_special_feature = $company->company_special_feature()->get();
            foreach ($company_special_feature as $item) {
                $company_special_feature_pivot[] = $item['pivot']['special_feature_id'];
            }
        }

        $company_type_of_travel_pivot = [];
        if ($company->company_type_of_travel()) {
            $company_type_of_travel = $company->company_type_of_travel()->get();
            foreach ($company_type_of_travel as $item) {
                $company_type_of_travel_pivot[] = $item['pivot']['type_of_travel_id'];
            }
        }

        $company_special_experience_pivot = [];
        if ($company->company_special_experience()) {
            $company_special_experience = $company->company_special_experience()->get();
            foreach ($company_special_experience as $item) {
                $company_special_experience_pivot[] = $item['pivot']['special_experience_id'];
            }
        }

        $business_types = $this->business_type_repo->getAll();
        $languages = $this->language_repo->getAll();
        $languages_for_branches = $this->language_repo->getBranchAll();
        $support_menus = $this->support_menu_repo->getAll();
        $provinces = $this->province_repo->getAllGroupProvince();
        $seasons = $this->category_repo->getSeasonAll();
        $special_features = $this->category_repo->getSpecialFeatureAll();
        $type_of_traves = $this->category_repo->getTypeOfTravelAll();
        $special_experiences = $this->category_repo->getSpecialExperienceAll();

        return view('sellers.mypage.mypage', compact(
            'business_types',
            'languages',
            'languages_for_branches',
            'support_menus',
            'provinces',
            'seasons',
            'special_features',
            'type_of_traves',
            'special_experiences',
            'company',
            'business_pivot',
            'language_pivot',
            'services',
            'suports',
            'language_branch_pivot',
            'company_region_pivot',
            'company_season_pivot',
            'company_special_feature_pivot',
            'company_type_of_travel_pivot',
            'company_special_experience_pivot',
            'company_region_pivot_name',
            'id',
        ));
    }

    public function loginHistory()
    {
        $model = Auth::guard('seller')->user();

        $histories = Activity::query()
            ->orderByDesc('created_at')
            ->inLog(['Seller Auth'])
            ->causedBy($model)
            ->paginate(20);

        return view('sellers.mypage.login-history', compact('histories'));
    }

    public function forgotPassword(Request $request)
    {
        $this->validate($request, [
            'emial' => 'email',
        ]);

        $seller = $this->seller_repo->findUsername($request->email);
        $request->last_login = Carbon::now();
        $this->seller_repo->valuable($seller, $request);

        activity('Seller forgot Password')
            ->withProperties([
                'ip' => request()->ip()
            ])
            ->causedBy($seller)
            ->log('set forgot password');

        //TODO::Send email reset passowrd
        SendMailResetPassword::dispatchSync($seller->id, 'JNTO B2B Website : Reset Password', 'reset-password');

        return view('sellers.reset-email-send');
    }

    public function resetPasswordPage(string $token)
    {
        if (empty($token)) {
            abort(403);
        }
        $seller = $this->seller_repo->findToken($token);

        return view('sellers.reset-password', compact('token'));
    }

    public function resetPassword(PasswordRequest $request)
    {
        $seller = $this->seller_repo->findToken($request->remember_token);

        if (Hash::check($request->password, $seller->password)) {
            return redirect()->back()->withErrors(['password' => '入力されたパスワードは過去に使用しているため設定できません。他のパスワードをご使用ください。<br>กรุณาตั้งรหัสที่คุณยังไม่เคยใช้ในเว็บไซต์นี้']);
        }

        $request->last_login = Carbon::now();
        $this->seller_repo->valuable($seller, $request);

        activity('Seller Reset Password')
            ->withProperties([
                'ip' => request()->ip()
            ])
            ->causedBy($seller)
            ->log('set reset password');

        //TODO::auto login
        Auth::guard('seller')->login($seller);

        return view('sellers.reset-password-thanks');
    }

    public function renewPassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required'
        ]);

        $seller = $this->seller_repo->findUsername($request->user_login);

        if (Hash::check($request->password, $seller->password)) {
            return redirect()->back()->withErrors(['password' => '過去に使用したパスワードは使用できません。<br>กรุณาตั้งรหัสที่คุณยังไม่เคยใช้ในเว็บไซต์นี้']);
        }
        $request->last_login = Carbon::now();
        $this->seller_repo->valuable($seller, $request);
        Auth::guard('seller')->login($seller);

        return redirect()->route('seller.mypage');
    }

    public function privicySetting()
    {
        $seller = Auth::guard('seller')->user();

        return view('sellers.mypage.privacy-setting', compact('seller'));
    }

    public function privicySettingUpdate(Request $request)
    {
        $seller = Auth::guard('seller')->user();

        if ($seller->privacy != $request->privacy) {
            activity()
                ->causedBy($seller->getModel())
                ->withProperties([
                    'old' => [
                        'privacy' => $seller->privacy,
                    ],
                    'attributes' => [
                        'privacy' => $request->privacy,
                    ]
                ])
                ->inLog('Seller change privacy')
                ->log('Seller change privacy');
        }

        $this->seller_repo->valuable($seller, $request);

        $redirect = redirect()->back();
        return $redirect->with([
            'message' => __('voyager::generic.successfully_updated'),
            'alert-type' => 'success',
        ]);
    }

    public function changeEmail(Request $request)
    {
        $this->validate($request, [
            'email_change' => 'email|required|unique:companies,user_login',
            're_email' => 'email|required',
            'password' => [
                'required',
                'min:12',
                'regex:/^.*(?=.{12,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!"#$%&()*+,\-.\:\;<=>?@[\]^_`{|}~\']).*$/',
            ],
        ], [
            'email_change.unique' => 'このメールアドレスは既に使われています。อีเมลนี้ถูกใช้ไปแล้ว '
        ]);

        $seller = $this->seller_repo->find(Auth::guard('seller')->user()->id);


        if (!Hash::check($request->password, $seller->password)) {
            return redirect()->back()->withErrors(['password' => 'パスワードが間違っています。รหัสผ่านไม่ถูกต้อง']);
        }

        activity()
            ->causedBy($seller->getModel())
            ->withProperties([
                'old' => [
                    'email' => $seller->email,
                ],
                'attributes' => [
                    'email' => $request->email_change,
                ]
            ])
            ->inLog('Seller change E-mail')
            ->log('Seller change E-mail');

        $this->seller_repo->valuable($seller, $request);

        //send confirm email
        SendMailConfirmChangeEmail::dispatchSync($seller->id, 'JNTO B2B Website : Change Login E-mail', 'change-email');

        $redirect = redirect()->route('seller.change-email.page');
        return $redirect->with([
            'message' => __('voyager::generic.successfully_updated'),
            'alert-type' => 'success',
        ]);
    }

    public function changeEmailPassword($token)
    {
        $this->seller_repo->findToken($token);
        return view('sellers.change-email-password', compact('token'));
    }

    public function changeEmailConfirm(Request $request)
    {
        $this->validate($request, [
            'password' => [
                'required',
                'min:12',
                'regex:/^.*(?=.{12,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!"#$%&()*+,\-.\:\;<=>?@[\]^_`{|}~\']).*$/',
            ],
            'new_password' => [
                'required',
                'confirmed',
                'min:12',
                'regex:/^.*(?=.{12,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!"#$%&()*+,\-.\:\;<=>?@[\]^_`{|}~\']).*$/',
            ],
            'new_password_confirmation' => 'required|min:12',
        ]);

        $seller = $this->seller_repo->findToken($request->token);

        if (!Hash::check($request->password, $seller->password)) {
            return redirect()->back()->withErrors(['password' => 'パスワードが間違っています。รหัสผ่านไม่ถูกต้อง']);
        }
        if (Hash::check($request->new_password, $seller->password)) {
            return redirect()->back()->withErrors(['new_password' => '過去に使用したパスワードは使用できません。<br>กรุณาตั้งรหัสที่คุณยังไม่เคยใช้ในเว็บไซต์นี้']);
        }

        activity()
            ->causedBy($seller->getModel())
            ->withProperties([
                'old' => [
                    'email' => $seller->email,
                ],
                'attributes' => [
                    'email' => $seller->email_change,
                ]
            ])
            ->inLog('Seller change Email confirm')
            ->log('Seller change Email confirm');

        $make_token = Str::random(60);
        $email_change = $seller->email_change;
        $seller->user_login = !empty($email_change) ? $email_change : $seller->user_login;
        $seller->email_change = null;
        $seller->password = Hash::make($request->new_password);
        $seller->remember_token = hash('sha256', $make_token);

        $seller->save();


        Auth::guard('seller')->login($seller);

        return view('sellers.change-email-password-thanks');
    }

    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'password' => [
                'required',
                'min:12',
                'regex:/^.*(?=.{12,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!"#$%&()*+,\-.\:\;<=>?@[\]^_`{|}~\']).*$/',
            ],
            'new_password' => [
                'required',
                'confirmed',
                'min:12',
                'regex:/^.*(?=.{12,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!"#$%&()*+,\-.\:\;<=>?@[\]^_`{|}~\']).*$/',
            ],

            'new_password_confirmation' => 'required|min:12',
        ]);

        $seller = $this->seller_repo->find(Auth::guard('seller')->user()->id);
        if (!Hash::check($request->password, $seller->password)) {
            return redirect()->back()->withErrors(['password' => 'パスワードが間違っています。รหัสผ่านไม่ถูกต้อง']);
        }
        if (Hash::check($request->new_password, $seller->password)) {
            return redirect()->back()->withErrors(['new_password' => '過去に使用したパスワードは使用できません。<br>กรุณาตั้งรหัสที่คุณยังไม่เคยใช้ในเว็บไซต์นี้']);
        }

        $seller->password = Hash::make($request->new_password);
        $seller->save();

        activity()
            ->causedBy($seller->getModel())
            ->withProperties([
                'old' => [
                    'password' => $seller->password,
                ],
                'attributes' => [
                    'password' => Hash::make($request->new_password),
                ]
            ])
            ->inLog('Seller change password')
            ->log('Seller change password');

        return view('sellers.mypage.change-password-thanks');
    }
}
