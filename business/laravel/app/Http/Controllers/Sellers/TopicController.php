<?php

namespace App\Http\Controllers\Sellers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\UploadeFileController;
use App\Repositories\TopicRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class TopicController extends Controller
{

    protected $topic_repo;

    public function __construct(
        TopicRepository $topicRepository
    )
    {
        $this->topic_repo = $topicRepository;
    }

    public function page()
    {
        $topic = $this->topic_repo->getTopic(Auth::guard('seller')->user()->id);

        return view('sellers.mypage.company-topic', compact('topic'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'category' => 'required',
            'title' => 'required',
            'detail' => 'required',
            'event_date' => 'nullable|date'
        ]);
        //TODO::image
        $uploade = new UploadeFileController();
        if (!empty($request->photo)) {
            $request->photo = $uploade->uploadImage($request->photo, 'topic', Str::random(10));
        }

        if (!empty($request->event_date)) {
            $request->event_date_start = $request->event_date;
            $request->event_date_end = $request->event_date;
        }
        if ($request->id) {
            //update
            $topic = $this->topic_repo->find($request->id);
            $this->topic_repo->valiable($topic, $request);

        } else {
            //store
            $this->topic_repo->store($request);
        }

        return redirect()->back()
            ->with([
                'message' => __('voyager::generic.successfully_updated'),
                'alert-type' => 'success',
            ]);
    }
}
