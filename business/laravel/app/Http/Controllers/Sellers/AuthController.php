<?php

namespace App\Http\Controllers\Sellers;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Repositories\CompanyRepository;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = RouteServiceProvider::HOME;

    public function showLoginForm()
    {
        session()->forget('url_previous');
        session(['url_previous' => url()->previous()]);
        $uri = str_replace(config('app.url'), '', session('url_previous'));
        if (url()->full() == session('url_previous') || $uri == '/login' || $uri == '/admin/login' || $uri == '/seller/login' || $uri == '') {
            session()->forget('url_previous');
        }
        return view('sellers.seller-login');
    }

    public function showLoginFormBuyer()
    {
        session()->forget('url_previous');
        session(['url_previous' => url()->previous()]);
        if (url()->full() == session('url_previous')) {
            session()->forget('url_previous');
        }
        return view('buyer.login');
    }

    public function username()
    {
        return 'user_login';
    }

    protected function guard()
    {
        return Auth::guard('seller');
    }

    public function showRegisterForm()
    {
        return view('sellers.register-email');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        $seller_repo = new CompanyRepository();
        $seller = $seller_repo->checkHasUser($request->user_login);

        if (empty($seller)) {
            return back()
                ->withInput()
                ->withErrors(['no_email' => 'Not Email']);
        } else {
            //ล็อกอินนานเกินกว่า 6 เดือน
            $seller = $seller_repo->findUsername($request->user_login);
            $this_month = Carbon::now()->floorMonth();
            $start_month = Carbon::parse($seller->last_login)->floorMonth();
            $diff = $start_month->diffInMonths($this_month);
            if ($diff > 6) {
                return redirect()->route('seller.renew-password.page', ['login' => $request->user_login]);
            }
        }

        if ($this->attemptLogin($request)) {
            if (session()->has('url_previous')) {
                if(session('url_previous') != config('app.url') .'/seller/register-form'){
                    if(!empty(strpos(session('url_previous'),'/business/'))){
                        return redirect(session('url_previous'));
                    }
                }
            }
            return redirect()->route('seller.mypage');
        } else {
            return redirect()->back()->withErrors([
                'password' => 'パスワードが間違っています。รหัสผ่านไม่ถูกต้อง'
            ]);
        }

        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }
}
