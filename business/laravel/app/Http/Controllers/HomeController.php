<?php

namespace App\Http\Controllers;

use App\Repositories\BannerRepository;
use App\Repositories\BusinessTypeRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\CovidRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\NewsJntoPartnerRepository;
use App\Repositories\NewsJntoRepository;
use App\Repositories\ProvinceRepository;
use App\Repositories\SeasonRepository;
use App\Repositories\SellerRevisionHistoryRepository;
use App\Repositories\SeminarRepository;
use App\Repositories\ServiceRepository;
use App\Repositories\SupportMenuRepository;
use App\Repositories\TopicRepository;
use App\Repositories\UsefulLinkRepository;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use TCG\Voyager\Models\Setting;

class HomeController extends Controller
{
    protected $business_type_repo;
    protected $language_repo;
    protected $support_menu_repo;
    protected $province_repo;
    protected $category_repo;
    protected $company_repo;
    protected $service_repo;
    protected $season_repo;
    protected $news_repo;
    protected $news_partner_repo;
    protected $seminar_repo;
    protected $covid_repo;
    protected $useful_links_repo;
    protected $topic_repo;
    protected $banner_repo;
    protected $seller_revision_history_repo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        BusinessTypeRepository          $business_type_repo,
        LanguageRepository              $language_repo,
        SupportMenuRepository           $support_menu_repo,
        ProvinceRepository              $province_repo,
        CategoryRepository              $category_repo,
        CompanyRepository               $company_repo,
        ServiceRepository               $service_repo,
        SeasonRepository                $season_ropo,
        NewsJntoRepository              $newsJnto_repo,
        NewsJntoPartnerRepository       $news_partner_repo,
        SeminarRepository               $seminar_repo,
        CovidRepository                 $covidRepository,
        UsefulLinkRepository            $usefulLinkRepository,
        TopicRepository                 $topicRepository,
        BannerRepository                $banner_repo,
        SellerRevisionHistoryRepository $seller_revision_history_repo
    )
    {
        //$this->middleware('auth');

        $this->business_type_repo = $business_type_repo;
        $this->language_repo = $language_repo;
        $this->support_menu_repo = $support_menu_repo;
        $this->province_repo = $province_repo;
        $this->category_repo = $category_repo;
        $this->company_repo = $company_repo;
        $this->service_repo = $service_repo;
        $this->season_repo = $season_ropo;
        $this->news_repo = $newsJnto_repo;
        $this->news_partner_repo = $news_partner_repo;
        $this->seminar_repo = $seminar_repo;
        $this->covid_repo = $covidRepository;
        $this->useful_links_repo = $usefulLinkRepository;
        $this->topic_repo = $topicRepository;
        $this->banner_repo = $banner_repo;
        $this->seller_revision_history_repo = $seller_revision_history_repo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $banners = $this->banner_repo->all_published();
        if (Auth::guard('seller')->check()) {
            if (Auth::guard('seller')->user()->privacy == 'private') {
                //$companies = $this->company_repo->getHomePrivate();
                $companies = 0;
            } else {
                $companies = $this->company_repo->getHome();
            }
        } else {
            $companies = $this->company_repo->getHome();
        }

        $province_arr = [];
        $provinces_implode = [];
        if (!empty($companies)) {
            foreach ($companies as $company) {
                if (is_object($company)) {
                    if (!empty($company->company_region)) {
                        foreach ($company->company_region as $key => $item) {
                            $province_arr[$company->id][] = $item->name_th;
                        }
                    }
                    if (isset($provinces_implode[$company->id])) {
                        $provinces_implode[$company->id] = collect($province_arr[$company->id])->implode(' ');
                        if (!empty($company->province_all)) {
                            $provinces_implode[$company->id] = 'ทั่วญี่ปุ่น';
                        }
                    }
                }
                if (isset($provinces_implode[$company->id])) {
                    $provinces_implode[$company->id] = collect($province_arr[$company->id])->implode(' ');
                    if (!empty($company->province_all)) {
                        $provinces_implode[$company->id] = 'ทั่วญี่ปุ่น';
                    }
                }
            }
        }

        //news
        $news_jnto = $this->news_repo->getAll(3);
        $news_jnto_partner = $this->news_partner_repo->getAll(3);

        //seminar
        $seminars_jp = $this->seminar_repo->all_published('jp');
        $seminar_th_first = $this->seminar_repo->getFirst('th');
        $seminars_th = $this->seminar_repo->all_published('th', true);

        $groups = $this->useful_links_repo->group_category();

        $covids = $this->covid_repo->all_published();

        $topics = [];
        //check login
        if (Auth::guard('web')->check()) //buyer
        {
            $guard = 'buyer';
            $privacy = 'public';
            $company_id = null;
            //get topic
            $topics = $this->topic_repo->getTopics([
                'status'   => 'done',
                'event'    => 'event',
                'today'    => true,
                'jnto'     => false,
                'is_event' => true,
                'is_home'  => true
            ]);
        }
        if (Auth::guard('seller')->check())//seller
        {
            $guard = 'seller';
            $privacy = Auth::guard('seller')->user()->privacy;
            $company_id = Auth::guard('seller')->user()->id;

            $topics = $this->topic_repo->getTopics([
                'status'     => 'done',
                'event'      => 'event',
                'privacy'    => $privacy,
                'company_id' => $company_id,
                'today'      => true,
                'jnto'       => false,
                'is_event'   => true,
                'is_home'    => true
            ]);
        }

        $data_topics = $topics->chunk(4);

        $seller_revision_histories = [];
        $seller_revision_histories_data = $this->seller_revision_history_repo->getHomeHitoryList();

        foreach ($seller_revision_histories_data as $key => $history) {
            if ($history->revision == 'App\Models\Company') {
                if ($history->sellers->status == 'published') {
                    $seller_revision_histories[$key]['type'] = 'seller';
                    $seller_revision_histories[$key]['title'] = $history->revision_details['title'] ?? '-';
                    $seller_revision_histories[$key]['link'] = route('seller-info-detail', $history->sellers->ref_id) . "#Toppic";
                    $seller_revision_histories[$key]['created_at'] = $history->created_at;
                }
            } elseif ($history->revision == 'App\Models\NewsJntoPartner') {
                if (@$history->newsJntoPartners->status == 'published') {
                    $seller_revision_histories[$key]['type'] = 'news';
                    $seller_revision_histories[$key]['title'] = $history->revision_details['title'] . ' (JNTO Partner’s News)';

                    $seller_revision_histories[$key]['link'] = route('news-jnto-partner.show', $history->newsJntoPartners->ref_id);
                    $seller_revision_histories[$key]['created_at'] = $history->created_at;
                }
            } elseif ($history->revision == 'App\Models\NewsJnto') {
                if (@$history->newsJnto->status == 'published') {
                    $seller_revision_histories[$key]['type'] = 'news';
                    $seller_revision_histories[$key]['title'] = $history->revision_details['title'] . ' (JNTO News)';

                    $seller_revision_histories[$key]['link'] = route('news-jnto.show', $history->newsJnto->ref_id);
                    $seller_revision_histories[$key]['created_at'] = $history->created_at;
                }
            } else {
                if (@$history->seller->status == 'published') {
                    $seller_revision_histories[$key]['type'] = 'seller';
                    $seller_revision_histories[$key]['title'] = @$history->seller->name_en.' '.$history->revision;
                    $seller_revision_histories[$key]['link'] = route('seller-info-detail', $history->seller->ref_id) . "#Toppic";
                    $seller_revision_histories[$key]['created_at'] = $history->created_at;
                }
            }
        }

        //TODO::Calendar JNTO
        $topics_events = [];
        $topics_event_infos = $this->topic_repo->getTopicAllEvent(
            $guard,
            $privacy,
            $company_id,
            'jnto'
        );

        foreach ($topics_event_infos as $data_event) {
            if (isset($data_event->company)) {
                if ($data_event->company->privacy == 'public') {
                    $topics_events[] = $data_event;
                }
            } else {
                $topics_events[] = $data_event;
            }
        }

        $topic_date_jnto = [];
        $event_info_date = [];
        if (sizeof($topics_events) > 0) {
            foreach ($topics_events as $topics_event) {
                if (!empty($topics_event->event_date_start)) {
                    $period = CarbonPeriod::create($topics_event->event_date_start, $topics_event->event_date_end);
                    foreach ($period as $date) {
                        $topic_date_jnto[$date->format('Y-m-d')] = $topics_event;
                        $event_info_date[] = $date->format('Y-m-d');
                    }
                }
            }
        }

        //TODO::Calendar partner
        $topics_partner = [];
        $topics_event_partners = $this->topic_repo->getTopicAllEvent(
            $guard,
            $privacy,
            $company_id,
            'partner'
        );


        $topic_date_partner = [];
        $event_partner_date = [];
        if (sizeof($topics_event_partners) > 0) {
            foreach ($topics_event_partners as $topics_eventpartners) {
                if (!empty($topics_eventpartners->event_date_start)) {
                    $period = CarbonPeriod::create($topics_eventpartners->event_date_start, $topics_eventpartners->event_date_end);
                    foreach ($period as $date) {
                        $topic_date_partner[$date->format('Y-m-d')] = $topics_eventpartners;
                        $event_partner_date[] = $date->format('Y-m-d');
                    }
                }
            }
        }

        /*$dates = [];
        if (sizeof($topic_date) > 0) {
            $event_mount_start = Arr::first($topic_date);
            $event_mount_start = Carbon::parse($event_mount_start->event_date_start)->startOfMonth()->format('Y-m-d');

            $event_mount_end = Arr::last($topic_date);
            $event_mount_end = Carbon::parse($event_mount_end->event_date_end)->endOfMonth()->format('Y-m-d');

            $dates = CarbonPeriod::create($event_mount_start, $event_mount_end);
        } else {
            $today = Carbon::today();
            $event_mount_start = $today->startOfMonth()->format('Y-m-d');
            $event_mount_end = Carbon::parse($today)->endOfYear()->format('Y-m-d');
            $dates = CarbonPeriod::create($event_mount_start, $event_mount_end);
        }*/

        /* $holidays = [];

         foreach ($dates as $is_date) {
             if (!isset($topic_date[$is_date->format('Y-m-d')])) {
                 $holidays[] = $is_date->format('Y-m-d');
             }
         }*/

        $business_types = $this->business_type_repo->getAll();
        $support_menus = $this->support_menu_repo->getAll();

        return view('frontend.pages.home',
            compact(
                'companies',
                'provinces_implode',
                'news_jnto',
                'news_jnto_partner',
                'seminars_jp',
                'seminars_th',
                'covids',
                'groups',
                'data_topics',
                'banners',
                'seller_revision_histories',
                'seminar_th_first',
                //'event_mount_start',
                //'event_mount_end',
                //'holidays',
                'event_info_date',
                'event_partner_date',
                'business_types',
                'support_menus'
            )
        );
    }

    public function thank_page()
    {
        $key_value_text = 'admin.thank_page.value';
        $setting_value = $this->find($key_value_text);

        return view('admin.thank_page', compact(
            'setting_value'));
    }

    public function update_thank_page(Request $request)
    {
        $setting = $this->find("admin.thank_page.value");

        if (!empty($setting)) {
            $data = Setting::find($setting->id);
        } else {
            $data = new Setting();
        }

        $display_name_text = 'Thank page';
        $type_text = 'rich_text_box';
        $group = 'Admin';

        $id = $this->store($data, $display_name_text, "admin.thank_page.value", $type_text, $request->value, $group);

        return redirect()->route('setting-thank-page.index');
    }

    public function contactPage()
    {
        $key_value_text_th = 'admin.contact-th';
        $setting_value_th = $this->find($key_value_text_th);

        $key_value_text_jp = 'admin.contact-jp';
        $setting_value_jp = $this->find($key_value_text_jp);

        $key_email = 'admin.email';
        $setting_email = $this->find($key_email);

        $key_signature = 'admin.signature';
        $setting_signature = $this->find($key_signature);

        $key_privacy_th = 'admin.privacy-policy-url-th';
        $setting_privacy_th = $this->find($key_privacy_th);

        $key_privacy_jp = 'admin.privacy-policy-url-jp';
        $setting_privacy_jp = $this->find($key_privacy_jp);

        $key_cookie = 'admin.cookie-policy-url';
        $setting_cookie = $this->find($key_cookie);

        $key_copyrights = 'admin.copyrights';
        $setting_copyrights = $this->find($key_copyrights);

        $key_footer_text = 'admin.footer-text';
        $setting_footer_text = $this->find($key_footer_text);

        return view('admin.contact', compact(
            'setting_value_th',
            'setting_value_jp',
            'setting_email',
            'setting_signature',
            'setting_privacy_th',
            'setting_privacy_jp',
            'setting_cookie',
            'setting_copyrights',
            'setting_footer_text'
        ));
    }

    public function update_contact_page(Request $request)
    {
        $setting_th = $this->find("admin.contact-th");
        $setting_jp = $this->find("admin.contact-jp");
        $setting_email = $this->find("admin.email");
        $setting_signature = $this->find("admin.signature");
        $setting_privacy_th = $this->find("admin.privacy-policy-url-th");
        $setting_privacy_jp = $this->find("admin.privacy-policy-url-jp");
        $setting_cookie = $this->find("admin.cookie-policy-url");
        $setting_copyrights = $this->find("admin.copyrights");
        $setting_footer_text = $this->find("admin.footer-text");

        if (!empty($setting_th)) {
            $data_th = Setting::find($setting_th->id);
        } else {
            $data_th = new Setting();
        }

        if (!empty($setting_jp)) {
            $data_jp = Setting::find($setting_jp->id);
        } else {
            $data_jp = new Setting();
        }

        if (!empty($setting_email)) {
            $data_email = Setting::find($setting_email->id);
        } else {
            $data_email = new Setting();
        }

        if (!empty($setting_signature)) {
            $data_signature = Setting::find($setting_signature->id);
        } else {
            $data_signature = new Setting();
        }

        if (!empty($setting_privacy_th)) {
            $data_privacy_th = Setting::find($setting_privacy_th->id);
        } else {
            $data_privacy_th = new Setting();
        }

        if (!empty($setting_privacy_jp)) {
            $data_privacy_jp = Setting::find($setting_privacy_jp->id);
        } else {
            $data_privacy_jp = new Setting();
        }

        if (!empty($setting_cookie)) {
            $data_cookie = Setting::find($setting_cookie->id);
        } else {
            $data_cookie = new Setting();
        }

        if (!empty($setting_copyrights)) {
            $data_copyrights = Setting::find($setting_copyrights->id);
        } else {
            $data_copyrights = new Setting();
        }

        if (!empty($setting_footer_text)) {
            $data_footer_text = Setting::find($setting_footer_text->id);
        } else {
            $data_footer_text = new Setting();
        }

        $display_name_text = 'Contact';
        $type_text = 'rich_text_box';
        $group = 'Admin';

        $this->store($data_th, $display_name_text, "admin.contact-th", $type_text, $request->value_th, $group);
        $this->store($data_jp, $display_name_text, "admin.contact-jp", $type_text, $request->value_jp, $group);
        $this->store($data_email, $display_name_text, "admin.email", "text", $request->email, $group);
        $this->store($data_signature, $display_name_text, "admin.signature", $type_text, $request->signature, $group);
        $this->store($data_privacy_th, $display_name_text, "admin.privacy-policy-url-th", "text", $request->privacy_th, $group);
        $this->store($data_privacy_jp, $display_name_text, "admin.privacy-policy-url-jp", "text", $request->privacy_jp, $group);
        $this->store($data_cookie, $display_name_text, "admin.cookie-policy-url", "text", $request->policy, $group);
        $this->store($data_copyrights, $display_name_text, "admin.copyrights", $type_text, $request->copyrights, $group);
        $this->store($data_footer_text, $display_name_text, "admin.footer-text", $type_text, $request->footer_text, $group);

        return redirect()->route('setting-contact.index');
    }


    public function dashboard()
    {
        $key_value_text = 'admin.dashboard.value';
        $key_value_file = 'admin.dashboard.file';
        $key_value_file2 = 'admin.dashboard.file2';
        $key_value_file3 = 'admin.dashboard.file3';
        $key_value_file4 = 'admin.dashboard.file4';
        $key_value_file5 = 'admin.dashboard.file5';
        $key_value_file6 = 'admin.dashboard.file6';
        $key_value_file7 = 'admin.dashboard.file7';
        $key_value_file8 = 'admin.dashboard.file8';
        $key_value_file9 = 'admin.dashboard.file9';
        $key_value_file10 = 'admin.dashboard.file10';
        $key_start_event = 'admin.dashboard.start_event';

        $setting_value = $this->find($key_value_text);

        $setting_file1 = $this->find($key_value_file);
        $icon1 = $this->icon($setting_file1);

        $setting_file2 = $this->find($key_value_file2);
        $icon2 = $this->icon($setting_file2);

        $setting_file3 = $this->find($key_value_file3);
        $icon3 = $this->icon($setting_file3);

        $setting_file4 = $this->find($key_value_file4);
        $icon4 = $this->icon($setting_file4);

        $setting_file5 = $this->find($key_value_file5);
        $icon5 = $this->icon($setting_file5);

        $setting_file6 = $this->find($key_value_file6);
        $icon6 = $this->icon($setting_file6);

        $setting_file7 = $this->find($key_value_file7);
        $icon7 = $this->icon($setting_file7);

        $setting_file8 = $this->find($key_value_file8);
        $icon8 = $this->icon($setting_file8);

        $setting_file9 = $this->find($key_value_file9);
        $icon9 = $this->icon($setting_file9);

        $setting_file10 = $this->find($key_value_file10);
        $icon10 = $this->icon($setting_file10);

        $setting_start_event = $this->find($key_start_event);

        if (Auth::user()->role->name == 'Editor') {
            $page = 'admin.dashboard-edit';
        } else {
            $page = 'admin.dashboard';
        }
        return view($page, compact(
            'setting_value',
            'setting_file1',
            'setting_file2',
            'setting_file3',
            'setting_file4',
            'setting_file5',
            'setting_file6',
            'setting_file7',
            'setting_file8',
            'setting_file9',
            'setting_file10',
            'icon1',
            'icon2',
            'icon3',
            'icon4',
            'icon5',
            'icon6',
            'icon7',
            'icon8',
            'icon9',
            'icon10',
            'setting_start_event'
        ));
    }

    public function setting(Request $request)
    {
        $key_value_text = 'admin.dashboard.value';
        $key_value_file = 'admin.dashboard.file';
        $key_value_file2 = 'admin.dashboard.file2';
        $key_value_file3 = 'admin.dashboard.file3';
        $key_value_file4 = 'admin.dashboard.file4';
        $key_value_file5 = 'admin.dashboard.file5';
        $key_value_file6 = 'admin.dashboard.file6';
        $key_value_file7 = 'admin.dashboard.file7';
        $key_value_file8 = 'admin.dashboard.file8';
        $key_value_file9 = 'admin.dashboard.file9';
        $key_value_file10 = 'admin.dashboard.file10';
        $key_start_event = 'admin.dashboard.start_event';

        $type_text = 'rich_text_box';
        $type_file = 'file';
        $group = 'Admin';
        $display_name_text = 'Dashboard Detail';
        $display_name_file = 'Dashboard Detail';

        $setting = $this->find($key_value_text);

        if (!empty($setting)) {
            $data = Setting::find($setting->id);
        } else {
            $data = new Setting();
        }

        $this->store($data, $display_name_text, $key_value_text, $type_text, $request->value, $group);

        //file1
        $setting_file1 = $this->find($key_value_file);
        $setting_file2 = $this->find($key_value_file2);
        $setting_file3 = $this->find($key_value_file3);
        $setting_file4 = $this->find($key_value_file4);
        $setting_file5 = $this->find($key_value_file5);
        $setting_file6 = $this->find($key_value_file6);
        $setting_file7 = $this->find($key_value_file7);
        $setting_file8 = $this->find($key_value_file8);
        $setting_file9 = $this->find($key_value_file9);
        $setting_file10 = $this->find($key_value_file10);
        $setting_start_event = $this->find($key_start_event);

        if (!empty($setting_file1)) {
            $data_file1 = Setting::find($setting_file1->id);
        } else {
            $data_file1 = new Setting();
        }

        if (!empty($setting_file2)) {
            $data_file2 = Setting::find($setting_file2->id);
        } else {
            $data_file2 = new Setting();
        }

        if (!empty($setting_file3)) {
            $data_file3 = Setting::find($setting_file3->id);
        } else {
            $data_file3 = new Setting();
        }

        if (!empty($setting_file4)) {
            $data_file4 = Setting::find($setting_file4->id);
        } else {
            $data_file4 = new Setting();
        }

        if (!empty($setting_file5)) {
            $data_file5 = Setting::find($setting_file5->id);
        } else {
            $data_file5 = new Setting();
        }

        if (!empty($setting_file6)) {
            $data_file6 = Setting::find($setting_file6->id);
        } else {
            $data_file6 = new Setting();
        }

        if (!empty($setting_file7)) {
            $data_file7 = Setting::find($setting_file7->id);
        } else {
            $data_file7 = new Setting();
        }

        if (!empty($setting_file8)) {
            $data_file8 = Setting::find($setting_file8->id);
        } else {
            $data_file8 = new Setting();
        }

        if (!empty($setting_file9)) {
            $data_file9 = Setting::find($setting_file9->id);
        } else {
            $data_file9 = new Setting();
        }

        if (!empty($setting_file10)) {
            $data_file10 = Setting::find($setting_file10->id);
        } else {
            $data_file10 = new Setting();
        }

        if (!empty($setting_start_event)) {
            $data_start_event = Setting::find($setting_start_event->id);
        } else {
            $data_start_event = new Setting();
        }

        $uploade = new UploadeFileController();

        $file1 = null;
        if (!empty($request->file1)) {
            $pdf = $uploade->uploadPDF($request->file1, 'dashboard/file', Str::random(10));
            $file1 = $pdf['path'];
        }
        $file2 = null;
        if (!empty($request->file2)) {
            $pdf = $uploade->uploadPDF($request->file2, 'dashboard/file', Str::random(10));
            $file2 = $pdf['path'];
        }
        $file3 = null;
        if (!empty($request->file3)) {
            $pdf = $uploade->uploadPDF($request->file3, 'dashboard/file', Str::random(10));
            $file3 = $pdf['path'];
        }
        $file4 = null;
        if (!empty($request->file4)) {
            $pdf = $uploade->uploadPDF($request->file4, 'dashboard/file', Str::random(10));
            $file4 = $pdf['path'];
        }
        $file5 = null;
        if (!empty($request->file5)) {
            $pdf = $uploade->uploadPDF($request->file5, 'dashboard/file', Str::random(10));
            $file5 = $pdf['path'];
        }
        $file6 = null;
        if (!empty($request->file6)) {
            $pdf = $uploade->uploadPDF($request->file6, 'dashboard/file', Str::random(10));
            $file6 = $pdf['path'];
        }
        $file7 = null;
        if (!empty($request->file7)) {
            $pdf = $uploade->uploadPDF($request->file7, 'dashboard/file', Str::random(10));
            $file7 = $pdf['path'];
        }
        $file8 = null;
        if (!empty($request->file8)) {
            $pdf = $uploade->uploadPDF($request->file8, 'dashboard/file', Str::random(10));
            $file8 = $pdf['path'];
        }
        $file9 = null;
        if (!empty($request->file9)) {
            $pdf = $uploade->uploadPDF($request->file9, 'dashboard/file', Str::random(10));
            $file9 = $pdf['path'];
        }
        $file10 = null;
        if (!empty($request->file10)) {
            $pdf = $uploade->uploadPDF($request->file10, 'dashboard/file', Str::random(10));
            $file10 = $pdf['path'];
        }

        $this->store($data_file1, $display_name_file, $key_value_file, $type_file, $file1, $group);
        $this->store($data_file2, $display_name_file, $key_value_file2, $type_file, $file2, $group);
        $this->store($data_file3, $display_name_file, $key_value_file3, $type_file, $file3, $group);
        $this->store($data_file4, $display_name_file, $key_value_file4, $type_file, $file4, $group);
        $this->store($data_file5, $display_name_file, $key_value_file5, $type_file, $file5, $group);
        $this->store($data_file6, $display_name_file, $key_value_file6, $type_file, $file6, $group);
        $this->store($data_file7, $display_name_file, $key_value_file7, $type_file, $file7, $group);
        $this->store($data_file8, $display_name_file, $key_value_file8, $type_file, $file8, $group);
        $this->store($data_file9, $display_name_file, $key_value_file9, $type_file, $file9, $group);
        $this->store($data_file10, $display_name_file, $key_value_file10, $type_file, $file10, $group);
        $this->store($data_start_event, $display_name_file, $key_start_event, $type_file, $request->start_event, $group);

        return redirect()->back();
    }

    public function icon($setting_file)
    {
        $icon = 'assets/images/TXT@2x.png';
        if (!empty($setting_file->value)) {
            $file = explode(".", $setting_file->value);

            switch ($file[1]) {
                case 'xlsx':
                    $icon = 'assets/images/XLS@2x.png';
                    break;
                case 'pptx':
                    $icon = 'assets/images/PPT@2x.png';
                    break;
                case 'docx':
                    $icon = 'assets/images/DOC@2x.png';
                    break;
                case 'zip':
                case 'rar':
                    $icon = 'assets/images/ZIP@2x.png';
                    break;
                case 'png':
                case 'gif':
                case 'jpge':
                case 'jpg':
                    $icon = 'assets/images/IMG@2x.png';
                    break;
                case 'pdf':
                    $icon = 'assets/images/PDF@2x.png';
                    break;
                default:
                    $icon = 'assets/images/TXT@2x.png';
                    break;
            }
        }

        return $icon;
    }

    public function find($key_value_text)
    {
        $setting_value = Setting::query();
        $setting_value->where('key', $key_value_text);
        return $setting_value = $setting_value->first();
    }

    public function store(
        $data,
        $display_name_text,
        $key_value_text,
        $type_text,
        $value,
        $group
    )
    {
        $data->display_name = $display_name_text;
        $data->key = $key_value_text;
        $data->type = $type_text;
        $data->value = $value ?? $data->value;
        $data->group = $group;
        $data->save();

        return $data->id;
    }

    public function contactSeller()
    {
        return view('sellers.mypage.contact-staff');
    }
}
