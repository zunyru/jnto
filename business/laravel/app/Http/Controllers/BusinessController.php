<?php

namespace App\Http\Controllers;

use App\Exports\SellersExport;
use App\Http\Requests\StoreBusiness;
use App\Http\Requests\StoreBusinessSeller;
use App\Jobs\SendgridEmailJob;
use App\Models\Company;
use App\Repositories\BusinessTypeRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\ProvinceRepository;
use App\Repositories\SellerRevisionHistoryRepository;
use App\Repositories\ServiceRepository;
use App\Repositories\SupportMenuRepository;
use App\Repositories\TopicRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Activitylog\Models\Activity;
use TCG\Voyager\Models\Setting;

class BusinessController extends Controller
{
    /** @var string */
    private $filesystem;

    private $upload_path = 'company';

    protected $business_type_repo;
    protected $language_repo;
    protected $support_menu_repo;
    protected $province_repo;
    protected $category_repo;
    protected $company_repo;
    protected $service_repo;
    protected $topic_repo;

    public function __construct(
        BusinessTypeRepository $business_type_repo,
        LanguageRepository     $language_repo,
        SupportMenuRepository  $support_menu_repo,
        ProvinceRepository     $province_repo,
        CategoryRepository     $category_repo,
        CompanyRepository      $company_repo,
        ServiceRepository      $service_repo,
        TopicRepository        $topicRepository
    )
    {
        $this->business_type_repo = $business_type_repo;
        $this->language_repo = $language_repo;
        $this->support_menu_repo = $support_menu_repo;
        $this->province_repo = $province_repo;
        $this->category_repo = $category_repo;
        $this->company_repo = $company_repo;
        $this->service_repo = $service_repo;
        $this->topic_repo = $topicRepository;
    }

    public function index(Request $request)
    {
        $business_types = $this->business_type_repo->getAll();
        $support_menus = $this->support_menu_repo->getAll();
        $provinces = $this->province_repo->getAll();
        $seasons = $this->category_repo->getSeasonAll();
        $type_of_traves = $this->category_repo->getTypeOfTravelAll();

        return view('admin.seller-info', compact(
                'business_types',
                'support_menus',
                'provinces',
                'seasons',
                'type_of_traves'
            )
        );
    }

    public function dataIndex(Request $request)
    {
        $companies = $this->company_repo->getPaginate($request);
        $provinces = $this->province_repo->getAll();
        foreach ($companies as $key => $company) {

            $business_type_str = '<ul>';
            foreach ($company->business_type_companies as $business_type) {
                $business_type_str .= "<li><span>• {$business_type->name_en}</span></li>";
            }
            $business_type_str .= '</ul>';

            $support_menu_str = '<ul>';
            if (!empty($company->support_menu)) {
                if (!empty($company->support_menu->support_menu1_value)) {
                    $support_menu_str .= "<li>• {$company->support_menu->support_menu1_value->name_en}</li>";
                }
                if (!empty($company->support_menu->support_menu2_value)) {
                    $support_menu_str .= "<li>• {$company->support_menu->support_menu2_value->name_en}</li>";
                }
                if (!empty($company->support_menu->support_menu3_value)) {
                    $support_menu_str .= "<li>• {$company->support_menu->support_menu3_value->name_en}</li>";
                }
                if (!empty($company->support_menu->support_menu4_value)) {
                    $support_menu_str .= "<li>• {$company->support_menu->support_menu4_value->name_en}</li>";
                }
                if (!empty($company->support_menu->support_menu5_value)) {
                    $support_menu_str .= "<li>• {$company->support_menu->support_menu5_value->name_en}</li>";
                }
                if (!empty($company->support_menu->support_menu6_value)) {
                    $support_menu_str .= "<li>• {$company->support_menu->support_menu6_value->name_en}</li>";
                }
                if (!empty($company->support_menu->support_menu7_value)) {
                    $support_menu_str .= "<li>• {$company->support_menu->support_menu7_value->name_en}</li>";
                }
                if (!empty($company->support_menu->support_menu8_value)) {
                    $support_menu_str .= "<li>• {$company->support_menu->support_menu8_value->name_en}</li>";
                }
            }
            $support_menu_str .= '</ul>';

            $company_region_srt = '<ul>';
            if (!empty($company->province_all)) {
                $company_region_srt = "<li>• All over Japan</li>";
            } else {
                if (!empty($company->company_region)) {
                    foreach ($company->company_region as $company_region) {
                        if (!empty($company_region->id)) {
                            foreach ($provinces as $province) {
                                if ($company_region->id == $province->id) {
                                    $company_region_srt .= "<li>• {$province->name_en}</li>";
                                }
                            }
                        }
                    }
                }
            }
            $company_region_srt .= '</ul>';

            $select_srt = "";
            $selected_confirm = ($company->status == 'confirm') ? 'selected' : '';
            $selected_published = ($company->status == 'published') ? 'selected' : '';
            if (auth()->user()->role->name == 'Viewer') {
                $status = $company->status == 'confirm' ? "Draft" : "Published";
                $select_srt .= $status;
            } else {
                $select_srt .= "<select name=\"status\" data-id=\"{$company->id}\" onchange=\"update_staus(this);\" class=\"select_status block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500\"><option " . $selected_confirm . " value=\"confirm\">Draft</option><option " . $selected_published . " value=\"published\">Published</option></select>";
            }

            $select_active_srt = "";
            $selected_active = ($company->active_status == 1) ? 'selected' : '';
            $selected_inactive = ($company->active_status == 0) ? 'selected' : '';
            if (auth()->user()->role->name == 'Viewer') {
                $active_status = $company->active_status == 1 ? "Active" : "Inactive";
                $select_active_srt .= $active_status;
            } else {
                $select_active_srt .= "<select name=\"active_status\" data-id=\"{$company->id}\" onchange=\"update_staus_active(this);\" class=\"select_status block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 lg:px-4 px-1 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500\"><option " . $selected_active . " value=\"1\">Active</option><option " . $selected_inactive . " value=\"0\">Inactive</option></select>";
            }

            $edit_btn = "";
            if (auth()->user()->role->name == 'Viewer') {
                $edit_btn .= "<a href=\"" . route('business.edit.admin', $company->ref_id) . "\" data=\"$company->id\" class=\"bg-orange-500 hover:bg-orange-700 focus:border-orange-400 active:border-orange-700 text-white font-bold py-3 lg:px-4 px-1 rounded edited\">View</a>";
            } else {
                $edit_btn .= "<a href=\"" . route('business.edit.admin', $company->ref_id) . "\" data=\"$company->id\" class=\"bg-orange-500 hover:bg-orange-700 focus:border-orange-400 active:border-orange-700 text-white font-bold py-3 lg:px-4 px-1 rounded edited\">Edit</a>";
            }

            $delete_btn = "";
            $delete_btn .= "<a href=\"#\" onclick=\"delete_item(this);\" data-id=\"$company->id\" class=\"bg-red-500 hover:bg-red-700 focus:border-red-400 active:border-red-700 text-white font-bold py-3 lg:px-4 px-1 rounded deleted\">Delete</a>";


            $column[$key]['no'] = $key;
            $column[$key]['id'] = $company->ref_id;
            $column[$key]['company_name'] = $company->name_en;
            $column[$key]['user_login'] = $company->user_login;
            $column[$key]['business_type'] = $business_type_str;
            $column[$key]['title'] = $company->service_information->title ?? '';
            $column[$key]['support_menu'] = $support_menu_str;
            $column[$key]['province'] = $company_region_srt;
            $column[$key]['status'] = $select_srt;
            $column[$key]['active_status'] = $select_active_srt;
            $column[$key]['edit_btn'] = $edit_btn;
            if (auth()->user()->role->name != 'Viewer') {
                $column[$key]['delete_btn'] = $delete_btn;
            }
        }

        $data['data'] = $column ?? [];
        $data['recordsTotal'] = $companies->total();
        $data['recordsFiltered'] = $companies->total();
        $data['draw'] = $request->draw;

        return response($data);
    }


    public function create(Request $request)
    {
        $business_types = $this->business_type_repo->getAll();
        $languages = $this->language_repo->getAll();
        $languages_for_branches = $this->language_repo->getBranchAll();
        $support_menus = $this->support_menu_repo->getAll();
        $provinces = $this->province_repo->getAllGroupProvince();
        $seasons = $this->category_repo->getSeasonAll();
        $special_features = $this->category_repo->getSpecialFeatureAll();
        $type_of_traves = $this->category_repo->getTypeOfTravelAll();
        $special_experiences = $this->category_repo->getSpecialExperienceAll();

        if ($request->admin_mode || request()->segment(1) == 'admin') {
            $view = 'admin.company-information-add';
        } else {
            $view = 'company-information-add';
        }

        return view($view,
            compact(
                'business_types',
                'languages',
                'languages_for_branches',
                'support_menus',
                'provinces',
                'seasons',
                'special_features',
                'type_of_traves',
                'special_experiences'
            )
        );
    }

    public function store(StoreBusinessSeller $request)
    {
        try {
            if(empty($request->is_show_banner)){
                $request->is_show_banner = 0;
            }

            $revision_arr = [];

            if (!$request->admin_mode) {
                $request->status = Company::STATUS_CREATE;
                if (($request->mode == 'edit') && empty($request->id)) {
                    return redirect()->route('business.create');
                }
            }
            //TODO::image
            $uploade = new UploadeFileController();
            if (!empty($request->logo)) {
                $request->logo = $uploade->uploadImage($request->logo, 'logo', $request->name_en);
            }

            //TODO::PDF
            if (!empty($request->company_profile_pdf)) {
                $company_profile_pdf = $uploade->uploadPDF($request->company_profile_pdf, 'company_profile_pdf', $request->name_en);
                $request->company_profile_pdf = $company_profile_pdf['path'];
                $request->company_profile_pdf_size = $company_profile_pdf['file_size'];

                //revision
                $revision_arr['company_profile_pdf'] = $request->company_profile_pdf;
            }
            //TODO::PDF
            if (!empty($request->company_profile_pdf2)) {
                $company_profile_pdf = $uploade->uploadPDF($request->company_profile_pdf2, 'company_profile_pdf', $request->name_en);
                $request->company_profile_pdf2 = $company_profile_pdf['path'];
                $request->company_profile_pdf2_size = $company_profile_pdf['file_size'];

                //revision
                $revision_arr['company_profile_pdf2'] = $request->company_profile_pdf2;
            }
            //TODO::PDF
            if (!empty($request->company_profile_pdf3)) {
                $company_profile_pdf = $uploade->uploadPDF($request->company_profile_pdf3, 'company_profile_pdf', $request->name_en);
                $request->company_profile_pdf3 = $company_profile_pdf['path'];
                $request->company_profile_pdf3_size = $company_profile_pdf['file_size'];

                //revision
                $revision_arr['company_profile_pdf3'] = $request->company_profile_pdf3;
            }

            //TODO::PDF
            if (!empty($request->support_menu_about_pdf)) {
                $company_profile_pdf = $uploade->uploadPDF($request->support_menu_about_pdf, 'company_profile_pdf', $request->name_en);
                $request->support_menu_about_pdf = $company_profile_pdf['path'];
                $request->support_menu_about_pdf_size = $company_profile_pdf['file_size'];
            }

            if (!empty($request->id)) {
                $Company = $this->company_repo->find($request->id);
            }

            if (!empty($Company)) {
                $last_id = $this->company_repo->valuable($Company, $request);
            } else {
                if ($request->admin_mode == 1) {
                    $request->active_status = 1;
                }
                $last_id = $this->company_repo->store($request);
            }

            $request->company_id = $last_id;

            //Service
            $service_count = $this->service_repo->findByCompanyIDCount($request->company_id);

            //TODO::image
            $uploade = new UploadeFileController();
            if (!empty($request->main_photo)) {
                $request->main_photo = $uploade->uploadImage(
                    $request->main_photo,
                    'photo',
                    $request->name_en . "_main_photo"
                );
                //revision
                $revision_arr['main_photo'] = $request->main_photo;

            }

            //TODO::image
            if (!empty($request->other_photo)) {
                $request->other_photo = $uploade->uploadImage(
                    $request->other_photo,
                    'photo',
                    $request->name_en . "_other_photo"
                );
                //revision
                $revision_arr['other_photo'] = $request->other_photo;
            }
            //TODO::image
            if (!empty($request->other_photo2)) {
                $request->other_photo2 = $uploade->uploadImage(
                    $request->other_photo2,
                    'photo',
                    $request->name_en . "_other_photo2"
                );
                //revision
                $revision_arr['other_photo2'] = $request->other_photo2;

            }
            //TODO::image
            if (!empty($request->other_photo3)) {
                $request->other_photo3 = $uploade->uploadImage(
                    $request->other_photo3,
                    'photo',
                    $request->name_en . "_other_photo3"
                );
                //revision
                $revision_arr['other_photo3'] = $request->other_photo3;
            }

            if (empty($service_count)) {
                //TODO::VIDEO
                if (!empty($request->pr_youtube)) {
                    //revision
                    $revision_arr['pr_youtube'] = $request->pr_youtube;
                }
                $service_id = $this->service_repo->store($request);

            } else {
                $service = $this->service_repo->findByCompanyID($request->company_id);
                //TODO::VIDEO
                if (!empty($request->pr_youtube)) {
                    if ($service->pr_youtube != $request->pr_youtube) {
                        //revision
                        $revision_arr['pr_youtube'] = $request->pr_youtube;
                    }
                }
                $service_id = $this->service_repo->valuable($service, $request);
            }

            if (!empty($request->business_type_id)) {
                $data = $this->company_repo->find($last_id);
                $data->business_type_companies()->sync($request->business_type_id);
            }


            $log_value = [];
            $company_language_old = $data->company_language()->get();
            $log_value[] = $this->getOldData($company_language_old, $request->language_id, 'company_language');
            $data->company_language()->sync($request->language_id);


            $company_company_language_for_branch_old = $data->company_language_for_branch()->get();
            $log_value[] = $this->getOldData($company_company_language_for_branch_old, $request->language_for_branch_id, 'company_language_for_branch');
            $data->company_language_for_branch()->sync($request->language_for_branch_id);


            $support_menu_count = $this->support_menu_repo->findByCompanyIDCount($request->company_id);
            if (empty($support_menu_count)) {
                $this->support_menu_repo->storeCompanySupportMenu($request);

                //revision
                if (!empty($request->support_menu1) || !empty($request->manu_name1)) {
                    $revision_arr['support_menu'][] = [
                        $request->support_menu1,
                        $request->manu_name1
                    ];
                }

                //revision
                if (!empty($request->support_menu2) || !empty($request->manu_name2)) {
                    $revision_arr['support_menu'][] = [
                        $request->support_menu2,
                        $request->manu_name2
                    ];
                }

                //revision
                if (!empty($request->support_menu3) || !empty($request->manu_name3)) {
                    $revision_arr['support_menu'][] = [
                        $request->support_menu3,
                        $request->manu_name3
                    ];
                }

                //revision
                if (!empty($request->support_menu4) || !empty($request->manu_name4)) {
                    $revision_arr['support_menu'][] = [
                        $request->support_menu4,
                        $request->manu_name4
                    ];
                }

                //revision
                if (!empty($request->support_menu5) || !empty($request->manu_name5)) {
                    $revision_arr['support_menu'][] = [
                        $request->support_menu5,
                        $request->manu_name5
                    ];
                }

                //revision
                if (!empty($request->support_menu6) || !empty($request->manu_name6)) {
                    $revision_arr['support_menu'][] = [
                        $request->support_menu6,
                        $request->manu_name6
                    ];
                }

                //revision
                if (!empty($request->support_menu1) || !empty($request->manu_name1)) {
                    $revision_arr['support_menu'][] = [
                        $request->support_menu1,
                        $request->manu_name1
                    ];
                }

                //revision
                if (!empty($request->support_menu7) || !empty($request->manu_name7)) {
                    $revision_arr['support_menu'][] = [
                        $request->support_menu7,
                        $request->manu_name7
                    ];
                }

                //revision
                if (!empty($request->support_menu8) || !empty($request->manu_name8)) {
                    $revision_arr['support_menu'][] = [
                        $request->support_menu8,
                        $request->manu_name8
                    ];
                }
            } else {
                $support_menu = $this->support_menu_repo->findByCompanyID($request->company_id);

                if ($support_menu->support_menu1 != $request->support_menu1
                    || $support_menu->manu_name1 != $request->manu_name1
                ) {
                    //revision
                    if (!empty($request->support_menu1) || !empty($request->manu_name1)) {
                        $revision_arr['support_menu'][] = [
                            $request->support_menu1,
                            $request->manu_name1
                        ];
                    }
                }

                if ($support_menu->support_menu2 != $request->support_menu2
                    || $support_menu->manu_name2 != $request->manu_name2
                ) {
                    //revision
                    if (!empty($request->support_menu2) || !empty($request->manu_name2)) {
                        $revision_arr['support_menu'][] = [
                            $request->support_menu2,
                            $request->manu_name2
                        ];
                    }
                }

                if ($support_menu->support_menu2 != $request->support_menu2
                    || $support_menu->manu_name2 != $request->manu_name2
                ) {
                    //revision
                    if (!empty($request->support_menu2) || !empty($request->manu_name2)) {
                        $revision_arr['support_menu'][] = [
                            $request->support_menu2,
                            $request->manu_name2
                        ];
                    }
                }

                if ($support_menu->support_menu3 != $request->support_menu3
                    || $support_menu->manu_name3 != $request->manu_name3
                ) {
                    //revision
                    if (!empty($request->support_menu3) || !empty($request->manu_name3)) {
                        $revision_arr['support_menu'][] = [
                            $request->support_menu3,
                            $request->manu_name3
                        ];
                    }
                }

                if ($support_menu->support_menu4 != $request->support_menu4
                    || $support_menu->manu_name4 != $request->manu_name4
                ) {
                    //revision
                    if (!empty($request->support_menu4) || !empty($request->manu_name4)) {
                        $revision_arr['support_menu'][] = [
                            $request->support_menu4,
                            $request->manu_name4
                        ];
                    }
                }

                if ($support_menu->support_menu5 != $request->support_menu5
                    || $support_menu->manu_name5 != $request->manu_name5
                ) {
                    //revision
                    if (!empty($request->support_menu5) || !empty($request->manu_name5)) {
                        $revision_arr['support_menu'][] = [
                            $request->support_menu5,
                            $request->manu_name5
                        ];
                    }
                }

                if ($support_menu->support_menu6 != $request->support_menu6
                    || $support_menu->manu_name6 != $request->manu_name6
                ) {
                    //revision
                    if (!empty($request->support_menu6) || !empty($request->manu_name6)) {
                        $revision_arr['support_menu'][] = [
                            $request->support_menu6,
                            $request->manu_name6
                        ];
                    }
                }

                if ($support_menu->support_menu7 != $request->support_menu7
                    || $support_menu->manu_name7 != $request->manu_name7
                ) {
                    //revision
                    if (!empty($request->support_menu7) || !empty($request->manu_name7)) {
                        $revision_arr['support_menu'][] = [
                            $request->support_menu7,
                            $request->manu_name7
                        ];
                    }
                }

                if ($support_menu->support_menu8 != $request->support_menu8
                    || $support_menu->manu_name8 != $request->manu_name8
                ) {
                    //revision
                    if (!empty($request->support_menu8) || !empty($request->manu_name8)) {
                        $revision_arr['support_menu'][] = [
                            $request->support_menu8,
                            $request->manu_name8
                        ];
                    }
                }

                $this->support_menu_repo->valuable($support_menu, $request);
            }

            //TODO::revision history
            if (sizeof($revision_arr) > 0) {
                $revision_repo = new SellerRevisionHistoryRepository();
                $revision_repo->store($request->company_id, $revision_arr);
            }

            $company_company_region_old = $data->company_region()->get();
            $log_value[] = $this->getOldData($company_company_region_old, $request->region, 'company_region');
            $data->company_region()->sync($request->region);

            //$data->company_season()->sync($request->season);

            $company_special_feature_old = $data->company_special_feature()->get();
            $log_value[] = $this->getOldData($company_special_feature_old, $request->special_feature, 'company_special_feature');
            $data->company_special_feature()->sync($request->special_feature);


            $company_type_of_travel_old = $data->company_type_of_travel()->get();
            $log_value[] = $this->getOldData($company_type_of_travel_old, $request->type_of_trave, 'company_type_of_travel');
            $data->company_type_of_travel()->sync($request->type_of_trave);

            $company_special_experience_old = $data->company_special_experience()->get();
            $log_value[] = $this->getOldData($company_special_experience_old, $request->special_experience, 'company_special_experience');
            $data->company_special_experience()->sync($request->special_experience);


            //event
            if (!empty($request->company_id) && (!empty($request->event_status))) {
                //update
                $topic = $this->topic_repo->getTopic($request->company_id);

                $request->status = $request->event_status;
                $request->title = $request->event_title;
                $request->detail = $request->event_detail;

                if (!empty($request->photo)) {
                    $request->photo = $uploade->uploadImage($request->photo, 'topic', Str::random(10));
                }
                if ($topic) {
                    $this->topic_repo->valiable($topic, $request);
                } else {
                    $this->topic_repo->store($request);
                }
            }


            $log = Activity::all()->last();
            if ($log->log_name == 'Seller' && $log->subject_id == $last_id && $log->updated_at == Carbon::now()) {
                $this->logActivitySeller($log, $log_value, $last_id);
            } elseif (!empty($log_value)) {
                //TODO:: new log
                $log = new Activity();
                $this->logActivitySeller($log, $log_value, $last_id);
            }

            if ($request->admin_mode == 1) {
                $redirect = back();
                return $redirect->with([
                    'message'    => __('voyager::generic.successfully_updated'),
                    'alert-type' => 'success',
                ]);
            } elseif ($request->admin_mode == 2) {
                $redirect = redirect()->route('seller.mypage');
                return $redirect->with([
                    'message'    => __('voyager::generic.successfully_updated'),
                    'alert-type' => 'success',
                ]);
            } else {
                return redirect()->route('business.confirm', $data->ref_id);
            }

        } catch (\Exception $e) {
            return $message = $e->getMessage();
        }

    }

    private function getOldData($data_old, $data_new, $name = '')
    {
        $log_arrtibute = [];
        if (!empty($data_old) && !empty($data_new)) {
            $old_data = collect($data_old)->pluck('id')->toArray();
            $diff_data = array_diff($old_data, $data_new);
            if ((sizeof($old_data) != sizeof($data_new)) || (sizeof($diff_data) > 0)) {
                $log_arrtibute = [
                    $name => [
                        'old'        => $old_data,
                        'attributes' => $data_new
                    ]
                ];
            }
        }

        return $log_arrtibute;
    }

    private function logActivitySeller($log_model, $log_value, $seller_id = '')
    {
        if (!empty($log_model->properties)) {
            $properties = $log_model->properties;
            $old = collect($properties['old']);
            $new = collect($properties['attributes']);
            foreach ($log_value as $values) {
                if (!empty($values)) {
                    foreach ($values as $key => $value) {
                        $old->push([$key => $value['old']]);
                        $new->push([$key => $value['attributes']]);
                    }

                }
            }
        } else {
            $old = collect([]);
            $new = collect([]);

            foreach ($log_value as $values) {
                if (!empty($values)) {
                    foreach ($values as $key => $value) {
                        $old->push([$key => $value['old']]);
                        $new->push([$key => $value['attributes']]);
                    }

                }
            }
        }
        $new_properties = [
            'old'        => $old,
            'attributes' => $new
        ];

        $log_model->log_name = 'Seller';
        $log_model->description = "Seller updated";
        $log_model->subject_type = "App\Models\Company";
        $log_model->subject_id = $seller_id;
        $log_model->properties = $new_properties;
        $log_model->save();
    }

    private function logSupportMenu($request)
    {
        $support_menu_count = $this->support_menu_repo->findByCompanyIDCount($request->company_id);
        if (!empty($support_menu_count)) {

        }

    }

    public function getCompanyID($name)
    {
        $count = $this->company_repo->findNameCount($name);
        if (empty($count)) {
            return false;
        } else {
            $data = $this->company_repo->findName($name);
            return $data;
        }
    }

    public function service($request)
    {
        $service_count = $this->service_repo->findByCompanyIDCount($request->company_id);

        $revision_arr = [];
        //TODO::image
        $uploade = new UploadeFileController();
        if (!empty($request->main_photo)) {
            $request->main_photo = $uploade->uploadImage(
                $request->main_photo,
                'photo',
                $request->name_en . "_main_photo"
            );
            //revision
            $revision_arr[] = [
                'main_photo' => $request->main_photo
            ];
        }
        //TODO::image
        if (!empty($request->other_photo)) {
            $request->other_photo = $uploade->uploadImage(
                $request->other_photo,
                'photo',
                $request->name_en . "_other_photo"
            );
            //revision
            $revision_arr[] = [
                'other_photo' => $request->other_photo
            ];
        }
        //TODO::image
        if (!empty($request->other_photo2)) {
            $request->other_photo2 = $uploade->uploadImage(
                $request->other_photo2,
                'photo',
                $request->name_en . "_other_photo2"
            );
            //revision
            $revision_arr[] = [
                'other_photo2' => $request->other_photo2
            ];
        }
        //TODO::image
        if (!empty($request->other_photo3)) {
            $request->other_photo3 = $uploade->uploadImage(
                $request->other_photo3,
                'photo',
                $request->name_en . "_other_photo3"
            );
            //revision
            $revision_arr[] = [
                'other_photo3' => $request->other_photo3
            ];
        }

        //TODO::revision history Photo
        if (sizeof($revision_arr) > 0) {
            $revision_repo = new SellerRevisionHistoryRepository();
            $revision_repo->store($request->company_id, $revision_arr);
        }

        if (empty($service_count)) {
            $service_id = $this->service_repo->store($request);
        } else {
            $service = $this->service_repo->findByCompanyID($request->company_id);
            $service_id = $this->service_repo->valuable($service, $request);
        }

        return $service_id;
    }

    public function edit(Request $request, $id)
    {
        $code = substr($id, -6);
        $id = intval($code);

        $company = $this->company_repo->find($id);

        $business_pivot = [];
        if ($company->business_type_companies()) {
            $business_type_companies = $company->business_type_companies()->get();
            foreach ($business_type_companies as $item) {
                $business_pivot[] = $item['pivot']['business_type_id'];
            }
        }

        $language_pivot = [];
        if ($company->company_language()) {
            $company_languages = $company->company_language()->get();
            foreach ($company_languages as $item) {
                $language_pivot[] = $item['pivot']['language_id'];
            }
        }

        $language_branch_pivot = [];
        if ($company->company_language_for_branch()) {
            $company_languages = $company->company_language_for_branch()->get();
            foreach ($company_languages as $item) {
                $language_branch_pivot[] = $item['pivot']['language_for_branch_id'];
            }
        }

        $services = $this->service_repo->findByCompanyID($id);

        $suports = $this->support_menu_repo->findByCompanyID($id);

        $company_region_pivot = [];
        $company_region_pivot_name = [];

        if ($company->company_region()) {
            $company_region = $company->company_region()->get();
            foreach ($company_region as $item) {
                $company_region_pivot[] = $item['pivot']['region_id'];
                $company_region_pivot_name[] = $item['name_en'];
            }
        }

        $company_season_pivot = [];
        if ($company->company_season()) {
            $company_season = $company->company_season()->get();
            foreach ($company_season as $item) {
                $company_season_pivot[] = $item['pivot']['season_id'];
            }
        }

        $company_special_feature_pivot = [];
        if ($company->company_special_feature()) {
            $company_special_feature = $company->company_special_feature()->get();
            foreach ($company_special_feature as $item) {
                $company_special_feature_pivot[] = $item['pivot']['special_feature_id'];
            }
        }

        $company_type_of_travel_pivot = [];
        if ($company->company_type_of_travel()) {
            $company_type_of_travel = $company->company_type_of_travel()->get();
            foreach ($company_type_of_travel as $item) {
                $company_type_of_travel_pivot[] = $item['pivot']['type_of_travel_id'];
            }
        }

        $company_special_experience_pivot = [];
        if ($company->company_special_experience()) {
            $company_special_experience = $company->company_special_experience()->get();
            foreach ($company_special_experience as $item) {
                $company_special_experience_pivot[] = $item['pivot']['special_experience_id'];
            }
        }

        $business_types = $this->business_type_repo->getAll();
        $languages = $this->language_repo->getAll();
        $languages_for_branches = $this->language_repo->getBranchAll();
        $support_menus = $this->support_menu_repo->getAll();
        $provinces = $this->province_repo->getAllGroupProvince();
        $seasons = $this->category_repo->getSeasonAll();
        $special_features = $this->category_repo->getSpecialFeatureAll();
        $type_of_traves = $this->category_repo->getTypeOfTravelAll();
        $special_experiences = $this->category_repo->getSpecialExperienceAll();

        if ($request->admin_mode || request()->segment(1) == 'admin') {
            $view = 'admin.company-information-edit';
        } else if (!empty(request()->segment(3) == 'confirm')) {
            $view = 'confirm';
        } else {
            $view = 'company-information-edit';
        }

        $ref_id = $company->ref_id;

        //get event by seller
        $event = $this->topic_repo->getTopic($company->id);

        return view($view, compact(
            'business_types',
            'languages',
            'languages_for_branches',
            'support_menus',
            'provinces',
            'seasons',
            'special_features',
            'type_of_traves',
            'special_experiences',
            'company',
            'business_pivot',
            'language_pivot',
            'services',
            'suports',
            'language_branch_pivot',
            'company_region_pivot',
            'company_season_pivot',
            'company_special_feature_pivot',
            'company_type_of_travel_pivot',
            'company_special_experience_pivot',
            'company_region_pivot_name',
            'id',
            'ref_id',
            'event'
        ));
    }

    public function storeConfirm(Request $request, $id)
    {
        $code = substr($id, -6);
        $id = intval($code);

        $request->status = Company::STATUS_CONFIRM;

        $data = $this->company_repo->confirm($request, $id);
        $ref_id = $data->ref_id;

        $setting = Setting::where('key', 'admin.thank_page.value')->first();

        SendgridEmailJob::dispatchSync($id);

        return view('thankyou', compact('ref_id', 'setting'));
    }

    public function updateStatus(Request $request)
    {
        $code = substr($request->id, -6);
        $id = intval($code);

        $data = $this->company_repo->confirm($request, $id);

        return response([
            'message'    => __('voyager::generic.successfully_updated'),
            'alert-type' => 'success',
        ]);
    }

    public function updateStatusActive(Request $request)
    {
        $code = substr($request->id, -6);
        $id = intval($code);

        $data = $this->company_repo->active($request, $id);

        return response([
            'message'    => __('voyager::generic.successfully_updated'),
            'alert-type' => 'success',
        ]);
    }

    public function delete(Request $request)
    {
        $code = substr($request->id, -6);
        $id = intval($code);

        $data = $this->company_repo->delete($request, $id);

        return response([
            'message'    => __('voyager::generic.successfully_updated'),
            'alert-type' => 'success',
        ]);
    }

    public function export()
    {
        $date = Carbon::now();
        $date = $date->isoFormat('YMD');
        return Excel::download(new SellersExport, "{$date}_csv_sellers.xlsx");
    }

    public function quickCreate()
    {
        return view('admin.quick-create');
    }

    public function quickStore(Request $request)
    {
        $validated = $request->validate([
            'user_login' => 'required|unique:companies,deleted_at|max:255|email'
        ], [
            'user_login.unique' => 'The Email login has already been taken.',
        ]);

        $company = new Company();
        $company->user_login = $request->user_login;
        $company->password = Hash::make('P@ssW0rd');
        $company->remember_token = hash('sha256', Str::random(60));
        $company->privacy = 'public';
        $company->status = 'confirm';
        $company->active_status = 1;
        $company->save();

        return redirect()->route('business.index')->with([
            'message'    => __('voyager::generic.successfully_updated'),
            'alert-type' => 'success',
        ]);
    }
}
