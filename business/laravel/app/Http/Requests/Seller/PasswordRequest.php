<?php

namespace App\Http\Requests\Seller;

use Illuminate\Foundation\Http\FormRequest;

class PasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => [
                'required',
                'confirmed',
                'min:12',
                'regex:/^.*(?=.{12,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!"#$%&()*+,\-.\:\;<=>?@[\]^_`{|}~\']).*$/',
                //'regex:/^.*(?=[A-Za-z])(?=.*[0-9])(?=.*[!"#$%&()*+,\-./:;<=>?@[\]^_`{|}~\'])(.{12,}$)/'
            ],
            'password_confirmation' => 'required|min:12',
        ];
        //"/^(?=.*[A-Za-z])(?=.*[0-9])(?=.*[!"#$%&'()*+,\-./:;<=>?@[\]^_`{|}~])(.{12,}$)/"

    }
}
