<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBusinessSeller extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $is_branch_thai = $this->input('is_branch_thai', false);

        $rules = [
            'logo' => 'mimes:jpg,jpeg,png|max:2048',
            'is_partner' => 'required',
            'name_en' => 'required|max:255',
            'name_ja' => 'required|max:255',
            'business_type_id' => 'required',
            'address' => 'required',
            'official_website_url' => 'required',
            'department' => 'required',
            'email' => 'required|email',
            //'tel' => 'required',
            'language_id' => 'required',
            'is_branch_thai' => 'required',

            //services
            'title' => 'required|max:100',
            'detail' => 'required',
            'main_photo' => 'mimes:jpg,jpeg,png|max:1024',
            'other_photo' => 'mimes:jpg,jpeg,png|max:1024',
            'other_photo1' => 'mimes:jpg,jpeg,png|max:1024',
            'other_photo2' => 'mimes:jpg,jpeg,png|max:1024',
            'about' => 'max:100',
            'about_url' => 'max:100',
            'about_url2' => 'max:100',
            'about_url3' => 'max:100',
            //'is_accept' => 'required',

            //Support menu
            /*'support_menu1' => 'required',
            'support_menu2' => 'required',
            'support_menu3' => 'required',
            'support_menu4' => 'required',
            'support_menu5' => 'required',*/

            //Category tag
            'region' => 'required',
            //'user_login' => 'required|unique:companies|max:255|email'

        ];

        $collection = collect($rules);

        if ($is_branch_thai == 1) {
            $rules_branch_thai = $this->branchThai();
            $merged = $collection->mergeRecursive($rules_branch_thai);
            $rules = $merged->all();
        }

        $edit = ($this->input('mode') == 'edit');
        if(!$edit){
            $rules_add = [
                'logo' => 'required|mimes:jpg,jpeg,png|max:1024',
                'main_photo' => 'required|mimes:jpg,jpeg,png|max:1024',
            ];
            array_merge($rules,$rules_add);
        }

        return $rules;
    }

    private function branchThai()
    {
        $rules = [
            'branch_company_name_en' => 'required|max:255',
            'branch_address' => 'required',
            'branch_thai_website_url' => 'required',
            //'branch_tel' => 'required',
            'branch_email' => 'required|email',
            'language_for_branch_id' => 'required',
        ];

        return $rules;
    }
}
