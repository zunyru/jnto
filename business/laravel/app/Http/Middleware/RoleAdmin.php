<?php


namespace App\Http\Middleware;


use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RoleAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        $user = Auth::user();

        if (($user->role->name == 'Administrator'
            || $user->role->name == 'Super admin'
            || $user->role->name == 'Editor'
            || $user->role->name == 'Viewer')
        ) {
            if ($user->role->name == 'Editor') {
                if ($request->segment(2) == 'user') {
                    return abort(403);
                }
            }
            return $next($request);
        }

        if ($user->role->name == 'Access web') {
            return redirect()->route('home');
        } else {
            return redirect()->route('business.create');
        }
        return abort(403);
    }
}