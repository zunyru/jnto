<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomePageAuth
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $seller = Auth::guard('seller')->check();
        $buyer = Auth::guard('web')->check();

        if (!$seller && !$buyer) {
            $news = [
                'news-jnto.all',
                'news-jnto.show',
                'news-jnto-partner.all',
                'news-jnto-partner.show',
                'home',
                'seller-info-detail',
                'seller-info.search',
                'seller-info.get_search',
                'contact-form-store',
                'preview',
                'business.edit',
                'business.confirm',
                'business.store',
                'business.store.confirm',
                'seminar.all',
                'seminar.show',
                'topic.all',
                'topic.event',
                'topic.news',
                'news-jnto.all',
                'event-detail',
                'event.show.preview'
            ];

            if (in_array(\Request::route()->getName(), $news)) {
                return redirect()->route('login');
            }
            return redirect()->route('seller.login.index');
        }
        /*if ($buyer) {
            $user = Auth::guard('web')->user();
            if ($user->role->name != 'Access web') {
                return redirect()->route('dashboard');
            }
        }*/

        return $next($request);
    }
}
