<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class EncodingValidateParams
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        foreach ($request->all() as $val) {
            if (!$this->isValidEncoding($val)) {
                abort(400, 'Bad Request');
            }
        }

        return $next($request);
    }

    private function isValidEncoding($val): bool
    {
        if (mb_check_encoding($val, mb_internal_encoding())) {
            return true;
        }

        return false;
    }
}
