<?php

namespace App\Libs;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class Sendgrid
{

    protected static function newRequest()
    {
        $api_key = config('mail.sendgrid.api_key');
        return Http::withHeaders([
            'content-type' => 'application/json',
            'authorization' => 'Bearer ' . $api_key
        ]);
    }

    public static function sendWithTemplate(
        $template_id,
        $recv_email,
        $params,
        $sender_email = null,
        $sender_name = null
    )
    {
        $url = config('mail.sendgrid.url');

        $sender_email = $sender_email ?? config('mail.from.address');
        $sender_name = $sender_name ?? config('mail.from.name');

        $emails = [];
        foreach ($recv_email as $key => $item)
        {

            if($item != []) {
                $emails[] = [
                    'to' => [
                        $item
                    ],
                    'dynamic_template_data' => $params
                ];
            }
        }

        $body = [
            'template_id' => $template_id,
            'from' => [
                'email' => $sender_email,
                'name' => $sender_name,
            ],
            'personalizations' => $emails,
        ];

        // request
        $response = self::newRequest()
            ->post($url, $body);

        // check response
        if ($response->status() !== Response::HTTP_ACCEPTED) {
            //dd($response->body());
            Log::error($response->body());
            return false;
        }

        return true;
    }

}
