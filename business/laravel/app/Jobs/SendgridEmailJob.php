<?php

namespace App\Jobs;

use App\Libs\Sendgrid;
use App\Repositories\CompanyRepository;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendgridEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $ref_id;
    protected $subject;
    protected $template;

    /**
     * Setting
     */
    public $tries = 3;
    public $timeout = 10;
    public $backoff = 20;
    public $maxExceptions = 3;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($ref_id,$subject = 'JNTO Test Mail',$template = 'mail')
    {
        $this->ref_id = $ref_id;
        $this->subject = $subject;
        $this->template = $template;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //$this->run();
        $this->MailSend();
    }

    protected function run()
    {
        $template_id = config('mail.sendgrid.templates_id');

        $company = (new CompanyRepository)->find($this->ref_id);

        if (!$company) {
            throw new \Exception('company not found');
        }

        $email = [
            'email' => $company->email,
            'name' => $company->name_en
        ];
        $branch = [];
        if ($company->is_branch_thai !== 2) {
            $branch = [
                'email' => $company->branch_email,
                'name' => $company->branch_company_name_en
            ];
        }
        $emails = array_merge_recursive([$email, $branch]);

        $sequence_no = str_pad($this->ref_id, 6, '0', STR_PAD_LEFT);
        $cast_data = [
            'application_id' => $sequence_no
        ];

        $params = $company->toArray();

        $params = array_merge($params, $cast_data);

        $res = Sendgrid::sendWithTemplate($template_id, $emails, $params);

        if (!$res) {
            $this->release();
            return;
        }

        $company->mail_send_at = Carbon::now();
        try {
            $company->save();
        } catch (\Exception $e) {
            //sentry_log($e);
            throw \Exception($e);
        }
    }

    protected function MailSend()
    {
        $this->template = 'change-email';

        $company = (new CompanyRepository)->find($this->ref_id);

        if (!$company) {
            throw new \Exception('company not found');
        }

        $email = [
            'email' => $company->email,
            'name' => $company->name_en
        ];
        $branch = [];
        if ($company->is_branch_thai !== 2) {
            $branch = [
                'email' => $company->branch_email,
                'name' => $company->branch_company_name_en
            ];
        }
        $emails = array_merge_recursive([$email, $branch]);

        $sequence_no = str_pad($this->ref_id, 6, '0', STR_PAD_LEFT);
        $cast_data = [
            'application_id' => $sequence_no
        ];

        $params = $company->toArray();

        $params = array_merge($params, $cast_data);

        foreach ($emails as $email) {
            if(isset($email['email'])) {
                Mail::send("emails.{$this->template}", $params, function ($message) use ($email) {
                    $message->to($email['email'], $email['name'])
                        ->subject($this->subject);
                    $message->from(config('mail.from.address'), config('app.name'));
                });
            }
        }

        echo "Success";
    }
}
