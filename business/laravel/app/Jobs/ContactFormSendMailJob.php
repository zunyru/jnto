<?php

namespace App\Jobs;

use App\Libs\SendgridContact;
use App\Models\ContactForm;
use App\Repositories\CompanyRepository;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ContactFormSendMailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $params;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        $this->params = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->run();
    }

    private function run()
    {
        $contact = ContactForm::find($this->params['last_id']);
        if (!empty($this->params['branch_japan_seller'])) {
            $template_id = config('mail.sendgrid.contact.branch_japan.seller.templates_id');
            $contact->mail_sellers_at = Carbon::now();
            $contact->type = 'JAPAN_SELLER';
        }

        if (!empty($this->params['branch_japan_user'])) {
            $template_id = config('mail.sendgrid.contact.branch_japan.user.templates_id');
            $contact->mail_user_at = Carbon::now();
            $contact->type = 'JAPAN_USER';
        }

        if (!empty($this->params['branch_thai_seller'])) {
            $template_id = config('mail.sendgrid.contact.branch_thai.seller.templates_id');
            $contact->mail_sellers_at = Carbon::now();
            $contact->type = 'THAI_SELLER';
        }

        if (!empty($this->params['branch_thai_user'])) {
            $template_id = config('mail.sendgrid.contact.branch_thai.user.templates_id');
            $contact->mail_user_at = Carbon::now();
            $contact->type = 'THAI_USER';
        }

        $email = $this->params['send_mail_to'];

        $data = [
            'company_name_th_representative' => $this->params['branch_company_name_en'] ?? false,
            'company_name_jp' => $this->params['company_name_jp'] ?? false,
            'company_name_en' => $this->params['company_name_en'] ?? false,
            'company_name' => $this->params['requests']['company_name'],
            'offcial_website_url' => $this->params['requests']['offcial_website_url'],
            'name' => $this->params['requests']['name'],
            'position' => $this->params['requests']['position'],
            'email' => $this->params['requests']['email'],
            'message' => $this->params['requests']['message']
        ];

        $res = SendgridContact::sendWithTemplate($template_id, $email, $data);

        if (!$res) {
            $this->release();
            return;
        }

        $contact->save();
    }
}
