<?php

namespace App\Jobs\Seller;

use App\Libs\Sendgrid;
use App\Repositories\CompanyRepository;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class SendMailResetPassword implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $id;
    protected $subject;
    protected $template;

    /**
     * Setting
     */
    public $tries = 3;
    public $timeout = 10;
    public $backoff = 20;
    public $maxExceptions = 3;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id,$subject = 'JNTO Test Mail',$template = 'mail')
    {
        $this->id = $id;
        $this->subject = $subject;
        $this->template = $template;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //$this->run();
        $this->MailSend();
    }

    private function run()
    {
        $template_id = config('mail.sendgrid.seller.forgot_password');

        $company = (new CompanyRepository)->find($this->id);

        if (!$company) {
            throw new \Exception('company not found');
        }

        $email = [
            'email' => $company->user_login,
            'name' => $company->name_en
        ];
        $branch = [];

        $emails = array_merge_recursive([$email, $branch]);

        $sequence_no = str_pad($this->id, 6, '0', STR_PAD_LEFT);

        if (empty($company->remember_token)) {
            $token = Str::random(60);
            $remember_token = hash('sha256', $token);
            $company->remember_token = $remember_token;
            $company->save();
        }
        $cast_data = [
            'application_id' => $sequence_no,
            'link' => route('seller.reset-password.page', $company->remember_token)
        ];

        $params = $company->toArray();

        $params = array_merge($params, $cast_data);

        $res = Sendgrid::sendWithTemplate($template_id, $emails, $params);

        if (!$res) {
            $this->release();
            return;
        }

        $company->email_password_send_at = Carbon::now();
        try {
            $company->save();
        } catch (\Exception $e) {
            //sentry_log($e);
            throw \Exception($e);
        }
    }

    private function MailSend()
    {
        $template_id = config('mail.sendgrid.seller.forgot_password');

        $company = (new CompanyRepository)->find($this->id);

        if (!$company) {
            throw new \Exception('company not found');
        }

        $email = [
            'email' => $company->user_login,
            'name' => $company->name_en
        ];
        $branch = [];

        $emails = array_merge_recursive([$email, $branch]);

        $sequence_no = str_pad($this->id, 6, '0', STR_PAD_LEFT);

        if (empty($company->remember_token)) {
            $token = Str::random(60);
            $remember_token = hash('sha256', $token);
            $company->remember_token = $remember_token;
            $company->save();
        }
        $cast_data = [
            'application_id' => $sequence_no,
            'link' => route('seller.reset-password.page', $company->remember_token)
        ];

        $params = $company->toArray();

        $params = array_merge($params, $cast_data);

        foreach ($emails as $email) {
            if(isset($email['email'])) {
                Mail::send("emails.{$this->template}", $params, function ($message) use ($email) {
                    $message->to($email['email'], $email['name'])
                        ->subject($this->subject);
                    $message->from(config('mail.from.address'), config('app.name'));
                });
            }
        }

        $company->email_password_send_at = Carbon::now();
        try {
            $company->save();
        } catch (\Exception $e) {
            //sentry_log($e);
            Log::error($e->getMessage());
            throw \Exception($e);
        }
    }
}
