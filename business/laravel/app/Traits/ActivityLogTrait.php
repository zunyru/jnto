<?php

namespace App\Traits;

use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

trait ActivityLogTrait
{
    use LogsActivity;

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->useLogName($this->getLogName())
            ->logOnly(['*'])
            ->logOnlyDirty()
            ->dontSubmitEmptyLogs();
    }

    public function getLogName()
    {
        return !empty($this->logName) ? $this->logName : (new \ReflectionClass(static::class))->getShortName();
    }

    /*public function logName()
    {
        $reflect = new \ReflectionClass(static::class);

        return $reflect->getShortName();
    }*/
}
