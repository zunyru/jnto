<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Region extends Model
{
    use SoftDeletes;

    public function province()
    {
        return $this->belongsTo(
            Province::class,
            'province_id',
            'id'
        );
    }
}
