<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class TypeOfTravel extends Model
{
    use SoftDeletes;

    protected $table = 'type_of_travels';
}
