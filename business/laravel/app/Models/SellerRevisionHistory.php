<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SellerRevisionHistory extends Model
{
    use HasFactory;

    public function seller()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    protected function setRevisionDetailsAttribute($value)
    {
        $this->attributes['revision_details'] = json_encode($value);
    }

    protected function getRevisionDetailsAttribute($value)
    {
        return json_decode($this->attributes['revision_details'], true);
    }

    public function sellers()
    {
        return $this->morphTo(
            Company::class,
            'revision',
            'company_id',
            'id'
        );
    }

    public function newsJntoPartners()
    {
        return $this->morphTo(
            NewsJntoPartner::class,
            'revision',
            'company_id',
            'id'
        );
    }

    public function newsJnto()
    {
        return $this->morphTo(
            NewsJnto::class,
            'revision',
            'company_id',
            'id'
        );
    }
}
