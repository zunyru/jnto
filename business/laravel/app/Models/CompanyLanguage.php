<?php

namespace App\Models;

use App\Traits\ActivityLogTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class CompanyLanguage extends Model
{
    use HasFactory, ActivityLogTrait;

    protected $table = 'company_language';

    /**
     * Log
     */
    protected $logName = 'Seller';

    protected static $logAttributes = ['*'];

    protected static $logOnlyDirty = false;

    protected static $submitEmptyLogs = true;

    public function getDescriptionForEvent(string $eventName): string
    {
        return "{$eventName} Company Language";
    }
}
