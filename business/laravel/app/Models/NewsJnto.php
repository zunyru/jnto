<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class NewsJnto extends Model
{
    use SoftDeletes;

    const STATUS_PUBLISHED = 'published';
    const STATUS_DRAF = 'draf';

    protected $appends = [
        'ref_id'
    ];

    public function getRefIdAttribute()
    {
        $sequence_no = str_pad($this->id, 6, '0', STR_PAD_LEFT);

        return "{$sequence_no}";
    }

    public function scopePublished($query)
    {
        return $query->where('status', self::STATUS_PUBLISHED);
    }

    public function histories()
    {
        return $this->morphMany('App\Models\NewsJnto', 'newsJnto');
    }
}
