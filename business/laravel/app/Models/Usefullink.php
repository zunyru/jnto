<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Usefullink extends Model
{
    use HasFactory,SoftDeletes;

    const STATUS_PUBLISHED = 'published';
    const STATUS_DRAF = 'draf';

    public function category()
    {
        return $this->belongsTo(
            Category::class
        );
    }

    public function scopePublished($query)
    {
        return $query->where('status', self::STATUS_PUBLISHED);
    }
}
