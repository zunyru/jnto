<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class NewsJntoPartner extends Model
{
    use SoftDeletes;

    protected $appends = [
        'ref_id'
    ];

    const STATUS_PUBLISHED = 'published';
    const STATUS_DRAF = 'draf';

    public function getRefIdAttribute()
    {
        $sequence_no = str_pad($this->id, 6, '0', STR_PAD_LEFT);

        return "{$sequence_no}";
    }

    public function company()
    {
        return $this->hasOne(
            Company::class,
            'id',
            'company_id'
        );
    }

    public function scopePublished($query)
    {
        return $query->where('status', self::STATUS_PUBLISHED);
    }
}
