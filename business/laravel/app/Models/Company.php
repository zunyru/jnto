<?php

namespace App\Models;

use App\Traits\ActivityLogTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Activitylog\ActivityLogger;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;


class Company extends Authenticatable
{
    use SoftDeletes, ActivityLogTrait;

    const STATUS_CREATE    = 'create';
    const STATUS_CONFIRM   = 'confirm';
    const STATUS_PUBLISHED = 'published';

    protected $appends = [
        'ref_id'
    ];

    protected $casts = [
        'is_partner' => 'boolean'
    ];

    protected $hidden = [
        'password',
    ];

    /**
     * Log
     */
    protected $logName = 'Seller';

    protected static $logAttributes = ['*'];

    protected static $logAttributesToIgnore = [
        'email_change',
        'password',
        'last_login',
        'remember_token'
    ];

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    protected static $recordEvents = [
        'created',
        'updated',
        'deleted'
    ];

    public function getDescriptionForEvent(string $eventName): string
    {
        return "Seller {$eventName}";
    }

    public function scopePublished($query)
    {
        return $query->where('status', self::STATUS_PUBLISHED)
            ->where('active_status', true);
    }

    public function getRefIdAttribute()
    {
        $sequence_no = str_pad($this->id, 6, '0', STR_PAD_LEFT);

        return "{$sequence_no}";
    }

    public function business_type_companies()
    {
        return $this->belongsToMany(
            BusinessType::class
        )->withTimestamps();
    }


    public function company_language()
    {
        return $this->belongsToMany(
            Language::class
        )->withTimestamps();
    }

    public function company_language_for_branch()
    {
        return $this->belongsToMany(
            LanguageForBranch::class
        )->withTimestamps();
    }

    public function company_region()
    {
        return $this->belongsToMany(
            Province::class,
            'company_region',
            'company_id',
            'region_id'
        )->withTimestamps();
    }

    public function company_season()
    {
        return $this->belongsToMany(
            Season::class
        )->withTimestamps();
    }

    public function company_special_experience()
    {
        return $this->belongsToMany(
            SpecialExperience::class
        )->withTimestamps();
    }

    public function company_special_feature()
    {
        return $this->belongsToMany(
            SpecialFeature::class
        )->withTimestamps();
    }


    public function company_type_of_travel()
    {
        return $this->belongsToMany(
            TypeOfTravel::class
        )->withTimestamps();
    }

    public function support_menu()
    {
        return $this->hasOne(
            CompanySupportMenu::class,
            'company_id',
            'id'
        );
    }

    public function service_information()
    {
        return $this->hasOne(
            Service::class,
            'company_id',
            'id'
        );
    }

    public function news_partner()
    {
        return $this->belongsTo(
            NewsJntoPartner::class,
            "company_id",
            "id"
        );
    }

    public function topic()
    {
        return $this->hasOne(
            Topic::class,
            "company_id",
            "id"
        );
    }

    public function getRegisteredTextAttribute()
    {
        if($this->registered == 1)
        {
            return "Registered travel agency";
        }elseif ($this->registered == 2){
            return "Registered land operator";
        }
        return "";
    }

    public function histories()
    {
        return $this->morphMany(SellerRevisionHistory::class, 'revision');
    }
}
