<?php

namespace App\Models;

use App\Traits\ActivityLogTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class CompanySupportMenu extends Model
{
    use HasFactory, ActivityLogTrait;

    /**
     * Log
     */
    protected $logName = 'Seller';

    protected static $logAttributes = ['*'];

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return "{$eventName} Company Support Menu";
    }

    public function support_menu1_value()
    {
        return $this->hasOne(
            SupportMenu::class,
            'id',
            'support_menu1'
        );
    }

    public function support_menu2_value()
    {
        return $this->hasOne(
            SupportMenu::class,
            'id',
            'support_menu2'
        );
    }

    public function support_menu3_value()
    {
        return $this->hasOne(
            SupportMenu::class,
            'id',
            'support_menu3'
        );
    }

    public function support_menu4_value()
    {
        return $this->hasOne(
            SupportMenu::class,
            'id',
            'support_menu4'
        );
    }

    public function support_menu5_value()
    {
        return $this->hasOne(
            SupportMenu::class,
            'id',
            'support_menu5'
        );
    }

    public function support_menu6_value()
    {
        return $this->hasOne(
            SupportMenu::class,
            'id',
            'support_menu6'
        );
    }

    public function support_menu7_value()
    {
        return $this->hasOne(
            SupportMenu::class,
            'id',
            'support_menu7'
        );
    }

    public function support_menu8_value()
    {
        return $this->hasOne(
            SupportMenu::class,
            'id',
            'support_menu8'
        );
    }
}
