<?php

namespace App\Models;

use App\Traits\ActivityLogTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Topic extends Model
{
    use HasFactory, ActivityLogTrait;

    const STATUS_PUBLISHED = 'published';
    const STATUS_DRAF = 'draf';

    protected $appends = [
        'ref_id'
    ];

    protected $casts = [
        'event_date' => 'date',
        'event_date_start' => 'date',
        'event_date_end' => 'date'
    ];
    /**
     * Log
     */
    protected $logName = 'Topic';

    protected static $logAttributes = ['*'];

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return "{$eventName} Topic";
    }

    public function company()
    {
        return $this->belongsTo(
            Company::class,
            'company_id',
            'id'
        );
    }

    public function getRefIdAttribute()
    {
        $sequence_no = str_pad($this->id, 6, '0', STR_PAD_LEFT);

        return "{$sequence_no}";
    }

    public function scopePublished($query)
    {
        return $query->where('status', self::STATUS_PUBLISHED);
    }

    public function scopePublishedDone($query)
    {
        return $query->where('topics.status', 'done');
    }
}
