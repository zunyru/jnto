<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Province extends Model
{
    use SoftDeletes;

    public function regions()
    {
        return $this->hasMany(
            Region::class,
            'province_id',
            'id'
        );
    }
}
