<?php

namespace App\Exports;

use App\Models\Company;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;

class SellersExport implements FromView
{
    public function view(): View
    {
        $sellers = Company::query()->with([
            'service_information',
            'company_region',
            'support_menu.support_menu1_value',
            'support_menu.support_menu2_value',
            'support_menu.support_menu3_value',
            'support_menu.support_menu4_value',
            'support_menu.support_menu5_value',
            'support_menu.support_menu6_value',
            'support_menu.support_menu7_value',
            'support_menu.support_menu8_value',
            'topic'
        ])
            ->where('status', '!=', 'create')
            ->get();

        return view('exports.sellers', [
            'sellers' => $sellers
        ]);
    }
}
