<?php


namespace App\Repositories;

use App\Models\ContactForm;

class ContactFormReposiroty
{
    public function store($request)
    {
        $data = new ContactForm();
        return $this->valuable($data, $request);
    }

    private function valuable(ContactForm $data, $request)
    {
        $data->company_id = $request['company_id'];
        $data->company_name = $request['company_name'];
        $data->offcial_website_url = $request['offcial_website_url'];
        $data->name = $request['name'];
        $data->position = $request['position'];
        $data->email = $request['email'];
        $data->message = $request['message'];
        $data->type = $request['type'] ?? 'JAPAN';
        $data->save();

        return $data->id;
    }
}