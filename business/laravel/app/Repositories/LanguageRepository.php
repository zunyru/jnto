<?php


namespace App\Repositories;


use App\Models\Language;
use App\Models\LanguageForBranch;

class LanguageRepository
{
    public function getAll()
    {
        return Language::all();
    }

    public function getBranchAll()
    {
        return LanguageForBranch::all();
    }
}