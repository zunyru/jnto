<?php

namespace App\Repositories;

use Spatie\Activitylog\Models\Activity;

class ActivityLogRepository
{
    public function find($id)
    {
        return Activity::query()->findOrFail($id);
    }

    public function getPaginate($param)
    {
        $page = $param['page'] ?? 0;
        $start = $param['start'] ?? 0;
        $orders = $param['order'] ?? '';
        $id = $param->filters['id'] ?? '';

        $search = $param->filters['search'] ?? '';

        $length = $param['length'] ?? 50;

        $data = Activity::query();

        //search
        if (!empty($search)) {

            $data->orWhere(function ($query) use ($search) {
                $query->where('log_name', 'LIKE', "%$search%");
                $query->orWhere('description', 'LIKE', "%$search%");
                $query->orWhere('subject_id', 'LIKE', "%$search%");
                $query->orWhere('causer_id', 'LIKE', "%$search%");
            });
        }
        $data->orderByDesc('id');

        if (!empty($start)) {
            $page = $start / $length + 1;
        }

        return $data->paginate($length, '*', 'page', $page);
    }
}
