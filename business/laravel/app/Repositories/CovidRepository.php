<?php

namespace App\Repositories;

use App\Models\Covid19information;

class CovidRepository
{

    public function all_published()
    {
        return Covid19information::published()->get();
    }
    public function find($id)
    {
        return Covid19information::findOrFail($id);
    }

    public function getPaginate($param)
    {
        $orders = $param['order'] ?? '';
        $id = $param->filters['id'] ?? '';

        $search = $param->filters['search'] ?? '';

        $length = $param['length'] ?? 50;

        $data = Covid19information::query();

        //search
        if (!empty($search)) {

            $data->orWhere(function ($query) use ($search) {
                $query->where('title', 'LIKE', "%$search%");
            });
        }

        //order
        if (!empty($orders)) {
            $column_name = '';
            foreach ($orders as $order) {
                if ($order['column'] == 0 || $order['column'] == 1) {
                    $column_name = 'id';
                }
                if ($order['column'] == 2) {
                    $column_name = 'title';
                }
            }
            if (!empty($column_name)) {
                $data->orderBy($column_name, $order['dir']);
            }
        } else {
            $data->orderBy('id', 'DESC');
        }

        return $data->paginate($length);
    }

    public function store($request)
    {
        $data = new Covid19information();
        return $this->valuable($data, $request);
    }

    public function valuable(Covid19information $data, $request)
    {
        $data->title = $request->title ?? null;
        $data->url = $request->url ?? null;
        $data->status = $request->status ?? 'draft';
        $data->save();

        return $data->id;
    }

    public function delete($id)
    {
        $data = Covid19information::find($id);
        $data->delete();
    }

    public function status($request, $id)
    {
        $data = Covid19information::find($id);
        $data->status = $request->status;
        $data->save();

        return $data;
    }
}
