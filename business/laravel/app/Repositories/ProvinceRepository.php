<?php


namespace App\Repositories;


use App\Models\Province;
use App\Models\Region;

class ProvinceRepository
{
    public function getAllGroupProvince($options = [])
    {
        $provice = Region::with('province')->get();

        $collection = collect($provice->toArray());
        $grouped = $collection->groupBy('name_en');

        return $provinces = $grouped->all();
    }

    public function getAll($options = [])
    {
        return Province::all();
    }
}