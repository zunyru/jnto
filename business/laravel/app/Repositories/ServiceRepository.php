<?php


namespace App\Repositories;


use App\Models\Service;

class ServiceRepository
{
    public function find($id)
    {
        return Service::findOrFail($id);
    }

    public function store($request)
    {
        $data = new Service();
        return $this->valuable($data, $request);
    }

    public function findByCompanyIDCount($id)
    {
        return Service::where('company_id', $id)
            ->count();
    }

    public function findByCompanyID($id)
    {
        return Service::where('company_id', $id)
            ->first();
    }

    public function valuable($data, $request)
    {
        $data->company_id  = $request->company_id;
        $data->title = $request->title ?? null;
        $data->detail = $request->detail ?? null;
        if(!empty($request->main_photo)) {
            $data->main_photo = $request->main_photo ?? null;
        }
        if(!empty($request->other_photo)) {
            $data->other_photo = $request->other_photo ?? null;
        }
        if(!empty($request->other_photo2)) {
            $data->other_photo2 = $request->other_photo2 ?? null;
        }
        if(!empty($request->other_photo3)) {
            $data->other_photo3 = $request->other_photo3 ?? null;
        }

        !empty($request->download_main_photo) ? $data->download_main_photo = $request->download_main_photo : $data->download_main_photo = null;
        !empty($request->download_other_photo) ? $data->download_other_photo = $request->download_other_photo : $data->download_other_photo = null;
        !empty($request->download_other_photo2) ? $data->download_other_photo2 = $request->download_other_photo2 : $data->download_other_photo2 = null;
        !empty($request->download_other_photo3) ? $data->download_other_photo3 = $request->download_other_photo3 : $data->download_other_photo3 = null;
        !empty($request->download_condition) ? $data->download_condition = $request->download_condition : $data->download_condition = null;

        $data->pr_youtube = $request->pr_youtube ?? null;
        if(!empty($request->company_profile_image)) {
            $data->company_profile_image = $request->company_profile_image ?? null;
        }
        if(!empty($request->company_profile_pdf)) {
            $data->company_profile_pdf = $request->company_profile_pdf ?? null;
            $data->company_profile_pdf_size = $request->company_profile_pdf_size ?? null;
        }
        $data->about = $request->about ?? null;

        if(!empty($request->company_profile_pdf2)) {
            $data->company_profile_pdf2 = $request->company_profile_pdf2 ?? null;
            $data->company_profile_pdf2_size = $request->company_profile_pdf2_size ?? null;
        }
        $data->about2 = $request->about2 ?? null;

        if(!empty($request->company_profile_pdf3)) {
            $data->company_profile_pdf3 = $request->company_profile_pdf3 ?? null;
            $data->company_profile_pdf3_size = $request->company_profile_pdf3_size ?? null;
        }
        $data->about3 = $request->about3 ?? null;

        $data->photo_gallery_url = $request->photo_gallery_url ?? null;
        $data->about_url = $request->about_url ?? null;
        $data->photo_gallery_url2 = $request->photo_gallery_url2 ?? null;
        $data->about_url2 = $request->about_url2 ?? null;
        $data->photo_gallery_url3 = $request->photo_gallery_url3 ?? null;
        $data->about_url3 = $request->about_url3 ?? null;
        $data->is_accept = $request->is_accept ?? null;
        $data->save();

        return $data->id;
    }
}