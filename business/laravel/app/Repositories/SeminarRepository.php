<?php

namespace App\Repositories;

use App\Models\Seminar;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use League\Flysystem\SafeStorage;

class SeminarRepository
{
    public function all()
    {
        return Seminar::all();
    }

    public function all_published($type = '', $limit = '', $year = true, $limit_all = '')
    {
        $seminar_count = Seminar::query()->published()->get()->count();
        $seminar = Seminar::query();
        if (Auth::guard('seller')->check()) {
            $seminar->whereNotIn('target', ['buyer']);
            if ($type == 'jp') {
                $seminar->whereIn('target', ['seller']);
            }
            if ($type == 'th') {
                $seminar->whereIn('target', ['all', 'seller_th']);
            }
        } else if (Auth::guard('web')->check()) {
            $seminar->whereIn('target', ['all', 'buyer']);
        }

        if ($limit) {
            $seminar->skip(1)
                ->limit(4);
        }

        if ($limit_all) {
            $seminar->skip(1)
                ->limit($seminar_count - 1);
        }

        if (Auth::guard('web')->check()) {
            if ($year) {
                $seminar->where(function ($query) {
                    $query->where('year', Carbon::now()->year);
                    $query->orWhere('year', Carbon::now()->year - 1);
                });
            }
        }

        return $seminar->published()
            ->orderByDesc('year')
            ->orderByDesc('id')
            ->get();
    }

    public function getFirst($type = '', $year = true)
    {
        $seminar = Seminar::query();
        if (Auth::guard('seller')->check()) {
            $seminar->whereNotIn('target', ['buyer']);
            if ($type == 'th') {
                $seminar->whereIn('target', ['all', 'seller_th']);

            } else {
                $seminar->whereIn('target', ['all', 'buyer']);
            }
        } else if (Auth::guard('web')->check()) {
            $seminar->whereIn('target', ['all', 'buyer']);
        }
        if ($year) {
            $seminar->where(function ($query) {
                $query->where('year', Carbon::now()->year);
                $query->orWhere('year', Carbon::now()->year - 1);
            });
        }

        return $seminar->published()
            ->orderByDesc('year')
            ->orderByDesc('id')
            ->first();
    }

    public function find($ref_id)
    {
        $code = substr($ref_id, -6);
        $id = intval($code);

        return Seminar::findOrFail($id);
    }

    public function store($request)
    {
        $data = new Seminar();
        return $this->valuable($data, $request);
    }

    public function valuable(Seminar $data, $request)
    {
        $data->title = $request->title ?? null;
        $data->thumbnail = $request->thumbnail ?? $data->thumbnail;
        $data->video = $request->video ?? null;
        $data->list = $request->list ?? null;
        $data->sub_title = $request->sub_title ?? null;
        $data->display_date = $request->display_date ?? null;
        $data->display_time = $request->display_time ?? null;
        $data->subject_name = $request->subject_name ?? null;
        $data->detail = $request->detail ?? null;
        $data->pdf1 = $request->pdf1 ?? $data->pdf1;
        $data->pdf2 = $request->pdf2 ?? $data->pdf2;
        $data->pdf3 = $request->pdf3 ?? $data->pdf3;
        $data->pdf_title1 = $request->pdf_title1 ?? $data->pdf_title1;
        $data->pdf_title2 = $request->pdf_title2 ?? $data->pdf_title2;
        $data->pdf_title3 = $request->pdf_title3 ?? $data->pdf_title3;
        $data->pdf_size1 = $request->pdf_size1 ?? $data->pdf_size1;
        $data->pdf_size2 = $request->pdf_size2 ?? $data->pdf_size2;
        $data->pdf_size3 = $request->pdf_size3 ?? $data->pdf_size3;
        $data->status = $request->status ?? $data->status;
        $data->target = $request->target ?? $data->target ?? 'all';
        $data->year = $request->year ?? $data->year;
        !isset($data->updated_at) ?: $data->updated_at = $request->updated_at ?? $data->updated_at;
        $data->save();

        return $data->id;
    }

    public function getDetail($ref_id)
    {
        $code = substr($ref_id, -6);
        $id = intval($code);

        $seminar = Seminar::query();
        /*if (Auth::guard('seller')->check()) {
            $seminar->whereIn('target', ['seller','all']);
        } else if (Auth::guard('web')->check()) {
            $seminar->whereIn('target', ['all', 'buyer']);
        }*/

        return $seminar->published()->findOrFail($id);
    }

    public function getOther($ref_id)
    {
        $code = substr($ref_id, -6);
        $id = intval($code);

        $this_seminar = $this->find($id);

        $seminar = Seminar::query();

        if ($this_seminar->target == 'seller') {
            $seminar->whereIn('target', ['seller']);
        } else {
            if (Auth::guard('seller')->check()) {
                $seminar->where('target', 'all');
            } else {
                $seminar->whereIn('target', ['all', 'buyer']);
            }
        }

        $seminar->limit(3)
            //->offset($id)
            //->where('target', $this_seminar->target)
            ->where('created_at', '>=', $this_seminar->created_at)
            ->where('id', '!=', $this_seminar->id)
            ->published();

        return $seminar->get();
    }

    public function getOtherShow($ref_id)
    {
        $code = substr($ref_id, -6);
        $id = intval($code);

        $this_seminar = $this->find($id);

        $seminar = Seminar::query();


        if (Auth::guard('seller')->check()) {
            if ($this_seminar->target == 'seller') {
                $seminar->whereIn('target', ['seller']);
            } else {
                $seminar->whereIn('target', ['all', 'seller_th']);
            }
        } else {
            $seminar->whereIn('target', ['all', 'buyer']);
        }


        //$seminar->limit()
        //->offset($id)
        //->where('target', $this_seminar->target)
        if ($this_seminar->target != 'seller') {
            /*$seminar->where('year', '>=', $this_seminar->year);
            $seminar->where('list', '>=', $this_seminar->list);*/
            $seminar->where('updated_at', '>=', $this_seminar->updated_at);
        }

        $seminar
            //->where('created_at', '>=', $this_seminar->created_at)
            ->where('id', '!=', $this_seminar->id)
            //->orderBy('year', 'asc')
            //->orderBy('list', 'asc')
            ->orderBy('updated_at','ASC')
            ->published();

        return $seminar->get();
    }

    public function getPaginate($param)
    {
        $orders = $param['order'] ?? '';
        $id = $param->filters['id'] ?? '';
        $business_types = $param->filters['title'] ?? '';

        $search = $param->filters['search'] ?? '';

        $length = $param['length'] ?? 50;

        $data = Seminar::query();

        //search
        if (!empty($search)) {

            $code = substr($search, -6);
            $id = intval($code);
            $data->where(function ($query) use ($search, $id) {
                $query->where('id', 'LIKE', "%$id%")
                    ->orWhere('title', 'LIKE', "%$search%")
                    ->orWhere('year', 'LIKE', "%$search%");
            });
        }
        //order
        if (!empty($orders)) {
            $column_name = '';
            foreach ($orders as $order) {
                if ($order['column'] == 0 || $order['column'] == 1) {
                    $column_name = 'id';
                }
                if ($order['column'] == 2) {
                    $column_name = 'title';
                }
            }
            if (!empty($column_name)) {
                $data->orderBy($column_name, $order['dir']);
            }
        }

        return $data->paginate($length);
    }

    public function delete($id)
    {
        $data = Seminar::find($id);
        $data->delete();
    }

    public function status($request, $id)
    {
        $data = Seminar::find($id);
        $data->status = $request->status;
        $data->save();

        return $data;
    }

    public function statusTarget($request, $id)
    {
        $data = Seminar::find($id);
        $data->target = $request->target;
        $data->save();

        return $data;
    }
}
