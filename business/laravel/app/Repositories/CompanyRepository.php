<?php


namespace App\Repositories;


use App\Models\Company;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use phpDocumentor\Reflection\Types\Null_;
use Illuminate\Database\Eloquent\Builder;

class CompanyRepository
{
    public function all()
    {
        return Company::all();
    }

    public function findByName($name_en)
    {
        return Company::where('name_en', $name_en)->first();
    }

    public function find($id)
    {
        return Company::findOrFail($id);
    }

    public function findNameCount($name)
    {
        return Company::where('name_en', $name)->count();
    }

    public function findName($name)
    {
        return Company::where('name_en', $name)->firstOrFail();
    }

    public function store($request)
    {
        $data = new Company();
        return $this->valuable($data, $request);
    }

    public function valuable($data, $request)
    {
        if (!empty($request->logo)) {
            $data->logo = $request->logo;
        }

        $data->is_partner = $request->is_partner ?? $data->is_partner;
        $data->name_en = $request->name_en ?? $data->name_en;
        $data->name_ja = $request->name_ja ?? $data->name_ja;
        $data->address = $request->address ?? $data->address;
        $data->google_map_url = $request->google_map_url ?? $data->google_map_url;
        $data->official_website_url = $request->official_website_url ?? $data->official_website_url;
        $data->website_url = $request->website_url ?? $data->website_url;
        $data->facebook_url = $request->facebook_url ?? $data->facebook_url;
        $data->instagram_url = $request->instagram_url ?? $data->instagram_url;
        $data->youtube_channel_url = $request->youtube_channel_url ?? $data->youtube_channel_url;
        $data->twitter = $request->twitter ?? $data->twitter;
        $data->line = $request->line ?? $data->line;
        $data->department = $request->department ?? $data->department;
        $data->contact_person_name = $request->contact_person_name ?? $data->contact_person_name;
        $data->email = $request->email ?? $data->email;
        $data->tel = $request->tel ?? $data->tel;
        $data->is_branch_thai = $request->is_branch_thai ?? $data->is_branch_thai;
        $data->branch_company_name_en = $request->branch_company_name_en ?? $data->branch_company_name_en;
        $data->branch_company_name_th = $request->branch_company_name_th ?? $data->branch_company_name_th;
        $data->branch_address = $request->branch_address ?? $data->branch_address;
        $data->branch_google_map_url = $request->branch_google_map_url ?? $data->branch_google_map_url;
        $data->branch_thai_website_url = $request->branch_thai_website_url ?? $data->branch_thai_website_url;
        $data->branch_contact_person_name = $request->branch_contact_person_name ?? $data->branch_contact_person_name;
        $data->branch_department = $request->branch_department ?? $data->branch_department;
        $data->branch_email = $request->branch_email ?? $data->branch_email;
        $data->branch_tel = $request->branch_tel ?? $data->branch_tel;
        $data->status = !empty($request->status) ? $request->status : $data->status ?? 'confirm';
        $data->is_show_banner = $request->is_show_banner ?? $data->is_show_banner;
        $data->url_banner = $request->url_banner ?? $data->url_banner;
        $data->is_feature_banner = $request->is_feature_banner ?? $data->is_feature_banner;
        !empty($request->province_all) ? $data->province_all = $request->province_all ?? $data->province_all ?? 0 : $data->province_all = 0;
        $data->policy = $request->policy ?? $data->policy ?? 0;
        $data->user_login = $request->user_login ?? $data->user_login;
        $data->password = !empty($request->password) ? Hash::make($request->password) : $data->password;
        $data->last_login = $request->last_login ?? $data->last_login;
        $data->email_password_send_at = $request->email_password_send_at ?? $data->email_password_send_at;
        $data->active_status = $request->active_status ?? $data->active_status ?? 0;
        $data->privacy = $request->privacy ?? $data->privacy ?? 'public';
        $data->email_change = $request->email_change ?? $data->email_change ?? null;
        $data->send_email_change_at = $request->send_email_change_at ?? $data->send_email_change_at ?? null;

        !isset($request->registered) ?: $data->registered = $request->registered ?? $data->registered;
        !isset($request->licens_no) ?: $data->licens_no = $request->licens_no ?? $data->licens_no;

        !isset($request->email_contact) ?: $data->email_contact = $request->email_contact ?? $data->email_contact;

        !isset($request->pic_name) ?: $data->pic_name = $request->pic_name ?? $data->pic_name;
        !isset($request->pic_email) ?: $data->pic_email = $request->pic_email ?? $data->pic_email;

        $data->save();

        return $data->id;
    }

    public function confirm($request, $id)
    {
        $data = Company::find($id);
        $data->status = $request->status;
        $data->save();

        return $data;
    }

    public function active($request, $id)
    {
        $data = Company::find($id);
        $data->active_status = $request->active_status;
        $data->save();

        return $data;
    }

    public function delete($request, $id)
    {
        $data = Company::find($id);
        $data->delete();
    }

    public function getPaginate($param)
    {
        $page = $param['page'] ?? 0;
        $start = $param['start'] ?? 0;
        $orders = $param['order'] ?? false;
        $is_partner = $param->filters['is_partner'] ?? '';
        $is_public = $param->filters['is_public'] ?? '';
        $business_types = $param->filters['business_types'] ?? '';
        $is_branch_thai = $param->filters['is_branch_thai'] ?? '';
        $support_menus = $param->filters['support_menus'] ?? '';
        $province = $param->filters['province'] ?? '';
        $season = $param->filters['season'] ?? '';
        $type_of_travels = $param->filters['type_of_travels'] ?? '';
        $status = $param->filters['status'] ?? '';
        $active_status = $param->filters['status_active'] ?? '';

        $search = $param->filters['search'] ?? '';

        $length = $param['length'] ?? 50;

        $company = Company::query()
            ->with('support_menu')
            ->with('business_type_companies')
            ->with('service_information')
            ->with('support_menu.support_menu1_value')
            ->with('support_menu.support_menu2_value')
            ->with('support_menu.support_menu3_value')
            ->with('company_region');

        if (!empty($is_public)) {
            $company->published();
        } else {
            $company->where('status', '!=', Company::STATUS_CREATE);
        }

        //filter
        if ($is_partner != '') {
            $company->where('is_partner', $is_partner);
        }
        if ($is_branch_thai != '') {
            $company->where('is_branch_thai', $is_branch_thai);
        }
        if (!empty($status)) {
            $company->where('status', $status);
        }

        if ($active_status != '') {
            $company->where('active_status', $active_status);
        }

        if (!empty($business_types)) {
            $company->whereHas('business_type_companies', function ($query) use ($business_types) {
                return $query->where('business_type_id', $business_types);
            })->get();
        }

        if (!empty($support_menus)) {
            if ($support_menus == 'none') {
                $company->whereHas('support_menu', function ($query) {
                    return $query->whereNull('support_menu1')
                        ->whereNull('support_menu2')
                        ->whereNull('support_menu3')
                        ->whereNull('support_menu4')
                        ->whereNull('support_menu5')
                        ->whereNull('support_menu6')
                        ->whereNull('support_menu7')
                        ->whereNull('support_menu8');
                })->get();
            } else {
                $company->whereHas('support_menu', function ($query) use ($support_menus) {
                    return $query->where('support_menu1', $support_menus)
                        ->orWhere('support_menu2', $support_menus)
                        ->orWhere('support_menu3', $support_menus)
                        ->orWhere('support_menu4', $support_menus)
                        ->orWhere('support_menu5', $support_menus)
                        ->orWhere('support_menu6', $support_menus)
                        ->orWhere('support_menu7', $support_menus)
                        ->orWhere('support_menu8', $support_menus);
                })->get();
            }
        }


        if (!empty($province)) {
            if ($province != 'all') {
                $company->whereHas('company_region', function ($query) use ($province) {
                    return $query->where('region_id', $province);
                })->get();
            } else {
                $company->where('province_all', '1');
            }
        }

        if (!empty($season)) {
            $company->whereHas('company_season', function ($query) use ($season) {
                return $query->where('season_id', $season);
            })->get();
        }

        if (!empty($type_of_travels)) {
            $company->whereHas('company_type_of_travel', function ($query) use ($type_of_travels) {
                return $query->where('type_of_travel_id', $type_of_travels);
            })->get();
        }

        //search
        if (!empty($search)) {

            $code = substr($search, -6);
            $id = intval($code);
            $company->where(function ($query) use ($search, $id) {
                //$query->where('id', 'LIKE', "%$id%")
                $query->where('id', $id)
                    ->orWhere('name_en', 'LIKE', "%$search%")
                    ->orWhere('user_login', 'LIKE', "%$search%");
            });
            /* $company->orWhere(function ($query) use ($search) {
                 $query->whereHas('service_information', function ($query) use ($search) {
                     return $query->where('title', 'LIKE', "%$search%")
                         ->orWhere('detail', 'LIKE', "%$search%");
                 })->get();
             });*/
        }

        if (!empty($orders)) {
            $column_name = '';
            foreach ($orders as $order) {
                if ($order['column'] == 0) {
                    $column_name = 'id';
                }
                if ($order['column'] == 1) {
                    $column_name = 'name_en';
                }

                if ($order['column'] == 2) {
                    $column_name = 'user_login';
                }
                if ($order['column'] == 3) {
                    $column_name = 'user_login';
                }

                if ($order['column'] == 5) {
                    $column_name = 'status';
                }

                if (!empty($column_name)) {
                    $company->orderBy($column_name, $order['dir']);
                }
            }
        }


        if (!empty($start)) {
            $page = $start / $length + 1;
        }

        if (Auth::guard('seller')->check()) {
            $company->where('privacy', 'public');
        }

        return $company->paginate($length, '*', 'page', $page);
    }

    public function getPaginateByFrontend($param)
    {
        $page = $param['page'] ?? 0;
        $start = $param['start'] ?? 0;
        $orders = $param['order'] ?? false;
        $is_partner = $param->filters['is_partner'] ?? '';
        $is_public = $param->filters['is_public'] ?? '';
        $business_types = $param->filters['business_types'] ?? '';
        $is_branch_thai = $param->filters['is_branch_thai'] ?? '';
        $support_menus = $param->filters['support_menus'] ?? '';
        $province = $param->filters['province'] ?? [];
        $season = $param->filters['season'] ?? '';
        $type_of_travels = $param->filters['type_of_travels'] ?? '';
        $status = $param->filters['status'] ?? '';
        $active_status = $param->filters['status_active'] ?? '';

        $search = $param['search_keyword'] ?? '';

        $length = $param['length'] ?? 50;

        $random = $param['random'] ?? false;;

        $company = Company::query()
            ->selectRaw("companies.*,GROUP_CONCAT(company_region.region_id SEPARATOR ',') AS regions")
            ->with([
                'business_type_companies',
                'support_menu',
                'service_information',
                'support_menu.support_menu1_value',
                'support_menu.support_menu2_value',
                'support_menu.support_menu3_value',
            ])
            ->leftJoin('company_region', 'company_region.company_id', 'companies.id');

        if (!empty($is_public)) {
            $company->published();
        } else {
            $company->where('status', '!=', Company::STATUS_CREATE);
        }

        if ($is_partner != '') {
            $company->where('is_partner', $is_partner);
        }
        if ($is_branch_thai != '') {
            $company->where('is_branch_thai', $is_branch_thai);
        }
        if (!empty($status)) {
            $company->where('status', $status);
        }

        if ($active_status != '') {
            $company->where('active_status', $active_status);
        }

        if (!empty($business_types)) {
            $random = false;
            $company->whereHas('business_type_companies', function ($query) use ($business_types) {
                return $query->where('business_type_id', $business_types);
            })->get();
        }

        if (!empty($support_menus)) {
            $random = false;
            if ($support_menus == 'none') {
                $company->whereHas('support_menu', function ($query) {
                    return $query->whereNull('support_menu1')
                        ->whereNull('support_menu2')
                        ->whereNull('support_menu3')
                        ->whereNull('support_menu4')
                        ->whereNull('support_menu5')
                        ->whereNull('support_menu6')
                        ->whereNull('support_menu7')
                        ->whereNull('support_menu8');
                })->get();
            } else {
                $company->whereHas('support_menu', function ($query) use ($support_menus) {
                    return $query->where('support_menu1', $support_menus)
                        ->orWhere('support_menu2', $support_menus)
                        ->orWhere('support_menu3', $support_menus)
                        ->orWhere('support_menu4', $support_menus)
                        ->orWhere('support_menu5', $support_menus)
                        ->orWhere('support_menu6', $support_menus)
                        ->orWhere('support_menu7', $support_menus)
                        ->orWhere('support_menu8', $support_menus);
                })->get();
            }
        }

        if (!empty($province)) {
            $random = false;
            if ($province != 'all') {
                $company->whereHas('company_region', function ($query) use ($province) {
                    return $query->whereIn('region_id', $province);
                })->get();
            } else {
                $company->where('province_all', '1');
            }
        }

        if (!empty($season)) {
            $company->whereHas('company_season', function ($query) use ($season) {
                return $query->where('season_id', $season);
            })->get();
        }

        if (!empty($type_of_travels)) {
            $company->whereHas('company_type_of_travel', function ($query) use ($type_of_travels) {
                return $query->where('type_of_travel_id', $type_of_travels);
            })->get();
        }

        //order


        //search
        if (!empty($search)) {
            $random = false;
            $code = substr($search, -6);
            $id = intval($code);
            $company->where(function ($query) use ($search, $id) {
                //$query->where('id', 'LIKE', "%$id%")
                $query->where('id', $id)
                    ->orWhere('name_en', 'LIKE', "%$search%")
                    ->orWhere('user_login', 'LIKE', "%$search%");
            });
        }

        if (!empty($start)) {
            $page = $start / $length + 1;
        }

        if (Auth::guard('seller')->check()) {
            $company->where('privacy', 'public');
        }

        if (!empty($orders)) {
            $company->groupBy('id');
            if (sizeof($province) > 0) {
                $arr_province = collect($province)->implode(',');
                $company->orderByRaw("GROUP_CONCAT(company_region.region_id SEPARATOR ',') = '{$arr_province}' DESC");
            }
            $company->orderByRaw("companies.province_all = 1 ASC");
        }

        if ($random) {
            $company->inRandomOrder();
        }

        return $company->paginate($length, '*', 'page', $page);
    }

    /*public function getPaginateByFrontend($param)
    {
        $page = $param['page'] ?? 0;
        $start = $param['start'] ?? 0;
        $orders = $param['order'] ?? false;
        $is_partner = $param->filters['is_partner'] ?? '';
        $is_public = $param->filters['is_public'] ?? '';
        $business_types = $param->filters['business_types'] ?? '';
        $is_branch_thai = $param->filters['is_branch_thai'] ?? '';
        $support_menus = $param->filters['support_menus'] ?? '';
        $province = $param->filters['province'] ?? '';
        $season = $param->filters['season'] ?? '';
        $type_of_travels = $param->filters['type_of_travels'] ?? '';
        $status = $param->filters['status'] ?? '';
        $active_status = $param->filters['status_active'] ?? '';

        $search = $param['search_keyword'] ?? '';

        $length = $param['length'] ?? 50;

        $random = true;

        $company = Company::query()
            ->with('support_menu')
            ->with('business_type_companies')
            ->with('service_information')
            ->with('support_menu.support_menu1_value')
            ->with('support_menu.support_menu2_value')
            ->with('support_menu.support_menu3_value')
            ->with('company_region');

        if (!empty($is_public)) {
            $company->published();
        } else {
            $company->where('status', '!=', Company::STATUS_CREATE);
        }

        //filter
        if ($is_partner != '') {
            $company->where('is_partner', $is_partner);
        }
        if ($is_branch_thai != '') {
            $company->where('is_branch_thai', $is_branch_thai);
        }
        if (!empty($status)) {
            $company->where('status', $status);
        }

        if ($active_status != '') {
            $company->where('active_status', $active_status);
        }

        if (!empty($business_types)) {
            $random = false;
            $company->whereHas('business_type_companies', function ($query) use ($business_types) {
                return $query->where('business_type_id', $business_types);
            })->get();
        }

        if (!empty($support_menus)) {
            $random = false;
            if ($support_menus == 'none') {
                $company->whereHas('support_menu', function ($query) {
                    return $query->whereNull('support_menu1')
                        ->whereNull('support_menu2')
                        ->whereNull('support_menu3')
                        ->whereNull('support_menu4')
                        ->whereNull('support_menu5')
                        ->whereNull('support_menu6')
                        ->whereNull('support_menu7')
                        ->whereNull('support_menu8');
                })->get();
            } else {
                $company->whereHas('support_menu', function ($query) use ($support_menus) {
                    return $query->where('support_menu1', $support_menus)
                        ->orWhere('support_menu2', $support_menus)
                        ->orWhere('support_menu3', $support_menus)
                        ->orWhere('support_menu4', $support_menus)
                        ->orWhere('support_menu5', $support_menus)
                        ->orWhere('support_menu6', $support_menus)
                        ->orWhere('support_menu7', $support_menus)
                        ->orWhere('support_menu8', $support_menus);
                })->get();
            }
        }


        if (!empty($province)) {
            $random = false;
            if ($province != 'all') {
                $company->whereHas('company_region', function ($query) use ($province) {
                    return $query->where('region_id', $province);
                })->get();
            } else {
                $company->where('province_all', '1');
            }
        }

        if (!empty($season)) {
            $company->whereHas('company_season', function ($query) use ($season) {
                return $query->where('season_id', $season);
            })->get();
        }

        if (!empty($type_of_travels)) {
            $company->whereHas('company_type_of_travel', function ($query) use ($type_of_travels) {
                return $query->where('type_of_travel_id', $type_of_travels);
            })->get();
        }

        //search
        if (!empty($search)) {
            $random = false;
            $code = substr($search, -6);
            $id = intval($code);
            $company->where(function ($query) use ($search, $id) {
                //$query->where('id', 'LIKE', "%$id%")
                $query->where('id', $id)
                    ->orWhere('name_en', 'LIKE', "%$search%")
                    ->orWhere('user_login', 'LIKE', "%$search%");
            });
        }

        if (!empty($orders)) {
            $column_name = '';
            foreach ($orders as $order) {
                if ($order['column'] == 0) {
                    $column_name = 'id';
                }
                if ($order['column'] == 1) {
                    $column_name = 'name_en';
                }

                if ($order['column'] == 2) {
                    $column_name = 'user_login';
                }
                if ($order['column'] == 3) {
                    $column_name = 'user_login';
                }

                if ($order['column'] == 5) {
                    $column_name = 'status';
                }

                if (!empty($column_name)) {
                    $company->orderBy($column_name, $order['dir']);
                }
            }
        }

        if (!empty($start)) {
            $page = $start / $length + 1;
        }

        if (Auth::guard('seller')->check()) {
            $company->where('privacy', 'public');
        }

        if ($random) {
            $company->inRandomOrder();
        }

        return $company->paginate($length, '*', 'page', $page);
    }*/

    public function getDetail($ref_id, $preview = false)
    {
        $code = substr($ref_id, -6);
        $id = intval($code);

        $company = Company::where('id', $id);

        if (!$preview) {
            $company->where('status', Company::STATUS_PUBLISHED);
        }

        if (Auth::guard('seller')->check()) {
            if (Auth::guard('seller')->user()->id != $id) {
                $company->where('privacy', 'public');
            }
        }

        $company->with([
            'service_information',
            'support_menu.support_menu1_value',
            'support_menu.support_menu2_value',
            'support_menu.support_menu3_value',
            'support_menu.support_menu4_value',
            'support_menu.support_menu5_value',
            'company_region',
            'company_season',
            'company_special_feature',
            'company_type_of_travel',
            'business_type_companies',
            'company_language',
            'company_language_for_branch',
            'topic'
        ]);

        if (!$preview) {
            $company->published();
        }

        if (!$preview) {
            if (Auth::guard('seller')->check()) {
                if (Auth::guard('seller')->user()->privacy == 'private') {
                    $company->where('privacy', 'private');
                    $company->where('id', Auth::guard('seller')->user()->id);
                } else {
                    $company->where('privacy', 'public');
                }
            }
        }

        return $company->first();
    }

    public function getOther($ref_id, $preview = false, $mode = 'all')
    {
        $code = substr($ref_id, -6);
        $id = intval($code);

        $company = Company::where('id', $id)
            ->with([
                'business_type_companies',
                'company_region',
                'topic'
            ])
            ->first();

        $business_types_arr = [];
        if (!empty($company->business_type_companies)) {
            foreach ($company->business_type_companies as $key => $item) {
                $business_types_arr[] = $item->id;
            }
        }

        $province_arr = [];
        if (!empty($company->company_region)) {
            foreach ($company->company_region as $key => $item) {
                $province_arr[$key] = $item->id;
            }
        }

        $companies = Company::where('id', '!=', $id)
            ->with([
                'service_information',
                'company_region',
                'support_menu',
                'topic'
            ]);


        if ($mode == 'region') {
            $companies->whereHas('company_region', function ($query) use ($province_arr) {
                return $query->whereIn('region_id', $province_arr);
            });
        } else if ($mode == 'business_type') {
            $companies->whereHas('company_region', function ($query) use ($province_arr) {
                return $query->whereIn('region_id', $province_arr);
            });

            $companies->orWhereHas('business_type_companies', function ($query) use ($business_types_arr) {
                return $query->whereIn('business_type_id', $business_types_arr);
            });
        }

        $companies->inRandomOrder();
        $companies->published();

        if (Auth::guard('seller')->check()) {
            $companies->where('privacy', 'public');
        }

        $companies->limit(12);

        return $companies->get();
    }

    public function getHomePrivate()
    {
        $companies = Company::with([
            'service_information',
            'company_region',
            'support_menu',
            'topic'
        ])->where('id', Auth::guard('seller')->user()->id);

        return $companies->get();
    }

    public function getHome()
    {
        $companies = Company::with([
            'service_information',
            'company_region',
            'support_menu',
            'topic'
        ]);

        $companies->inRandomOrder()
            ->limit(12)
            ->published();

        if (Auth::guard('seller')->check()) {
            $companies->where('privacy', 'public');
        }


        return $companies->get();
    }

    public function checkHasUser(string $user_name): int
    {
        return Company::query()->where('user_login', $user_name)
            ->whereIn('status', ['confirm', 'published'])
            ->where('active_status', true)
            ->count();
    }

    public function findUsername(string $user_name)
    {
        return Company::query()->where('user_login', $user_name)
            ->whereIn('status', ['confirm', 'published'])
            ->where('active_status', true)
            ->first();
    }

    public function findToken(string $token)
    {
        return Company::query()->where('remember_token', $token)
            ->whereIn('status', ['confirm', 'published'])
            ->firstOrFail();
    }
}