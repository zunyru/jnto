<?php

namespace App\Repositories;

use App\Models\Company;
use App\Models\NewsJnto;
use App\Models\NewsJntoPartner;
use App\Models\SellerRevisionHistory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class SellerRevisionHistoryRepository
{
    public function getHomeList()
    {
        return SellerRevisionHistory::query()
            ->with(['seller'])
            ->whereHas('seller', function (Builder $query) {
                if (Auth::guard('seller')->check()) {
                    $query->where('privacy', 'public');
                }
                $query->published();
            })
            ->limit(10)
            ->orderByDesc('id')
            ->get();
    }

    public function getHomeHitoryList()
    {
        return SellerRevisionHistory::query()
            ->orWhereHas('seller', function ($q) {
                $q->where('status', 'published');
            })
            ->orWhereHasMorph(
                'newsJnto',
                [NewsJnto::class],
                function (Builder $query) {
                    $query->where('status', 'published');
                }
            )
            ->orWhereHasMorph(
                'newsJntoPartners',
                [NewsJntoPartner::class],
                function (Builder $query) {
                    $query->where('status', 'published');
                }
            )
            ->orWhereHasMorph(
                'sellers',
                [Company::class],
                function (Builder $query) {
                    $query->where('status', 'published');
                }
            )
            ->limit(10)
            ->orderByDesc('id')
            ->get();
    }

    public function store($company_id, $params)
    {
        $revision_history = SellerRevisionHistory::query()
            ->where('company_id', $company_id)
            ->whereDate('created_at', '>=', Carbon::now()->subDays(1)->toDateTimeString())
            ->first();

        //เช็คข้อมูลที่อัปเดท เกิน 24 ชม.
        if (empty($revision_history)) {
            $revision_text = [];
            if (isset($params['main_photo'])
                || isset($params['other_photo'])
                || isset($params['other_photo2'])
                || isset($params['other_photo3'])
            ) {
                $revision_text[] = 'Photo';
            }

            if (isset($params['pr_youtube'])) {
                $revision_text[] = 'Video';
            }

            if (isset($params['company_profile_pdf'])
                || isset($params['company_profile_pdf2'])
                || isset($params['company_profile_pdf3'])
            ) {
                $revision_text[] = 'PDF';
            }

            if (isset($params['support_menu'])) {
                $revision_text[] = 'Support Menu';
            }

            $seller = (new CompanyRepository())->find($company_id);

            $revision_text_implode = collect($revision_text)->implode(',');

            $params['title'] = $seller->name_en . " อัพเดทข้อมูลใหม่ (" . $revision_text_implode . ")";

            $revision = new SellerRevisionHistory();
            $revision->company_id = $company_id;
            $revision->revision = "App\\Models\\Company";
            $revision->revision_details = $params;
            $revision->save();
        }

        return true;
    }

    public function findDuplicate($id, $model)
    {
        return SellerRevisionHistory::query()
            ->where('company_id', $id)
            ->where('revision', $model)
            ->first();
    }
}
