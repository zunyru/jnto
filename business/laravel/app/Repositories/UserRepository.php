<?php

namespace App\Repositories;


use App\Models\User;

class UserRepository
{
    public function find($id)
    {
        return User::findOrFail($id);
    }

    public function findByToken($token)
    {
        return User::query()->where('remember_token', $token)->first();
    }

    public function getPaginate($param)
    {
        $orders = $param['order'] ?? '';
        $id = $param->filters['id'] ?? '';

        $search = $param->filters['search'] ?? '';

        $length = $param['length'] ?? 50;

        $data = User::query();

        $data->with('role', function ($q) {
            return $q->where('id', '!=', 1);
        });
        $data->where('id', '!=', 1);
        //search
        if (!empty($search)) {

            $data->where(function ($query) use ($search) {
                $query->where('username', 'LIKE', "%$search%");
            });
        }
        $data->whereIn('role_id', [
            2, 3, 4, 5, 6
        ]);
        //order
        if (!empty($orders)) {
            $column_name = '';
            foreach ($orders as $order) {
                if ($order['column'] == 0 || $order['column'] == 1) {
                    $column_name = 'id';
                }
                if ($order['column'] == 2) {
                    $column_name = 'title';
                }
            }
            if (!empty($column_name)) {
                $data->orderBy($column_name, $order['dir']);
            }
        } else {
            $data->orderBy('id', 'DESC');
        }

        return $data->paginate($length);
    }
}
