<?php


namespace App\Repositories;


use App\Models\Season;

class SeasonRepository
{
    public function all()
    {
        return Season::all();
    }
}