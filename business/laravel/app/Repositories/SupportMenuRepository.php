<?php


namespace App\Repositories;


use App\Models\CompanySupportMenu;
use App\Models\SupportMenu;

class SupportMenuRepository
{
    public function getAll()
    {
        return SupportMenu::all();
    }

    public function find($id)
    {
        return SupportMenu::find($id);
    }

    public function findByCompanyID($id)
    {
        return CompanySupportMenu::where('company_id', $id)
            ->first();
    }

    public function findByCompanyIDCount($id)
    {
        return CompanySupportMenu::where('company_id', $id)
            ->count();
    }

    public function storeCompanySupportMenu($request)
    {
        $data = new CompanySupportMenu();
        return $this->valuable($data, $request);
    }

    public function valuable($data, $request)
    {
        $data->company_id = $request->company_id;
        $data->support_menu1 = !empty($request->support_menu1) ? $request->support_menu1 : null;
        $data->manu_name1 = !empty($request->manu_name1) ? $request->manu_name1 : null;
        $data->support_menu2 = !empty($request->support_menu2) ? $request->support_menu2 : null;
        $data->manu_name2 = !empty($request->manu_name2) ? $request->manu_name2 : null;
        $data->support_menu3 = !empty($request->support_menu3) ? $request->support_menu3 : null;
        $data->manu_name3 = !empty($request->manu_name3) ? $request->manu_name3 : null;
        $data->support_menu4 = !empty($request->support_menu4) ? $request->support_menu4 : null;
        $data->manu_name4 = !empty($request->manu_name4) ? $request->manu_name4 : null;
        $data->support_menu5 = !empty($request->support_menu5) ? $request->support_menu5 : null;
        $data->manu_name5 = !empty($request->manu_name5) ? $request->manu_name5 : null;
        $data->support_menu6 = !empty($request->support_menu6) ? $request->support_menu6 : null;
        $data->manu_name6 = !empty($request->manu_name6) ? $request->manu_name6 : null;
        $data->support_menu7 = !empty($request->support_menu7) ? $request->support_menu7 : null;
        $data->manu_name7 = !empty($request->manu_name7) ? $request->manu_name7 : null;
        $data->support_menu8 = !empty($request->support_menu8) ? $request->support_menu8 : null;
        $data->manu_name8 = !empty($request->manu_name8) ? $request->manu_name8 : null;
        if(!empty($request->support_menu_about_pdf)) {
            $data->support_menu_about_pdf = $request->support_menu_about_pdf ?? null;
            $data->support_menu_about_pdf_size = $request->support_menu_about_pdf_size ?? null;
        }
        if(!empty($request->support_menu_about_image)) {
            $data->support_menu_about_image = $request->support_menu_about_image ?? null;
        }
        $data->support_about = $request->support_about ?? null;
        $data->save();

        return $data->id;
    }
}