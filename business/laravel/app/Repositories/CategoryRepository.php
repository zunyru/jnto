<?php


namespace App\Repositories;


use App\Models\Season;
use App\Models\SpecialExperience;
use App\Models\SpecialFeature;
use App\Models\TypeOfTravel;

class CategoryRepository
{
    public function getSeasonAll()
    {
        return Season::all();
    }

    public function getSpecialFeatureAll()
    {
        return SpecialFeature::all();
    }

    public function getTypeOfTravelAll()
    {
        return TypeOfTravel::all();
    }

    public function getSpecialExperienceAll()
    {
        return SpecialExperience::all();
    }
}