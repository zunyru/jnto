<?php


namespace App\Repositories;


use App\Models\BusinessType;

class BusinessTypeRepository
{
    public function getAll()
    {
        return BusinessType::all();
    }

    public function getRendom()
    {
        return BusinessType::query()->inRandomOrder()->get();
    }
}