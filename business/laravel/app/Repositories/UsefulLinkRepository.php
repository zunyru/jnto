<?php

namespace App\Repositories;

use App\Models\Category;
use App\Models\Usefullink;
use Illuminate\Database\Eloquent\Builder;

class UsefulLinkRepository
{
    public function all_published()
    {
        return Usefullink::published()->get();
    }

    public function group_category()
    {
        return Category::query()
            ->with('useful_links')
            ->whereHas('useful_links', function (Builder $query) {
                $query->where('status', Usefullink::STATUS_PUBLISHED);
            })->published()
            ->orderBy('order', 'asc')
            ->get();
    }

    public function find($id)
    {
        return Usefullink::findOrFail($id);
    }

    public function getPaginate($param)
    {
        $orders = $param['order'] ?? '';
        $id = $param->filters['id'] ?? '';

        $search = $param->filters['search'] ?? '';

        $length = $param['length'] ?? 50;

        $data = Usefullink::query();

        //search
        if (!empty($search)) {

            $data->orWhere(function ($query) use ($search) {
                $query->where('title', 'LIKE', "%$search%");
            });
            $data->OrWhereHas('category', function ($query) use ($search) {
                $query->where('title', 'LIKE', "%$search%");
            });
        }

        //order
        if (!empty($orders)) {
            $column_name = '';
            foreach ($orders as $order) {
                if ($order['column'] == 0 || $order['column'] == 1) {
                    $column_name = 'id';
                }
                if ($order['column'] == 2) {
                    $column_name = 'title';
                }
            }
            if (!empty($column_name)) {
                $data->orderBy($column_name, $order['dir']);
            }
        } else {
            $data->orderBy('order', 'ASC');
        }


        return $data->paginate($length);
    }

    public function store($request)
    {
        $data = new Usefullink();
        return $this->valuable($data, $request);
    }

    public function valuable(Usefullink $data, $request)
    {
        $data->title = $request->title ?? null;
        $data->thumbnail_photo = $request->thumbnail_photo ?? $data->thumbnail_photo;
        $data->icon = $request->icon ?? $data->icon;
        $data->url = $request->url ?? null;
        $data->category_id = $request->category_id ?? null;
        $data->status = $request->status ?? 'draft';
        !isset($request->order) ?: $data->order = $request->order ?? $data->order;
        $data->save();

        return $data->id;
    }

    public function delete($id)
    {
        $data = Usefullink::find($id);
        $data->delete();
    }

    public function status($request, $id)
    {
        $data = Usefullink::find($id);
        $data->status = $request->status;
        $data->save();

        return $data;
    }
}
