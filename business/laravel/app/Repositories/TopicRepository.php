<?php

namespace App\Repositories;

use App\Models\Topic;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Models\Setting;

class TopicRepository
{
    public function getTopic($company_id)
    {
        return Topic::query()
            ->where('company_id', $company_id)
            ->first();
    }

    public function find($id)
    {
        return Topic::query()->find($id);
    }

    public function store($request)
    {
        $data = new Topic();
        return $this->valiable($data, $request);
    }

    public function storeEvent($request)
    {
        $data = new Topic();
        return $this->valiableEvent($data, $request);
    }

    public function valiable(Topic $data, $request)
    {
        !empty($request->company_id) ? $data->company_id = $request->company_id : $data->company_id = null;
        $data->category = $request->category ?? $data->category;
        $data->photo = $request->photo ?? $data->photo;
        $data->event_date = $request->category == 'event'
            ? $request->event_date
            : null;
        $data->title = $request->title ?? $data->title;
        $data->detail = $request->detail ?? $data->detail;
        $data->status = $request->status ?? $data->status;

        !isset($request->template) ?: $data->template = $request->template ?? $data->template;

        $data->event_date_start = $request->event_date_start ?? $data->event_date_start;
        $data->event_date_end = $request->event_date_end ?? $data->event_date_end;

        $data->save();

        return $data;
    }

    public function valiableEvent(Topic $data, $request)
    {
        $data->category = $request->category ?? $data->category;

        $data->title = $request->title ?? $data->title;

        $data->status = $request->status ?? $data->status;

        !isset($request->template) ?: $data->template = $request->template ?? $data->template;

        $data->event_date_start = $request->event_date_start ?? $data->event_date_start;
        $data->event_date_end = $request->event_date_end ?? $data->event_date_end;

        if ($request->template == 'page') {
            $data->detail = $request->detail ?? null;
            $data->photo1 = $request->photo1 ?? $data->photo1;
            $data->photo2 = $request->photo2 ?? $data->photo2;
            $data->photo3 = $request->photo3 ?? $data->photo3;
            $data->photo4 = $request->photo4 ?? $data->photo4;
            $data->pdf1 = $request->pdf1 ?? $data->pdf1;
            $data->pdf2 = $request->pdf2 ?? $data->pdf2;
            $data->pdf3 = $request->pdf3 ?? $data->pdf3;
            $data->pdf_size1 = $request->pdf_size1 ?? $data->pdf_size1;
            $data->pdf_size2 = $request->pdf_size2 ?? $data->pdf_size2;
            $data->pdf_size3 = $request->pdf_size3 ?? $data->pdf_size3;
            $data->file_description1 = $request->file_description1 ?? $data->file_description1;
            $data->file_description2 = $request->file_description2 ?? $data->file_description2;
            $data->file_description3 = $request->file_description3 ?? $data->file_description3;

            $data->link = null;

        }
        if ($request->template == 'link-only') {
            $data->link = $request->link ?? null;

            $data->detail = null;
            $data->photo1 = null;
            $data->photo2 = null;
            $data->photo3 = null;
            $data->photo4 = null;
            $data->pdf1 = null;
            $data->pdf2 = null;
            $data->pdf3 = null;
            $data->pdf_size1 = null;
            $data->pdf_size2 = null;
            $data->pdf_size3 = null;
            $data->file_description1 = null;
            $data->file_description2 = null;
            $data->file_description3 = null;

        }

        $data->save();

        return $data;
    }

    public function delete($id)
    {
        return $this->find($id)->delete();
    }

    public function getTopics($params = [])
    {
        $status = $params['status'] ?? false;
        $privacy = $params['privacy'] ?? false;
        $company_id = $params['company_id'] ?? false;
        $event = $params['event'] ?? false;
        $paginate = $params['paginate'] ?? false;
        $today = $params['today'] ?? false;
        $jnto = $params['jnto'] ?? false;
        $event_start = $params['event_start'] ?? false;
        $is_event = $params['is_event'] ?? false;
        $is_home = $params['is_home'] ?? false;


        $topics = Topic::query();

        !$status ?: $topics->where('topics.status', $status);
        !$event ?: $topics->where('category', $event);

        if (Auth::guard('seller')->check()) {
            !$privacy ?: $topics->with(['company'])
                ->whereHas('company', function ($sub_q) use ($privacy, $company_id) {

                    $sub_q->where('privacy', $privacy);
                    $sub_q->where('active_status', true);
                    $sub_q->where('status', 'published');
                });
        } elseif (Auth::guard('web')->check()) {
            $topics->with(['company'])
                ->whereHas('company', function ($sub_q) {
                    $sub_q->where('active_status', true);
                    $sub_q->where('status', 'published');
                });
        }

        if ($privacy == 'private') {
            $topics->where('company_id', $company_id);
        }

        if ($privacy != 'private') {
            $topics->limit(12);
        }

        !$today ?:
            $topics->where(function ($q) {
                $q->whereDate('event_date_start', '>=', Carbon::today())
                    ->orWhereDate('event_date_end', '>=', Carbon::today());
            });

        $start_event = Setting::query()
            ->where('key', 'admin.dashboard.start_event')
            ->first();

        if (!empty($start_event)) {
            !$event_start ?:
                $topics->whereDate('topics.updated_at', '>=', $start_event->value);

        }

        if ($is_event) {
            $topics->where(function ($q) {
                $q->whereNotNull('event_date_start');
                $q->whereNotNull('event_date_end');
            });
        }

        if ($is_event) {
            if($is_home){
                $topics
                    ->rightJoin('companies', 'companies.id', 'topics.company_id')
                    ->orderByDesc('topics.updated_at');
            }else {
                $topics
                    ->rightJoin('companies', 'companies.id', 'topics.company_id')
                    ->orderBy('topics.event_date_start')
                    ->orderBy('topics.event_date_end')
                    ->orderBy('companies.name_en', 'ASC');
            }
        }

        if (!empty($event) && $event == 'event') {
            $topics->orderBy('event_date_start');
        } else {
            $topics->orderByDesc('topics.updated_at');
        }

        if ($jnto) {
            $topics->whereNull('company_id');
            $topics->publishedJnd();
        } else {
            $topics->whereNotNull('company_id');
            $topics->publishedDone();
        }

        return $paginate
            ? $topics->paginate(16)
            : $topics->get();
    }

    public function updateStatus($param, $seller_id)
    {
        $topic = Topic::query()->where('company_id', $seller_id)->first();

        if ($topic) {
            $topic->status = $param->topic_status;
            return $topic->save();
        }
        return;
    }

    public function getPaginate($param)
    {
        $public = $param['public'] ?? false;
        $start = $param['start'] ?? 0;
        $orders = $param['order'] ?? '';
        $orderBy = $param['orderBy'] ?? '';
        $id = $param->filters['id'] ?? '';
        $business_types = $param->filters['title'] ?? '';

        $search = $param->filters['search'] ?? '';

        $length = $param['length'] ?? 50;

        $news = Topic::query()->whereNull('company_id');
        if ($public) {
            $news->published();
        }
        //search
        if (!empty($search)) {

            $code = substr($search, -6);
            $id = intval($code);
            $news->where(function ($query) use ($search, $id) {
                $query->where('id', 'LIKE', "%$id%")
                    ->orWhere('title', 'LIKE', "%$search%");
            });
        }
        //order
        if (!empty($orders)) {
            $column_name = '';
            foreach ($orders as $order) {
                if ($order['column'] == 0 || $order['column'] == 1) {
                    $column_name = 'id';
                }
                if ($order['column'] == 2) {
                    $column_name = 'title';
                }
            }
            if (!empty($column_name)) {
                $news->orderBy($column_name, $order['dir']);
            }
        }
        if (!empty($orderBy)) {
            $news->orderBy('created_at', 'DESC');
        }

        $page = $start / $length + 1;
        return $news->paginate($length, '*', 'page', $page);
    }

    public function status($request, $id)
    {
        $data = Topic::find($id);
        $data->status = $request->status;
        $data->save();

        return $data;
    }

    public function getEventJntoDetail($ref_id, $preview = false)
    {
        $code = substr($ref_id, -6);
        $id = intval($code);

        $topic = Topic::query()
            ->whereNull('company_id');

        if (!$preview) {
            $topic->published();
        }

        return $topic->findOrFail($id);
    }

    public function getEventJntoOther($ref_id, $preview = false)
    {
        $code = substr($ref_id, -6);
        $id = intval($code);

        $topic = Topic::query()
            ->where('id', '!=', $id)
            ->whereNull('company_id')
            ->orderByDesc('updated_at');
        if (!$preview) {
            $topic->published();
        }
        $today = Carbon::today();
        $topic->whereDate('event_date_start', '>=', $today)
            ->whereDate('event_date_end', '>=', $today);

        $topic->orWhere(function ($q) use ($today) {
            $q->whereDate('event_date_end', '<=', $today);
            $q->whereDate('event_date_end', '>=', $today);
        });

        return $topic->get();
    }

    public function getTopicAllEvent($guard, $privacy, $company_id, $type)
    {
        $topics = Topic::query();
        $topics->where('category', 'event');
        if ($type == 'jnto') {
            $topics->whereIn('status', ['published']);
            $topics->whereNull('company_id');
            $topics->orderBy('event_date_start', 'asc');
        } else {
            $topics->whereIn('topics.status', ['done']);
            $topics->with(['company'])
                ->whereHas('company', function ($sub_q) use ($privacy, $company_id) {

                    if (Auth::guard('seller')->check()) {
                        $sub_q->where('privacy', $privacy);
                    }

                    $sub_q->where('active_status', true);
                    $sub_q->where('status', 'published');

                    if ($privacy == 'private') {
                        $sub_q->where('company_id', $company_id);
                    }
                });

            $topics
                ->rightJoin('companies', 'companies.id', 'topics.company_id')
                ->orderBy('topics.event_date_start')
                ->orderBy('topics.event_date_end')
                ->orderBy('companies.name_en', 'ASC');

        }


        /*$topics->where(function ($q) {
            $q->whereDate('event_date_start', '>=', Carbon::today())
                ->orWhereDate('event_date_end', '>=', Carbon::today());
        });*/


        return $topics->get();
    }

    public function getTopicCalendarList($date, $is_next = false)
    {
        $_today = Carbon::today();
        $topics = Topic::query()
            ->where('category', 'event');


        if ($is_next != "false") {
            if (empty($date)) {
                $today = Carbon::today();
            } else {
                $today = Carbon::parse($date);
            }

            $startOfMonth = $today->startOfMonth()->format('Y-m-d');
            $endOfMonth = $today->endOfMonth()->format('Y-m-d');
            $topics->where(function ($q) use ($today, $endOfMonth, $startOfMonth) {
                $q->whereDate('event_date_start', '<=', $today)
                    ->whereDate('event_date_end', '>=', $endOfMonth);

                $q->orWhere(function ($q) use ($startOfMonth, $endOfMonth) {
                    $q->whereDate('event_date_end', '<=', $endOfMonth);
                    $q->whereDate('event_date_end', '>=', $startOfMonth);
                });
            });

        } else {
            $current_date = Carbon::parse($date)->format('Y-m-d');

            $topics->whereDate('event_date_start', '<=', $current_date)
                ->whereDate('event_date_end', '>=', $current_date);

        }


        $topics->orderBy('event_date_start', 'ASC');
        $topics->orderBy('event_date_end', 'ASC');

        return $topics->get();
    }

    public function getTopicsAll($params = [])
    {
        $paginate = $params['paginate'] ?? false;
        $today = $params['today'] ?? false;

        $topics = Topic::query()->select('topics.*');

        $topics->whereIn('topics.status', ['done', 'published']);


        if (Auth::guard('seller')->check()) {
            $privacy = Auth::guard('seller')->user()->privacy;
            $company_id = Auth::guard('seller')->user()->company_id;
            $topics->with(['company'])
                ->whereHas('company', function ($sub_q) use ($privacy, $company_id) {

                    $sub_q->where('privacy', $privacy);
                    $sub_q->where('active_status', true);
                    $sub_q->where('status', 'published');
                });
        } elseif (Auth::guard('web')->check()) {
            $topics->with(['company'])
                ->whereHas('company', function ($sub_q) {
                    $sub_q->where('active_status', true);
                    $sub_q->where('status', 'published');
                });
        }

        $start_event = Setting::query()
            ->where('key', 'admin.dashboard.start_event')
            ->first();

        if (!empty($start_event)) {
            $topics->whereDate('topics.updated_at', '>=', $start_event->value);
        }

        !$today ?:
            $topics->where(function ($q) {
                //$q->whereDate('event_date_start', '>=', Carbon::today())->orWhereDate('event_date_end', '>=', Carbon::today());
                $q->whereRaw("IF( category = 'event',( date( `event_date_start` ) >= ? OR date( `event_date_end` ) >= ? ), company_id IS NOT NULL )",
                    [
                        Carbon::today(),
                        Carbon::today()
                    ]
                );
            });

        $start_event = Setting::query()
            ->where('key', 'admin.dashboard.start_event')
            ->first();

        if (!empty($start_event)) {
            $topics->whereDate('topics.updated_at', '>=', $start_event->value);
        }


        $topics
            ->rightJoin('companies', 'companies.id', 'topics.company_id');


        if (!empty($event) && $event == 'event') {
            $topics->orderBy('event_date_start');
        } else {
            $topics->orderByDesc('topics.updated_at');
        }


        return $paginate
            ? $topics->paginate(16)
            : $topics->get();
    }
}
