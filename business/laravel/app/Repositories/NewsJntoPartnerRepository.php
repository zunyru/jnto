<?php

namespace App\Repositories;

use App\Models\NewsJntoPartner;
use App\Models\SellerRevisionHistory;
use Illuminate\Support\Facades\Auth;

class NewsJntoPartnerRepository
{
    public function find($ref_id)
    {
        $code = substr($ref_id, -6);
        $id = intval($code);

        return NewsJntoPartner::query()->findOrFail($id);
    }

    public function store($request)
    {
        $data = new NewsJntoPartner();
        $request->is_create = true;
        return $this->valuable($data, $request);
    }

    public function valuable(NewsJntoPartner $data, $request)
    {
        $request = (object)$request;
        $data->company_id = !empty($request->company_id) ? $request->company_id : null;
        $data->title = $request->title ?? null;
        $data->template = $request->template;

        if ($request->template == 'full-content') {
            $data->detail = $request->detail;
            $data->photo1 = $request->photo1 ?? $data->photo1;
            $data->photo2 = $request->photo2 ?? $data->photo2;
            $data->photo3 = $request->photo3 ?? $data->photo3;
            $data->photo4 = $request->photo4 ?? $data->photo4;
            $data->pdf1 = $request->pdf1 ?? $data->pdf1;
            $data->pdf2 = $request->pdf2 ?? $data->pdf2;
            $data->pdf3 = $request->pdf3 ?? $data->pdf3;
            $data->pdf_size1 = $request->pdf_size1 ?? $data->pdf_size1;
            $data->pdf_size2 = $request->pdf_size2 ?? $data->pdf_size2;
            $data->pdf_size3 = $request->pdf_size3 ?? $data->pdf_size3;
            $data->file_description1 = $request->file_description1 ?? null;
            $data->file_description2 = $request->file_description2 ?? null;
            $data->file_description3 = $request->file_description3 ?? null;

            $data->link = null;

            $data->pdf_only = null;
            $data->pdf_size_only = null;
            $data->file_description_only = null;
        }
        if ($request->template == 'link-only') {
            $data->link = $request->link ?? null;

            $data->detail = null;
            $data->photo1 = null;
            $data->photo2 = null;
            $data->photo3 = null;
            $data->photo4 = null;
            $data->pdf1 = null;
            $data->pdf2 = null;
            $data->pdf3 = null;
            $data->pdf_size1 = null;
            $data->pdf_size2 = null;
            $data->pdf_size3 = null;
            $data->file_description1 = null;
            $data->file_description2 = null;
            $data->file_description3 = null;

            $data->pdf_only = null;
            $data->pdf_size_only = null;
            $data->file_description_only = null;
        }
        if ($request->template == 'PDF-only') {
            $data->pdf_only = $request->pdf_only ?? $data->pdf_only;
            $data->pdf_size_only = $request->pdf_size_only ?? $data->pdf_size_only;
            $data->file_description_only = $request->file_description_only ?? $data->file_description_only;

            $data->detail = null;
            $data->photo1 = null;
            $data->photo2 = null;
            $data->photo3 = null;
            $data->photo4 = null;
            $data->pdf1 = null;
            $data->pdf2 = null;
            $data->pdf3 = null;
            $data->pdf_size1 = null;
            $data->pdf_size2 = null;
            $data->pdf_size3 = null;
            $data->file_description1 = null;
            $data->file_description2 = null;
            $data->file_description3 = null;

            $data->link = null;
        }


        $data->status = $request->status;
        $data->save();

        $params = [];
        if (!empty($request->is_create)
            || ($data->status == 'draft' && $request->status == 'published')) {

            $params['title'] = $request->title;
            $params['details'] = $request->all();

            $id = $data->id;
            $model = "App\\Models\\NewsJntoPartner";

            $this->storeHistory($id, $model, $params);
        }

        return $data->id;
    }

    public function getDetail($ref_id, $preview = false)
    {
        $code = substr($ref_id, -6);
        $id = intval($code);

        $news = NewsJntoPartner::query();
        if (!$preview) {
            $news->published();
        }

        return $news->findOrFail($id);
    }

    public function getOther($ref_id, $preview = false)
    {
        $code = substr($ref_id, -6);
        $id = intval($code);

        $news = NewsJntoPartner::where('id', '!=', $id);

        if (!$preview) {
            $news->published();
        }

        if (Auth::guard('seller')->check()) {
            $news->whereHas('company', function ($query) {
                $query->where('privacy', 'public')
                    ->where('status', 'published')
                    ->where('active_status', true);
            });
        } elseif (Auth::guard('web')->check()) {
            $news->whereHas('company', function ($query) {
                $query->where('status', 'published')
                    ->where('active_status', true);
            });
        }

        $news->orWhere(function ($q) {
            $q->whereNull('company_id')
                ->published();
        });

        $news->orderBy('updated_at', 'DESC')
            ->limit(12);

        return $news->get();
    }

    public function getOtherShow($ref_id, $preview = false)
    {
        $code = substr($ref_id, -6);
        $id = intval($code);

        $news = NewsJntoPartner::where('id', '!=', $id);

        $news->published();
        if (!$preview) {
            $news->published();
        }

        if (Auth::guard('seller')->check()) {
            $news->whereHas('company', function ($query) {
                $query->where('privacy', 'public')
                    ->where('status', 'published')
                    ->where('active_status', true);
            });
        } elseif (Auth::guard('web')->check()) {
            $news->whereHas('company', function ($query) {
                $query->where('status', 'published')
                    ->where('active_status', true);
            });
        }

        $news->orWhere(function ($q) {
            $q->whereNull('company_id')
                ->published();
        });

        $news->orderBy('created_at', 'DESC')
            ->limit(12);

        return $news->get();
    }

    public function getAll($page = 12)
    {
        $news = NewsJntoPartner::limit($page)
            ->published();

        if (Auth::guard('seller')->check()) {
            $news->whereHas('company', function ($query) {
                $query->where('privacy', 'public')
                    ->where('status', 'published')
                    ->where('active_status', true);
            });
        } elseif (Auth::guard('web')->check()) {
            $news->whereHas('company', function ($query) {
                $query->where('status', 'published')
                    ->where('active_status', true);
            });
        }
        $news->orWhere(function ($q) {
            $q->whereNull('company_id')
                ->published();
        });

        $news->orderBy('created_at', 'DESC');

        return $news->get();
    }

    public function getPaginate($param)
    {
        $public = $param['public'] ?? false;
        $start = $param['start'] ?? 0;
        $orders = $param['order'] ?? '';
        $orderBy = $param['orderBy'] ?? '';
        $id = $param->filters['id'] ?? '';
        $business_types = $param->filters['title'] ?? '';

        $search = $param->filters['search'] ?? '';

        $length = $param['length'] ?? 50;

        $news = NewsJntoPartner::query();

        if ($public) {
            $news->published();
        }

        if (Auth::guard('seller')->check()) {
            $news->whereHas('company', function ($query) {
                $query->where('privacy', 'public')
                    ->where('status', 'published')
                    ->where('active_status', true);
            });

        } elseif (Auth::guard('web')->check()) {
            $news->whereHas('company', function ($query) {
                $query->where('status', 'published')
                    ->where('active_status', true);
            });
        }
        $news->orWhere(function ($q) {
            $q->whereNull('company_id')
                ->published();
        });


        //search
        if (!empty($search)) {

            $code = substr($search, -6);
            $id = intval($code);
            $news->where(function ($query) use ($search, $id) {
                $query->where('id', 'LIKE', "%$id%")
                    ->orWhere('title', 'LIKE', "%$search%");
            });

            $news->orWhereHas('company', function ($query) use ($search) {
                return $query->orWhere('name_en', 'LIKE', "%$search%");
            })->get();
        }
        //order
        if (!empty($orders)) {
            $column_name = '';
            foreach ($orders as $order) {
                if ($order['column'] == 0 || $order['column'] == 1) {
                    $column_name = 'id';
                }
                if ($order['column'] == 2) {
                    $column_name = 'title';
                }
            }
            if (!empty($column_name)) {
                $news->orderBy($column_name, $order['dir']);
            }
        }
        if (!empty($orderBy)) {
            $news->orderBy('updated_at', 'DESC');
        } else {
            $news->orderBy('created_at', 'DESC');
        }

        $page = $start / $length + 1;
        //return $news->paginate($length, '*', 'page', $page);
        return $news->paginate($length);
    }

    public function getPaginateByBackend($param)
    {
        $public = $param['public'] ?? false;
        $start = $param['start'] ?? 0;
        $orders = $param['order'] ?? '';
        $orderBy = $param['orderBy'] ?? '';
        $id = $param->filters['id'] ?? '';
        $business_types = $param->filters['title'] ?? '';
        $admin = $param['admin'] ?? false;

        $search = $param->filters['search'] ?? '';

        $length = $param['length'] ?? 50;

        $news = NewsJntoPartner::query();

        if ($public) {
            $news->published();
        }

        if ($admin) {
            $news->with('company');
        }

        //search
        if (!empty($search)) {

            $code = substr($search, -6);
            $id = intval($code);
            $news->where(function ($query) use ($search, $id) {
                $query->where('id', 'LIKE', "%$id%")
                    ->orWhere('title', 'LIKE', "%$search%");
            });

            $news->orWhereHas('company', function ($query) use ($search) {
                return $query->orWhere('name_en', 'LIKE', "%$search%");
            })->get();
        }
        //order
        if (!empty($orders)) {
            $column_name = '';
            foreach ($orders as $order) {
                if ($order['column'] == 0 || $order['column'] == 1) {
                    $column_name = 'id';
                }
                if ($order['column'] == 2) {
                    $column_name = 'title';
                }
            }
            if (!empty($column_name)) {
                $news->orderBy($column_name, $order['dir']);
            }
        }
        if (!empty($orderBy)) {
            $news->orderBy('updated_at', 'DESC');
        } else {
            $news->orderBy('created_at', 'DESC');
        }

        $page = $start / $length + 1;
        //return $news->paginate($length, '*', 'page', $page);
        return $news->paginate($length);
    }

    public function delete($id)
    {
        $data = NewsJntoPartner::find($id);
        $data->delete();
    }

    public function status($request, $id)
    {
        $data = NewsJntoPartner::find($id);

        if (($data->status == 'draft' && $request->status == 'published')) {

            $params['title'] = $data->title;
            $params['details'] = $data;

            $model = "App\\Models\\NewsJntoPartner";

            $this->storeHistory($id, $model, $params);
        }

        $data->status = $request->status;
        $data->save();

        return $data;
    }

    public function storeHistory($id, $model, $params)
    {
        if (empty((new SellerRevisionHistoryRepository())->findDuplicate($id, $model))) {
            $revision = new SellerRevisionHistory();
            $revision->company_id = $id;
            $revision->revision = "App\\Models\\NewsJntoPartner";
            $revision->revision_details = $params;
            $revision->save();
        }
    }
}
