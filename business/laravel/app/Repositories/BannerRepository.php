<?php

namespace App\Repositories;

use App\Models\Banner;
use Illuminate\Support\Facades\Auth;

class BannerRepository
{
    public function all_published()
    {
        $banner = Banner::query()->published();
        if (Auth::guard('seller')->check()) {
            $banner->where('target', 'seller');
        } else {
            $banner->where('target', 'buyer');
        }
        return $banner->get();
    }

    public function find($id)
    {
        return Banner::findOrFail($id);
    }

    public function getPaginate($param)
    {
        $orders = $param['order'] ?? '';
        $id = $param->filters['id'] ?? '';

        $search = $param->filters['search'] ?? '';

        $length = $param['length'] ?? 50;

        $data = Banner::query();

        //search
        if (!empty($search)) {

            $data->orWhere(function ($query) use ($search) {
                $query->where('url', 'LIKE', "%$search%");
            });
        }

        //order
        if (!empty($orders)) {
            $column_name = '';
            foreach ($orders as $order) {
                if ($order['column'] == 0 || $order['column'] == 1) {
                    $column_name = 'id';
                }
                if ($order['column'] == 2) {
                    $column_name = 'url';
                }
            }
            if (!empty($column_name)) {
                $data->orderBy($column_name, $order['dir']);
            }
        } else {
            $data->orderBy('id', 'DESC');
        }

        return $data->paginate($length);
    }

    public function store($request)
    {
        $data = new Banner();
        return $this->valuable($data, $request);
    }

    public function valuable(Banner $data, $request)
    {
        $data->url = $request->url ?? $data->url;
        $data->thumbnail = $request->thumbnail ?? $data->thumbnail;
        $data->target = $request->target ?? $data->target;
        $data->status = $request->status ?? 'draft';
        $data->save();

        return $data->id;
    }

    public function delete($id)
    {
        $data = Banner::find($id);
        $data->delete();
    }

    public function status($request, $id)
    {
        $data = Banner::find($id);
        $data->status = $request->status;
        $data->save();

        return $data;
    }

    public function statusTarget($request, $id)
    {
        $data = Banner::find($id);
        $data->target = $request->target;
        $data->save();

        return $data;
    }
}
