<?php

use App\Http\Controllers\Admin\NewsController;
use App\Http\Controllers\Admin\NewsPartnerController;
use App\Http\Controllers\Admin\SeminarController;
use App\Http\Controllers\Frontend\SeminarController as FrontendSeminarController;
use App\Http\Controllers\BusinessController;
use App\Http\Controllers\Frontend\ContactFormController;
use App\Http\Controllers\Frontend\SellerInfoController;
use App\Http\Controllers\Frontend\TopicController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UploadeFileController;
use App\Jobs\SendgridEmailJob;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group([
    'middleware' => [
        'underconstruction'
    ],
], function () {
    Route::any('/', function () {
        return redirect()->route('login');
    })->name('index');
});


Route::get('/storage-link', function () {
    $targetFolder = $_SERVER['DOCUMENT_ROOT'] . '/business/laravel/storage/app/public';
    $linkFolder = $_SERVER['DOCUMENT_ROOT'] . '/business/storage';
    symlink($targetFolder, $linkFolder);
    echo 'Symlink completed';
});

//Frontend
Route::group([
    'middleware' => [
        'auth.home.page',
        'cookie',
        //'page_speed',
        'underconstruction'
    ],
], function () {

    Route::get('/home', [HomeController::class, 'index'])
        ->name('home');

    Route::get('/page/{ref_id}', function ($ref_id) {
        return "Frontend {$ref_id}";
    })->name('permalink');

    Route::get('page-seller-info/{ref_id}', [SellerInfoController::class, 'show'])
        ->name('seller-info-detail');

    Route::get('/seller-info-search', [SellerInfoController::class, 'search'])
        ->name('seller-info.search');

    Route::post('/seller-info-search', [SellerInfoController::class, 'get_search'])
        ->name('seller-info.get_search');

    Route::post('contact-form-store', [ContactFormController::class, 'store'])
        ->name('contact-form-store');

    //Preview
    Route::get('preview/page-seller-info/{ref_id}', [SellerInfoController::class, 'show'])
        ->name('preview');

    $seller_info = 'form';

    Route::get($seller_info . '/create', [BusinessController::class, 'create'])
        ->name('business.create');
    Route::get($seller_info . '/{id}/edit', [BusinessController::class, 'edit'])
        ->name('business.edit');

    Route::get($seller_info . '/{ref_id}/confirm', [BusinessController::class, 'edit'])
        ->name('business.confirm');
    Route::post($seller_info . '/store-business', [BusinessController::class, 'store'])
        ->name('business.store');
    Route::any($seller_info . '/thankyou/{id}', [BusinessController::class, 'storeConfirm'])
        ->name('business.store.confirm');

    //TODO::news jnto
    Route::get('/news-jnto-all', [NewsController::class, 'all_jnto'])
        ->name('news-jnto.all');

    Route::get('/news-jnto/{ref_id}', [NewsController::class, 'show'])
        ->name('news-jnto.show');

    Route::get('/news-jnto/preview/{ref_id}', [NewsController::class, 'show'])
        ->name('news-jnto.show.preview');

    //TODO::news jnto partner
    Route::get('/news-jnto-partner-all', [NewsPartnerController::class, 'all_jnto'])
        ->name('news-jnto-partner.all');

    Route::get('/news-jnto-partner/{ref_id}', [NewsPartnerController::class, 'show'])
        ->name('news-jnto-partner.show');

    Route::get('/news-jnto-partner/preview/{ref_id}', [NewsPartnerController::class, 'show'])
        ->name('news-jnto-partner.show.preview');

    //TODO::seminar
    /*Route::get('/seminar-all', [SeminarController::class, 'all'])
        ->name('seminar.all');*/

    Route::get('seminar-list', [FrontendSeminarController::class, 'all'])
        ->name('seminar-list');

    Route::get('/seminar/{ref_id}', [SeminarController::class, 'show'])
        ->name('seminar.show');

    Route::get('topics', [TopicController::class, 'all'])
        ->name('topic.all');

    Route::get('topics-event', [TopicController::class, 'getEvent'])
        ->name('topic.event');

    Route::get('topics-news', [TopicController::class, 'getNews'])
        ->name('topic.news');

    /*Event By JNTO*/
    Route::get('event-detail/{ref_id}', [TopicController::class, 'getEventJnto'])
        ->name('event-detail');

    Route::get('event-detail/preview/{ref_id}', [TopicController::class, 'getEventJnto'])
        ->name('event.show.preview');

    Route::post('get-event-list', [TopicController::class, 'getEventList'])
        ->name('get-event-list');

});

Route::group([
    'middleware' => [
        'underconstruction'
    ],
], function () {

    Route::get('privacy-policy', function () {
        return view('frontend.pages.privacy-policy');
    })->name('privacy-policy');

});


//TODO::Delete file
Route::post('file-delete', [UploadeFileController::class, 'fileDelete'])
    ->name('file.delete');


Route::get('test-send-email/{id}', function ($id) {
    SendgridEmailJob::dispatchSync($id);
});

Route::get('test-email', function () {
    return view('emails.change-email');
});

Route::get('clear-cache', function () {
    /* php artisan migrate */
    \Artisan::call('cache:clear');
    \Artisan::call('view:clear');
    \Artisan::call('config:clear');
    \Artisan::call('route:clear');

    return "Clear cache success";
});

Route::get('migrate', function () {
    define('STDIN', fopen("php://stdin", "r"));
    \Artisan::call('migrate');

    return "Migrate create success";
});


Route::get('underconstruction', function () {
    return view('layouts.underconstruction');
})->name('underconstruction');


