<?php

//sellers
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Sellers\AuthController;
use App\Http\Controllers\Sellers\SellerController;
use App\Http\Controllers\Sellers\TopicController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Route::group([
    'prefix' => 'seller',
    'middleware' => [
        'underconstruction'
    ],
], function () {
    Route::get('login', [AuthController::class, 'showLoginForm'])
        ->name('seller.login.index');

    Route::post('login', [AuthController::class, 'login'])
        ->name('seller.login');

    Route::get('register-form', [AuthController::class, 'showRegisterForm'])
        ->name('seller.register.index');

    Route::post('check-has-email', [SellerController::class, 'checkHasEmail'])
        ->name('check-has-email');

    Route::get('register', function () {
        return view('sellers.register');
    })->name('seller.register.page');

    Route::post('register', [SellerController::class, 'register'])
        ->name('seller.register');

    Route::get('set-password/{token}', [SellerController::class, 'setPasswordPage'])
        ->name('seller.set-password.page');

    Route::post('set-password', [SellerController::class, 'setPassword'])
        ->name('seller.set-password');


    Route::get('forgot-password', function () {
        return view('sellers.forgot-password');
    })->name('forgot-password');

    Route::post('forgot-password', [SellerController::class, 'forgotPassword'])
        ->name('seller.forgot-password');

    Route::get('reset-password/{token}', [SellerController::class, 'resetPasswordPage'])
        ->name('seller.reset-password.page');

    Route::post('reset-password', [SellerController::class, 'resetPassword'])
        ->name('seller.reset-password');


    Route::get('renew-password/{login}', function ($login) {
        return view('sellers.renew-password', compact('login'));
    })->name('seller.renew-password.page');

    Route::post('renew-password', [SellerController::class, 'renewPassword'])
        ->name('seller.renew-password');

    Route::get('change-email-confirm/{token}', [SellerController::class, 'changeEmailPassword'])
        ->name('change-email-confirm');

    Route::post('change-email-confirm', [SellerController::class, 'changeEmailConfirm'])
        ->name('change-email-confirm.store');


    //Login
    Route::middleware(['auth.seller'])->group(function () {

        Route::get('my-page', [SellerController::class, 'myPage'])
            ->name('seller.mypage');

        Route::get('login-history', [SellerController::class, 'loginHistory'])
            ->name('login-history');

        Route::get('privicy-setting', [SellerController::class, 'privicySetting'])
            ->name('privicy-setting');

        Route::post('privicy-setting', [SellerController::class, 'privicySettingUpdate'])
            ->name('privicy-setting.update');

        Route::get('privacy-policy', function () {
            return view('sellers.mypage.privacy-policy');
        })->name('seller.privacy-policy.page');

        Route::get('contact-staff', [HomeController::class,'contactSeller'])
        ->name('contact-staff');

        Route::get('company-topic', [TopicController::class, 'page'])
            ->name('company-topic');

        Route::post('company-topic', [TopicController::class, 'store'])
            ->name('company-topic.store');

        Route::get('change-email', function () {
            return view('sellers.mypage.change-email');
        })->name('change-email');

        Route::post('change-email', [SellerController::class, 'changeEmail'])
            ->name('seller.change-email');

        Route::get('change-email-send', function () {
            return view('sellers.mypage.change-email-send');
        })->name('seller.change-email.page');

        Route::get('change-password', function () {
            return view('sellers.mypage.change-password');
        })->name('change-password');

        Route::post('change-password', [SellerController::class, 'changePassword'])
            ->name('seller.change-password.store');


    });

    Route::post('logout-seller', function () {
        Auth::guard('seller')->logout();
        return redirect()->route('seller.login');
    })->name('logout-seller');
});
