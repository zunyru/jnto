<?php

use App\Http\Controllers\Buyer\AuthController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => [
        'underconstruction'
    ],
], function () {
    Route::get('login', [AuthController::class, 'showLoginFormBuyer'])
        ->name('login');

    Route::post('buyer/login', [AuthController::class, 'login'])
        ->name('login.buyer');

    Route::post('/logout', [AuthController::class, 'logout'])
        ->name('logout');
    /*Auth::routes();*/

    Auth::routes([
        'login' => false,
        'logout' => true,
        'register' => false,
        'reset' => false,
        'verify' => false,
    ]);
});