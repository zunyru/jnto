<?php

use App\Http\Controllers\Admin\ActivityLogController;
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\BannerController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\CovidController;
use App\Http\Controllers\Admin\EventController;
use App\Http\Controllers\Admin\NewsController;
use App\Http\Controllers\Admin\NewsPartnerController;
use App\Http\Controllers\Admin\SeminarController;
use App\Http\Controllers\Admin\UsefulLinkController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\BusinessController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use TCG\Voyager\Facades\Voyager;


/*Route::group(['prefix' => 'admins'], function () {
    Voyager::routes();
});*/

Route::group(['prefix' => 'admin'], function () {
    Route::get('login', function () {
        return view('auth.login');
    })->name('login.admin');

    Route::post('login', [AuthController::class, 'login'])
        ->name('admin.login');

    Route::post('logout', function () {
        Auth::guard('web')->logout();
        return redirect()->route('login.admin');
    })->name('logout.admin');

    /*Route::get('otp-page/{token}', function ($token) {
        return view('auth.otp-page', compact('token'));
    })->name('otp-page');*/

    Route::get('otp-page/{token}',[AuthController::class, 'otpPage'])
        ->name('otp-page');

    Route::post('otp', [AuthController::class, 'verifyOtp'])
        ->name('admin.otp.verify');
});

Route::group([
    'middleware' => 'auth',
], function () {
    $seller_info = 'form';
    Route::group([
        'prefix'     => 'admin',
        'middleware' => 'role.admin'
    ], function () use ($seller_info) {

        Route::get('/dashboard', [HomeController::class, 'dashboard'])
            ->name('dashboard');

        //TODO::seller_info
        Route::get($seller_info, [BusinessController::class, 'index'])
            ->name('business.index');

        Route::get($seller_info . '/{id}/edit', [BusinessController::class, 'edit'])
            ->name('business.edit.admin');

        Route::get($seller_info . '/create', [BusinessController::class, 'create'])
            ->name('business.create.admin');

        Route::get($seller_info . '/quick-create', [BusinessController::class, 'quickCreate'])
            ->name('business.quick-create.admin');

        Route::post($seller_info . '/quick-create', [BusinessController::class, 'quickStore'])
            ->name('business.storeQuick');

        Route::post($seller_info . '/update-status', [BusinessController::class, 'updateStatus'])
            ->name('business.update.status');

        Route::post($seller_info . '/update-status-active', [BusinessController::class, 'updateStatusActive'])
            ->name('business.update.status_active');

        Route::post($seller_info . '/delete', [BusinessController::class, 'delete'])
            ->name('business.delete');

        Route::post('data-index-' . $seller_info, [BusinessController::class, 'dataIndex'])
            ->name('business.data.index');

        Route::get($seller_info . '/export', [BusinessController::class, 'export'])
            ->name('business.export');


        //TODO::news-jnto
        $news_jnto = 'news-jnto';
        Route::get($news_jnto, [NewsController::class, 'index'])
            ->name('news-jnto.index');

        Route::get($news_jnto . '/create', [NewsController::class, 'create'])
            ->name('news-jnto.create');

        Route::get($news_jnto . '/{id}/edit', [NewsController::class, 'edit'])
            ->name('news-jnto.edit.admin');

        Route::post($news_jnto . '/store', [NewsController::class, 'store'])
            ->name('news-jnto.store');

        Route::post($news_jnto . '/delete', [NewsController::class, 'delete'])
            ->name('news-jnto.delete');

        Route::post('data-index-' . $news_jnto, [NewsController::class, 'dataIndex'])
            ->name('news-jnto.data.index');

        Route::put($news_jnto . '/update/{ref_id}', [NewsController::class, 'update'])
            ->name('news-jnto.update');

        Route::post($news_jnto . '/update-status', [NewsController::class, 'updateStatus'])
            ->name('news-jnto.update.status');


        //TODO::news-jnto-partner
        $news_jnto_partner = 'news-jnto-partner';
        Route::get('/' . $news_jnto_partner, [NewsPartnerController::class, 'index'])
            ->name('news-jnto-partner.index');

        Route::get('/' . $news_jnto_partner . '/create', [NewsPartnerController::class, 'create'])
            ->name('news-jnto-partner.create');

        Route::put($news_jnto_partner . '/update/{ref_id}', [NewsPartnerController::class, 'update'])
            ->name('news-jnto-partner.update');

        Route::get($news_jnto_partner . '/{id}/edit', [NewsPartnerController::class, 'edit'])
            ->name('news-jnto-partner.edit.admin');

        Route::post('/' . $news_jnto_partner . '/store', [NewsPartnerController::class, 'store'])
            ->name('news-jnto-partner.store');

        Route::post($news_jnto_partner . '/delete', [NewsPartnerController::class, 'delete'])
            ->name('news-jnto-partner.delete');

        Route::post('data-index-' . $news_jnto_partner, [NewsPartnerController::class, 'dataIndex'])
            ->name('news-jnto-partner.data.index');

        Route::post($news_jnto_partner . '/update-status', [NewsPartnerController::class, 'updateStatus'])
            ->name('news-jnto-partner.update.status');


        //TODO::online-seminar
        $online_seminar = 'online-seminar';
        Route::get($online_seminar, [SeminarController::class, 'index'])
            ->name('seminar.index');

        Route::get($online_seminar . '/create', [SeminarController::class, 'create'])
            ->name('seminar.create');

        Route::post($online_seminar . '/store', [SeminarController::class, 'store'])
            ->name('seminar.store');

        Route::get($online_seminar . '/{id}/edit', [SeminarController::class, 'edit'])
            ->name('seminar.edit.admin');

        Route::put($online_seminar . '/update/{ref_id}', [SeminarController::class, 'update'])
            ->name('seminar.update');

        Route::post('data-index-' . $online_seminar, [SeminarController::class, 'dataIndex'])
            ->name('seminar.data.index');

        Route::post($online_seminar . '/delete', [SeminarController::class, 'delete'])
            ->name('seminar.delete');

        Route::post($online_seminar . '/update-status', [SeminarController::class, 'updateStatus'])
            ->name('seminar.update.status');

        Route::post($online_seminar . '/update-status-target', [SeminarController::class, 'updateStatusTarget'])
            ->name('seminar.update.status.target');


        //TODO::users
        $user = 'user';
        Route::get($user, [UserController::class, 'index'])
            ->name('user.index');

        Route::post('data-index-' . $user, [UserController::class, 'dataIndex'])
            ->name('user.data.index');

        Route::get($user . '/{id}/edit', [UserController::class, 'edit'])
            ->name('user.edit.admin');

        Route::put($user . '/update/{id}', [UserController::class, 'update'])
            ->name('user.update');

        Route::post($user . '/update', [UserController::class, 'store'])
            ->name('user.store');

        Route::get($user . '/create', [UserController::class, 'create'])
            ->name('user.create');

        Route::post($user . '/delete', [UserController::class, 'delete'])
            ->name('user.delete');

        //TODO::Setting
        Route::post('settings-update', [HomeController::class, 'setting'])
            ->name('settings.update');

        //TODO::Useful-links
        $useful_links = 'useful-links';
        Route::get($useful_links, [UsefulLinkController::class, 'index'])
            ->name('useful-link.index');

        Route::post('data-index-' . $useful_links, [UsefulLinkController::class, 'dataIndex'])
            ->name('useful-link.data.index');

        Route::get($useful_links . '/{id}/edit', [UsefulLinkController::class, 'edit'])
            ->name('useful-link.edit.admin');

        Route::get($useful_links . '/create', [UsefulLinkController::class, 'create'])
            ->name('useful-link.create');

        Route::put($useful_links . '/update/{id}', [UsefulLinkController::class, 'update'])
            ->name('useful-link.update');

        Route::post($useful_links . '/update', [UsefulLinkController::class, 'store'])
            ->name('useful-link.store');

        Route::get($useful_links . '/create', [UsefulLinkController::class, 'create'])
            ->name('useful-link.create');

        Route::post($useful_links . '/delete', [UsefulLinkController::class, 'delete'])
            ->name('useful-link.delete');

        Route::post($useful_links . '/update-status', [UsefulLinkController::class, 'updateStatus'])
            ->name('useful-link.update.status');

        //TODO::Covid-19
        $covid = 'covid-19-informations';
        Route::get($covid, [CovidController::class, 'index'])
            ->name('covid.index');

        Route::post('data-index-' . $covid, [CovidController::class, 'dataIndex'])
            ->name('covid.data.index');

        Route::get($covid . '/{id}/edit', [CovidController::class, 'edit'])
            ->name('covid.edit.admin');

        Route::get($covid . '/create', [CovidController::class, 'create'])
            ->name('covid.create');

        Route::put($covid . '/update/{id}', [CovidController::class, 'update'])
            ->name('covid.update');

        Route::post($covid . '/update', [CovidController::class, 'store'])
            ->name('covid.store');

        Route::get($covid . '/create', [CovidController::class, 'create'])
            ->name('covid.create');

        Route::post($covid . '/delete', [CovidController::class, 'delete'])
            ->name('covid.delete');

        Route::post($covid . '/update-status', [CovidController::class, 'updateStatus'])
            ->name('covid.update.status');

        //TODO::Category
        $category = 'category';
        Route::get($category, [CategoryController::class, 'index'])
            ->name('category.index');

        Route::post('data-index-' . $category, [CategoryController::class, 'dataIndex'])
            ->name('category.data.index');

        Route::get($category . '/{id}/edit', [CategoryController::class, 'edit'])
            ->name('category.edit.admin');

        Route::get($category . '/create', [CategoryController::class, 'create'])
            ->name('category.create');

        Route::put($category . '/update/{id}', [CategoryController::class, 'update'])
            ->name('category.update');

        Route::post($category . '/update', [CategoryController::class, 'store'])
            ->name('category.store');

        Route::get($category . '/create', [CategoryController::class, 'create'])
            ->name('category.create');

        Route::post($category . '/delete', [CategoryController::class, 'delete'])
            ->name('category.delete');

        Route::post($category . '/update-status', [CategoryController::class, 'updateStatus'])
            ->name('category.update.status');

        //TODO::Thank page
        Route::get('setting-page-thank', [HomeController::class, 'thank_page'])
            ->name('setting-thank-page.index');

        Route::post('setting-thank-pages', [HomeController::class, 'update_thank_page'])
            ->name('setting-thank-page.update');

        Route::get('setting-page-contact', [HomeController::class, 'contactPage'])
            ->name('setting-contact.index');

        Route::post('setting-contact', [HomeController::class, 'update_contact_page'])
            ->name('setting-contact.update');


        //Todo:Banner
        $banner = 'banner';
        Route::get($banner, [BannerController::class, 'index'])
            ->name('banner.index');

        Route::post('data-index-' . $banner, [BannerController::class, 'dataIndex'])
            ->name('banner.data.index');

        Route::get($banner . '/{id}/edit', [BannerController::class, 'edit'])
            ->name('banner.edit.admin');

        Route::get($banner . '/create', [BannerController::class, 'create'])
            ->name('banner.create');

        Route::put($banner . '/update/{id}', [BannerController::class, 'update'])
            ->name('banner.update');

        Route::post($banner . '/update', [BannerController::class, 'store'])
            ->name('banner.store');

        Route::get($banner . '/create', [BannerController::class, 'create'])
            ->name('banner.create');

        Route::post($banner . '/delete', [BannerController::class, 'delete'])
            ->name('banner.delete');

        Route::post($banner . '/update-status', [BannerController::class, 'updateStatus'])
            ->name('banner.update.status');

        Route::post($banner . '/update-status-target', [BannerController::class, 'updateStatusTarget'])
            ->name('banner.update.status.target');


        $log = 'activity-log';
        Route::get($log, [ActivityLogController::class, 'index'])
            ->name('log.index');

        Route::post('data-index-' . $log, [ActivityLogController::class, 'dataIndex'])
            ->name('log.data.index');

        Route::get($log . '/show/{id}', [ActivityLogController::class, 'show'])
            ->name('log.view');


        //Todo:Banner
        $event = 'event';
        Route::get($event, [EventController::class, 'index'])
            ->name('event.index');

        Route::get($event . '/create', [EventController::class, 'create'])
            ->name('event.create');

        Route::get($event . '/{id}/edit', [EventController::class, 'edit'])
            ->name('event.edit.admin');

        Route::post($event . '/store', [EventController::class, 'store'])
            ->name('event.store');

        Route::post($event . '/delete', [EventController::class, 'delete'])
            ->name('event.delete');

        Route::post('data-index-' . $event, [EventController::class, 'dataIndex'])
            ->name('event.data.index');

        Route::put($event . '/update/{ref_id}', [EventController::class, 'update'])
            ->name('event.update');

        Route::post($event . '/update-status', [EventController::class, 'updateStatus'])
            ->name('event.update.status');

    });

});





