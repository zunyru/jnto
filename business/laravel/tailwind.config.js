module.exports = {
    content: [
            './resources/views/**/*.blade.php',
            './resources/js/**/*.js',
    ],
    theme: {
        container: {
            center: true,
        },
        extend: {},
    },
    variants: {},
    plugins: [
        require('@tailwindcss/forms'),
    ]
}
