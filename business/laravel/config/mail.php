<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Mailer
    |--------------------------------------------------------------------------
    |
    | This option controls the default mailer that is used to send any email
    | messages sent by your application. Alternative mailers may be setup
    | and used as needed; however, this mailer will be used by default.
    |
    */

    'default' => env('MAIL_MAILER', 'smtp'),

    /*
    |--------------------------------------------------------------------------
    | Mailer Configurations
    |--------------------------------------------------------------------------
    |
    | Here you may configure all of the mailers used by your application plus
    | their respective settings. Several examples have been configured for
    | you and you are free to add your own as your application requires.
    |
    | Laravel supports a variety of mail "transport" drivers to be used while
    | sending an e-mail. You will specify which one you are using for your
    | mailers below. You are free to add additional mailers as required.
    |
    | Supported: "smtp", "sendmail", "mailgun", "ses",
    |            "postmark", "log", "array"
    |
    */

    'mailers' => [
        'smtp' => [
            'transport'  => 'smtp',
            'host'       => env('MAIL_HOST', 'smtp.mailgun.org'),
            'port'       => env('MAIL_PORT', 587),
            'encryption' => env('MAIL_ENCRYPTION', 'tls'),
            'username'   => env('MAIL_USERNAME'),
            'password'   => env('MAIL_PASSWORD'),
            'timeout'    => null,
            'auth_mode'  => null,
        ],

        'ses' => [
            'transport' => 'ses',
        ],

        'mailgun' => [
            'transport' => 'mailgun',
        ],

        'postmark' => [
            'transport' => 'postmark',
        ],

        'sendmail' => [
            'transport' => 'sendmail',
            'path'      => '/usr/sbin/sendmail -bs',
        ],

        'log' => [
            'transport' => 'log',
            'channel'   => env('MAIL_LOG_CHANNEL'),
        ],

        'array' => [
            'transport' => 'array',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Global "From" Address
    |--------------------------------------------------------------------------
    |
    | You may wish for all e-mails sent by your application to be sent from
    | the same address. Here, you may specify a name and address that is
    | used globally for all e-mails that are sent by your application.
    |
    */

    'from' => [
        'address' => env('MAIL_FROM_ADDRESS', 'hello@example.com'),
        'name'    => env('MAIL_FROM_NAME', 'Example'),
    ],

    /*
    |--------------------------------------------------------------------------
    | Markdown Mail Settings
    |--------------------------------------------------------------------------
    |
    | If you are using Markdown based email rendering, you may configure your
    | theme and component paths here, allowing you to customize the design
    | of the emails. Or, you may simply stick with the Laravel defaults!
    |
    */

    'markdown' => [
        'theme' => 'default',

        'paths' => [
            resource_path('views/vendor/mail'),
        ],
    ],

    /*
   |--------------------------------------------------------------------------
   | Sendgrid Settings
   |--------------------------------------------------------------------------
   |
   */

    'sendgrid' => [
        'url'     => env('SENDGRID_URL', 'https://api.sendgrid.com/v3/mail/send'),
        'api_key' => env('SENDGRID_API_KEY', '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'),

        'templates_id' => env('SENDGRID_TEMPLATE_ID', 'd-62a2ec4591aa4c17a568ca9b1dd4a3a8'),

        'contact' => [
            'branch_japan' => [
                'seller' => [
                    'templates_id' => env('BRANCH_JAPAN_SELLER_TEMPLATE_ID', 'd-4021cf4c9922405da3846162496b4a5c'),
                ],
                'user'   => [
                    'templates_id' => env('BRANCH_JAPAN_USER_TEMPLATE_ID', 'd-3f738493f726492899e34b008086d09c'),
                ],
            ],
            'branch_thai'  => [
                'seller' => [
                    'templates_id' => env('BRANCH_THAI_SELLER_TEMPLATE_ID', 'd-1621c4b67dc94fa195bd35aa678dd7c3'),
                ],
                'user'   => [
                    'templates_id' => env('BRANCH_THAI_USER_TEMPLATE_ID', 'd-db28643c26864ae296ac994ae9485999'),
                ],
            ]
        ],
        'seller'  => [
            'set_password'      => env('SELLER_SET_PASSWORD', 'd-35c47777204e4db7b33d4542d46d370c'),
            'signup_successful' => env('SELLER_SIGNUP_SUCCESSFUL', 'd-47bd9fe7e4b842ce86e5cbbe63467899'),
            'forgot_password'   => env('SELLER_FORGOT_PASSWORD', 'd-390e3365218f458cbe133a82fbb36890'),
            'change_email'      => env('SELLER_CHANGE_EMAIL', 'd-c72a5b734b7c4866af228a5894528b49')
        ]
    ],
];
