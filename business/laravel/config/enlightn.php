<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Enlightn Analyzer Classes
    |--------------------------------------------------------------------------
    |
    | The following array lists the "analyzer" classes that will be registered
    | with Enlightn. These analyzers run an analysis on the application via
    | various methods such as static analysis. Feel free to customize it.
    |
    */
    'analyzers' => ['*'],

    // If you wish to skip running some analyzers, list the classes in the array below.
    'exclude_analyzers' => [],

    // If you wish to skip running some analyzers in CI mode, list the classes below.
    'ci_mode_exclude_analyzers' => [],

    /*
    |--------------------------------------------------------------------------
    | Enlightn Analyzer Paths
    |--------------------------------------------------------------------------
    |
    | The following array lists the "analyzer" paths that will be searched
    | recursively to find analyzer classes. This option will only be used
    | if the analyzers option above is set to the asterisk wildcard. The
    | key is the base namespace to resolve the class name.
    |
    */
    'analyzer_paths' => [
        'Enlightn\\Enlightn\\Analyzers' => base_path('vendor/enlightn/enlightn/src/Analyzers'),
        'Enlightn\\EnlightnPro\\Analyzers' => base_path('vendor/enlightn/enlightnpro/src/Analyzers'),
    ],

    /*
    |--------------------------------------------------------------------------
    | Enlightn Base Path
    |--------------------------------------------------------------------------
    |
    | The following array lists the directories that will be scanned for
    | application specific code. By default, we are scanning your app
    | folder, migrations folder and the seeders folder.
    |
    */
    'base_path' => [
        app_path(),
        database_path('migrations'),
        database_path('seeders'),
    ],

    /*
    |--------------------------------------------------------------------------
    | Environment Specific Analyzers
    |--------------------------------------------------------------------------
    |
    | There are some analyzers that are meant to be run for specific environments.
    | The options below specify whether we should skip environment specific
    | analyzers if the environment does not match.
    |
    */
    'skip_env_specific' => env('ENLIGHTN_SKIP_ENVIRONMENT_SPECIFIC', true),

    /*
    |--------------------------------------------------------------------------
    | Guest URL
    |--------------------------------------------------------------------------
    |
    | Specify any guest url or path (preferably your app's login url) here. This
    | would be used by Enlightn to inspect your application HTTP headers.
    | Example: '/login'.
    |
    */
    'guest_url' => null,

    /*
    |--------------------------------------------------------------------------
    | Exclusions From Reporting
    |--------------------------------------------------------------------------
    |
    | Specify the analyzer classes that you wish to exclude from reporting. This
    | means that if any of these analyzers fail, they will not be counted
    | towards the exit status of the Enlightn command. This is useful
    | if you wish to run the command in your CI/CD pipeline.
    | Example: [\Enlightn\Enlightn\Analyzers\Security\XSSAnalyzer::class].
    |
    */
    'dont_report' => [Enlightn\Enlightn\Analyzers\Performance\QueueDriverAnalyzer::class, Enlightn\EnlightnPro\Analyzers\Reliability\DeadRouteAnalyzer::class, Enlightn\Enlightn\Analyzers\Reliability\EnvExampleAnalyzer::class, Enlightn\Enlightn\Analyzers\Security\FilePermissionsAnalyzer::class, Enlightn\Enlightn\Analyzers\Security\LicenseAnalyzer::class],

    /*
    |--------------------------------------------------------------------------
    | Ignoring Errors
    |--------------------------------------------------------------------------
    |
    | Use this config option to ignore specific errors. The key of this array
    | would be the analyzer class and the value would be an associative
    | array with path and details. Run php artisan enlightn:baseline
    | to auto-generate this. Patterns are supported in details.
    |
    */
    'ignore_errors' => [],

    /*
    |--------------------------------------------------------------------------
    | Analyzer Configurations
    |--------------------------------------------------------------------------
    |
    | The following configuration options pertain to individual analyzers.
    | These are recommended options but feel free to customize them based
    | on your application needs.
    |
    */
    'license_whitelist' => [
        'Apache-2.0', 'Apache2', 'BSD-2-Clause', 'BSD-3-Clause', 'LGPL-2.1-only', 'LGPL-2.1',
        'LGPL-2.1-or-later', 'LGPL-3.0', 'LGPL-3.0-only', 'LGPL-3.0-or-later', 'MIT', 'ISC',
        'CC0-1.0', 'Unlicense',
    ],

    /*
    |--------------------------------------------------------------------------
    | Credentials
    |--------------------------------------------------------------------------
    |
    | The following credentials are used to share your Enlightn report with
    | the Enlightn Github Bot. This allows the bot to compile the report
    | and add review comments on your pull requests.
    |
    */
    'credentials' => [
        'username' => env('ENLIGHTN_USERNAME'),
        'api_token' => env('ENLIGHTN_API_TOKEN'),
    ],

    // Set this value to your Github repo for integrating with the Enlightn Github Bot
    // Format: "myorg/myrepo" like "laravel/framework".
    'github_repo' => env('ENLIGHTN_GITHUB_REPO'),

    // Set to true to restrict the max number of files displayed in the enlightn
    // command for each check. Set to false to display all files.
    'compact_lines' => true,

    // List your commercial packages (licensed by you) below, so that they are not
    // flagged by the License Analyzer.
    'commercial_packages' => [
        'laravel/nova', 'enlightn/enlightnpro',
    ],

    'allowed_permissions' => [
        base_path() => '775',
        app_path() => '775',
        resource_path() => '775',
        storage_path() => '775',
        public_path() => '775',
        config_path() => '775',
        database_path() => '775',
        base_path('routes') => '775',
        app()->bootstrapPath() => '775',
        app()->bootstrapPath('cache') => '775',
        app()->bootstrapPath('app.php') => '664',
        base_path('artisan') => '775',
        public_path('index.php') => '664',
        public_path('server.php') => '664',
    ],

    'writable_directories' => [
        storage_path(),
        app()->bootstrapPath('cache'),
    ],

    /*
    |--------------------------------------------------------------------------
    | Telescope Analyzer Configurations
    |--------------------------------------------------------------------------
    |
    | The following configuration options pertain to Telescope analyzers.
    | These are recommended options but feel free to customize them
    | based on your application needs.
    |
    */
    'disk_usage_threshold' => 96.55, // %

    'hydration_limit' => 50,

    'request_memory_limit' => 50, // MB

    'slow_response_threshold' => 500, // ms

    'database_connections' => [
        'mysql',
    ],

    /*'unsafe_sql_functions' => [
        'mysqli_connect', 'mysqli_execute', 'mysqli_stmt_execute', 'mysqli_stmt_close', 'mysqli_stmt_fetch',
        'mysqli_stmt_get_result', 'mysqli_stmt_more_results', 'mysqli_stmt_next_result', 'mysqli_stmt_prepare',
        'mysqli_close', 'mysqli_commit', 'mysqli_begin_transaction', 'mysqli_init', 'mysqli_insert_id',
        'mysqli_prepare', 'mysqli_query', 'mysqli_real_connect', 'mysqli_real_query', 'mysqli_store_result',
        'mysqli_use_result', 'mysqli_multi_query',

        'pg_connect', 'pg_close', 'pg_affected_rows', 'pg_delete', 'pg_execute', 'pg_fetch_all', 'pg_fetch_result',
        'pg_fetch_row', 'pg_fetch_all_columns', 'pg_fetch_array', 'pg_fetch_assoc', 'pg_fetch_object', 'pg_flush',
        'pg_insert', 'pg_get_result', 'pg_pconnect', 'pg_prepare', 'pg_query', 'pg_query_params', 'pg_select',
        'pg_send_execute', 'pg_send_prepare', 'pg_send_query', 'pg_send_query_params', 'pg_affected_rows',
    ],*/


    'global_function_blacklist' => [
        'header', 'header_remove', 'headers_list', 'http_response_code', 'setcookie', 'setrawcookie',
        'session_abort', 'session_cache_expire', 'session_cache_limiter', 'session_commit', 'session_create_id',
        'session_decode', 'session_destroy', 'session_encode', 'session_gc', 'session_get_cookie_params',
        'session_id', 'session_is_registered', 'session_module_name', 'session_name', 'session_regenerate_id',
        'session_register_shutdown', 'session_register', 'session_reset', 'session_save_path',
        'session_set_cookie_params', 'session_set_save_handler', 'session_start', 'session_status',
        'session_unregister', 'session_unset', 'session_write_close', 'getenv', 'putenv',
    ],

    'debug_blacklist' => [
        'var_dump', 'dump', 'dd', 'print_r', 'var_export', 'debug_print_backtrace',
        'debug_backtrace', 'debug_zval_dump',
    ],



];
