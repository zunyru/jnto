<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Resources for Travel Agents</title>
<meta name="viewport" content="width=device-width,initial-scale=1">
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<link rel="icon" href="favicon-32x32.png" type="image" sizes="32x32">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Montserrat:wght@300;400;500;600;700&family=Sarabun:wght@100;500&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
	.BoxAll{ font-family: 'Kanit', sans-serif; padding: 30px; text-align: left; max-width: 750px; margin: 20px auto; border:1px solid #eee; border-radius: 20px;}
	.BoxAll p{ margin-top: 0px; }
	@media(max-width:767px)
	{
	    .BoxAll{ margin-left: 20px; margin-right: 20px;}
	}
</style>
</head>
<body>

	<div class="BoxAll">
		<p style="color: #a67c52; font-size: 25px; font-weight: 500;">ดำเนินการปิดปรับปรุงเว็บไซต์ชั่วคราว</p>
		<p style="color: #213054; margin-top: 30px;">เว็บไซต์ Resources for Travel Agents จะเปิดให้ทุกท่านเข้าชมอีกครั้งในวันที่<br><span style="font-size: 20px;">1 เมษายน 2565 เป็นต้นไป </span><br>ขออภัยในความไม่สะดวกมา ณ ที่นี้ <span style="color: #000; font-size: 13px; font-weight: 300;">*กำหนดการอาจมีการเปลี่ยนแปลง</span></p>
		<p style="color: #666; font-size: 14px; font-weight: 300; margin-top: 30px;">ติดต่อสอบถาม:<br>
		องค์การส่งเสริมการท่องเที่ยวแห่งประเทศญี่ปุ่น-สำนักงานกรุงเทพ<br>
		E-mail: info_bangkok@jnto.go.jp<br>
		เวลาทำการ: จันทร์-ศุกร์ 10:00-17:30 น. 
		</p>

		<hr style="margin-top: 30px; margin-bottom: 30px;">

		<p style="color: #a67c52; font-size: 25px; font-weight: 500;">ただいまメンテナンス中です。</p>
		<p style="color: #000; margin-top: 30px;">現在、「タイ旅行会社向け訪日情報発信サイト」のメンテナンス作業を行なっております。</p>
		<p style="color: #213054; margin-top: 30px;">
		<span style="font-size: 19px;">2022年4月1日（金）からアクセスしていただけます。</span><br>※日程を変更する場合がございます</p>
		<p style="color: #000; font-size: 14px; font-weight: 300; margin-top: 30px;">ご不便、ご迷惑をおかけいたしますが、何とぞご理解いただきますようお願い申し上げます。</p>
		<p style="color: #666; font-size: 14px; font-weight: 300; margin-top: 30px;">【お問い合わせ先】<br>
		JNTOバンコク事務所<br>
		E-mail : jnto_bangkok@jnto.go.jp<br>
		受付時間：月～金曜日 12:00～19:30（日本時間・土日祝休み）
		</p>
	</div>

</body>
</html>